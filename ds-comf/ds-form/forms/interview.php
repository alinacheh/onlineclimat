<?php

return array (
  'charset' => 'utf-8',
  'validate' => 
  array (
    'html5' => false,
    'strlen' => 3,
    'error' => 'Поля не заполнены или слишком короткие!',
  ),
  'smtp' => 
  array (
    'on' => false,
    'host' => '',
    'secure' => 'ssl',
    'port' => 465,
    'auth' => true,
    'username' => '',
    'password' => '',
    'from_email' => true,
  ),
  'mail' => 
  array (
    'to_email' => 
    array (
      0 => 'wp-art@mail.ru',
    ),
    'cc_email' => 
    array (
    ),
    'from_email' => 'info@wp-art.ru',
    'from_name' => 'Info',
    'subject' => 'Опрос с сайта',
    'reverse_email' => false,
  ),
  'configform' => 
  array (
    0 => 
    array (
      'type' => 'freearea',
      'value' => '<div class="form-head">Опрос</div>',
      'container' => false,
    ),
    1 => 
    array (
      'type' => 'freearea',
      'container' => true,
      'id' => 'table-cr',
      'class' => '',
      'value' => '<table><tr><td>',
    ),
    2 => 
    array (
      'type' => 'freearea',
      'id' => '',
      'class' => '',
      'value' => '<div class="field-1">1. Сотрудничали ли вы ранее с нами?</div>',
      'container' => false,
    ),
    3 => 
    array (
      'type' => 'freearea',
      'id' => '',
      'class' => '',
      'value' => '<div style="clear:both;"></div>',
      'container' => false,
    ),
    4 => 
    array (
      'type' => 'input',
      'attributs' => 
      array (
        'name' => 'oprospol[]',
        'type' => 'radio',
        'value' => 'да',
        'checked' => '',
      ),
      'container' => true,
      'id' => '',
      'class' => '',
      'formail' => true,
      'label' => 'Да',
      'name_mail' => 'Сотрудничали ли вы ранее с нами?',
      'error' => '',
    ),
    5 => 
    array (
      'type' => 'input',
      'attributs' => 
      array (
        'name' => 'oprospol[]',
        'type' => 'radio',
        'value' => 'нет',
        'checked' => '',
      ),
      'container' => true,
      'id' => '',
      'class' => '',
      'formail' => true,
      'label' => 'Нет',
      'name_mail' => 'Сотрудничали ли вы ранее с нами?',
      'error' => '',
    ),
    6 => 
    array (
      'type' => 'textarea',
      'attributs' => 
      array (
        'name' => 'question2',
        'type' => 'text',
        'rows' => '3',
        'cols' => '35',
        'placeholder' => 'Комментарий по желанию',
        'value' => '',
      ),
      'id' => '',
      'class' => '',
      'formail' => true,
      'label' => '',
      'name_mail' => 'Есть ли у Вас пожелания и/или рекомендации к работе наших сотрудников? текст',
      'error' => '',
      'container' => false,
    ),
    7 => 
    array (
      'type' => 'freearea',
      'container' => true,
      'id' => '',
      'class' => '',
      'value' => '</td><td>',
    ),
    





22212231 => 
    array (
      'type' => 'freearea',
      'id' => '',
      'class' => '',
      'value' => '<div class="field-1 bottom-1">4. Чтобы вы хотели увидеть/добавить/изменить в нашем сайте?</div>',
      'container' => false,
    ),
    
    63456786786 => 
    array (
      'type' => 'textarea',
      'attributs' => 
      array (
        'name' => 'question2657575',
        'type' => 'text',
        'rows' => '3',
        'cols' => '35',
        'placeholder' => 'Комментарий по желанию',
        'value' => '',
      ),
      'id' => '',
      'class' => '',
      'formail' => true,
      'label' => '',
      'name_mail' => 'Чтобы вы хотели увидеть/добавить/изменить',
      'error' => '',
      'container' => false,
    ),




    13 => 
    array (
      'type' => 'freearea',
      'container' => true,
      'id' => '',
      'class' => '',
      'value' => '</td></tr><tr><td>',
    ),
    21111 => 
    array (
      'type' => 'freearea',
      'id' => '',
      'class' => '',
      'value' => '<div class="field-11 bottom-1">2. Почему вы решили сотрудничать с нами?</div>',
      'container' => false,
    ),
    634353 => 
    array (
      'type' => 'textarea',
      'attributs' => 
      array (
        'name' => 'question21111',
        'type' => 'text',
        'rows' => '3',
        'cols' => '35',
        'placeholder' => 'Комментарий по желанию',
        'value' => '',
      ),
      'id' => '',
      'class' => '',
      'formail' => true,
      'label' => '',
      'name_mail' => 'Почему вы решили сотрудничать',
      'error' => '',
      'container' => false,
    ),
    71 => 
    array (
      'type' => 'freearea',
      'container' => true,
      'id' => '',
      'class' => '',
      'value' => '</td><td>',
    ),

    14 => 
    array (
      'type' => 'freearea',
      'id' => '',
      'class' => '',
      'value' => '<div class="field-7 nowrap">5. Будете ли вы рекомендовать нас своим друзьям или коллегам?</div>',
      'container' => false,
    ),
    15 => 
    array (
      'type' => 'freearea',
      'id' => '',
      'class' => '',
      'value' => '<div style="clear:both;"></div>',
      'container' => false,
    ),
    16 => 
    array (
      'type' => 'input',
      'attributs' => 
      array (
        'name' => 'oprospol3[]',
        'type' => 'radio',
        'value' => 'да',
        'checked' => '',
      ),
      'container' => true,
      'id' => '',
      'class' => '',
      'formail' => true,
      'label' => 'Да',
      'name_mail' => 'Будете ли вы рекомендовать нас своим друзьям или коллегам?',
      'error' => '',
    ),
    17 => 
    array (
      'type' => 'input',
      'attributs' => 
      array (
        'name' => 'oprospol3[]',
        'type' => 'radio',
        'value' => 'нет',
        'checked' => '',
      ),
      'container' => true,
      'id' => '',
      'class' => '',
      'formail' => true,
      'label' => 'Нет',
      'name_mail' => 'Будете ли вы рекомендовать нас своим друзьям или коллегам?',
      'error' => '',
    ),
    18 => 
    array (
      'type' => 'textarea',
      'formail' => 1,
      'name_mail' => 'Будете ли вы рекомендовать нас своим друзьям или коллегамв? текст',
      'attributs' => 
      array (
        'name' => 'question3',
        'type' => 'text',
        'rows' => '5',
        'cols' => '35',
        'placeholder' => 'Комментарий по желанию',
        'value' => '',
      ),
      'container' => false,
    ),
    191 => 
    array (
      'type' => 'freearea',
      'container' => true,
      'id' => '',
      'class' => '',
      'value' => '</td></tr><tr><td>',
    ),
    8 => 
    array (
      'type' => 'freearea',
      'id' => '',
      'class' => '',
      'value' => '<div class="field-4">3. Все ли вас устраивает на нашем сайте?</div>',
      'container' => false,
    ),
    9 => 
    array (
      'type' => 'freearea',
      'id' => '',
      'class' => '',
      'value' => '<div style="clear:both;"></div>',
      'container' => false,
    ),
    10 => 
    array (
      'type' => 'input',
      'attributs' => 
      array (
        'name' => 'oprospol2[]',
        'type' => 'radio',
        'value' => 'да',
        'checked' => '',
      ),
      'container' => true,
      'id' => '',
      'class' => '',
      'formail' => true,
      'label' => 'Да',
      'name_mail' => 'Все ли вас устраивает на нашем сайте?',
      'error' => '',
    ),
    11 => 
    array (
      'type' => 'input',
      'attributs' => 
      array (
        'name' => 'oprospol2[]',
        'type' => 'radio',
        'value' => 'нет',
        'checked' => '',
      ),
      'container' => true,
      'id' => '',
      'class' => '',
      'formail' => true,
      'label' => 'Нет',
      'name_mail' => 'Все ли вас устраивает на нашем сайте?',
      'error' => '',
    ),
    12 => 
    array (
      'type' => 'textarea',
      'formail' => 1,
      'name_mail' => 'Все ли вас устраивает на нашем сайте? текст',
      'attributs' => 
      array (
        'name' => 'question4',
        'type' => 'text',
        'rows' => '5',
        'cols' => '35',
        'placeholder' => 'Комментарий по желанию',
        'value' => '',
      ),
      'container' => false,
    ),
192 => 
    array (
      'type' => 'freearea',
      'container' => true,
      'id' => '',
      'class' => '',
      'value' => '</td><td>',
    ),
    21 => 
    array (
      'type' => 'input',
      'attributs' => 
      array (
        'name' => '',
        'type' => 'submit',
        'value' => 'Отправить',
      ),
      'container' => true,
      'id' => '',
      'class' => 'buttonform',
      'label' => '',
      'name_mail' => '',
      'error' => '',
    ),
    19 => 
    array (
      'type' => 'freearea',
      'container' => true,
      'id' => '',
      'class' => '',
      'value' => '</td></tr></table>',
    ),
    20 => 
    array (
      'type' => 'freearea',
      'id' => '',
      'class' => '',
      'value' => '<div style="clear:both;"></div>',
      'container' => false,
    ),
    
    22 => 
    array (
      'type' => 'freearea',
      'value' => '<div class="error_form"></div>',
      'container' => false,
    ),
  ),
);