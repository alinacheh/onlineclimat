<?php

return array (
  'charset' => 'utf-8',
  'validate' => 
  array (
    'html5' => false,
    'strlen' => 3,
    'error' => 'Поля не заполнены или слишком короткие!',
  ),
  'smtp' => 
  array (
    'on' => false,
    'host' => '',
    'secure' => 'ssl',
    'port' => 465,
    'auth' => true,
    'username' => '',
    'password' => '',
    'from_email' => true,
  ),
  'mail' => 
  array (
    'to_email' => 
    array (
      0 => 'a@b.com',
    ),
    'cc_email' => 
    array (
    ),
    'from_email' => 'info@constructor.gray-group.ru',
    'from_name' => 'Info',
    'subject' => 'ttt',
    'reverse_email' => false,
  ),
  'configform' => 
  array (
    0 => 
    array (
      'type' => 'freearea',
      'container' => false,
      'value' => '<div class="form-head">Новая форма</div>',
    ),
    1 => 
    array (
      'type' => 'input',
      'container' => true,
      'class' => 'buttonform',
      'attributs' => 
      array (
        'type' => 'submit',
        'value' => 'Отправить',
      ),
    ),
    2 => 
    array (
      'type' => 'freearea',
      'container' => false,
      'value' => '<div class="error_form"></div>',
    ),
    3 => 
    array (
      'type' => 'input',
      'attributs' => 
      array (
        'name' => 'btn',
        'type' => 'checkbox',
      ),
      'container' => true,
      'id' => '',
      'class' => '',
      'formail' => true,
      'label' => 'Требуется монтаж',
      'name_mail' => 'Требуется монтаж',
      'error' => 'Поле "Требуется монтаж" заполнено некорректно',
    ),
    4 => 
    array (
      'type' => 'input',
      'attributs' => 
      array (
        'name' => 'btn1',
        'type' => 'checkbox',
      ),
      'container' => true,
      'id' => '',
      'class' => '',
      'formail' => true,
      'label' => 'Требуется доставка',
      'name_mail' => 'Требуется доставка',
      'error' => 'Поле "Требуется доставка" заполнено некорректно',
    ),
  ),
);