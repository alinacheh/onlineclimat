<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
<div class="contacts_map">
    <?
    $APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view", 
	".default", 
	array(
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:55.73829999998174;s:10:\"yandex_lon\";d:37.59459999999997;s:12:\"yandex_scale\";i:10;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:37.583699502563;s:3:\"LAT\";d:55.671915107361;s:4:\"TEXT\";s:46:\"Нахимовский проспект, д.24\";}}}",
		"MAP_WIDTH" => "100%",
		"MAP_HEIGHT" => "450",
		"CONTROLS" => array(
			0 => "ZOOM",
			1 => "MINIMAP",
			2 => "TYPECONTROL",
			3 => "SCALELINE",
		),
		"OPTIONS" => array(
			0 => "ENABLE_SCROLL_ZOOM",
			1 => "ENABLE_DBLCLICK_ZOOM",
			2 => "ENABLE_DRAGGING",
		),
		"MAP_ID" => "",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);
//     $APPLICATION->IncludeComponent(
// 	"bitrix:map.google.view", 
// 	"map", 
// 	array(
// 		"INIT_MAP_TYPE" => "ROADMAP",
// 		"MAP_DATA" => "a:4:{s:10:\"google_lat\";d:55.67187155314944;s:10:\"google_lon\";d:37.58369223546992;s:12:\"google_scale\";i:16;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:4:\"TEXT\";s:23:\"Наш павильон\";s:3:\"LON\";d:37.584821769524;s:3:\"LAT\";d:55.671740010875;}}}",
// 		"MAP_WIDTH" => "100%",
// 		"MAP_HEIGHT" => "400",
// 		"CONTROLS" => array(
// 		),
// 		"OPTIONS" => array(
// 			0 => "ENABLE_DBLCLICK_ZOOM",
// 			1 => "ENABLE_DRAGGING",
// 		),
// 		"MAP_ID" => "",
// 		"ZOOM_BLOCK" => array(
// 			"POSITION" => "right center",
// 		),
// 		"COMPONENT_TEMPLATE" => "map",
// 		"API_KEY" => "AIzaSyAdJ1DT8KQJ-A7lya6p0aC8PUlVi0tOB-4",
// 		"COMPOSITE_FRAME_MODE" => "A",
// 		"COMPOSITE_FRAME_TYPE" => "AUTO"
// 	),
// 	false
// );

?>
</div>
<div class="wrapper_inner">
	<div class="contacts_left">
		<div class="store_description">
			<div class="store_property">
				<div class="title">Адрес</div>
				<div class="value">
					<?$APPLICATION->IncludeFile(SITE_DIR."include/address.php", Array(), Array("MODE" => "html", "NAME" => "Адрес"));?>
				</div>
			</div>
			<div class="store_property">
				<div class="title">Телефон</div>
				<div class="value">
					<?$APPLICATION->IncludeFile(SITE_DIR."include/phone.php", Array(), Array("MODE" => "html", "NAME" => "Телефон"));?>
				</div>
			</div>
			<div class="store_property">
				<div class="title">Email</div>
				<div class="value">
					<?$APPLICATION->IncludeFile(SITE_DIR."include/email.php", Array(), Array("MODE" => "html", "NAME" => "Email"));?>
				</div>
			</div>
			<div class="store_property">
				<div class="title">Режим работы</div>
				<div class="value">
					<?$APPLICATION->IncludeFile(SITE_DIR."include/schedule.php", Array(), Array("MODE" => "html", "NAME" => "Время работы"));?>
				</div>
			</div>
			<div class="store_property">
				<div class="value">
					<a href="/contacts/?print=1" target="_blank" class="print_version">Версия для печати</a>
				</div>
			</div>
		</div>
	</div>
	<div class="contacts_right">
		<blockquote><?$APPLICATION->IncludeFile(SITE_DIR."include/contacts_text.php", Array(), Array("MODE" => "html", "NAME" => GetMessage("CONTACTS_TEXT")));?></blockquote>
		<?Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("form-feedback-block");?>
		<?$APPLICATION->IncludeComponent("aku:form.result.new", "inline",
			Array(
				"WEB_FORM_ID" => "3",
				"IGNORE_CUSTOM_TEMPLATE" => "N",
				"USE_EXTENDED_ERRORS" => "Y",
				"SEF_MODE" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600000",
				"LIST_URL" => "",
				"EDIT_URL" => "",
				"SUCCESS_URL" => "?send=ok",
				"CHAIN_ITEM_TEXT" => "",
				"CHAIN_ITEM_LINK" => "",
				"VARIABLE_ALIASES" => Array(
					"WEB_FORM_ID" => "WEB_FORM_ID",
					"RESULT_ID" => "RESULT_ID"
				)
			)
		);?>
		<?Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("form-feedback-block", "");?>
	</div>
</div>
<div class="clearboth"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>