<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Sale\Compatible\DiscountCompatibility;
use Bitrix\Sale\Basket;
use Bitrix\Sale\Discount\Gift;
use Bitrix\Sale\Fuser;
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
CModule::IncludeModule("iblock");
CModule::IncludeModule("currency");



$arSelect0 = Array("ID", "IBLOCK_ID", "PROPERTY_*");
$arFilter0 = Array("IBLOCK_ID"=>1, "ACTIVE"=>"Y");
$res0 = CIBlockElement::GetList(Array("SORT" => "DESC"), $arFilter0, false, Array("nPageSize"=>20000), $arSelect0);
while($ob0 = $res0->GetNextElement()){
$arFields0 = $ob0->GetFields();
$arProps0 = $ob0->GetProperties();

if($arProps0['IS_PRESENT']['VALUE']){continue;}




 //скрипт для получеия подарков у товара
 $giftProductIds = [];
 DiscountCompatibility::stopUsageCompatible();
 $giftManager = Gift\Manager::getInstance();

 $potentialBuy = [
     'ID'                     => $arFields0['ID'],
     'MODULE'                 => 'catalog',
     'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
     'QUANTITY'               => 1,
 ];

 $basket = Basket::loadItemsForFUser(Fuser::getId(), SITE_ID);
 $basketPseudo = $basket->copy();
 foreach ($basketPseudo as $basketItem) {
     $basketItem->delete();
 }
 $collections = $giftManager->getCollectionsByProduct($basketPseudo, $potentialBuy);

if($collections){
foreach ($collections as $collection) {
 /** @var \Bitrix\Sale\Discount\Gift\Gift $gift */
 foreach ($collection as $gift) {
     $giftProductIds[] = $gift->getProductId();
 }
}
DiscountCompatibility::revertUsageCompatible();

//$newArrPresents[]= [$arFields['ID'] => $giftProductIds];//кладу в массив подарок и ID товара, к которому он идет

CIBlockElement::SetPropertyValuesEx($arFields0['ID'], 1, Array('TYPE'=>'HTML',"IS_PRESENT" => $giftProductIds[0]));

// echo '<pre>';
// print_r($giftProductIds);
// echo '</pre>';
}
}




require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>