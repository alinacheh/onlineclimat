<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
CModule::IncludeModule("iblock");
CModule::IncludeModule("currency");


$arSelect0 = Array("ID");
$arFilter0 = Array("IBLOCK_ID"=>1, "ACTIVE"=>"N");
$res0 = CIBlockElement::GetList(Array("SORT" => "DESC"), $arFilter0, false, Array("nPageSize"=>20000), $arSelect0);
while($ob0 = $res0->GetNextElement()){
$arFields0 = $ob0->GetFields();


 $arrId[]=$arFields0['ID'];
}



// echo '<pre>';
// print_r($arrId);
// echo '</pre>';





















for($r=0;$r<count($arrId);$r++){

$arSelect = Array("ID", "IBLOCK_ID", "NAME","IBLOCK_SECTION_ID", "DATE_ACTIVE_FROM","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>1, "ID" => $arrId[$r]);
$res = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilter, false, Array("nPageSize"=>2), $arSelect);
$ob = $res->GetNextElement();
$arFields = $ob->GetFields();
$arProps = $ob->GetProperties();

    $dbProps = \CIBlockElement::GetProperty(1, $arFields['ID'], 'sort', 'asc', ['CODE' => 'ATT_PDF_PRODUCT']);
    $arr = [];
    // Разбираем построчно данные из БД
    while ($arItem = $dbProps->Fetch()) {
       // Если указан ключ VALUE, то есть картинка
       if ($arItem['VALUE']) {
          // Так, набираем массив, для удаления
          $arr[$arItem['PROPERTY_VALUE_ID']] = ['VALUE' => ['del' => 'Y']];
          // Не похоже, что без этого фотографии действительно удаляются, лучше проверить
          $res = CFile::Delete($arItem['VALUE']);
       }
    }
    // Одной вилкой чистим множественной свойство картинок
    \CIBlockElement::SetPropertyValueCode(
        $arFields['ID'],
       'ATT_PDF_PRODUCT',
       $arr
    );
    
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>