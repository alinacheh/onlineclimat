<?define("NO_KEEP_STATISTIC", true); // Не собираем стату по действиям AJAX
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header("Content-Type: text/html; charset=utf-8");
$APPLICATION->RestartBuffer();
CModule::IncludeModule("iblock");
CModule::IncludeModule("user");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");



$arSelect = Array("ID", "IBLOCK_ID", "NAME","IBLOCK_SECTION_ID", "DETAIL_PICTURE","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>1, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("ID" => "ASC"), $arFilter, false, Array("nPageSize"=>10000), $arSelect);
while($ob = $res->GetNextElement()){
$arFields = $ob->GetFields();
$arProps = $ob->GetProperties();
// print_r($arFields);

$arImg[] = $arFields['DETAIL_PICTURE'];
$arID[] = $arFields['ID'];
}

for($i=0;$i<count($arImg);$i++){
$renderImage1 = CFile::ResizeImageGet($arImg[$i], Array("width" => 170, "height" => 170), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, false);
//$renderImage2 = CFile::ResizeImageGet($arImg[$i], Array("width" => 400, "height" => 400), BX_RESIZE_IMAGE_EXACT, false);
// echo $renderImage1["src"];
// echo $renderImage2["src"];


$el = new CIBlockElement;

$arLoadProductArray = Array(
    "PREVIEW_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$renderImage1["src"])
    //"DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$renderImage2["src"])
  );


$res = $el->Update($arID[$i], $arLoadProductArray);
}

echo 'ok';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
