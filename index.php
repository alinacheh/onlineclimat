<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Магазин Онлайн-Климат предлагает широкий выбор кондиционеров, радиаторов отопления, вентиляционных установок и другой климатической техники по низким ценам ↓. Доставка по Москве и России ⚡️. Весь товар сертифицирован √. Работы под ключ √. Звоните ☎️");
$APPLICATION->SetPageProperty("viewed_show", "Y");
$APPLICATION->SetTitle("Онлайн-Климат - магазин климатической техники: кондиционеры, радиаторы отопления, вентиляционные установки в Москве");
?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/comp_banners_top_slider.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false
);?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/comp_tizers.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false
);?>

<?
	$arOrder = array("SORT"=>"ASC", "ID" => "ASC");
	$arFilter = array("IBLOCK_ID" => 27, "ACTIVE" => "Y");
	$arSelect = array("IBLOCK_ID", "ID", "NAME", "DETAIL_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_ACTION", "PROPERTY_ON_MAIN", "PROPERTY_NAME_ON_MAIN", "PROPERTY_IMG_ON_MAIN");
	$res = CIBlockElement::getList($arOrder, $arFilter, false, false, $arSelect);
	while ($element = $res->GetNext()) {
        $idAction = $element["PROPERTY_ACTION_VALUE"];
        $DETAIL_PICTURE = CFile::GetPath($element["PROPERTY_IMG_ON_MAIN_VALUE"]);
        /*echo "<pre>";
        print_r($element);
        echo "</pre>";*/
	if (($element["PROPERTY_ON_MAIN_VALUE"] == "Y")) {?>
		<section class="actions-block">
            <div class="action">
            <?if ($element["PROPERTY_NAME_ON_MAIN_VALUE"]) {?>
                <h2 class="action-name"><?= ($element["PROPERTY_NAME_ON_MAIN_VALUE"]) ?></h2>
                <?}?>
            <?if ($element["PROPERTY_IMG_ON_MAIN_VALUE"]) { ?>
                    <a href="<?= ($element["DETAIL_PAGE_URL"]) ?>" class="action-link-detail" target="_blank">
                        <div class="action-banner-block">
                            <img src="<?= $DETAIL_PICTURE ?>" alt="<?= ($element["PROPERTY_NAME_ON_MAIN_VALUE"]) ?>"
                                 class="action-banner">
                        </div>
                    </a>
                <?}?>

            <?
            // Выводим товары из акции
            CModule::IncludeModule("sale");
            // Теперь достаем новым методом скидки с условиями. P.S. Старым методом этого делать не нужно из-за очень высокой нагрузки (уже тестировал)
            $actions = \Bitrix\Sale\Internals\DiscountTable::getList(array(
                'select' => array("ID", "ACTIONS_LIST", "CONDITIONS_LIST"),
                'filter' => array("ACTIVE" => "Y", "USE_COUPONS" => "N", "DISCOUNT_TYPE" => "P", "ID" => $idAction)
            ));
            // Перебираем каждую скидку и подготавливаем условия фильтрации для CIBlockElement::GetList
            while ($arrAction = $actions->fetch()) {
                //var_dump($arrAction["CONDITIONS_LIST"]["CHILDREN"][0]["CHILDREN"][0]["DATA"]["value"]);
                if ($arrAction["ACTIONS_LIST"]["CHILDREN"][0]["CLASS_ID"] === "ActSaleBsktGrp") {
                    if (empty($arrAction["CONDITIONS_LIST"]["CHILDREN"][0]["CHILDREN"][0]["DATA"]["value"])) {
                        foreach ($arrAction["ACTIONS_LIST"]["CHILDREN"][0]["CHILDREN"] as $arSeccc) {
                            $arrActions[$arSeccc["DATA"]["value"]]['ID'] = $arSeccc["DATA"]["value"];
                        }

                        $arSection["SECTION_FILTER"] = [];
                        foreach ($arrActions as $arSect) {
                            $arSection["SECTION_FILTER"][] = $arSect["ID"];
                            $rsParentSection = CIBlockSection::GetByID($arSect["ID"]);
                        }
                        if ($arParentSection = $rsParentSection->GetNext()) {
                            $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],
                                "ACTIVE" => "Y", '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
                                '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'], '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']
                            );
                            $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter, false, array("ID", "NAME", "IBLOCK_SECTION_ID"));
                            while ($arSect_w = $rsSect->GetNext()) {
                                $arSection["SECTION_FILTER"][] = $arSect_w["ID"];
                            }
                        }

                        global $arrFilter;
                        $arrFilter["IBLOCK_SECTION_ID"] = $arSection["SECTION_FILTER"];
                        $arrFilter["ID"] = [];

                    }

                    if (!empty($arrAction["CONDITIONS_LIST"]["CHILDREN"][0]["CHILDREN"][0]["DATA"]["value"])) {
                        $arrActions2 = [];
                        foreach ($arrAction["CONDITIONS_LIST"]["CHILDREN"][0]["CHILDREN"] as $arSec2) {
                            $arrActions2[$arrAction['ID']]['ID'][] = $arSec2["DATA"]["value"];
                        }
                        $arSection["SECTION_FILTER"] = [];
                        $rsParentSection = [];
                        foreach ($arrActions2 as $arSect) {
                            $arSection["SECTION_FILTER"][] = $arSect["ID"][0];
                            $rsParentSection = CIBlockSection::GetByID($arSect["ID"][0]);
                        }
                        if ($arParentSection = $rsParentSection->GetNext()) {
                            $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],
                                "ACTIVE" => "Y", '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
                                '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'], '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']
                            );
                            $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter, false, array("ID", "NAME", "IBLOCK_SECTION_ID"));
                            while ($arSect_w = $rsSect->GetNext()) {
                                $arSection["SECTION_FILTER"][] = $arSect_w["ID"];
                            }
                        }
                        $arrFilter["IBLOCK_SECTION_ID"] = $arSection["SECTION_FILTER"];
                    }
                }

                if ($arrAction["ACTIONS_LIST"]["CHILDREN"][0]["CLASS_ID"] === "GiftCondGroup") {
                    foreach ($arrAction["ACTIONS_LIST"]["CHILDREN"][0]["CHILDREN"][0]["CHILDREN"] as $arSeccc) {
                        $arrActions[$arSeccc["DATA"]["value"]]['ID'] = $arSeccc["DATA"]["value"];
                    }
                    if ($arrAction["CONDITIONS_LIST"]["CHILDREN"][0]["CHILDREN"][0]["DATA"]["value"]) {
                        $arrFilter = [];
                        foreach ($arrAction["CONDITIONS_LIST"]["CHILDREN"][0]["CHILDREN"][0]["DATA"]["value"] as $arElem) {
                            $arrActions[$arrAction['ID']]['ID'] = $arElem;
                            $arrFilter["ID"][] = $arrActions[$arrAction['ID']]['ID'];
                        }
                    }
                }
            }?>

            <?/*$arrActions = [];
            while ($arrAction = $actions->fetch()) {
                if ($arrAction["ACTIONS_LIST"]["CHILDREN"][0]["CLASS_ID"] === "ActSaleBsktGrp") {
                    if ($arrAction["ACTIONS_LIST"]["CHILDREN"][0]["CHILDREN"][0]["CHILDREN"]) {
                        foreach ($arrAction["ACTIONS_LIST"]["CHILDREN"][0]["CHILDREN"][0]["CHILDREN"] as $arSeccc) {
                            $arrActions[$arSeccc["DATA"]["value"]]['ID'] = $arSeccc["DATA"]["value"];
                        }
                    } else {
                        foreach ($arrAction["ACTIONS_LIST"]["CHILDREN"][0]["CHILDREN"] as $arSeccc) {
                            $arrActions[$arSeccc["DATA"]["value"]]['ID'] = $arSeccc["DATA"]["value"];
                        }
                    }
                }
                if ($arrAction["ACTIONS_LIST"]["CHILDREN"][0]["CLASS_ID"] === "GiftCondGroup") {
                    foreach ($arrAction["ACTIONS_LIST"]["CHILDREN"][0]["CHILDREN"][0]["CHILDREN"] as $arSeccc) {
                        $arrActions[$arSeccc["DATA"]["value"]]['ID'] = $arSeccc["DATA"]["value"];
                    }
                }
                if ($arrAction["CONDITIONS_LIST"]["CHILDREN"][0]["CHILDREN"][0]["DATA"]["value"]) {
                    $arrActions[$arrAction['ID']]['ID'] = $arrAction["CONDITIONS_LIST"]["CHILDREN"][0]["CHILDREN"][0]["DATA"]["value"];
                }
            }

            $arSection["SECTION_FILTER"] = [];
            foreach ($arrActions as $arSect) {
                $arSection["SECTION_FILTER"][] = $arSect["ID"];
                $rsParentSection = CIBlockSection::GetByID($arSect["ID"]);
                if ($arParentSection = $rsParentSection->GetNext()) {
                    $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],
                        "ACTIVE" => "Y", '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
                        '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'], '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']
                    );
                    $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter, false, array("ID", "NAME", "IBLOCK_SECTION_ID"));
                    while ($arSect_w = $rsSect->GetNext()) {
                        //$arSection["SECTION_FILTER"][] = $arSect_w;
                        $arSection["SECTION_FILTER"][] = $arSect_w["ID"];
                    }
                }
            }

            $arrFilter = [];
            if ($arSection["SECTION_FILTER"][0][0]) {
                // Вывод подарков
                foreach ($arSection["SECTION_FILTER"][0] as $key => $ids) {
                    $arFilter = array("IBLOCK_ID" => 1, "ID" => $ids, "ACTIVE" => "Y");
                    $res = CIBlockElement::GetList(array(), $arFilter, false, false, false);
                    while ($ob = $res->GetNext()) {
                        global $arrFilter;
                        $arrFilter["ID"][] = $ob["ID"];
                    }
                }
            } else {
                global $arrFilter;
                $arrFilter["IBLOCK_SECTION_ID"] = $arSection["SECTION_FILTER"];
            }
            */?>
            <? if (!empty($arrFilter)) {
                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section",
                    "catalog_block_sale",
                    array(
                        "SEF_MODE" => "Y",
                        "SEF_RULE" => "",
                        "AJAX_MODE" => "Y",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => "1",
                        "SECTION_ID" => "",
                        "SECTION_CODE" => "",
                        "SECTION_USER_FIELDS" => array(
                            0 => "",
                            1 => "",
                        ),
                        "ELEMENT_SORT_FIELD" => "CATALOG_PRICE_1",
                        "ELEMENT_SORT_ORDER" => "asc",
                        "ELEMENT_SORT_FIELD2" => "id",
                        "ELEMENT_SORT_ORDER2" => "asc",
                        "FILTER_NAME" => "arrFilter",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "SHOW_ALL_WO_SECTION" => "Y",
                        "SECTION_URL" => "",
                        "DETAIL_URL" => "",
                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                        "SET_TITLE" => "N",
                        "SET_BROWSER_TITLE" => "N",
                        "BROWSER_TITLE" => "-",
                        "SET_META_KEYWORDS" => "N",
                        "META_KEYWORDS" => "-",
                        "SET_META_DESCRIPTION" => "N",
                        "META_DESCRIPTION" => "-",
                        "SET_LAST_MODIFIED" => "N",
                        "USE_MAIN_ELEMENT_SECTION" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "PAGE_ELEMENT_COUNT" => "8",
                        "LINE_ELEMENT_COUNT" => "4",
                        "PROPERTY_CODE" => array(
                            0 => "54",
                            1 => "CML2_LINK",
                            2 => "",
                        ),
                        "OFFERS_FIELD_CODE" => array(
                            0 => "NAME",
                            1 => "CML2_LINK",
                            2 => "DETAIL_PAGE_URL",
                            3 => "",
                        ),
                        "OFFERS_PROPERTY_CODE" => array(
                            0 => "CML2_ARTICLE",
                            1 => "SIZE",
                            2 => "COLOR",
                            3 => "CONF",
                            4 => "",
                        ),
                        "OFFERS_SORT_FIELD" => "sort",
                        "OFFERS_SORT_ORDER" => "asc",
                        "OFFERS_SORT_FIELD2" => "id",
                        "OFFERS_SORT_ORDER2" => "desc",
                        "OFFERS_LIMIT" => "2",
                        "PRICE_CODE" => array(
                            0 => "main",
                        ),
                        "USE_PRICE_COUNT" => "N",
                        "SHOW_PRICE_COUNT" => "1",
                        "PRICE_VAT_INCLUDE" => "Y",
                        "BASKET_URL" => "/personal/basket.php",
                        "ACTION_VARIABLE" => "action",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRODUCT_PROPERTIES" => array(),
                        "BACKGROUND_IMAGE" => "-",
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "COMPATIBLE_MODE" => "Y",
                        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Товары",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "SET_STATUS_404" => "N",
                        "SHOW_404" => "N",
                        "MESSAGE_404" => "",
                        "CUSTOM_FILTER" => "",
                        "SHOW_DISCOUNT_PERCENT" => "Y",
                        "SHOW_OLD_PRICE" => "Y",
                        "HIDE_NOT_AVAILABLE" => "N",
                        "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                        "CONVERT_CURRENCY" => "Y",
                        "OFFERS_CART_PROPERTIES" => array(),
                        "DISPLAY_COMPARE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "COMPONENT_TEMPLATE" => "catalog_block_sale",
                        "SECTION_CODE_PATH" => "",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "CURRENCY_ID" => "RUB"
                    ),
                    false
                );
            }?>
                <div class="link-all-products-block">
                    <a href="<?=($element["DETAIL_PAGE_URL"])?>" class="link-all-products" target="_blank">Показать ещё товары</a>
                </div>
            </div>
        </section>
        <?}
        }?>


<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/comp_banners_float.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false
);?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/comp_catalog_hit.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false
);?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/comp_news_akc.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false
);?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/inc_company.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false
);?>	

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/comp_brands.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>

<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "article_on_main",
    Array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "info",
        "IBLOCK_ID" => "4",
        "NEWS_COUNT" => "3",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array("NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DATE_ACTIVE_FROM"),
        "PROPERTY_CODE" => array(),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "j F Y",
        "SET_TITLE" => "N",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_LAST_MODIFIED" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "STRICT_SECTION_CHECK" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "PAGER_TEMPLATE" => ".default",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => "",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "IS_VERTICAL" => "Y",
        "BIG_IMG" => "Y"
    ),
    false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>


