<?php
$time = microtime(true);
$_SERVER["DOCUMENT_ROOT"] = __DIR__ . '/../../';
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
define("CRON_SCRIPT", true);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("BX_CRONTAB", true);
define('BX_NO_ACCELERATOR_RESET', true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
@set_time_limit(0);
@ignore_user_abort(true);
$entity = "YMarketCategories";
$list = file('categories.txt');
foreach ($list as $k => &$v) {
    $v = str_replace('Все товары', '', $v);
    $v = ltrim($v, '/');
    $v = trim($v);
    if (strlen($v) <= 0) {
        unset($list[$k]);
    }
}
unset($v);
$hlCatObject = new \Astdesign\Tools\HighLoad($entity);

function getParent($name, $hlCatObject)
{
    $tmpName = explode("/", $name);
    unset($tmpName[count($tmpName) - 1]);
    $tmpName = implode("/", $tmpName);
    $hlCatObject->setFilter(['UF_YM_NAME' => $tmpName]);
    $hlCatObject->setSelect(['ID']);
    $parent = $hlCatObject->getList();
    if ($parent) {
        $parentId = current($parent)['ID'];
    } else {
        $parentId = addItem($tmpName, $hlCatObject);
    }

    return $parentId;
}

function addItem($name, $hlCatObject)
{
    $hlCatObject->setFilter(['UF_YM_NAME' => $name]);
    $hlCatObject->setSelect(['ID']);
    $result = $hlCatObject->getList();
    if (!$result) {
        $parentId = 0;
        if (stripos($name, '/') !== false) {
            $parentId = getParent($name, $hlCatObject);
        }
        $shortName = $name;
        if (stripos($name, "/") !== false) {
            $shortName = explode("/", $name);
            $shortName = $shortName[count($shortName) - 1];
        }
        $result = $hlCatObject->add([
            'UF_YM_NAME' => $name,
            'UF_YM_SHORT_NAME' => $shortName,
            'UF_YM_PARENT' => $parentId
        ]);
        return $result;
    }
    return false;
}

foreach ($list as $name) {
    addItem($name, $hlCatObject);
}

echo "<br/>" . (microtime(true) - $time) . " c.";