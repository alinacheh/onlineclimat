<?php
declare(strict_types=1);

namespace Astdesign\Tools;

use Bitrix\Highloadblock as HL;

/**
 * Class HighLoad
 */
class HighLoad
{
    private $object;
    private $select = [];
    private $filter = [];
    private $group = [];
    private $order = [];
    private $limit = 0;
    private $offset = 0;
    private $error = "";
    private $id;

    /**
     * HighLoad constructor.
     * @param string $hlName
     */
    function __construct($hlName)
    {
        \Bitrix\Main\Loader::includeModule('highloadblock');
        $filter = ['NAME' => $hlName];
        $rsData = HL\HighloadBlockTable::getList(['filter' => $filter]);
        if ($arData = $rsData->fetch()) {
            $this->id = $arData['ID'];
            $entity = HL\HighloadBlockTable::compileEntity($arData);
            $this->object = $entity->getDataClass();
        }
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * добавляет элемент
     * @param array $fields
     * @return bool|integer
     */
    public function create(array $fields = [])
    {
        $result = $this->object->add($fields);
        if (!$result->isSuccess()) {
            $this->error = implode(', ', $result->getErrorMessages());
            return false;
        } else {
            return $result->getId();
        }
    }

    /**
     * добавляет элемент (алиас)
     * @param array $fields
     * @return bool|int
     */
    public function add(array $fields = [])
    {
        return $this->create($fields);
    }

    /**
     * обновляет элемент
     * @param int $id
     * @param array $fields
     * @return bool
     */
    public function update(int $id, array $fields = [])
    {
        $result = $this->object->update($id, $fields);
        if (!$result->isSuccess()) {
            $this->error = implode(', ', $result->getErrorMessages());
            return false;
        } else {
            return true;
        }
    }

    /**
     * удаляет элемент
     * @param integer $id
     * @return bool
     */
    public function delete(int $id)
    {
        $result = $this->object->delete($id);
        if (!$result->isSuccess()) {
            $this->error = implode(', ', $result->getErrorMessages());
            return false;
        } else {
            return true;
        }
    }

    /**
     * получение списка записей
     * @return array
     */
    public function getList()
    {
        $fields = [];
        if ($this->select) {
            $fields['select'] = $this->select;
        } else {
            $fields['select'] = ['*'];
        }
        if ($this->filter) {
            $fields['filter'] = $this->filter;
        }
        if ($this->group) {
            $fields['group'] = $this->group;
        }
        if ($this->order) {
            $fields['order'] = $this->order;
        }
        if ($this->limit) {
            $fields['limit'] = $this->limit;
        }
        if ($this->offset) {
            $fields['offset'] = $this->offset;
        }
        $this->object = new $this->object;
        $res = $this->object->getList($fields);
        $result = $res->fetchAll();

        return $result;
    }

    public function getById(int $id)
    {
        $this->setFilter(['ID' => $id]);
        return current($this->getList());
    }

    /**
     * возвращает количество записей
     * @return int
     */
    public function getCount()
    {
        $fields = [];
        if ($this->filter) {
            $fields['filter'] = $this->filter;
        }
        if ($this->limit) {
            $fields['limit'] = $this->limit;
        }
        if ($this->offset) {
            $fields['offset'] = $this->offset;
        }

        $res = $this->object->getList($fields);

        return $res->getSelectedRowsCount();
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    public function setSelect(array $fields = [])
    {
        if (!empty($fields)) {
            $this->select = $fields;
        }
    }

    public function setFilter(array $fields = [])
    {
        if (!empty($fields)) {
            $this->filter = $fields;
        }
    }

    public function setOrder(array $fields = [])
    {
        if (!empty($fields)) {
            $this->order = $fields;
        }
    }

    public function setGroup(array $fields = [])
    {
        if (!empty($fields)) {
            $this->group = $fields;
        }
    }

    public function setLimit($value)
    {
        if ($value) {
            $this->limit = $value;
        }
    }

    public function setOffset($value)
    {
        if ($value) {
            $this->offset = $value;
        }
    }

    public function resetQuery()
    {
        $this->select = [];
        $this->filter = [];
        $this->order = [];
        $this->group = [];
        $this->limit = 0;
        $this->offset = 0;
    }

    public static function getInstance()
    {
        return new self();
    }
}