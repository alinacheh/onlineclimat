<?php

namespace Astdesign\Tools\Local;

/**
 * Class Modifiers
 * @package Astdesign\Tools\Local
 */
class TdbgImport
{
    private $brands = [];
    private $properties = [];

    function __construct()
    {
        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('catalog');
        $this->iblock = \Astdesign\Tools\IBlock::getIblockByCode("catalog");
        $this->oElement = new \CIBlockElement();
        $this->oSection = new \CIBlockSection();
        $this->oProperty = new \CIBlockProperty();
    }

    public static function getStatus($statusCode)
    {
        $status = [
            'downloadYml' => 'Скачан yml файл',
            'downloadZip' => 'Скачан архив с картинками',
            'unpackZip' => 'Распакован архив с картинками',
            'processFile' => 'Выполняется обработка данных',
            'finished' => 'Обработка всех файлов завершена',
        ];
        return $status[$statusCode];
    }

    private function checkProperty($name = "", $code = "")
    {
        $result = false;
        if ($code) {
            $code = 'tdbg_' . $code;
        }
        if (empty($this->properties)) {
            $res = \CIBlockProperty::GetList(
                [],
                [
                    'IBLOCK_ID' => $this->iblock,
                    'ACTIVE' => 'Y'
                ]
            );
            while ($r = $res->fetch()) {
                $r['NAME'] = trim($r['NAME']);
                $this->properties['NAMES'][$r['NAME']] = $r['ID'];
                $this->properties['XML_IDS'][$r['XML_ID']] = $r['ID'];
            }
        }
        if (array_key_exists($code, $this->properties['XML_IDS'])) {
            $result = $this->properties['XML_IDS'][$code];
        }
        if (!$result && array_key_exists($name, $this->properties['NAME'])) {
            $result = $this->properties['NAME'][$name];
        }
        if (!$result) {
            $transCode = \Astdesign\Tools\Utils::translit($name);
            $transCode = ToUpper($code) . "_" . substr($transCode, 0, 10);
            $transCode = str_replace("-", "_", $transCode);
            $fields = [
                'NAME' => $name,
                'XML_ID' => $code,
                'IBLOCK_ID' => $this->iblock,
                'ACTIVE' => 'Y',
                'CODE' => $transCode,
                'SORT' => 500,
                'PROPERTY_TYPE' => 'S'
            ];
            $result = $this->oProperty->Add($fields);
            if ($result) {
                $this->properties['NAMES'][$fields['NAME']] = $result;
                $this->properties['XML_IDS'][$fields['XML_ID']] = $result;
            }
        }

        return $result;
    }

    private function checkBrand($name = "")
    {
        $iblock = \Astdesign\Tools\IBlock::getIblockByCode("brand");
        if (empty($this->brands)) {
            $res = \CIBlockElement::GetList(
                [],
                [
                    'IBLOCK_ID' => $iblock
                ]
            );
            while ($r = $res->fetch()) {
                $r['NAME'] = trim($r['NAME']);
                $this->brands[$r['NAME']] = $r['ID'];
            }
        }
        if (array_key_exists($name, $this->brands)) {
            $result = $this->brands[$name];
        } else {
            $fields = [
                'NAME' => $name,
                'IBLOCK_ID' => $iblock,
                'ACTIVE' => 'Y',
                'CODE' => \Astdesign\Tools\Utils::translit($name)
            ];
            $result = $this->oElement->Add($fields);
            if ($result) {
                $this->brands[$name] = $result;
            }
        }
        return $result;
    }

    private function deleteImages($itemId = false)
    {
        if (!$itemId) {
            return;
        }
        /**
         * удаляем существующие дополнительные картинки
         */
        $values = [];
        $res = \CIBlockElement::GetProperty(
            $this->iblock,
            $itemId,
            $by = "sort",
            $order = "asc",
            [
                'CODE' => 'MORE_PHOTO'
            ]
        );
        while ($r = $res->fetch()) {
            $values[$r['PROPERTY_VALUE_ID']] = [
                "VALUE" => [
                    'del' => 'Y'
                ]
            ];
        }
        if (!empty($values)) {
            \CIBlockElement::SetPropertyValues(
                $itemId,
                $this->iblock,
                $values,
                "MORE_PHOTO"
            );
        }
    }

    public function processItem($item = [], $section = false)
    {
        if (!$section) {
            return;
        }
        $this->__trimAll($item);
        foreach ($item as $key => $value) {
            if (!is_array($value)) {
                $item[$key] = trim($value);
            }
        }
        $needUpdate = false;
        $needAdd = false;
        $itemId = false;
        $productName = trim($item['name']) ? trim($item['name']) : trim($item['model']);
        $productCode = \Astdesign\Tools\Utils::translit($productName);
        if (strlen($productCode) > 90) {
            $productCode = $item['@id'] . "_" . substr($productCode, 0, 90);
        } else {
            $productCode = $item['@id'] . "_" . $productCode;
        }
        $data = [
            "IBLOCK_ID" => $this->iblock,
            "ACTIVE" => "Y",
            "NAME" => $productName,
            "XML_ID" => 'tdbg_' . $item['@id'],
            "DETAIL_TEXT" => $item['description'],
            "DETAIL_TEXT_TYPE" => "html",
            "CODE" => $productCode,
            "PROPERTY_VALUES" => [
                "BRAND_IBLOCK" => $this->checkBrand($item["vendor"]),
                "COUNTRY_OF_ORIGIN" => $item["country_of_origin"]
            ],
            "PICTURES" => []
        ];
        if (!empty($item['param'])) {
            if (isset($item['param']['@name'])) {
                $item['param'] = [$item['param']];
            }
            foreach ($item['param'] as $param) {
                $this->__trimAll($param);
                $code = $param["@code"];
                $name = $param["@name"];
                $unit = $param["@unit"];
                $value = $param["$"];
                if ($unit) {
                    $name .= ", " . $unit;
                }
                $propertyID = $this->checkProperty($name, $code);
                $data["PROPERTY_VALUES"][$propertyID] = $value;
            }
        }
        if (!empty($item['picture'])) {
            if (!is_array($item['picture'])) {
                $item['picture'] = [$item['picture']];
            }
            $data["PICTURES"] = $item['picture'];
        }



        $hash = md5(serialize($data));
        $data['TMP_ID'] = $hash;
        $res = \CIBlockElement::GetList(
            [],
            [
                'IBLOCK_ID' => $this->iblock,
                'XML_ID' => $data['XML_ID']
            ],
            false,
            false,
            [
                'ID',
                'TMP_ID'
            ]
        )->fetch();
        if (!$res) {
            $needAdd = true;
        } else {
            if ($res['TMP_ID'] != $hash) {
                $needUpdate = true;
                $itemId = $res['ID'];
            }
        }

        if ($needAdd || $needUpdate) {
            $data['DETAIL_PICTURE'] = \CFile::MakeFileArray($data["PICTURES"][0]);
            unset($data["PICTURES"][0]);
            if ($needUpdate && $itemId) {
                $this->deleteImages($itemId);
            }
            foreach ($data["PICTURES"] as $picture) {
                $data["PROPERTY_VALUES"]["MORE_PHOTO"][] = \CFile::MakeFileArray($picture);
            }
            unset($data['PICTURES']);
            if ($itemId) {
                $props = $data["PROPERTY_VALUES"];
                unset($data['PROPERTY_VALUES']);
                unset($data['NAME']);
                $this->oElement->Update($itemId, $data);
                foreach ($props as $pCode => $pValue) {
                    \CIBlockElement::SetPropertyValues(
                        $itemId,
                        $this->iblock,
                        $pValue,
                        $pCode
                    );
                }
            } else {
                $data["IBLOCK_SECTION_ID"] = $section;
                $itemId = $this->oElement->Add($data);
                if (!$itemId) {
                    echo "<pre>";
                    print_r($data);
                    echo "</pre>";
                    echo $this->oElement->LAST_ERROR;
                    die();
                }
            }
            if ($itemId) {
                // prices
                \CCatalogProduct::Add(
                    [
                        "ID" => $itemId,
                    ]
                );
                $price = $item["price"];
                if ($price) {
                    $res = \CPrice::GetList([],
                        array(
                            "PRODUCT_ID" => $itemId
                        )
                    )->fetch();
                    if ($res) {
                        \CPrice::Update($res["ID"], [
                            'CATALOG_GROUP_ID' => $res['CATALOG_GROUP_ID'],
                            'PRICE' => $price,
                            'CURRENCY' => $res['CURRENCY']
                        ]);
                    } else {
                        \CPrice::Add(array(
                            "PRODUCT_ID" => $itemId,
                            'CATALOG_GROUP_ID' => 1,
                            'PRICE' => $price,
                            'CURRENCY' => 'RUB'
                        ));
                    }
                }
            }
        }
    }

    private function __trimAll(&$array = [])
    {
        foreach ($array as $key => $value) {
            if (!is_array($value)) {
                $array[$key] = trim($value);
            }
        }
    }
}