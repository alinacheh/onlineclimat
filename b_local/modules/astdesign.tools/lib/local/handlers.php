<?php

namespace Astdesign\Tools\Local;

use Bitrix\Main\EventManager;

class Handlers
{
    public function __construct()
    {
        EventManager::getInstance()->addEventHandler(
            "main",
            "OnPageStart",
            array(
                "Astdesign\\Tools\\Local\\Handlers",
                "getUtmSources"
            )
        );

        EventManager::getInstance()->addEventHandler(
            "main",
            "OnEpilog",
            array(
                "Astdesign\\Tools\\Local\\Handlers",
                "_Check404Error"
            ),
            false,
            1
        );

        EventManager::getInstance()->addEventHandler(
            "main",
            "OnEpilog",
            array(
                "Astdesign\\Tools\\Local\\Handlers",
                "_Check404Error"
            ),
            false,
            1
        );

        EventManager::getInstance()->addEventHandler(
            "main",
            "OnUserTypeBuildList",
            array(
                "Astdesign\\Tools\\Local\\ATypeElementToProperty",
                "GetUserTypeDescription"
            )
        );

        EventManager::getInstance()->addEventHandler(
            "main",
            "OnUserTypeBuildList",
            array(
                "Astdesign\\Tools\\Local\\ATypeSectionToHlElement",
                "GetUserTypeDescription"
            )
        );

        EventManager::getInstance()->addEventHandler(
            "main",
            "OnBuildGlobalMenu",
            array(
                "Astdesign\\Tools\\Local\\Handlers",
                "addIcons"
            )
        );
    }

    /**
     * сохраняет UTM метки пользователя
     * сохраняет внешнюю страницу, с которой перешли на сайт
     */
    public static function getUtmSources()
    {
        foreach ($_GET as $k => $v) {
            if (stripos($k, 'utm_') !== false) {
                \Astdesign\Tools\Session::setSessionData($k, htmlspecialchars($v), 'UTM');
            }
        }
        if ($_SERVER['HTTP_REFERER'] != '') {
            if (stripos($_SERVER['HTTP_REFERER'], $_SERVER['SERVER_NAME']) === false) {
                \Astdesign\Tools\Session::setSessionData('HTTP_REFERER', htmlspecialchars($_SERVER['HTTP_REFERER']));
            }
        }
    }

    public static function _Check404Error()
    {
        if (defined('ERROR_404') && ERROR_404 == 'Y' || \CHTTP::GetLastStatus() == "404 Not Found") {
            \CHTTP::SetStatus('404 Not Found');
            GLOBAL $APPLICATION;
            $APPLICATION->RestartBuffer();
            include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/header.php';
            include $_SERVER['DOCUMENT_ROOT'] . '/404.php';
            include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/footer.php';
        }
    }

    public static function addIcons(&$arGlobalMenu, &$arModuleMenu)
    {
        $aMenu = array(
            "parent_menu" => "global_menu_store",
            "section" => "",
            "sort" => 10,
            "text" => 'ASTDESIGN: импорт прайс-листов tdbg.ru',
            "title" => 'ASTDESIGN: импорт прайс-листов tdbg.ru',
            "url" => "astdesign_price_import.php",
            "icon" => "sys_menu_icon",
            "page_icon" => "sys_menu_icon",
            "items_id" => "astdesign_price_import",
        );

        $arModuleMenu[] = $aMenu;
    }
}

class ATypeElementToProperty extends \CUserTypeEnum
{

    // инициализация пользовательского свойства для главного модуля
    function GetUserTypeDescription()
    {
        return array(
            "USER_TYPE_ID" => "to_property",
            "CLASS_NAME" => "ATypeElementToProperty",
            "DESCRIPTION" => "Привязка к свойствам инфоблока",
            'BASE_TYPE' => 'int',
        );
    }

    // инициализация пользовательского свойства для инфоблока
    function GetIBlockPropertyDescription()
    {
        return array(
            "PROPERTY_TYPE" => "N",
            "USER_TYPE" => "to_property",
            "DESCRIPTION" => "Привязка к свойствам инфоблока",
            'GetPropertyFieldHtml' => array('UserDataColor', 'GetPropertyFieldHtml'),
            'GetAdminListViewHTML' => array('UserDataColor', 'GetAdminListViewHTML'),
        );
    }

    // представление свойства
    function getViewHTML($name, $value)
    {
        $dbCities = CIBlockProperty::GetList(array("ID" => "ASC"), array("IBLOCK_ID" => 1, "FILTRABLE" => 1));
        $html = '<select multiple name="' . $name . '" size="5"><option value="">Нет</option>';
        while ($arCity = $dbCities->Fetch()) {
            //$arCity = CSaleLocation::GetByID($dbCity['LOCATION_ID'], LANGUAGE_ID);
            $selected = '';
            if (in_array($arCity['ID'], $value)) {
                $selected = ' selected="selected" ';
            }
            $html .= '<option value="' . $arCity['ID'] . '"' . $selected . '>' . $arCity['NAME'] . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    function GetList($arUserField)
    {
        $rsSection = false;
        if (CModule::IncludeModule('iblock')) {
            $obSection = new CIBlockPropertyEnumMy;
            $rsSection = $obSection->GetTreeList($arUserField["SETTINGS"]["IBLOCK_ID"]);
        }
        return $rsSection;
    }

    function PrepareSettings($arUserField)
    {
        $height = intval($arUserField["SETTINGS"]["LIST_HEIGHT"]);
        $iblock_id = intval($arUserField["SETTINGS"]["IBLOCK_ID"]);
        if ($iblock_id <= 0) {
            $iblock_id = "";
        }

        return array(
            "IBLOCK_ID" => $iblock_id,
            "LIST_HEIGHT" => ($height < 1 ? 1 : $height),
        );
    }

    function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm)
    {
        $result = '';

        if ($bVarsFromForm) {
            $iblock_id = $GLOBALS[$arHtmlControl["NAME"]]["IBLOCK_ID"];
        } elseif (is_array($arUserField)) {
            $iblock_id = $arUserField["SETTINGS"]["IBLOCK_ID"];
        } else {
            $iblock_id = "";
        }
        if (CModule::IncludeModule('iblock')) {
            $result .= '
                <tr>
                    <td>' . GetMessage("USER_TYPE_IBSEC_DISPLAY") . ':</td>
                    <td>
                        ' . GetIBlockDropDownList($iblock_id, $arHtmlControl["NAME"] . '[IBLOCK_TYPE_ID]',
                    $arHtmlControl["NAME"] . '[IBLOCK_ID]', false, 'class="adm-detail-iblock-types"',
                    'class="adm-detail-iblock-list"') . '
                    </td>
                </tr>
                ';
        } else {
            $result .= '
                <tr>
                    <td>' . GetMessage("USER_TYPE_IBSEC_DISPLAY") . ':</td>
                    <td>
                        <input type="text" size="6" name="' . $arHtmlControl["NAME"] . '[IBLOCK_ID]" value="' . htmlspecialcharsbx($value) . '">
                    </td>
                </tr>
                ';
        }


        if ($bVarsFromForm) {
            $value = intval($GLOBALS[$arHtmlControl["NAME"]]["LIST_HEIGHT"]);
        } elseif (is_array($arUserField)) {
            $value = intval($arUserField["SETTINGS"]["LIST_HEIGHT"]);
        } else {
            $value = 5;
        }
        $result .= '
            <tr>
                <td>' . GetMessage("USER_TYPE_IBSEC_LIST_HEIGHT") . ':</td>
                <td>
                    <input type="text" name="' . $arHtmlControl["NAME"] . '[LIST_HEIGHT]" size="10" value="' . $value . '">
                </td>
            </tr>
            ';


        return $result;
    }
}

\Bitrix\Main\Loader::includeModule('highloadblock');

class ATypeSectionToHlElement extends \CUserTypeHlblock
{
    // инициализация пользовательского свойства для главного модуля
    function GetUserTypeDescription()
    {
        return array(
            "USER_TYPE_ID" => "ast_hl_element",
            "CLASS_NAME" => "\\Astdesign\\Tools\\Local\\ATypeSectionToHlElement",
            "DESCRIPTION" => "AST: Привязка к элементам HL блока",
            'BASE_TYPE' => 'int'
        );
    }

    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        if (($arUserField["ENTITY_VALUE_ID"] < 1) && strlen($arUserField["SETTINGS"]["DEFAULT_VALUE"]) > 0) {
            $arHtmlControl["VALUE"] = intval($arUserField["SETTINGS"]["DEFAULT_VALUE"]);
        }
        //\CJSCore::Init(array("jquery"));
        $assetsPath = "/local/modules/astdesign.tools/assets/select2";
        //\Bitrix\Main\Page\Asset::getInstance()->addCss($assetsPath . '/css/select2.min.css');
        //\Bitrix\Main\Page\Asset::getInstance()->addJs($assetsPath . "/js/select2.min.js");
        ob_start();
        ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.js-select2').select2({ width: '100%' });
            });
        </script>
        <?php
        $js = ob_get_contents();
        ob_end_clean();
        $js = '<link rel="stylesheet" type="text/css" href="'.$assetsPath.'/css/select2.min.css"><script type="text/javascript" src="'.$assetsPath.'/js/select2.min.js"></script>'.$js;
        //\Bitrix\Main\Page\Asset::getInstance()->addString($js, true, \Bitrix\Main\Page\AssetLocation::AFTER_JS);

        $rsEnum = call_user_func_array(
            array($arUserField["USER_TYPE"]["CLASS_NAME"], "getlist"),
            array(
                $arUserField,
            )
        );
        if (!$rsEnum) {
            return '';
        }
        $bWasSelect = false;
        $result2 = '';
        while ($arEnum = $rsEnum->GetNext()) {
            $bSelected = (
                ($arHtmlControl["VALUE"] == $arEnum["ID"]) ||
                ($arUserField["ENTITY_VALUE_ID"] <= 0 && $arEnum["DEF"] == "Y")
            );
            $bWasSelect = $bWasSelect || $bSelected;
            $result2 .= '<option value="' . $arEnum["ID"] . '"' . ($bSelected ? ' selected' : '') . '>' . $arEnum["VALUE"] . '</option>';
        }

        if ($arUserField["SETTINGS"]["LIST_HEIGHT"] > 1) {
            $size = ' size="' . $arUserField["SETTINGS"]["LIST_HEIGHT"] . '"';
        } else {
            $arHtmlControl["VALIGN"] = "middle";
            $size = '';
        }

        $result = '<select class="js-select2" name="' . $arHtmlControl["NAME"] . '"' . $size . ($arUserField["EDIT_IN_LIST"] != "Y" ? ' disabled="disabled" ' : '') . '>';
        if ($arUserField["MANDATORY"] != "Y") {
            $result .= '<option value=""' . (!$bWasSelect ? ' selected' : '') . '>' . htmlspecialcharsbx(strlen($arUserField["SETTINGS"]["CAPTION_NO_VALUE"]) > 0 ? $arUserField["SETTINGS"]["CAPTION_NO_VALUE"] : GetMessage('MAIN_NO')) . '</option>';
        }
        $result .= $result2;
        $result .= '</select>'.$js;
        return $result;
    }

    function GetAdminListEditHTML($arUserField, $arHtmlControl){
        return "";
    }
}
