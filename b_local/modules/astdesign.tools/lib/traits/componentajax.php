<?php

namespace Astdesign\Tools\Traits;

use Astdesign\Tools\Ajax;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Json;

/**
 * Trait ComponentAjax
 * @package Astdesign\Tools\Traits
 */
trait ComponentAjax
{
    protected $_originalParameters;

    /**
     * @var string
     */
    protected $_ajaxTemplateFile;

    public function callAction($action)
    {
        try {
            $methodName = self::actionMethodName($action);
            if (!is_callable(get_class($this), $methodName)) {
                throw new \Exception();
            }

            return $this->{$methodName}();

        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * Получить название метода действия
     *
     * @param string $action
     * @return string
     */
    private static function actionMethodName($action)
    {
        return 'action' . ucfirst(preg_replace('/[^A-Za-z0-9\-]/', '', $action));
    }

    public function executeAjaxComponent()
    {
        $this->generateAjaxParameters();

        $request = Application::getInstance()->getContext()->getRequest();

        if ($request->isAjaxRequest() && $this->checkRightComponent()) {
            $success = $this->callAction($request->getPost('action'));
            $this->sendAjaxJsonAnswer($success);
        }

        return true;
    }

    private function checkRightComponent()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $currentName = $this->getName() . ':' . $this->getTemplateName();

        return $request->getPost('component') === $currentName;
    }

    protected function setOriginalParams($params)
    {
        $this->_originalParameters = $params;
    }

    /**
     * Отправить ответ с сервера в виде json
     *
     * @param bool $success успешность операции
     * @param mixed|bool|false $data Данные для отправки. Если их нет, то отправляется arResult
     */
    public function sendAjaxJsonAnswer($success, $data = false)
    {
        global $APPLICATION;

        $APPLICATION->RestartBuffer();

        if ($this->_ajaxTemplateFile) {
            ob_start();
            $this->includeComponentTemplate($this->_ajaxTemplateFile);
            $templateHtml = ob_get_contents();
            ob_clean();

            if (!$data) {
                $data = [];
            }

            $data['template'] = $templateHtml;
        }

        $response = [
            'success' => $success,
            'data' => $data ?: $this->arResult
        ];

        echo Json::encode($response);
        \CMain::FinalActions();
        die();
    }

    protected function generateAjaxParameters()
    {
        $AAjaxUtil = new Ajax($this->getName(), $this->getTemplateName(), $this->_originalParameters);
        $this->arParams['ajaxParams'] = htmlspecialchars(Json::encode([
            'params' => $AAjaxUtil->getAjaxParams(),
            'component' => $this->getAjaxComponentName()
        ]));
    }

    private function getAjaxComponentName()
    {
        return $this->getName() . ':' . $this->getTemplateName();
    }

    protected function setAjaxTemplateFile($fileName)
    {
        $this->_ajaxTemplateFile = $fileName;
    }
}