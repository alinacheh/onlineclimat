<?php

namespace Astdesign\Tools;

class IBlock
{
    static $iblocks = [];
    static $iblocksToType = [];

    public static function getIblockByCode($code)
    {
        if (empty(self::$iblocks)) {
            \Bitrix\Main\Loader::includeModule('iblock');
            $res = \CIBlock::GetList(['ID' => 'ASC'], ['ACTIVE' => 'Y']);
            while ($r = $res->Fetch()) {
                if (!isset(self::$iblocks[$r['CODE']])) {
                    self::$iblocks[$r['CODE']] = $r['ID'];
                }
            }
        }

        $id = 0;

        if (isset(self::$iblocks[$code])) {
            $id = self::$iblocks[$code];
        }
        return $id;
    }

    public static function getIblockTypeByIblockCode($code)
    {
        if (empty(self::$iblocksToType)) {
            \Bitrix\Main\Loader::includeModule('iblock');
            $res = \CIBlock::GetList(['ID' => 'ASC'], ['ACTIVE' => 'Y']);
            while ($r = $res->Fetch()) {
                if (!isset(self::$iblocksToType[$r['CODE']])) {
                    self::$iblocksToType[$r['CODE']] = $r['IBLOCK_TYPE_ID'];
                }
            }
        }

        $id = "";

        if (isset(self::$iblocksToType[$code])) {
            $id = self::$iblocksToType[$code];
        }
        return $id;
    }

    public static function translateArResult(&$arResult, $forceLang = false)
    {
        $lang = $forceLang ? $forceLang : LANGUAGE_ID;
        if ($lang != 'ru') {
            if (isset($arResult['ITEMS'])) { // is list
                foreach ($arResult['ITEMS'] as $km => &$arItem) {
                    foreach ($arItem['PROPERTIES'] as $k => $v) {
                        if (stripos($k, 'UNILANG_') !== false) {
                            $newKey = str_replace('UNILANG_', '', $k);
                            $arItem['PROPERTIES'][$newKey] = $v;
                            unset($arItem['PROPERTIES'][$k]);
                            continue;
                        }
                        if (stripos($k, $lang . '_') !== false) { // translated field
                            $fieldName = str_ireplace($lang . '_', '', $k);
                            if (is_array($v['VALUE']) && isset($v['VALUE']['TEXT'])) {
                                $v['VALUE'] = $v['VALUE']['TEXT'];
                            }
                            if (isset($arItem[$fieldName]) && !empty($v['VALUE'])) {
                                $arItem[$fieldName] = $v['VALUE'];
                            } elseif (isset($arItem['PROPERTIES'][$fieldName]) && !empty($v['VALUE'])) {
                                $arItem['PROPERTIES'][$fieldName] = $v;
                            } elseif (isset($arItem[$fieldName]) || isset($arItem['PROPERTIES'][$fieldName]) && empty($v['VALUE'])) {
                                unset($arResult['ITEMS'][$km]);
                            }
                        }
                    }
                }
                //translate section fields from user fields
                if (!empty($arResult['SECTION']['PATH'])) {
                    foreach ($arResult['SECTION']['PATH'] as $section) {
                        $arSIds[] = $section['ID'];
                    }
                    \Bitrix\Main\Loader::includeModule("iblock");
                    $res = \CIblockSection::GetList(
                        array('ID' => 'ASC'),
                        array('IBLOCK_ID' => $arResult['ID'], 'ID' => $arSIds),
                        false,
                        array('ID', 'IBLOCK_ID', 'UF_*')
                    );
                    while ($r = $res->Fetch()) {
                        foreach ($r as $k => $v) {
                            if (strpos($k, 'UF_') !== false) {
                                $key = str_replace('UF_' . ToUpper($lang) . '_', '', $k);
                                $arSections[$r['ID']][$key] = $v;
                            }
                        }
                    }
                    foreach ($arResult['SECTION']['PATH'] as &$section) {
                        if (isset($arSections[$section['ID']])) {
                            $section = array_merge($section, $arSections[$section['ID']]);
                        }
                    }
                }
            } else { // is element
                foreach ($arResult['PROPERTIES'] as $k => $v) {
                    if (stripos($k, 'UNILANG_') !== false) {
                        $newKey = str_replace('UNILANG_', '', $k);
                        $arResult['PROPERTIES'][$newKey] = $v;
                        unset($arResult['PROPERTIES'][$k]);
                        continue;
                    }
                    if (stripos($k, $lang . '_') !== false) { // translated field
                        $fieldName = str_ireplace($lang . '_', '', $k);
                        if (isset($arResult[$fieldName]) && !empty($v['VALUE'])) {
                            $arResult[$fieldName] = $v['VALUE'];
                        } elseif (isset($arResult['PROPERTIES'][$fieldName]) && !empty($v['VALUE'])) {
                            $arResult['PROPERTIES'][$fieldName] = $v;
                        } elseif (isset($arResult[$fieldName]) || isset($arResult['PROPERTIES'][$fieldName]) && empty($v['VALUE'])) {
                            $arResult = [];
                        }
                    }
                }
            }
        } else {
            if (isset($arResult['ITEMS'])) {
                foreach ($arResult['ITEMS'] as &$arItem) {
                    foreach ($arItem['PROPERTIES'] as $k => $v) {
                        if (stripos($k, 'UNILANG_') !== false) {
                            $newKey = str_replace('UNILANG_', '', $k);
                            $arItem['PROPERTIES'][$newKey] = $v;
                            unset($arItem['PROPERTIES'][$k]);
                            continue;
                        }
                    }
                }
            } else {
                foreach ($arResult['PROPERTIES'] as $k => $v) {
                    if (stripos($k, 'UNILANG_') !== false) {
                        $newKey = str_replace('UNILANG_', '', $k);
                        $arResult['PROPERTIES'][$newKey] = $v;
                        unset($arResult['PROPERTIES'][$k]);
                        continue;
                    }
                }
            }
        }
    }

    /**
     * @param $bid
     * @return array
     */
    public static function getIBlockMenu($bid, $elements = true)
    {
        \Bitrix\Main\Loader::includeModule('iblock');
        $result = [];
        if ($elements) {
            $res = \CIBlockElement::GetList(
                array('SORT' => 'ASC', 'NAME' => 'ASC'),
                array('SECTION_ID' => false, 'ACTIVE' => 'Y', 'IBLOCK_ID' => $bid),
                false, false, array('ID', 'NAME', 'DETAIL_PAGE_URL')
            );
            while ($r = $res->GetNext()) {
                $result[] = array($r['NAME'], $r["DETAIL_PAGE_URL"], array(), array());
            }
        }
        $res = \CIBlockSection::GetList(
            array('SORT' => 'ASC', 'NAME' => 'ASC'),
            array('SECTION_ID' => false, 'ACTIVE' => 'Y', 'IBLOCK_ID' => $bid),
            false, array('ID', 'NAME', 'SECTION_PAGE_URL')
        );
        while ($r = $res->GetNext()) {
            $result[] = array($r['NAME'], $r["SECTION_PAGE_URL"], array(), array());
        }
        return $result;
    }

    public static $__iblockProps = [];

    /**
     * @param $iblockId
     * @param $code
     * @return mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getPropertyIdByCode($iblockId, $code)
    {
        if (isset(self::$__iblockProps[$iblockId][$code])) {
            return self::$__iblockProps[$iblockId][$code];
        }
        self::$__iblockProps[$iblockId][$code] = false;
        \Bitrix\Main\Loader::includeModule('iblock');
        $res = \CIBlockProperty::GetList(
            [],
            [
                'IBLOCK_ID' => $iblockId,
                'CODE' => $code
            ]
        )->fetch();

        if ($res) {
            if ($res['PROPERTY_TYPE'] == 'L') {
                $res2 = \CIBlockPropertyEnum::GetList(
                    [],
                    ['PROPERTY_ID' => $res['ID']]
                );
                while ($r2 = $res2->fetch()) {
                    $res['VALUES'][$r2['ID']] = $r2['VALUE'];
                }
            }
            self::$__iblockProps[$iblockId][$code] = $res;
        }

        return self::$__iblockProps[$iblockId][$code];
    }
}