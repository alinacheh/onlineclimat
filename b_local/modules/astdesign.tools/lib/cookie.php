<?php

namespace Astdesign\Tools;

use \Bitrix\Main\Application;
use \Bitrix\Main\Web\Cookie as BitrixCookie;

class Cookie
{
    /**
     * @param $name
     * @return mixed|null|string
     */
    public static function get($name)
    {
        return Application::getInstance()->getContext()->getRequest()->getCookie($name);
    }

    public static function set($name, $value)
    {
        $application = Application::getInstance();
        $context = $application->getContext();

        $cookie = new BitrixCookie($name, $value, time() + 60 * 60 * 24 * 60);
        $cookie->setDomain($context->getServer()->getHttpHost());
        $cookie->setHttpOnly(false);

        $context->getResponse()->addCookie($cookie);
        $context->getResponse()->flush("");
    }
}