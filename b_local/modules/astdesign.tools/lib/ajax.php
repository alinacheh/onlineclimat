<?php

namespace Astdesign\Tools;


use Bitrix\Main\Security\Sign\Signer;

class Ajax
{
    private $_data;

    public function __construct($componentName, $templateName, $params)
    {
        $this->_data = [
            'templateName' => $templateName,
            'params' => $params,
            'componentName' => $componentName,
        ];
    }

    public function getAjaxParams()
    {
        return self::encode($this->_data);
    }

    public static function encode($data, $salt = null)
    {
        $signer = new Signer();
        return $signer->sign(base64_encode(serialize($data)), $salt);
    }
}