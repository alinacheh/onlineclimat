<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (class_exists('astdesign_tools')) {
    return;
}

Class astdesign_tools extends CModule
{

    private static $MODULE_POSITION = 'local';
    private $moduleAdminFilesPath;

    public function __construct()
    {
        $this->MODULE_ID = 'astdesign.tools';
        $this->MODULE_GROUP_RIGHTS = 'N';


        $arModuleVersion = array();

        $path = str_replace("\\", '/', __FILE__);
        $path = substr($path, 0, strlen($path) - strlen('/index.php'));
        include($path . '/' . 'version.php');

        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

        $this->MODULE_NAME = 'Инструменты ASTDESIGN';
        $this->MODULE_DESCRIPTION = '';

        $this->moduleAdminFilesPath = $_SERVER['DOCUMENT_ROOT'] . '/' . self::$MODULE_POSITION . '/modules/' . $this->MODULE_ID . '/admin';
    }

    function DoInstall()
    {
        $this->InstallFiles();
        $this->InstallDB();
        RegisterModule($this->MODULE_ID);
        $this->InstallEvents();
    }

    function InstallFiles($arParams = array())
    {

        if (is_dir($this->moduleAdminFilesPath)) {
            $dir = opendir($this->moduleAdminFilesPath);
            if ($dir) {
                while (false !== $item = readdir($dir)) {
                    if ($item === '..' || $item === '.' || $item === 'menu.php') {
                        continue;
                    }
                    file_put_contents($file = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $this->MODULE_ID . '_' . $item,
                        '<' . '? require($_SERVER["DOCUMENT_ROOT"]."/' . self::$MODULE_POSITION . '/modules/' . $this->MODULE_ID . '/admin/' . $item . '");?' . '>');
                }
                closedir($dir);
            }
        }

        return true;
    }

    function InstallEvents()
    {
        return true;
    }

    function DoUninstall()
    {
        $this->UnInstallEvents();
        UnRegisterModule($this->MODULE_ID);
        $this->UnInstallDB();
        $this->UnInstallFiles();
    }

    function UnInstallEvents()
    {
        return true;
    }

    function UnInstallFiles()
    {

        if (is_dir($this->moduleAdminFilesPath)) {
            $dir = opendir($this->moduleAdminFilesPath);
            if ($dir) {
                while (false !== $item = readdir($dir)) {
                    if ($item === '..' || $item === '.') {
                        continue;
                    }
                    unlink($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $this->MODULE_ID . '_' . $item);
                }
                closedir($dir);
            }
        }

        return true;
    }
}