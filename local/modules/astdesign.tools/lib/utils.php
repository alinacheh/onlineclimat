<?php

namespace Astdesign\Tools;

class Utils
{
    /**
     * Переделывает битриксовский линейный массив в дерево
     * @param $plain
     * @param string $parentProperty
     * @param int $rootId
     * @return mixed
     */
    public static function plain2Tree($plain, $parentProperty = 'DEPTH_LEVEL', $rootId = 0)
    {
        $tree = array($rootId => array('SUB' => array()));
        $isDepthLevel = ($parentProperty == 'DEPTH_LEVEL');
        $curDepths = array($rootId);
        foreach ($plain as $id => &$item) {
            $item['SUB'] = array();
            $pid = $item[$parentProperty];
            if ($isDepthLevel) {
                $id = 'k' . $id; //prevent collision
                $curDepths[$pid] = $id;
                $pid = $pid > 0 ? $curDepths[$pid - 1] : $curDepths[0];
            } else {
                if ($item['ID']) {
                    $id = $item['ID'];
                }
                if (!$pid) {
                    $pid = $rootId;
                }
            }
            $tree[$pid]['SUB'][$id] = &$item;
            $tree[$id] = &$tree[$pid]['SUB'][$id];
        }
        unset($item);

        return $tree[$rootId]['SUB'];
    }

    public static function isAjaxRequest()
    {
        return (strtolower(filter_input(INPUT_SERVER,
                'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest' || isset($_REQUEST['ajax']));
    }

    public static function isMainPage()
    {
        global $APPLICATION;
        return $APPLICATION->GetCurPage(true) == SITE_DIR . 'index.php';
    }

    /**
     * проверяет, является ли посетитель поисковым роботом
     * @return bool
     */
    public static function isBot()
    {
        return preg_match(
            "~(Google|Yahoo|Rambler|Bot|Yandex|Spider|Snoopy|Crawler|Finder|Mail|curl)~i",
            $_SERVER['HTTP_USER_AGENT']
        );
    }

    /**
     * форматирует строку телефона для размещения в ссылке вида <a href="tel:xxx"></a>
     * @param string $phoneString
     * @return mixed|string
     */
    public static function getRawPhone($phoneString = "")
    {
        if ($phoneString) {
            $phoneString = strip_tags($phoneString);
            $phoneString = preg_replace('/[^0-9]/', '', $phoneString);
            $phoneString = trim($phoneString);
            if ((int)substr($phoneString, 0, 1) === 8 && stripos($phoneString, "8800") === false) {
                $phoneString = substr_replace($phoneString, "+7", 0, 1);
            }
        }
        return $phoneString;
    }

    /**
     * создаёт подключаемый файл или подключает его на странице, если он уже есть
     * повторяет вложенность физических разделов в инклудах шаблона
     * @param string $fileName
     * @param bool $hermitage
     * @param bool $lang
     */
    public static function includePhpFile($fileName = "", $hermitage = false, $forceDir = false, $lang = false)
    {
        if (!$fileName) {
            return;
        }
        global $APPLICATION;
        if ($forceDir !== false) {
            $dir = $forceDir;
        } else {
            $dir = dirname($_SERVER["PHP_SELF"]);
        }
        $dir = str_replace("\\", "/", $dir);
        $folder = "";
        if ($dir === SITE_DIR) {
            $dir = "index";
        }

        if ($lang && LANGUAGE_ID !== "ru") {
            $dir .= "/" . LANGUAGE_ID;
        }


        $arDirs = explode("/", $dir);
        $arDirs = array_filter($arDirs, function ($value) {
            return trim($value) !== "";
        });

        foreach ($arDirs as $dir) {
            if (!$folder) {
                $folder = $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/includes/" . $dir;
            } else {
                $folder .= "/" . $dir;
            }
            if (!is_dir($folder)) {
                mkdir($folder);
            }
        }
        $file = $folder . "/" . $fileName . ".php";
        $file = str_replace("//", "/", $file);
        if (!file_exists($file)) {
            $f = fopen($file, "w");
            fwrite($f, "");
            fclose($f);
        }

        if (!$hermitage) {
            include $file;
        } else {
            $file = str_replace($_SERVER['DOCUMENT_ROOT'], '', $file);
            $GLOBALS['APPLICATION']->IncludeFile($file);
        }
    }

    /**
     * 301 redirect
     * @param $url
     */
    public static function redirect($url)
    {
        LocalRedirect($url, false, "301 Moved permanently");
    }

    /**
     * @param string $message
     * @return string
     */
    public static function getLangValue($message = "")
    {
        return \Bitrix\Main\Localization\Loc::getMessage($message);
    }

    /**
     * @param $string
     * @param int $length
     * @return string
     */
    public static function smart_cropping($string, $length = 300)
    {
        if ((int)$length <= 0) {
            $length = 100;
        }

        $string_tmp = mb_substr($string, 0, $length);
        if (mb_strrpos($string_tmp, '.') !== false) {
            $string_tmp = mb_substr($string_tmp, 0, mb_strrpos($string_tmp, '.')) . '.';
        }

        if (empty($string_tmp)) {
            $string_tmp = $string;
        }

        return $string_tmp;
    }

    /**
     * @param string $date
     * @return string
     */
    public static function formatDate($date = "")
    {
        if ($date) {
            return ToLower(FormatDate("d.m.Y", MakeTimeStamp($date)));
        }
        return $date;
    }

    public static function getPreviewText($arItem, $length = 0, $dots = true)
    {
        if ($length) {
            $arItem['PREVIEW_TEXT'] = trim(strip_tags($arItem['PREVIEW_TEXT']));
            if (!$arItem['PREVIEW_TEXT']) {
                $arItem['~PREVIEW_TEXT'] = $arItem['DETAIL_TEXT'];
            }
            $arItem['PREVIEW_TEXT'] = trim(strip_tags($arItem['~PREVIEW_TEXT']));
            $arItem['PREVIEW_TEXT'] = html_entity_decode($arItem['PREVIEW_TEXT']);
            $arItem['PREVIEW_TEXT'] = urldecode($arItem['PREVIEW_TEXT']);
            $arItem['PREVIEW_TEXT'] = preg_replace('/[\t\s]+/', ' ', $arItem['PREVIEW_TEXT']);
            $arItem['PREVIEW_TEXT'] = trim($arItem['PREVIEW_TEXT'], " \t\n\r\0\x0B\xC2\xA0");
            $arItem['PREVIEW_TEXT'] = preg_replace('/ +/', ' ', $arItem['PREVIEW_TEXT']);
            $arItem['PREVIEW_TEXT'] = trim($arItem['PREVIEW_TEXT']);
            $arItem['PREVIEW_TEXT'] = self::smart_cropping(
                $arItem['PREVIEW_TEXT'],
                $length
            );
            if ($dots && $arItem['PREVIEW_TEXT']) {
                $arItem['PREVIEW_TEXT'] .= "...";
            }
        }

        return $arItem['PREVIEW_TEXT'];
    }

    public static function translit($str)
    {
        $params = [
            "max_len" => "100",
            "change_case" => "L",
            "replace_space" => "-",
            "replace_other" => "_",
            "delete_repeat_replace" => "true"
        ];
        $str = \CUtil::translit($str, "ru", $params);
        return $str;
    }

    /**
     * @return array
     */
    public static function getCatalogSectionBaseParams()
    {
        static $componentParams;
        if ($componentParams === null) {
            $iblockType = \Astdesign\Tools\IBlock::getIblockTypeByIblockCode("catalog");
            $iblockId = \Astdesign\Tools\IBlock::getIblockByCode("catalog");
            $componentParams = [
                "IBLOCK_TYPE" => $iblockType,
                "IBLOCK_ID" => $iblockId,
                "SECTION_ID" => "",
                "SECTION_CODE" => "",
                "SECTION_USER_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "ELEMENT_SORT_FIELD" => "sort",
                "ELEMENT_SORT_ORDER" => "asc",
                "ELEMENT_SORT_FIELD2" => "id",
                "ELEMENT_SORT_ORDER2" => "desc",
                "FILTER_NAME" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "SHOW_ALL_WO_SECTION" => "Y",
                "HIDE_NOT_AVAILABLE" => "N",
                "PAGE_ELEMENT_COUNT" => "30",
                "LINE_ELEMENT_COUNT" => "3",
                "PROPERTY_CODE" => array(
                    0 => "SALE",
                    1 => "COMMENTS_COUNT",
                    2 => "RAITING_COUNT",
                    3 => "RAITING",
                    4 => "HIT",
                    5 => "",
                ),
                "OFFERS_LIMIT" => "5",
                "SECTION_URL" => "",
                "DETAIL_URL" => "",
                "BASKET_URL" => "/personal/basket.php",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_GROUPS" => "N",
                "META_KEYWORDS" => "-",
                "META_DESCRIPTION" => "-",
                "BROWSER_TITLE" => "",
                "ADD_SECTIONS_CHAIN" => "N",
                "DISPLAY_COMPARE" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "CACHE_FILTER" => "Y",
                "PRICE_CODE" => array(
                    0 => "Розничная",
                    1 => "FULL",
                ),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "PRODUCT_PROPERTIES" => array(
                    0 => "",
                    1 => "",
                ),
                "USE_PRODUCT_QUANTITY" => "N",
                "CONVERT_CURRENCY" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_ADDITIONAL" => ""
            ];
        }
        return $componentParams;
    }

    /**
     * @param $price
     * @return string
     */
    public static function formatPrice($price, $symbol = "₽")
    {
        $string = number_format($price, 0, ',', ' ');
        if ($symbol) {
            return $string . " " . $symbol;
        }
        return $string;
    }

    /**
     * Convert xml to array
     * @param $xml
     * @param array $options
     * @return array
     */
    public static function xmlToArray($xml, $options = array())
    {
        $defaults = array(
            'namespaceSeparator' => ':',//you may want this to be something other than a colon
            'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
            'alwaysArray' => array(),   //array of xml tag names which should always become arrays
            'autoArray' => true,        //only create arrays for tags which appear more than once
            'textContent' => '$',       //key used for the text content of elements
            'autoText' => true,         //skip textContent key if node has no attributes or child nodes
            'keySearch' => false,       //optional search and replace on tag and attribute names
            'keyReplace' => false       //replace values for above search values (as passed to str_replace())
        );
        $options = array_merge($defaults, $options);
        $namespaces = $xml->getDocNamespaces();
        $namespaces[''] = null; //add base (empty) namespace

        //get attributes from all namespaces
        $attributesArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
                //replace characters in attribute name
                if ($options['keySearch']) {
                    $attributeName =
                        str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
                }
                $attributeKey = $options['attributePrefix']
                    . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                    . $attributeName;
                $attributesArray[$attributeKey] = (string)$attribute;
            }
        }

        //get child nodes from all namespaces
        $tagsArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->children($namespace) as $childXml) {
                //recurse into child nodes
                $childArray = self::xmlToArray($childXml, $options);
                list($childTagName, $childProperties) = each($childArray);

                //replace characters in tag name
                if ($options['keySearch']) {
                    $childTagName =
                        str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
                }
                //add namespace prefix, if any
                if ($prefix) {
                    $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;
                }

                if (!isset($tagsArray[$childTagName])) {
                    //only entry with this key
                    //test if tags of this type should always be arrays, no matter the element count
                    $tagsArray[$childTagName] =
                        in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
                            ? array($childProperties) : $childProperties;
                } elseif (
                    is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
                    === range(0, count($tagsArray[$childTagName]) - 1)
                ) {
                    //key already exists and is integer indexed array
                    $tagsArray[$childTagName][] = $childProperties;
                } else {
                    //key exists so convert to integer indexed array with previous value in position 0
                    $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
                }
            }
        }

        //get text content of node
        $textContentArray = array();
        $plainText = trim((string)$xml);
        if ($plainText !== '') {
            $textContentArray[$options['textContent']] = $plainText;
        }

        //stick it all together
        $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
            ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

        //return node as array
        return array(
            $xml->getName() => $propertiesArray
        );
    }
}