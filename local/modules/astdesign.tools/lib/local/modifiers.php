<?php

namespace Astdesign\Tools\Local;

/**
 * Class Modifiers
 * @package Astdesign\Tools\Local
 */
class Modifiers
{
    /**
     * @param array $elements
     * @param int $parentId
     * @return array
     */
    public static function createTree(array &$elements, $parentId = 0)
    {
        $branch = array();
        $tmp = [];
        foreach ($elements as $element) {
            $tmp[$element['ID']] = $element;
        }
        $elements = $tmp;

        foreach ($elements as $element) {
            if ((int)$element['IBLOCK_SECTION_ID'] == (int)$parentId) {
                $children = self::createTree($elements, $element['ID']);
                if ($children) {
                    $element['SUB'] = $children;
                }
                $branch[$element['ID']] = $element;
                unset($elements[$element['ID']]);
            }
        }
        return $branch;
    }

    /**
     * @array $arResult
     * @return void
     */
    public static function getSectionsPictures(&$arSections)
    {
        \Bitrix\Main\Loader::includeModule('iblock');
        foreach ($arSections as &$section) {
            if (!$section['PICTURE']) {
                $res = \CIblockElement::GetList(
                    ['RAND' => 'RAND'],
                    [
                        'IBLOCK_ID' => $section['IBLOCK_ID'],
                        '!DETAIL_PICTURE' => false,
                        'ACTIVE' => 'Y',
                        'SECTION_ID' => $section['ID'],
                        'INCLUDE_SUBSECTIONS' => 'Y'
                    ],
                    false,
                    false,
                    ['ID', 'DETAIL_PICTURE']
                )->fetch();
                if ($res) {
                    $section['PICTURE'] = $res['DETAIL_PICTURE'];
                }
            }
            $section['PICTURE_SRC'] = \Astdesign\Tools\File::getResizeSrc(
                $section['PICTURE'],
                200,
                200
            );
        }
    }
}