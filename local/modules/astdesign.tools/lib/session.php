<?php

namespace Astdesign\Tools;

class Session
{
    public static function getSessionData($key, $group = false)
    {
        if ($group) {
            return $_SESSION[$group][$key];
        } else {
            return $_SESSION[$key];
        }
    }

    public static function setSessionData($key, $value, $group = false)
    {
        if ($group) {
            $_SESSION[$group][$key] = $value;
        } else {
            $_SESSION[$key] = $value;
        }
    }
}