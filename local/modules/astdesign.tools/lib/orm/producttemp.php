<?php

namespace Astdesign\Tools\Orm;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\DataManager;

/**
 * Class ProductTempTable
 * @package Astdesign\Tools\Orm
 */
class ProductTempTable extends DataManager
{

    public static function getTableName()
    {
        return 'ast_catalogproductstemp';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('id', array(
                'primary' => true,
                'autocomplete' => true
            )),
            new Entity\IntegerField('productId', array(
                'required' => true,
            ))
        );
    }
}