<?php

namespace Astdesign\Tools;


class File
{
    const NO_PHOTO_PATH = '/local/modules/astdesign.tools/assets/images/no-image.jpg';

    private static function getDestinationPath($siteId, $width, $height)
    {
        return '/upload/no_photo_cache/' . $siteId . '/' . $width . '-' . $height . '.jpg';
    }

    /**
     * Получить заглущку фото
     *
     * @param int $width
     * @param int $height
     * @param int $method
     * @return string
     */
    public static function getNoPhoto($width, $height, $method = BX_RESIZE_IMAGE_PROPORTIONAL)
    {
        $source = $_SERVER["DOCUMENT_ROOT"] . self::NO_PHOTO_PATH;
        $destinationPath = self::getDestinationPath(SITE_ID, $width, $height);
        $data = array('src' => $destinationPath);
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $destinationPath)) {
            \CFile::ResizeImageFile(
                $source,
                $destinationFile = $_SERVER["DOCUMENT_ROOT"] . $destinationPath,
                array('width' => $width, 'height' => $height),
                $method
            );
        }

        return $data['src'];
    }

    /**
     * Отресайзить изображение. Если Изображения нет, то вернуть заглушку
     *
     * @param int|array $file
     * @param int $width
     * @param int|bool|false $height
     * @param int $method
     * @return array
     */
    public static function resizeImage(&$file, $width, $height = false, $method = BX_RESIZE_IMAGE_PROPORTIONAL)
    {
        $extend = false;
        if (!$height) {
            $height = $width;
        }
        if ($file && !is_array($file)) {
            $file = \CFile::GetFileArray($file);
        }
        if ($file) {
            $file['src'] = $file['SRC'];
            if ($width > 0) {
                if ($method == 'EXTRA') {
                    $method = BX_RESIZE_IMAGE_PROPORTIONAL;
                    $extend = true;
                }
                //prevent original image return and rewriting PROPORTIONAL method result with dummy filter
                $arFilters = $extend ? [["name" => "dummy"]] : false;
                $data = \CFile::ResizeImageGet(
                    $file,
                    ['width' => $width, 'height' => $height],
                    $method,
                    true,
                    $arFilters);
                $w2 = $data['width'];
                $h2 = $data['height'];
                if ($extend && $width < 900 && ($w2 < $width || $h2 < $height)) {
                    $_image = $_SERVER['DOCUMENT_ROOT'] . $data['src'];
                    $func = false;
                    $mime = mime_content_type($_image);
                    switch ($mime) {
                        case 'image/jpeg':
                            $func = "jpeg";
                            break;
                        case 'image/gif':
                            $func = "gif";
                            break;
                        case 'image/png':
                            $func = "png";
                            break;
                        default:
                            break;
                    }
                    $function1 = "imagecreatefrom" . $func;
                    $function2 = "image" . $func;
                    if ($func && is_callable($function1) && is_callable($function2)) {
                        $data_image = $function1($_image);
                        $w2 = imagesx($data_image);
                        $h2 = imagesy($data_image);
                        if ($w2 < $width || $h2 < $height) {
                            $result_image = imagecreatetruecolor($width, $height);
                            $white = imagecolorallocate($result_image, 255, 255, 255);
                            imagefill($result_image, 0, 0, $white);
                            imagecopy(
                                $result_image, $data_image,
                                intval(($width - $w2) / 2), intval(($height - $h2) / 2), //in place
                                0, 0, $w2, $h2
                            );
                            switch ($func) {
                                case 'jpeg':
                                    $function2($result_image, $_image, 98);
                                    break;
                                case 'png':
                                    $function2($result_image, $_image, 3);
                                    break;
                                case 'gif':
                                    $function2($result_image, $_image);
                                    break;
                            }
                            imagedestroy($result_image);
                            unset($result_image);
                        }
                        imagedestroy($data_image);
                        unset($data_image);
                    }
                }
            } else {
                $data = $file;
            }

            $data['SRC'] = $data['src'];
        } else {
            $data['SRC'] = self::getNoPhoto($width, $height, $method);
        }

        return $data;
    }

    /**
     * Получить ссылку на отресайзенное изображение
     *
     * @param int|array $file
     * @param int $width
     * @param int|bool|false $height
     * @param int $method
     * @return string
     */
    public static function getResizeSrc(&$file, $width, $height = false, $method = BX_RESIZE_IMAGE_PROPORTIONAL)
    {
        $data = self::resizeImage($file, $width, $height, $method);
        return $data['SRC'];
    }

    /**
     * Получить человекопонятный размер файла
     *
     * @param int $size размер файла (байты)
     * @param int $decimals количество дробных знаков после запятой
     * @return string
     */
    public static function getFileSizeString($size, $decimals = 2)
    {
        $units = array('Б', 'КБ', 'МБ', 'ГБ', 'ТБ');
        $power = $size > 0 ? floor(log($size, 1024)) : 0;
        return number_format($size / pow(1024, $power), $decimals, '.', ' ') . ' ' . $units[$power];
    }

    /**
     * удаляет папку рекурсивно
     * @param $dir
     */
    public static function removeDir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . "/" . $object)) {
                        self::removeDir($dir . "/" . $object);
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            rmdir($dir);
        }
    }
}