<?php

namespace Astdesign\Tools;

class Statistics
{
    /**
     * возвращает информацию о текущем посещении пользователя: откуда пришёл, геолокация, страница, с которой идёт отправка формы
     */
    public static function getUserStatInfo()
    {
        $ip = \Bitrix\Main\Service\GeoIp\Manager::getRealIp();
        $geoData = \Bitrix\Main\Service\GeoIp\Manager::getDataResult($ip, LANGUAGE_ID)->getGeoData();
        $commonMessageNames = [
            'IP' => 'IP адрес',
            'CITY' => 'Город',
            'REGION' => 'Регион',
            'COUNTRY' => 'Страна',
            'PAGE' => 'Страница, с которой отправлено сообщение',
            'UTM' => 'UTM метки',
            'HTTP_REFERER' => 'Внешний сайт, с которого пришли'
        ];
        $utmLang = [
            'utm_source' => 'Источник кампании',
            'utm_medium' => 'Канал кампании',
            'utm_campaign' => 'Название кампании',
            'utm_term' => 'Ключевое слово',
            'utm_content' => 'Содержание кампании'
        ];
        $commonData = [
            'IP' => $ip,
            'CITY' => $geoData->cityName,
            'REGION' => $geoData->regionName,
            'COUNTRY' => $geoData->countryName,
            'PAGE' => $_SERVER['HTTP_REFERER'],
            'UTM' => \Astdesign\Tools\Session::getSessionData('UTM'),
            'HTTP_REFERER' => \Astdesign\Tools\Session::getSessionData('HTTP_REFERER')
        ];

        if (stripos($commonData["HTTP_REFERER"], "yandex.ru") !== false) {
            $commonData["HTTP_REFERER"] = "https://www.yandex.ru/";
        }

        $commonText = "";
        foreach ($commonData as $k => $v) {
            if (!$v || is_null($v)) {
                continue;
            }
            if (!is_array($v)) {
                $commonText .= $commonMessageNames[$k] . ": " . $v . "\r\n";
            } else {
                $vtmp = "";
                foreach ($v as $k2 => $v2) {
                    if (isset($utmLang[$k2])) {
                        $k2 = $utmLang[$k2];
                    }
                    $vtmp .= "\r\n" . $k2 . ": " . $v2;
                }
                $commonText .= $commonMessageNames[$k] . ": " . $vtmp . "\r\n";
            }
        }
        return ['JSON' => json_encode($commonData), 'RAW' => $commonData, 'TEXT' => $commonText];
    }
}