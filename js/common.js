$(function() {
	if (!$('.agreement input[type=checkbox]').is(':checked')){
		$('.agreement').parent().find('.btn_a button').prop('disabled', true);
	}
	
	$('#callMe .agreement input[type=checkbox], #qorder_form .agreement input[type=checkbox]').click(function(){
		if ($(this).prop("checked")){
			$(this).parent().parent().find('.btn_a button').prop('disabled', false);
		}else{
			$(this).parent().parent().find('.btn_a button').prop('disabled', true);
		}
	});
	
	$('.contact-form .agreement input[type=checkbox]').click(function(){
		if ($(this).prop("checked")){
			$(this).parent().parent().parent().find('input[type=submit]').prop('disabled', false);
		}else{
			$(this).parent().parent().parent().find('input[type=submit]').prop('disabled', true);
		}
	});
});
