<?php $_SERVER["DOCUMENT_ROOT"] = '/var/www/on-lineclimat.ru/data/www/on-lineclimat.ru'; ?>
<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	
	@set_time_limit(0);
	@ignore_user_abort(true);

	$bitrix_storage = $_SERVER['DOCUMENT_ROOT'] . "/upload/";
	$pdf_storage = $_SERVER['DOCUMENT_ROOT'] . "/pdf_files/";

	$connection = Bitrix\Main\Application::getConnection();
	$sqlHelper = $connection->getSqlHelper();

	$arFilesCache = array();
	$result = $connection->query('SELECT * FROM b_file WHERE MODULE_ID = "iblock" AND CONTENT_TYPE = "application/pdf"');
	while ($row = $result->Fetch()) {
	    $arFilesCache[$row['FILE_NAME']] = $bitrix_storage.$row['SUBDIR'].'/'.$row['FILE_NAME'];
	}

	foreach ($arFilesCache as $filename => $full_file_path) {
		if(!file_exists($full_file_path) || is_link($full_file_path)) continue;

		$checksum = sha1_file($full_file_path);

		$sql = 'SELECT * FROM pdf_files WHERE checksum = "'.$sqlHelper->forSql($checksum).'"';
		$result = $connection->query($sql);

		$file = $result->Fetch();
		if(!$file) {
			$sql = 'INSERT INTO pdf_files (checksum, path) VALUES("'.$sqlHelper->forSql($checksum).'", "'.$sqlHelper->forSql($filename).'");';
			$connection->query($sql);

			copy($full_file_path, $pdf_storage.$filename);
			$file['path'] = $filename;
		}
var_dump($full_file_path);
		unlink($full_file_path);
		symlink($pdf_storage.$file['path'], $full_file_path);
	}
		