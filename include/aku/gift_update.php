<?php $_SERVER["DOCUMENT_ROOT"] = '/var/www/on-lineclimat.ru/data/www/on-lineclimat.ru'; ?>
<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	
	$products_limit_for_session = 200;
	$memory_start = memory_get_usage();
	$time_start = time();

	$data_file = __DIR__.'/last_product_id.data';
	$last_product_id = (int)@file_get_contents($data_file);


	@set_time_limit(0);
	@ignore_user_abort(true);

    CModule::IncludeModule("iblock");
    CModule::IncludeModule("sale");

    do {
	    $products = \CIBlockElement::GetList(
	        array("ID"=>"ASC"),
	        array("IBLOCK_ID" => 1, ">ID" => $last_product_id),
	        false,
	        false,
	        array('ID')
	    );

	    $memory = array();
	    $product_ids = array();

		$product = $products->Fetch();

		do {
			$product_ids[] = (int)$product["ID"];
			$products_limit_for_session--;

			$product = $products->Fetch();
		} while($product && $products_limit_for_session);

		if($products_limit_for_session) {
			$last_product_id = 0;
		}

	} while($products_limit_for_session);

    foreach ($product_ids as $index => $product_id) {
    	$collection = get_product_gifts_from_no_cache($product_id);

    	if($collection) {
    		$ids = array();

		    foreach ($collection as $gifts) {
	            foreach ($gifts as $gift) {
	                $ids[] = $gift->getProductId();
	            }
    		}     

	        $present = implode(',',$ids);
    	} else {
    		$present = '';
    	}
 

        CIBlockElement::SetPropertyValuesEx(
            $product_id, 
            1, 
            array('IS_PRESENT' => $present)
        );	

    	if(count($product_ids) == ($index+1)) {
    		file_put_contents($data_file, $product_id);
    	}
    }

    $time_end = time();
    $memory_end = memory_get_usage();

    echo 'Memory usage: '.formatBytes($memory_end-$memory_start).PHP_EOL;
    echo 'Time usage: '.formatBytes($time_end-$time_start).'sec'.PHP_EOL;
    echo 'Last ID: '.$product_id.PHP_EOL;

	function formatBytes($size, $precision = 2)	{
	    $base = log($size, 1024);
	    $suffixes = array('', 'K', 'M', 'G', 'T');   

	    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
	}


?>