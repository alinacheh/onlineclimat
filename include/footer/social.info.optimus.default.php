<?$APPLICATION->IncludeComponent(
	"aspro:social.info.optimus", 
	".default", 
	array(
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "3600000",
		"CACHE_TYPE" => "Y",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"FACE" => "",
		"GOOGLE_PLUS" => "",
		"INST" => "https://www.instagram.com/onlineclimat/",
		"MAIL" => "",
		"ODN" => "",
		"ORDER_SOC" => "inst,vk,odn,fb,tw,mail,youtube,google_plus,telegram,viber,whatsapp,skype",
		"SKYPE" => "",
		"TELEGRAM" => "",
		"TITLE_BLOCK" => "Мы в социальных сетях:",
		"TWIT" => "",
		"VIBER" => "viber://add?number=+79037203651",
		"VK" => "",
		"WHATSAPP" => "whatsapp://send?phone=+79037203651",
		"YOUTUBE" => "https://www.youtube.com/channel/UCcYDVTO2PXDjdO_95PadURQ",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>