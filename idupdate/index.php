<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
 
set_time_limit(0); //убираем ограничение на время выполнения скрипта

use Bitrix\Iblock\InheritedProperty; 
 
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

$cnt=0;
$sections = CIBlockSection::GetList(
    array(),
    array("IBLOCK_ID" => 1)
);

while ($section = $sections->Fetch()) {
	$ipropTemplates = new InheritedProperty\SectionTemplates($section['IBLOCK_ID'],$section['ID']);

	$ipropTemplates->set(array(
	         "ELEMENT_META_TITLE" => "",
	         "ELEMENT_META_DESCRIPTION" => "",
	         "ELEMENT_META_KEYWORDS" => ""
	));

    $cnt++;
}

var_dump($cnt);

/*
$els = CIBlockElement::GetList(
    array("SORT"=>"ASC"),
    array("IBLOCK_ID" => 1),
    false,
    false,
    array('ID','IBLOCK_ID', 'PROPERTY_PR_ID_COPY')
);

$s = $els->Fetch();
	$arTmp = CIBlockElement::GetProperty($s['IBLOCK_ID'], $s['ID'], array("sort" => "asc"), Array("CODE"=>'PR_ID_COPY'))->Fetch();
	var_dump($arTmp);


$cnt=0;

while ($s = $els->Fetch()) {
	$ok = CIBlockElement::SetPropertyValuesEx($s['ID'], $s['IBLOCK_ID'], array("PR_ID_COPY" => $s['ID']));

    $cnt++;
}
echo $cnt;
*/ 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>