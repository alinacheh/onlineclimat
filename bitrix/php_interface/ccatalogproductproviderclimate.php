<?
    CModule::IncludeModule("catalog");
    if(class_exists("CCatalogProductProvider")){
        class CCatalogProductProviderClimate extends CCatalogProductProvider{
            public static function GetProductData($arParams)
            {
                $ar = CCatalogProductProvider::GetProductData($arParams);
                $p = Refs::addInstallPrice($ar, $arParams["PRODUCT_ID"]);
                if($p > 0) $ar["PRICE"] = $p;
                return $ar;
            }

            public static function OrderProduct($arParams)
            {
                $arResult = CCatalogProductProvider::OrderProduct($arParams);
                $p = Refs::addInstallPrice($arResult, $arParams["PRODUCT_ID"]);
                if($p > 0) $arResult["PRICE"] = $p;
                return $arResult;
            }
        }
    }
?>