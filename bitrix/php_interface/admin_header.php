<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<?if ($_SERVER['SCRIPT_NAME'] == '/bitrix/admin/sale_order_detail.php'):
	CModule::IncludeModule('iblock');
	$installs = array();
	$res = CIBlockElement::GetList(array('ID'=>'ASC'), array('IBLOCK_CODE'=>'installs'), false, false, array('ID','NAME','PROPERTY_PRICE'));
	while ($r = $res->Fetch()) $installs[$r['ID']] = array(
		'NAME' => $r['NAME'],
		'PRICE' => $r['PROPERTY_PRICE_VALUE']
	);
	$rsSites = CSite::GetList($by="id", $order="asc");
	$site_dir = '/';
	while ($r = $rsSites->Fetch()) {
		$solution_installed = (COption::GetOptionString("climate", "wizard_installed", "N", $r['LID']) == 'Y');
		if ($solution_installed) {
			$site_dir = $r['DIR'];
			break;
		}
	}
	include $_SERVER['DOCUMENT_ROOT'] . $site_dir . 'ajax/getString.php';?>
	<script>
		var installs = <?=json_encode($installs)?>;
		$(function(){
			$('td.props').each(function(){
				var t = $(this);
				t.html(t.html().replace(/(<?=$name?>: )(\S+)/gi, function(src, n1, n2, offset, s){
					var result = ''; n2 = parseInt(n2);
					if (!n2) result = '<?=$no?>';
					else result = '<br/>' + installs[n2]['NAME'] + ' (' + installs[n2]['PRICE'] + ' <?=$rub?>.)';
					return n1 + result;
				}));
			})
		})
	</script>
<?endif?>
