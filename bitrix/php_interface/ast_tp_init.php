<?php
    function ast_autoload($classname){
        if(strstr($classname,"Climate")){
            include_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/".strtolower($classname).".php";
        }
    }
    spl_autoload_register('ast_autoload');

    if (!function_exists('mime_content_type')) {
		/* http://php.net/manual/ru/function.mime-content-type.php */
		function mime_content_type($filename) {
			$mime_types = array(
				// images
				'png' => 'image/png',
				'jpe' => 'image/jpeg',
				'jpeg' => 'image/jpeg',
				'jpg' => 'image/jpeg',
				'gif' => 'image/gif',
				'bmp' => 'image/bmp',
				'ico' => 'image/vnd.microsoft.icon',
				'tiff' => 'image/tiff',
				'tif' => 'image/tiff',
				'svg' => 'image/svg+xml',
				'svgz' => 'image/svg+xml',
			);
			$ext = strtolower(array_pop(explode('.', $filename)));
			if (array_key_exists($ext, $mime_types)) return $mime_types[$ext];
			elseif (function_exists('finfo_open')) {
				$finfo = finfo_open(FILEINFO_MIME);
				$mimetype = finfo_file($finfo, $filename);
				finfo_close($finfo);
				return $mimetype;
			}
			else return 'application/octet-stream';
		}
	}
	class Refs {
		const MODULE_ID = 'astdesign.climate';
		private static $cache = array();
		private static function getProperty($p) {
			if (!isset(self::$cache['PROPERTY'][$p])) {
				self::$cache['PROPERTY'][$p] = $GLOBALS['APPLICATION']->GetPageProperty($p);
			}
			return self::$cache['PROPERTY'][$p];
		}
		private static function getNoPhoto($w, $h, $method) {
			$source = SITE_DIR.'img/no_photo.png';
			$dest = SITE_DIR.'img/no_photo_'.$w.'_'.$h.'.png';
			$data = array('src'=>$dest);
			if (!file_exists($_SERVER['DOCUMENT_ROOT'].$dest)) {
				CFile::ResizeImageFile(
					$sourceFile = $_SERVER["DOCUMENT_ROOT"].$source,
					$destinationFile = $_SERVER["DOCUMENT_ROOT"].$dest,
					array('width'=>$w, 'height'=>$h),
					$method
				);
			}
			return $data;
		}
		public static function resize_image(&$file, $w, $h=false, $method=BX_RESIZE_IMAGE_PROPORTIONAL) {
			if (!$h) $h=$w;
			if (self::getProperty('ResizeImageExact')) $method=BX_RESIZE_IMAGE_EXACT;
			if ($file && !is_array($file)) $file = CFile::GetFileArray($file);
			if ($file) {
				$file['src'] = $file['SRC'];
				if ($w > 0) {
					$data = CFile::ResizeImageGet($file, array('width'=>$w, 'height'=>$h), $method, true);
					$w2 = $data['width']; $h2 = $data['height'];
					if ($w<900 && ($w2<$w || $h2<$h)) {
						$_image = $_SERVER['DOCUMENT_ROOT'].$data['src'];
						$func = false;
						$mime = mime_content_type($_image);
						switch ($mime) {
						     case 'image/jpeg': $func = "jpeg"; break;
						     case 'image/gif':  $func = "gif"; break;
						     case 'image/png':  $func = "png"; break;
						     default: break;
						}
						$function1 = "imagecreatefrom".$func;
						$function2 = "image".$func;
						if ($func && is_callable($function1) && is_callable($function2)) {
							$data_image = $function1($_image);
							$w2 = imagesx($data_image);
							$h2 = imagesy($data_image);
							if ($w2<$w || $h2<$h) {
								$result_image = imagecreatetruecolor($w, $h);
								$white = imagecolorallocate($result_image, 255, 255, 255);
								imagefill($result_image, 0, 0, $white);
								imagecopy(
									$result_image, $data_image,
									intval(($w-$w2)/2), intval(($h-$h2)/2), //in place
									0, 0,
									$w2, $h2
								);
								$r = $function2($result_image, $_image);
								imagedestroy($result_image);
								unset($result_image);
							}
							imagedestroy($data_image);
							unset($data_image);
						}
					}
				} else {
					$data = $file;
				}
			} else {
				$data = self::getNoPhoto($w, $h, $method);
			}
			$data['SRC'] = $data['src'];
			return $data;
		}
		public static function get_resize_src(&$file, $w, $h=false, $method=BX_RESIZE_IMAGE_PROPORTIONAL) {
			$data = self::resize_image($file, $w, $h, $method);
			return $data['SRC'];
		}
		private static $views = array('view1', 'view2', 'view3');
		private static $eviews = array('tabs', 'plain');
		private static $sorts = array('NAME|ASC', 'NAME|DESC');
		private static $views_count = array('view1'=>18, 'view2'=>4, 'view3'=>24);
		private static function get_self_vews() {
			return array(
				'view1' => intval(COption::GetOptionString(self::MODULE_ID, 'VIEWS_COUNT_VIEW1')),
				'view2' => intval(COption::GetOptionString(self::MODULE_ID, 'VIEWS_COUNT_VIEW2')),
				'view3' => intval(COption::GetOptionString(self::MODULE_ID, 'VIEWS_COUNT_VIEW3')),
			);
		}
		public static function getCatalogView($asdef = true) {
            CModule::IncludeModule("catalog");
            $arBasePrice = CCatalogGroup::GetBaseGroup();
            self::$sorts[2] = 'CATALOG_PRICE_'.$arBasePrice['ID'].'|ASC';
            self::$sorts[3] = 'CATALOG_PRICE_'.$arBasePrice['ID'].'|DESC';
			$view = $_SESSION['catalog_view'];
			if (!in_array($view, self::$views)) $view = $asdef ? self::$views[0] : false;
			$sort = $_SESSION['catalog_sort'];
			if (!in_array($sort, self::$sorts)) $sort = self::$sorts[2];//$asdef ? self::$sorts[0] : false;
			$eview = $_SESSION['catalog_eview'];
			if (!in_array($eview, self::$eviews)) $eview = $asdef ? self::$eviews[0] : false;
			self::$views_count = self::get_self_vews();
			return array(
				'VIEW'=>$view, 'COUNT'=>self::$views_count[$view], 'SORT'=>explode('|', $sort), 'EVIEW'=>$eview,
				'COUNT_FULL' => self::$views_count, 'VIEW_FULL' => self::$views, 'EVIEW_FULL' => self::$eviews
			);
		}
		public static function setCatalogView($view, $sort, $eview) {
			if ($view) {
				if (in_array($view, self::$views)) $_SESSION['catalog_view'] = $view;
				else $_SESSION['catalog_view'] = self::$views[0];
			}
			if ($sort) {
                CModule::IncludeModule("catalog");
                $arBasePrice = CCatalogGroup::GetBaseGroup();
                self::$sorts[2] = 'CATALOG_PRICE_'.$arBasePrice['ID'].'|ASC';
                self::$sorts[3] = 'CATALOG_PRICE_'.$arBasePrice['ID'].'|DESC';
				if (in_array($sort, self::$sorts)) $_SESSION['catalog_sort'] = $sort;
				else $_SESSION['catalog_sort'] = self::$sorts[0];
			}
			if ($eview) {
				if (in_array($eview, self::$eviews)) $_SESSION['catalog_eview'] = $eview;
				else $_SESSION['catalog_eview'] = self::$eviews[0];
			}
		}
		public static function getSectionByCode($code, $iblock) {
			if (!$code) return 0;
			CModule::IncludeModule('iblock');
			$res = CIBlockSection::GetList(array('ID'=>'ASC'), array('IBLOCK_ID'=>$iblock, 'CODE'=>$code))->Fetch();
			return intval($res['ID']);
		}
		public static function getProductInstallParam($val = 0) {
			include $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'ajax/getString.php';
			return array(
				'NAME' => $name,
				'CODE' => 'INSTALL',
				'VALUE' => $val
			);
		}
		private static function initBasketInstallCache() {
			if (isset(self::$cache['BASKET'])) return;
			CModule::IncludeModule('sale');
			$install = self::getProductInstallParam();
			$res = CSaleBasket::GetPropsList(array('ID' => "ASC"), array('CODE'=>$install['CODE']));
			while ($r = $res->Fetch()) self::$cache['BASKET'][$r['BASKET_ID']] = $r['VALUE'];
		}
		public static function getBasketInstallValue($bid, $pid) {
			self::initBasketInstallCache();
			if ($bid) return self::$cache['BASKET'][$bid];
			if ($pid) {
				CModule::IncludeModule('sale');
				//for product pid get basket..
				$res = CSaleBasket::GetList(
					array('ID'=>'ASC'),
					array(
						"FUSER_ID" => CSaleBasket::GetBasketUserID(),
		                "LID" => SITE_ID,
		                "ORDER_ID" => "NULL",
		                'PRODUCT_ID' => $pid
		            )
		        )->Fetch();
		        return self::$cache['BASKET'][$res['ID']];
			}
			return '';
		}
		public static function addInstallPrice(&$ar, $pid) {
			$install = self::getProductInstallParam();
	        $value = self::getBasketInstallValue(false, $pid);
	        if ($value) {
				//get install price
				CModule::IncludeModule('iblock');
				$res = CIBlockElement::GetList(
					array('ID'=>'ASC'),
					array('IBLOCK_CODE'=>'installs', 'ID'=>$value),
					false, false,
					array('ID','NAME','PROPERTY_PRICE')
				)->Fetch();
				//CModule::IncludeModule('catalog');
				//$res = CCatalogProduct::GetByIDEx($pid);
				//$install_price = floatval($res['PROPERTIES'][$install['CODE']]['VALUE']);
				$install_price = floatval($res['PROPERTY_PRICE_VALUE']);
				$ar['PRICE'] += $install_price;
	        }
		}
		public static function getXMLIDByEnumId($ids) {
			if (!is_array($ids)) $ids = array($ids);
			$res = CUserFieldEnum::GetList(array('ID'=>'ASC'), array('ID'=>$ids));
			$result = array();
			while ($r = $res->Fetch()) $result[$r['ID']] = $r['XML_ID'];
			return $result;
		}
		public static function getLinkProps($iblock_id, $section_id) {
			//HIDDEN_PROPERTIES
			//SHOW_ALL_PROPS
			$setting = COption::GetOptionString(self::MODULE_ID, "SHOW_ALL_PROPS");
			if ($setting != 'Y') {
				CModule::IncludeModule('iblock');
				$res = CIBlockSectionPropertyLink::GetArray($iblock_id, $section_id);
				if (count($res)) return $res;
			}

			$hidden_props = array();
			$setting = COption::GetOptionString(self::MODULE_ID, "HIDDEN_PROPERTIES");
			if (strlen($setting)) {
				$p_hprops = explode(',', preg_replace('/\s+/', '', $setting));
				foreach ($p_hprops as $ph) if ($ph) $hidden_props[$ph] = $ph;
			}

			$result = array();
			$res = CIBlockProperty::GetList(array('sort'=>'asc'), array('IBLOCK_ID'=>$iblock_id));
			while ($r = $res->Fetch()) {
				if ($r['CODE'] && isset($hidden_props[$r['CODE']])) continue;
				$result[] = array(
					"PROPERTY_ID" => $r["ID"],
					"SMART_FILTER" => "Y",
					"INHERITED" => "Y",
					"INHERITED_FROM" => 1,
					"SORT" => $r["SORT"],
				);
			}
			return $result;
		}
	}

	class Delayed {
		public static function add($str) {
			global $APPLICATION;
			$APPLICATION->AddBufferContent('Delayed::'.$str);
		}
		public static function ShowTitle() {
			global $APPLICATION;
			$title = $APPLICATION->GetTitle();
			if ($v = $APPLICATION->GetPageProperty("TITLE_REWRITE")) {$title = $v;}
            if ($v = $APPLICATION->GetPageProperty("title")) {$title = $v;}
			if ($APPLICATION->GetPageProperty("TitleExact") != 'Y') {
				$arSite = $APPLICATION->GetSiteByDir();
				$title .= ' - ' . $arSite['NAME'];
			}
			return $title;
		}
		public static function ShowH1() {
			global $APPLICATION;
			if ($APPLICATION->GetPageProperty("NO_H1")!='Y') return '<h1>'.$APPLICATION->GetPageProperty("PREPEND_H1").$APPLICATION->GetTitle('header').'</h1>';
		    return '';
		}
		public static function ShowContentClass() {
			global $APPLICATION;
			$cl = $APPLICATION->GetPageProperty("ContentClass");
			return $cl ? $cl : 'left';
		}
		public static function ShowContentId() {
			global $APPLICATION;
			$cl = $APPLICATION->GetPageProperty("ContentId");
			return $cl ? $cl : 'content';
		}
	}

	class Handlers {
		public static function OnBeforeIBlockElementAdd($ar) {
			//var_dump($ar);
			$ib = $ar["IBLOCK_ID"];
			CModule::IncludeModule('iblock');
			$res = CIBlock::GetByID($ib)->Fetch();
			if ($res['IBLOCK_TYPE_ID'] == 'catalog' && $res['CODE'] == 'catalog' && !empty($ar["PROPERTY_VALUES"])) {
				$props = array();
				$res = CIBlockProperty::GetList(array('id'=>'asc'), array('IBLOCK_ID'=>$ib, 'CODE'=>'BRAND%'));
				while ($r = $res->Fetch()) $props[$r['CODE']] = array('ID'=>$r['ID'], "LINK_IBLOCK_ID"=>$r["LINK_IBLOCK_ID"]);
				$brand_iblock = &$ar["PROPERTY_VALUES"][$props['BRAND_IBLOCK']['ID']];
				$k = array_keys($brand_iblock); $brand_iblock_id = $k[0];
				$brand = &$ar["PROPERTY_VALUES"][$props['BRAND']['ID']];
				$k = array_keys($brand); $brand_id = $k[0];
				$name = $brand[$brand_id]['VALUE'];
				$brand_value = $brand_iblock[$brand_iblock_id]['VALUE'];
				/*if (!$name) {
					$brand_iblock[$brand_iblock_id]['VALUE'] = '';
				} else {*/
				if ($name && !$brand_value) {
					//find
					$bid = $props['BRAND_IBLOCK']['LINK_IBLOCK_ID'];
					$res = CIBlockElement::GetList(array('ID'=>'ASC'), array('IBLOCK_ID'=>$bid,'NAME'=>$name))->Fetch();
					if (!$res) {
						//add
						$eb = new CIBlockElement();
						$id = $eb->Add(array('IBLOCK_ID'=>$bid, 'NAME'=>$name, 'CODE'=>Cutil::translit($name,"ru")));
					} else {
						$id = $res['ID'];
					}
					$brand_iblock[$brand_iblock_id]['VALUE'] = intval($id);
				}
			}
			return true;
		}
		public static function GetBasketPrice($productID, $quantity = 0, $renewal = "N", $intUserID = 0, $strSiteID = false) {
			CModule::IncludeModule('catalog');
			$ar = CatalogBasketCallback($productID, $quantity, $renewal, $intUserID, $strSiteID); //clear price
			Refs::addInstallPrice($ar, $productID);
			return $ar;
		}
		public static function GetBasketOrderPrice($productID, $quantity, $renewal = "N", $intUserID = 0, $strSiteID = false) {
			CModule::IncludeModule('catalog');
			$ar = CatalogBasketOrderCallback($productID, $quantity, $renewal, $intUserID, $strSiteID); //clear price
			Refs::addInstallPrice($ar, $productID);
			return $ar;
		}
	}

	AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("Handlers", "OnBeforeIBlockElementAdd"));
	AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("Handlers", "OnBeforeIBlockElementAdd"));
