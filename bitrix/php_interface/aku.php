<?php
	AddEventHandler('main', 'OnEpilog', array('CMainHandlers', 'OnEpilogHandler'));  
class CMainHandlers { 
  public static function OnEpilogHandler() {
           	if ( isset($_GET['PAGEN_1']) && (intval($_GET['PAGEN_1'])>0) && (!defined('ERROR_404')) ) {
        $title = $GLOBALS['APPLICATION']->GetProperty('title');
                 $GLOBALS['APPLICATION']->SetPageProperty('title', $title.' страница - '.intval($_GET['PAGEN_1']).'');
        $description = $GLOBALS['APPLICATION']->GetProperty('description');
        $GLOBALS['APPLICATION']->SetPageProperty('description', $description.' страница - '.intval($_GET['PAGEN_1']).'');        
     }
  }
}

/* Заполнения поля для поиска по id */
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "searchid");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "searchid");
function searchid(&$arFields){
    if($arFields["ID"]>0){
        $upd = CIBlockElement::SetPropertyValuesEx(
            $arFields["ID"], 
            $arFields['IBLOCK_ID'], 
            array('PR_ID_COPY' => $arFields["ID"])
        );
    }
}

function get_product_gifts($product_id) {
	$cachedData = returnResultCache(180, 'gifts_'.$product_id, 'get_product_gifts_from_no_cache', $product_id, 'gifts');
	return $cachedData;
}

function getCitiesList() {
	$cachedData = returnResultCache(86400*7, 'products_cities', 'getCitiesList_noCache', false, 'cities');
	return $cachedData;
}

function get_product_gifts_from_no_cache($product_id) {
	global $USER;
	
	$basket = \Bitrix\Sale\Basket::create(SITE_ID); // создаем пустую корзину
	
	$giftManager = \Bitrix\Sale\Discount\Gift\Manager::getInstance(); // получаем менеджера
	$giftManager->setUserId($USER->GetID()); // отбор подарков для текущего пользователя
	$giftManager->enableExistenceDiscountsWithGift(); // не знаю зачем это, но работает как включатель "существования" подарков для товаров
	
	$collections = $giftManager->getCollectionsByProduct($basket, [
	   'ID' => $product_id,
	   'MODULE' => 'catalog',
	   'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
	   'QUANTITY' => 1,
	   'CURRENCY' => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
	   'LID' => Bitrix\Main\Context::getCurrent()->getSite(),
	]);	
	
	return $collections;
}

function returnResultCache($timeSeconds, $cacheId, $callback, $arCallbackParams = '',$type) {
    $obCache = new CPHPCache();
    $cachePath = '/'.SITE_ID.'/'.$type.'/'.$cacheId;
    if( $obCache->InitCache($timeSeconds, $cacheId, $cachePath) ) {
        $vars = $obCache->GetVars();
        $result = $vars['result'];
    }
    elseif( $obCache->StartDataCache()  ) {
        $result = $callback($arCallbackParams);
        $obCache->EndDataCache(array('result' => $result));
    }
    return $result;
}

function get_prop_product_gifts() {
    $result = array();
    $products_list = array();
    $products_for_present= array();

    $products = \CIBlockElement::GetList(
        array("ID"=>"ASC"),
        array("IBLOCK_ID" => 1),
        false,
        false,
        array('ID', 'IBLOCK_ID', 'DETAIL_PICTURE', 'NAME', 'PROPERTY_IS_PRESENT')
    );

    do {
        $product = $products->GetNext();

        if (!empty($product['PROPERTY_IS_PRESENT_VALUE'])) {
            $products_for_present[$product['ID']] = explode(',', $product['PROPERTY_IS_PRESENT_VALUE']);
        }

        $products_list[$product['ID']] = array(
            'name' => $product['NAME'],
            'iamge' => CFile::GetPath($product['DETAIL_PICTURE'])
        );
    } while($product);

    foreach ($products_for_present as $product_id => $gifts) {
        foreach ($gifts as $gift) {
            if(isset($products_list[$gift])) {
                $result[$product_id][$gift] = $products_list[$gift];
            }
        }
    }
    
    return $result;
}

function is_active_filter($item_id, $section_id) {
	$categories_map = array(
		'konditsionery' => [4,56,52,61],
		'radiatory_otopleniya' => [4,56,213,212],
		'vodonagrevateli' => [4],
		'armatura_dlya_sistem_otopleniya' => [4],
		'teplye_poly' => [4],
		'konvektory_otopleniya_vodyanye' => [4],
		'kanalizatsionnye_ustanovki' => [4],
		'cushilki_dlya_ruk' => [4],
		'rehau' => [4],
		'bytovye_ventilyatory' => [4],
		'ventilyatsionnye_ustanovki' => [4,56],
		'generatory' => [4],
		'installyatsii' => [4],
		'obogrevateli_i_teplovye_zavesy' => [4],
		'polotentsesushiteli' => [4],
		'santekhnicheskie_lyuki' => [4],
		'sistema_zashchity_ot_protechek' => [4],
		'sistemy_ochistki_vody' => [4],
		'uvlazhniteli_i_moyki_vozdukha' => [4],
		'inzhenernaya_santekhnika' => [4],
		'ochistiteli_vozdukha' => [4,56],
		'otoplenie' => [4]
	);

	if(!isset($GLOBALS['aku_is_active_filter_section']) || !$GLOBALS['aku_is_active_filter_section']) {
		$section = CIBlockSection::GetByID($section_id)->GetNext();

		do {
			$parent_section = CIBlockSection::GetByID($section['IBLOCK_SECTION_ID'])->GetNext();

			if(!$parent_section) {
				$GLOBALS['aku_is_active_filter_section'] = $section['CODE'];
			}

			$section = $parent_section;
		} while($parent_section);
	}

	if($GLOBALS['aku_is_active_filter_section'] && isset($categories_map[$GLOBALS['aku_is_active_filter_section']])) {
		if(in_array($item_id, $categories_map[$GLOBALS['aku_is_active_filter_section']])) {
			return true;
		}
	}
	
	return false;
}

function get_parent_product_section($product_id) {
	$element = CIBlockElement::GetByID($product_id)->GetNext();

	if($element) {
		do {
			$element = CIBlockSection::GetByID($element['IBLOCK_SECTION_ID'])->GetNext();

			if($element) {
				$parent = $element;
			}
		} while($element);

		if(isset($parent['ID'])) {
			return $parent['ID'];
		}

		return false;
	}				
}

function transliterate($subject) {
	$cyr=array(
		"Щ", "Ш", "Ч","Ц", "Ю", "Я", "Ж","А","Б","В",
		"Г","Д","Е","Ё","З","И","Й","К","Л","М","Н",
		"О","П","Р","С","Т","У","Ф","Х","Ь","Ы","Ъ",
		"Э","Є", "Ї","І",
		"щ", "ш", "ч","ц", "ю", "я", "ж","а","б","в",
		"г","д","е","ё","з","и","й","к","л","м","н",
		"о","п","р","с","т","у","ф","х","ь","ы","ъ",
		"э","є", "ї","і"
	);

	$lat=array(
		"Shch","Sh","Ch","C","Yu","Ya","J","A","B","V",
		"G","D","E","E","Z","I","y","K","L","M","N",
		"O","P","R","S","T","U","F","H","", 
		"Y","" ,"E","E","Yi","I",
		"shch","sh","ch","c","Yu","Ya","j","a","b","v",
		"g","d","e","e","z","i","y","k","l","m","n",
		"o","p","r","s","t","u","f","h",
		"", "y","" ,"e","e","yi","i"
	);

	$subject = str_replace($cyr,$lat,$subject);

	return $subject;
}

function checkGoogleCaptcha() {	
	global $APPLICATION;

	if (isset($_REQUEST['g-recaptcha-response'])) {
		$httpClient = new \Bitrix\Main\Web\HttpClient;
	        	$result = $httpClient->post(
	            	'https://www.google.com/recaptcha/api/siteverify',
	            	array(
	                	'secret' => '6LfDCrkUAAAAAJuZLBPiXjqtiGBW3uxCK3HeyB6d',
	                	'response' => $_REQUEST['g-recaptcha-response'],
	                	'remoteip' => $_SERVER['HTTP_X_REAL_IP']
	         	)
		);
		$result = json_decode($result, true);

		if ($result['success'] === true) {
			return true;
		}
	}

	return false;
}

function getCitiesList_noCache() {
	$result = array();

	$cities_list = [
		'Курган', 'Тюмень', 'Челябинск', 'Екатеринбург', 'Сургут', 'Саратов', 'Новый Уренгой', 'Воронеж', 'Сочи'
	];


	foreach([1, 83] as $section_id) {
		
	    $products = \CIBlockElement::GetList(
	        array("RAND" => "ASC"),
	        array("IBLOCK_ID" => 1, "SECTION_ID" => $section_id, "INCLUDE_SUBSECTIONS" => "Y", "ACTIVE" => "Y"),
	        false,
	        Array ("nTopCount" => 100),
	        array("ID")
	    );

	    while($product = $products->GetNext()) {
	        $limit = mt_rand(3, 5);

	        $cities_list_tmp = $cities_list;

			$cities = [];
			do {
				$index = mt_rand(0, count($cities_list)-1);

				if(isset($cities_list_tmp[$index])) {
					$cities[] = $cities_list_tmp[$index];

					unset($cities_list_tmp[$index]);

					$limit--;
				}
			} while($limit);

			$result[$product["ID"]] = [
				'cities' => $cities
			];
	    }
	}

	return $result;
}