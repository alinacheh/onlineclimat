<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$bid = intval($arParams['BRAND_ID']);
$arResult['BRAND'] = array();
if ($bid) {
	CModule::IncludeModule('iblock');
	$res = CIBlockElement::GetByID($bid)->GetNext();
	$arResult['BRAND'] = $res;
}
$arResult['BRAND']['NAME'] = $arParams['BRAND_NAME'];
CModule::IncludeModule('sale');
$res = CSaleDelivery::GetList(array('PRICE'=>'ASC'),array("LID" => SITE_ID, 'ACTIVE'=>'Y'),false,false,array('*'));
while ($r = $res->Fetch()) $arResult['D'][] = $r;