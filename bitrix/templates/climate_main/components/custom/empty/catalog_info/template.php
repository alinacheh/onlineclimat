<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="tovarAside">
    <div class="made"><?=GetMessage("ASTDESIGN_CLIMATE_PROIZVODITELQ")?></div>
    <?if($arResult['BRAND']['ID']):?>
    	<a href="<?=$arResult['BRAND']['DETAIL_PAGE_URL']?>">
    		<img class="made" src="<?=Refs::get_resize_src($arResult['BRAND']['DETAIL_PICTURE'], 160)?>" alt="<?=$arResult['BRAND']['NAME']?>">
    	</a>
    <?else:?>
		<b><?=$arResult['BRAND']['NAME']?></b>
    <?endif?>
    <?if($arParams['SERVICE']):?>
    	<div class="service"><span></span></div>
    	<p><?=GetMessage("ASTDESIGN_CLIMATE_USTANOVKA")?></p>
    	<p><b><?=GetMessage("ASTDESIGN_CLIMATE_STANDARTNYY_MONTAJ")?></b>:<br /><?=(is_numeric($arParams['SERVICE']) ? number_format($arParams['SERVICE'],0,'.', ' ').' '.GetMessage("ASTDESIGN_CLIMATE_RUB") : $arParams['SERVICE'])?></p>
    <?endif?>
    <div class="courier"><span></span></div> <?/*���������� �������� �� 490 ���.*/?>
    <p><?=GetMessage("ASTDESIGN_CLIMATE_DOSTAVKA")?></p>
    <?foreach ($arResult['D'] as $d):?>
    <p>
    	<b><?=$d['NAME']?></b>, <?=$d["PRICE"]>0?number_format($d["PRICE"],0,'.',' ').' '.GetMessage("ASTDESIGN_CLIMATE_RUB"):GetMessage("ASTDESIGN_CLIMATE_BESPLATNO")?><br>
    	<?=GetMessage("ASTDESIGN_CLIMATE_SROKI_OT")?><?=$d["PERIOD_FROM"]?> <?=GetMessage("ASTDESIGN_CLIMATE_DO")?><?=$d["PERIOD_TO"]?>
    	<?switch($d["PERIOD_TYPE"]) {
			case 'D': echo GetMessage("ASTDESIGN_CLIMATE_DNEY"); break;
			case 'M': echo GetMessage("ASTDESIGN_CLIMATE_MESACEV"); break;
			case 'H': echo GetMessage("ASTDESIGN_CLIMATE_CASOV"); break;
    	}?>
    </p>
    <?endforeach?>
</div>