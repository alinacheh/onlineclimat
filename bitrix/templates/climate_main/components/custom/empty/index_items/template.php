<script>
	$(function(){
		timer_tabs = setInterval(function(){
			var elem = $('#tabs_wrapper .sort span.current'), next;
			if (elem.data('id') != '3') next = elem.next('span');
			else next = $('#tabs_wrapper .sort span.first');
			tabClick(next.children('a').attr('id'));
		}, 6000);
	});
</script>
<div id="tabs_wrapper">
	<div class="sort right">
	    <span data-id="1" class="first tabs"><a class="tabs" id="tab1" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_SPECPREDLOJENIA")?></a></span>
	    <span data-id="2" class="current tabs"><a class="tabs" id="tab2" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_NOVINKI")?></a></span>
	    <span data-id="3" class="last tabs"><a class="tabs" id="tab3" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_POPULARNYE")?></a></span>
	</div>
	<h1><?=GetMessage("ASTDESIGN_CLIMATE_PREDLOJENIA")?></h1>
	<div class="clear"></div>
	<?foreach(array(1=>'PROPERTY_IS_SPECIAL', 'PROPERTY_IS_NEW', 'PROPERTY_IS_POP') as $k=>$f) {
		$GLOBALS['innerFilter'] = array('!'.$f => false);
		?><div class="tabs con_tab<?=$k==2?' active':''?>" id="con_tab<?=$k?>"><?
			$APPLICATION->IncludeComponent("bitrix:catalog.section", "main", Array(
				"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],	// ��� ����-�����
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],	// ����-����
				"SECTION_ID" => 0,	// ID �������
				"SECTION_CODE" => "",	// ��� �������
				"SECTION_USER_FIELDS" => array(	// �������� �������
					0 => "",
					1 => "",
				),
				"ELEMENT_SORT_FIELD" => "RAND",	// �� ������ ���� ��������� ��������
				"ELEMENT_SORT_ORDER" => "ASC",	// ������� ���������� ���������
				"FILTER_NAME" => "innerFilter",	// ��� ������� �� ���������� ������� ��� ���������� ���������
				"INCLUDE_SUBSECTIONS" => "Y",	// ���������� �������� ����������� �������
				"SHOW_ALL_WO_SECTION" => "Y",	// ���������� ��� ��������, ���� �� ������ ������
				"PAGE_ELEMENT_COUNT" => $arParams['COUNT'],	// ���������� ��������� �� ��������
				"LINE_ELEMENT_COUNT" => "5",	// ���������� ��������� ��������� � ����� ������ �������
				"PROPERTY_CODE" => array(	// ��������
					0 => "IS_NEW",
				),
				"SECTION_URL" => "",	// URL, ������� �� �������� � ���������� �������
				"DETAIL_URL" => "",	// URL, ������� �� �������� � ���������� �������� �������
				"BASKET_URL" => SITE_DIR."personal/basket.php",	// URL, ������� �� �������� � �������� ����������
				"ACTION_VARIABLE" => "action",	// �������� ����������, � ������� ���������� ��������
				"PRODUCT_ID_VARIABLE" => "id",	// �������� ����������, � ������� ���������� ��� ������ ��� �������
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// �������� ����������, � ������� ���������� ���������� ������
				"PRODUCT_PROPS_VARIABLE" => "prop",	// �������� ����������, � ������� ���������� �������������� ������
				"SECTION_ID_VARIABLE" => "SECTION_ID",	// �������� ����������, � ������� ���������� ��� ������
				"AJAX_MODE" => "N",	// �������� ����� AJAX
				"AJAX_OPTION_JUMP" => "N",	// �������� ��������� � ������ ����������
				"AJAX_OPTION_STYLE" => "Y",	// �������� ��������� ������
				"AJAX_OPTION_HISTORY" => "N",	// �������� �������� ��������� ��������
				"CACHE_TYPE" => "A",	// ��� �����������
				"CACHE_TIME" => "36000000",	// ����� ����������� (���.)
				"CACHE_GROUPS" => "Y",	// ��������� ����� �������
				"META_KEYWORDS" => "-",	// ���������� �������� ����� �������� �� ��������
				"META_DESCRIPTION" => "-",	// ���������� �������� �������� �� ��������
				"BROWSER_TITLE" => "-",	// ���������� ��������� ���� �������� �� ��������
				"ADD_SECTIONS_CHAIN" => "N",	// �������� ������ � ������� ���������
				"DISPLAY_COMPARE" => "N",	// �������� ������ ���������
				"SET_TITLE" => "N",	// ������������� ��������� ��������
				"SET_STATUS_404" => "N",	// ������������� ������ 404, ���� �� ������� ������� ��� ������
				"CACHE_FILTER" => "N",	// ���������� ��� ������������� �������
				"PRICE_CODE" => array(	// ��� ����
					0 => "main",
				),
				"USE_PRICE_COUNT" => "N",	// ������������ ����� ��� � �����������
				"SHOW_PRICE_COUNT" => "1",	// �������� ���� ��� ����������
				"PRICE_VAT_INCLUDE" => "Y",	// �������� ��� � ����
				"PRODUCT_PROPERTIES" => array(	// �������������� ������
					0 => "IS_NEW",
					1 => "BRAND",
					2 => "IS_POP",
					3 => "IS_SPECIAL",
				),
				"USE_PRODUCT_QUANTITY" => "Y",	// ��������� �������� ���������� ������
				"DISPLAY_TOP_PAGER" => "N",	// �������� ��� �������
				"DISPLAY_BOTTOM_PAGER" => "N",	// �������� ��� �������
				"PAGER_TITLE" => GetMessage("ASTDESIGN_CLIMATE_NOVINKI1"),	// �������� ���������
				"PAGER_SHOW_ALWAYS" => "N",	// �������� ������
				"PAGER_TEMPLATE" => "",	// �������� �������
				"PAGER_DESC_NUMBERING" => "N",	// ������������ �������� ���������
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// ����� ����������� ������� ��� �������� ���������
				"PAGER_SHOW_ALL" => "N",	// ���������� ������ "���"
				"AJAX_OPTION_ADDITIONAL" => "5",	// �������������� �������������
				),
				$component
			);
		?></div><?
	}?>
</div>