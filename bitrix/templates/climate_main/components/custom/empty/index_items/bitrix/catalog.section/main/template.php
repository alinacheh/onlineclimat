<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $USER;
$notifyOption = COption::GetOptionString("sale", "subscribe_prod", "");
$arNotify = unserialize($notifyOption);?>
<div class="items">
	<?$i=0; foreach($arResult["ITEMS"] as $arItem):
		$i++;?>
		<article class="productBlock <?=($i%4==0)?'last':''?>">
		    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="img">
		    	<div style="padding:10px"><img src="<?=Refs::get_resize_src($arItem["DETAIL_PICTURE"], 110)?>" alt="<?=$arItem['NAME']?>"></div>
		    </a>
		    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="title"><?=$arItem['NAME']?></a>
		    <?if ($arItem["CAN_BUY"]):?>
		    	<span class="price"><?=number_format($arItem["PRICES"]['main']['DISCOUNT_VALUE'],0,'.', ' ')?> .-</span>
			    <div class="clear">
			        <div class="pm right">
			            <a href="#" class="p"></a>
			            <a href="#" class="m"></a>
			        </div>
			        <input type="text" class="count right" value="1">
			        <div class="have right add2basket<?=$arItem['ID']?>" style="display: none;"><?=GetMessage("ASTDESIGN_CLIMATE_TOVAR_UJE_V_KORZINE")?></div>
			        <a data-id="<?=$arItem["ID"]?>" href="#" class="add2basketq add2basket<?=$arItem['ID']?> cart left" rel="nofollow"></a>
			    </div>
			<?else:?>
				<span class="price wait"><?=GetMessage("ASTDESIGN_CLIMATE_NET_V_NALICII_TOVAR")?></span>
				<div class="relative clear">
					<span class="wait btn" ><a data-id="<?=$arItem["ID"]?>" class="notify notify<?=$arItem["ID"]?>" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_A_JDU")?></a></span>
				</div>
			<?endif?>
		</article>
		<?if ($i%4==0) echo '<div class="clear"></div></div><div class="line"></div><div class="items">';
	endforeach?>
	<div class="clear"></div>
</div>