<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"COUNT" => Array(
		"NAME" => "COUNT",
		"TYPE" => "STRING",
		"DEFAULT" => "8",
	)
);
?>
