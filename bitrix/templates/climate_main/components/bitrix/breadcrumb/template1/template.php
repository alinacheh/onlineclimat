<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include "lang/ru/template.php";
//delayed function must return a string
if(empty($arResult))
	return "";
$strReturn = '<div id="path"><span><a href="'.SITE_DIR.'">'.$MESS["ASTDESIGN_CLIMATE_GLAVNAA"].'</a></span>';
$page_title = $GLOBALS['APPLICATION']->GetTitle();
$itemSize = count($arResult)-1;
if ($arResult[$itemSize]["TITLE"] == $page_title) {
	$itemSize--;
}

for ($index = 0; $index <= $itemSize; $index++) {
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	if($arResult[$index]["LINK"] <> "")
		$strReturn .= ' <span><a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">'.$title.'</a></span>';
	else
		$strReturn .= ' <span>'.$title.'</span>';
}
if ($GLOBALS['APPLICATION']->GetPageProperty('Element')!='Y') $strReturn .= ' <span>'.$page_title.'</span>';
$strReturn .= '</div>';
return $strReturn;
?>
