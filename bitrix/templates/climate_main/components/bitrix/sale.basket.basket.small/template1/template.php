<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="title"><a <?if($arResult['QUANTITY']):?>href="<?=$arParams['PATH_TO_BASKET']?>"<?endif?>><?=GetMessage("ASTDESIGN_CLIMATE_KORZINA")?></a></div>
<div class="clear">
    <div class="inf left"><?=GetMessage("ASTDESIGN_CLIMATE_SUMMA")?><span><?=number_format($arResult['PRICE'],0,'.',' ')?> .-</span></div>
    <div class="inf right"><?=GetMessage("ASTDESIGN_CLIMATE_TOVAROV")?><span><?=$arResult['QUANTITY']?></span></div>
</div>
<script>
	in_basket = [<?foreach ($arResult["ITEMS"] as $v) if ($v["DELAY"]=="N" && $v["CAN_BUY"]=="Y") echo $v['PRODUCT_ID'].','?>0];
	in_subscribe = [<?foreach ($arResult["ITEMS"] as $v) if ($v["SUBSCRIBE"]=="Y") echo $v['PRODUCT_ID'].','?>0];
</script>