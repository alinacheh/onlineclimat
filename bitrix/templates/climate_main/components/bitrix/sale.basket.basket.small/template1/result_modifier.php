<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arResult['QUANTITY'] = 0;
$arResult['PRICE'] = 0;
foreach ($arResult["ITEMS"] as $v) {
	if ($v["DELAY"]=="N" && $v["CAN_BUY"]=="Y") {
		$arResult['QUANTITY'] += $v["QUANTITY"];
		$arResult['PRICE'] += $v["QUANTITY"] * $v["PRICE"];
	}
}