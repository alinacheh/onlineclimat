<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="relative left clear">
	<?if ($arResult["STATUS"] == "Y"):?>
		<span id="notify_after_<?=$arParams['NOTIFY_ID']?>" class="wait active btn"><a href="#"><?=GetMessage("ASTDESIGN_CLIMATE_A_JDU")?></a></span>
	<?elseif ($arResult["STATUS"] == "N"):?>
		<span id="notify_after_<?=$arParams['NOTIFY_ID']?>" class="wait btn">
			<a href="#" onClick="notifyProduct('<?=$arResult["NOTIFY_URL"]?>', <?=$arParams['NOTIFY_ID']?>);"><?=GetMessage("ASTDESIGN_CLIMATE_A_JDU")?></a>
		</span>
	<?elseif ($arResult["STATUS"] == "R"):?>
		<span id="notify_after_<?=$arParams['NOTIFY_ID']?>" class="z wait btn">
			<a href="#" onClick="showNotify(<?=$arParams['NOTIFY_ID']?>)" id="notify_product_<?=$arParams['NOTIFY_ID']?>"><?=GetMessage("ASTDESIGN_CLIMATE_A_JDU")?></a>
		</span>
	<?endif;?>
	<input type="hidden" value="<?=$arResult["NOTIFY_URL"]?>" name="notify_url_<?=$arParams['NOTIFY_ID']?>" id="notify_url_<?=$arParams['NOTIFY_ID']?>">
</div>
<?if (!defined("EXIST_FORM")):
	define("EXIST_FORM", "Y");?>
	<div class="window wait" id="popup_form_notify" style="display: none;">
        <div class="close"></div>
        <div class="title"><?=GetMessage("ASTDESIGN_CLIMATE_UVEDOMITQ_O_POSTUPLE")?></div>
        <form id="popup_form_notify_form">
        	<?=bitrix_sessid_post()?>
        	<div id="popup_n_error" style="color:red;"></div>
        	<input type="hidden" value="Y" name="ajax">
        	<input type="hidden" value="" name="popup_notify_url" id="popup_notify_url">
            <label class="input"><?=GetMessage('NOTIFY_POPUP_MAIL');?> <input type="text" value="" name="user_mail" id="popup_user_email" size="25"></label>
            <input type="hidden" name="notify_user_auth" id="notify_user_auth" value="N" >
            <div class="btn alt"><button type="submit"><?=GetMessage("ASTDESIGN_CLIMATE_OTPRAVITQ")?></button></div>
        </form>
    </div>
	<script>
		$(function(){
			$('#popup_form_notify_form').submit(function(){
				BX.showWait();
				$.post('/bitrix/components/bitrix/sale.notice.product/ajax.php', $(this).serialize(), function(res) {
					BX.closeWait();
					var rs = eval( '('+res+')' );
					if (rs['ERRORS'].length > 0) {
						if (rs['ERRORS'] == 'NOTIFY_ERR_NULL')
							BX('popup_n_error').innerHTML = '<?=GetMessageJS('NOTIFY_ERR_NULL')?>';
						else if (rs['ERRORS'] == 'NOTIFY_ERR_CAPTHA')
							BX('popup_n_error').innerHTML = '<?=GetMessageJS('NOTIFY_ERR_CAPTHA')?>';
						else if (rs['ERRORS'] == 'NOTIFY_ERR_MAIL_EXIST')
						{
							BX('popup_n_error').innerHTML = '<?=GetMessageJS('NOTIFY_ERR_MAIL_BUYERS_EXIST')?>';
							showAuth();
							BX('popup_user_email').value = '';
							BX('notify_user_login').focus();
						}
						else if (rs['ERRORS'] == 'NOTIFY_ERR_REG')
							BX('popup_n_error').innerHTML = '<?=GetMessageJS('NOTIFY_ERR_REG')?>';
						else
							BX('popup_n_error').innerHTML = rs['ERRORS'];
					} else if (rs['STATUS'] == 'Y') {
						notifyProduct(BX('popup_notify_url').value, 11);
						$('#popup_form_notify').hide();
					}
				});
				return false;
			});
		})
		function showNotify(id) {
			$('#popup_form_notify').insertAfter('#notify_after_'+id);
			$('#popup_form_notify').show();
			BX('popup_notify_url').value = BX('notify_url_'+id).value;
			BX('popup_user_email').focus();
		}

		function notifyProduct(url, id) {
			BX.showWait();
			BX.ajax.post(url, '', function(res) {
				BX.closeWait();
				document.body.innerHTML = res;
				if (BX('url_notify_'+id))
					BX('url_notify_'+id).innerHTML = '<?=GetMessage("MAIN_NOTIFY_MESSAGE");?>';
			});
		}
	</script>
<?endif;?>