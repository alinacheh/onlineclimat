<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult['DETAIL_PICTURE']):?>
	<img class="img_ext left" src="<?=Refs::get_resize_src($arResult['DETAIL_PICTURE'], 270)?>" width="270" alt="<?=$arResult['NAME']?>">
<?endif?>
<?if($arResult["DISPLAY_ACTIVE_FROM"]):?>
	<span class="date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
<?endif?>
<p><?=$arResult['DETAIL_TEXT']?></p>