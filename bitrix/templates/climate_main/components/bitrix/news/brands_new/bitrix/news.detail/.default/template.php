<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult['DETAIL_PICTURE']):?>
	<img class="img_ext p20 left" src="<?=Refs::get_resize_src($arResult['DETAIL_PICTURE'], 150)?>" width="150" alt="<?=$arResult['NAME']?>">
<?endif?>
<?if($arResult["DISPLAY_ACTIVE_FROM"]):?>
	<span class="date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
<?endif?>
<p><?=$arResult['DETAIL_TEXT']?></p>
<div class='clear'></div>