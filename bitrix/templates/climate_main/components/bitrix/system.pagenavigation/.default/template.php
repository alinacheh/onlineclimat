<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!$arResult["NavShowAlways"]) {
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : ""); // ��������� �������� ��� ���������� ���������
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
// ��� ���� ���� ��� ���� ������ - ������ � �����
if ($arResult["NavPageNomer"] > 1) {
	if ($arResult["NavPageNomer"] > 2) { // �� 3-� � ����� ��������
		$PREV='<span class="prev"><a href="'.$arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]-1).'"></a></span>';
		if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]){ // ���� ���� ��� ��������
			$NEXT='<span class="next"><a href="'.$arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1).'"></a></span>';
		} else { // ������ ������� ���
			$NEXT='';
		}
	} else { // �� �� ������ ��������
		$PREV='<span class="prev"><a href="'.$arResult["sUrlPath"].$strNavQueryStringFull.'"></a></span>';
		if ($arResult["NavPageCount"]<>$arResult["NavPageNomer"]) {
			$NEXT='<span class="next"><a href="'.$arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1).'"></a></span>';
		} else {
        	$NEXT='';
		}
	}
} else { // ������ �������� - ������
	$PREV='';
 	if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]) { // ���� ���� ��� ��������
        $NEXT='<span class="next"><a href="'.$arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1).'"></a></span>';
	} else { //  ������ ������� ���
        $NEXT='';
	}
}
// ����� ����� ����
// � ����� ������ ����� �������

echo $PREV;
while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>
	<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): // ������� �������� ?>
		<span class="current"><a href="#"><?=$arResult["nStartPage"]?></a></span>
	<?elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false): // ��� ������ �� ������ �������� ?>
		<span><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a></span>
	<?else:?>
		<span><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a></span>
	<?endif;
	$arResult["nStartPage"]++;
endwhile;
echo $NEXT;