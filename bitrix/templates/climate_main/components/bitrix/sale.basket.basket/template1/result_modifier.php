<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
function getImageById($id) {
	CModule::IncludeModule('catalog');
	$res = CCatalogProduct::GetByIDEx($id);
	return $res;
}
$ids = array();
foreach (array('AnDelCanBuy', 'DelDelCanBuy', 'nAnCanBuy', 'ProdSubscribe') as $code) {
	foreach ($arResult["ITEMS"][$code] as $i=>$item) {
		$elem = getImageById($item['PRODUCT_ID']);
		$arResult["ITEMS"][$code][$i]['DETAIL_PICTURE'] = $elem['DETAIL_PICTURE'];
		$arResult["ITEMS"][$code][$i]['INSTALL'] = $elem['PROPERTIES']['INSTALL']['VALUE'];
	}
}