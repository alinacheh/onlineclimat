<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?echo ShowError($arResult["ERROR_MESSAGE"]);?>
<?$pids = array();?>
<?if(count($arResult["ITEMS"]["AnDelCanBuy"])):?>
<table class="items basket view3">
    <tr>
        <th class="name"><?=GetMessage("ASTDESIGN_CLIMATE_TOVAR")?></th>
        <th class="count2"><?=GetMessage("ASTDESIGN_CLIMATE_KOL_VO")?></th>
        <th class="price"><?=GetMessage("ASTDESIGN_CLIMATE_CENA")?></th>
    </tr>
	<?foreach($arResult["ITEMS"]["AnDelCanBuy"] as $arBasketItems):
		$pids[] = intval($arBasketItems["PRODUCT_ID"]).':'.intval($arBasketItems["QUANTITY"]);?>
	    <tr class="basketitem">
	    	<input type="hidden" name="DELETE_<?=$arBasketItems["ID"]?>" id="DELETE_<?=$arBasketItems["ID"]?>" value="">
	        <input type="hidden" name="DELAY_<?=$arBasketItems["ID"]?>" id="DELAY_<?=$arBasketItems["ID"]?>" value="">
	        <td class="tovar clear item<?=$arBasketItems["ID"]?>">
	            <a href="<?=$arBasketItems["DETAIL_PAGE_URL"]?>" class="left img"><img src="<?=Refs::get_resize_src($arBasketItems['DETAIL_PICTURE'],130,130)?>"></a>
	            <div class="relative left">
	                <div class="title2">
	                	<a href="<?=$arBasketItems["DETAIL_PAGE_URL"]?>"><?=$arBasketItems["NAME"]?></a>
	                	<br />
	                	<?if($arBasketItems['INSTALL']):?>
		        			<p>
		        				<input type="checkbox" class="setInstall" data-id="<?=$arBasketItems["ID"]?>" <?if($arBasketItems['PROPS'][0]['VALUE']=='Y'):?>checked="checked"<?endif?> />
		        				<?=GetMessage("ASTDESIGN_CLIMATE_TREBUETSA_USTANOVKA")?><?=(is_numeric($arBasketItems['INSTALL']) ? number_format($arBasketItems['INSTALL'],0,'.', ' ').' '.GetMessage("ASTDESIGN_CLIMATE_RUB") : $arBasketItems['INSTALL'])?>)
		        			</p>
				        <?endif?>
	                </div>
	                <div class="clear"></div>
	                <div class="btn call left"><a data-id="<?=$arBasketItems["ID"]?>" class="delay" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_OTLOJITQ")?></a></div>
	            </div>
	        </td>
	        <td class="count2 clear item<?=$arBasketItems["ID"]?>">
	            <input type="text" class="count submit left" name="QUANTITY_<?=$arBasketItems["ID"] ?>" value="<?=$arBasketItems["QUANTITY"]?>">
	            <div class="pm left submit">
	                <a href="#" class="p"></a>
	                <a href="#" class="m"></a>
	            </div>
	        </td>
	        <td class="price relative">
		        <?=number_format($arBasketItems["PRICE"],0,'.', ' ')?> .-
		        <a data-id="<?=$arBasketItems["ID"]?>" href="#" class="close"></a>
	        </td>
	    </tr>
    <?endforeach?>
</table>
<div class="line"></div>
<div class="itogo">
    <div class="price right"><?=number_format($arResult["allSum"],0,'.',' ')?> .-</div>
    <div class="text right"><?=GetMessage("ASTDESIGN_CLIMATE_ITOGO")?></div>
</div>
<div class="line"></div>
<div class="buyBtn clear">
    <div class="btn right"><a href="#" onclick="$('#basketOrderButton2').val('Y');$(this).parents('form').submit(); return false;"><?=GetMessage("ASTDESIGN_CLIMATE_OFORMITQ_ZAKAZ")?></a></div>
    <div class="btn alt right"><a href="#" data-pid='<?=join(',', $pids)?>' class="qorder"><?=GetMessage("ASTDESIGN_CLIMATE_BYSTRYY_ZAKAZ")?></a></div>
    <?if ($arParams["HIDE_COUPON"] != "Y"):?>
		<label><?=GetMessage("ASTDESIGN_CLIMATE_NOMER_KUPONA_NA_SKID")?><input type="text" name="COUPON" style="padding: 5px;" value="<?=$arResult["COUPON"]?>" class="input"></label>
	<?endif;?>
	<input type="hidden" value="" name="BasketOrder"  id="basketOrderButton2">
</div>
<div class="line"></div>
<?else:?>
	<div class="items basket view3">
		<br /><br />
		<p><?=GetMessage("ASTDESIGN_CLIMATE_V_VASEY_KORZINE_NET")?></p>
	</div>
<?endif?>