<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arResult["ITEMS_ORDER"] = array(
	0 => 0,
	1 => 1
);
foreach($arResult["ITEMS"] as $pid => $parr) {
	if ($parr['PRICE']) $arResult["ITEMS_ORDER"][0] = $pid;
	elseif ($parr['CODE'] == 'BRAND') $arResult["ITEMS_ORDER"][1] = $pid;
	else $arResult["ITEMS_ORDER"][] = $pid;
}