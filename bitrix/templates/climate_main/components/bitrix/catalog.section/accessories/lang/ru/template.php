<?
$MESS["CATALOG_BUY"] = "Купить";
$MESS["CATALOG_ADD"] = "В корзину";
$MESS["CATALOG_COMPARE"] = "Сравнить";
$MESS["CATALOG_NOT_AVAILABLE"] = "(нет на складе)";
$MESS["CATALOG_QUANTITY"] = "Количество";
$MESS["CATALOG_QUANTITY_FROM_TO"] = "От #FROM# до #TO#";
$MESS["CATALOG_QUANTITY_FROM"] = "От #FROM#";
$MESS["CATALOG_QUANTITY_TO"] = "До #TO#";
$MESS["CT_BCS_QUANTITY"] = "Количество";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["CATALOG_SUBSCRIBE"] = "Уведомить о поступлении";
$MESS["ASTDESIGN_CLIMATE_CENA"] = "Цена:";
$MESS["ASTDESIGN_CLIMATE_TOVAR_UJE_V_KORZINE"] = "Товар уже в корзине";
$MESS["ASTDESIGN_CLIMATE_POKAZATQ_VSE_TOVARY"] = "Показать все товары категории ";
?>