<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule('iblock');
$cache = array();
foreach($arResult["ITEMS"] as &$arElement) {
	if (!isset($cache[$arElement['IBLOCK_SECTION_ID']])) {
		$res = CIBlockSectionPropertyLink::GetArray($arElement['IBLOCK_ID'], $arElement['IBLOCK_SECTION_ID']);
		$cache[$arElement['IBLOCK_SECTION_ID']] = array();
		foreach ($res as $prop) {
			if ($prop['INHERITED_FROM'] != 0) {
				$cache[$arElement['IBLOCK_SECTION_ID']][] = $prop['PROPERTY_ID'];
			}
		}
	}
	$db_props = CIBlockElement::GetProperty($arElement['IBLOCK_ID'], $arElement['ID']);
	while ($prop = $db_props->Fetch()) {
		$arElement['PROPERTIES'][$prop['ID']] = $prop;
	}
	$arElement['PROPS'] = $cache[$arElement['IBLOCK_SECTION_ID']];
}
$arResult['SECTIONS'] = array();
if (count($cache)) {
	$res = CIBlockSection::GetList(array('ID'=>'ASC'), array('ID'=>array_keys($cache)), false, array('ID', 'NAME', 'SECTION_PAGE_URL'));
	while ($r = $res->GetNext()) $arResult['SECTIONS'][$r['ID']] = $r;
}