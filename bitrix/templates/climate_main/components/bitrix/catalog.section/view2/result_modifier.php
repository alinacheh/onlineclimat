<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$cache = array();
foreach($arResult["ITEMS"] as &$arElement) {
	$sid = $arElement['~IBLOCK_SECTION_ID'] ? $arElement['~IBLOCK_SECTION_ID'] : $arElement['IBLOCK_SECTION_ID'];
	if (!isset($cache[$sid])) {
		$res = CIBlockSectionPropertyLink::GetArray($arElement['IBLOCK_ID'], $sid);
		$cache[$sid] = array();
		foreach ($res as $prop) {
			if ($prop['INHERITED_FROM'] != 0) {
				$cache[$sid][] = $prop['PROPERTY_ID'];
			}
		}
	}
	$db_props = CIBlockElement::GetProperty($arElement['IBLOCK_ID'], $arElement['ID']);
	while ($prop = $db_props->Fetch()) {
		$arElement['PROPERTIES'][$prop['ID']] = $prop;
	}
	$arElement['PROPS'] = $cache[$sid];
}
