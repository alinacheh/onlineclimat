<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div id="pagination_hidden" style="display:none"><?=$arResult["NAV_STRING"]?></div>
<div class="items view2">
	<?$i=0; $max=count($arResult["ITEMS"]); foreach($arResult["ITEMS"] as $arElement): $i++;?>
		<article class="productBlock <?=($i==$max)?'last':''?>">
            <div class="altBody left" style="padding:10px">
                <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="img">
                	<img src="<?=Refs::get_resize_src($arElement["DETAIL_PICTURE"], 110)?>">
                </a>
            	<label class="left"><input data-id="<?=$arElement["ID"]?>" class="compare compare<?=$arElement["ID"]?>" type="checkbox"> <?=GetMessage("ASTDESIGN_CLIMATE_SRAVNITQ")?></label>
            </div>
            <div class="altBody right">
            	<?if($arElement["CAN_BUY"] && $arElement['CATALOG_QUANTITY']):?>
            		<?$p1 = $arElement['PRICES']['main']['VALUE']; $p2 = 0;
            		if ($arElement['PRICES']['main']['DISCOUNT_VALUE'] < $arElement['PRICES']['main']['VALUE']) {
						$p1 = $arElement['PRICES']['main']['DISCOUNT_VALUE'];
						$p2 = $arElement['PRICES']['main']['VALUE'];
					}?>
	                <span class="price">
	                    <span><?=GetMessage("ASTDESIGN_CLIMATE_CENA")?></span>
	                    <?=number_format($p1,0,'.',' ')?> .-
	                    <?if($p2):?><span class="old"><?=number_format($p2,0,'.',' ')?>.</span><?endif?>
	                </span>
	                <div class="clear">
			            <div class="pm right">
			                <a href="#" class="p"></a>
			                <a href="#" class="m"></a>
			            </div>
			            <input type="text" class="count right" value="1">
			            <div class="have right add2basket<?=$arElement['ID']?>" style="display: none;"><?=GetMessage("ASTDESIGN_CLIMATE_TOVAR_UJE_V_KORZINE")?></div>
			            <a href="#" data-id="<?=$arElement['ID']?>" class="cart add2basketq add2basket<?=$arElement['ID']?> left"></a>
			        </div>
			    <?else:?>
					<span class="price wait"><?=GetMessage("CATALOG_NOT_AVAILABLE")?></span>
					<div class="relative clear">
						<span class="wait btn" ><a data-id="<?=$arElement["ID"]?>" class="notify notify<?=$arElement["ID"]?>" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_A_JDU")?></a></span>
					</div>
			    <?endif?>
            </div>
            <div class="body">
                <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="title"><?=$arElement["NAME"]?></a>
                <ul class="simple">
                	<?$pc=0; foreach($arElement['PROPS'] as $pid) if ($arElement['PROPERTIES'][$pid]['VALUE']):
                		$p = $arElement['PROPERTIES'][$pid];?>
						<li><?=$p['NAME']?>: <?=$p['VALUE_ENUM']?$p['VALUE_ENUM']:$p['VALUE']?></li>
                	<?$pc++; if($pc>4) break; endif?>
                </ul>
            </div>
            <div class="clear"></div>
        </article>
        <div class="line"></div>
	<?endforeach?>
</div>
<?if(!$i):?><div id="no_items"><?=GetMessage("ASTDESIGN_CLIMATE_IZVINITE_NICEGO_NE")?></div><?endif?>
<?if($arParams['ADD_SECTIONS_CHAIN'] == 'Y' && $arResult['DESCRIPTION']):?>
	<br /><p><?=$arResult['DESCRIPTION']?></p>
<?endif?>