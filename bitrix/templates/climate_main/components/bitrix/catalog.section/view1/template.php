<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="pagination_hidden" style="display:none"><?=$arResult["NAV_STRING"]?></div>
<div class="items">
	<?$i=0; foreach($arResult["ITEMS"] as $arElement): $i++;?>
		<article class="productBlock <?if($i%4==0):?>last<?endif?>">
	        <?if(!$arParams['IS_SEARCH_RESULT']):?>
	        	<label><input data-id="<?=$arElement["ID"]?>" class="compare compare<?=$arElement["ID"]?>" type="checkbox"> <?=GetMessage('CATALOG_COMPARE')?></label>
	        <?endif?>
	        <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="img">
	        	<div style="padding:10px"><img src="<?=Refs::get_resize_src($arElement["DETAIL_PICTURE"], 110)?>" alt="<?=$arElement["NAME"]?>"></div>
	        </a>
	        <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="title"><?=$arElement["NAME"]?></a>
	        <?if($arElement["CAN_BUY"] && $arElement['CATALOG_QUANTITY']):?>
	        	<span class="price"><?=number_format($arElement['PRICES']['main']['DISCOUNT_VALUE'],0,'.',' ')?> .-</span>
	        	<div class="clear">
			        <div class="pm right">
			            <a href="#" class="p"></a>
			            <a href="#" class="m"></a>
			        </div>
			        <input type="text" class="count right" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1" size="5">
			        <div class="have right add2basket<?=$arElement['ID']?>" style="display: none;"><?=GetMessage("ASTDESIGN_CLIMATE_TOVAR_UJE_V_KORZINE")?></div>
					<a href="#" data-id="<?=$arElement['ID']?>" class="add2basketq add2basket<?=$arElement['ID']?> cart left"></a>
			    </div>
			<?else:?>
				<span class="price wait"><?=GetMessage("CATALOG_NOT_AVAILABLE")?></span>
				<div class="relative clear">
					<span class="wait btn" ><a data-id="<?=$arElement["ID"]?>" class="notify notify<?=$arElement["ID"]?>" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_A_JDU")?></a></span>
				</div>
			<?endif?>
	    </article>
	    <?if($i%4==0):?><div class="clear"></div><?endif?>
	<?endforeach?>
	<div class="clear"></div>
</div>
<?if(!$i):?><div id="no_items"><?=GetMessage("ASTDESIGN_CLIMATE_IZVINITE_NICEGO_NE")?></div><?endif?>
<?if($arParams['ADD_SECTIONS_CHAIN'] == 'Y' && $arResult['DESCRIPTION']):?>
	<br /><p><?=$arResult['DESCRIPTION']?></p>
<?endif?>