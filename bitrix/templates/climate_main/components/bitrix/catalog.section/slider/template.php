<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="slides" class="container">
	<?foreach($arResult["ITEMS"] as $arElement):?>
		<div class="slide">
    		<img src="<?=Refs::get_resize_src($arElement['PROPERTIES']['SLIDER']['VALUE'], 930, 373)?>" alt="<?=$arElement['NAME']?>"/>
		    <div class="box">
        		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="title"><?=$arElement['NAME']?></a>
        		<p class="text"><?=strip_tags($arElement['PREVIEW_TEXT'])?></p>
		        <span class="price"><?=number_format($arElement['PRICES']['main']["DISCOUNT_VALUE"],0,'.',' ')?> .-</span>
		        <div class="btn right">
		        	<?/*<a data-parent="slide" data-id="<?=$arElement['ID']?>" class="add2basket add2basket<?=$arElement['ID']?>" href="#">� �������<b></b></a></div>*/?>
		        	<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=GetMessage("ASTDESIGN_CLIMATE_PODROBNEE")?><b></b></a></div>
		    </div>
		</div>
	<?endforeach?>
</div>
