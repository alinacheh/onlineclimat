<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="pagination_hidden" style="display:none"><?=$arResult["NAV_STRING"]?></div>
<table class="items view3">
	<tr>
        <th class="name"><?=GetMessage("CATALOG_TITLE")?></th>
        <th class="price"><?=$arResult["PRICES"]['main']["TITLE"]?></th>
        <th class="info">&nbsp;</th>
    </tr>
	<?$i=0; foreach($arResult["ITEMS"] as $arElement): $i++?>
		<tr>
            <td class="name"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></td>
            <td class="price">
            	<?$arPrice = $arElement["PRICES"]['main'];
				if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
					<?=number_format($arPrice["DISCOUNT_VALUE"],0,'.',' ')?> .-
					<span class="old"><?=number_format($arPrice["VALUE"],0,'.',' ')?></span>
				<?else:?>
					<?=number_format($arPrice["VALUE"],0,'.',' ')?> .-
				<?endif?>
            </td>
            <td class="info">
            	<?if($arElement["CAN_BUY"] && $arElement['CATALOG_QUANTITY']):?>
            		<a href="#" data-id="<?=$arElement['ID']?>" class="tableview buy add2basket add2basket<?=$arElement['ID']?> action"><?=GetMessage("ASTDESIGN_CLIMATE_KUPITQ")?></a>
            	<?else:?>
            		<div class="relative left clear">
						<span class="wait btn" ><a data-id="<?=$arElement["ID"]?>" class="notify notify<?=$arElement["ID"]?>" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_A_JDU")?></a></span>
					</div>
            	<?endif?>
            	<a data-id="<?=$arElement["ID"]?>" href="#" class="compare tableview compare<?=$arElement["ID"]?> action"><?=GetMessage("ASTDESIGN_CLIMATE_SRAVNITQ")?></a>
            </td>
        </tr>
	<?endforeach?>
</table>
<?if(!$i):?><div id="no_items"><?=GetMessage("ASTDESIGN_CLIMATE_IZVINITE_NICEGO_NE")?></div><?endif?>
<?if($arParams['ADD_SECTIONS_CHAIN'] == 'Y' && $arResult['DESCRIPTION']):?>
	<p class="describe"><?=$arResult['DESCRIPTION']?></p>
<?endif?>