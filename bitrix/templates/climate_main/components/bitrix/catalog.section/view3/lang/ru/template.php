<?
$MESS["CATALOG_BUY"] = "Купить";
$MESS["CATALOG_ADD"] = "В корзину";
$MESS["CATALOG_NOT_AVAILABLE"] = "(нет на складе)";
$MESS["CATALOG_TITLE"] = "Наименование";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["CATALOG_SUBSCRIBE"] = "Уведомить о поступлении";
$MESS["ASTDESIGN_CLIMATE_KUPITQ"] = "Купить";
$MESS["ASTDESIGN_CLIMATE_A_JDU"] = "Я жду!";
$MESS["ASTDESIGN_CLIMATE_SRAVNITQ"] = "Сравнить";
$MESS["ASTDESIGN_CLIMATE_IZVINITE_NICEGO_NE"] = "Извините, ничего не можем предложить";
?>