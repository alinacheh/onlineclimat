<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["FORM_TYPE"] == "login"):?>
	<div id="reg" class="right"><a href="<?=$arResult["AUTH_REGISTER_URL"]?>"><?=GetMessage("AUTH_REGISTER")?></a></div>
	<div class="relative right">
	    <div id="enter" class="right btn"><a href="#"><?=GetMessage("ASTDESIGN_CLIMATE_VHOD")?></a></div>
	    <div class="window enter">
	        <div class="close"></div>
	        <div class="title"><?=GetMessage("ASTDESIGN_CLIMATE_VHOD")?></div>
	        <form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=SITE_DIR?>auth/">
        		<?if($arResult["BACKURL"] <> ''):?>
					<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<?endif?>
				<?foreach ($arResult["POST"] as $key => $value):?>
					<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
				<?endforeach?>
				<input type="hidden" name="AUTH_FORM" value="Y" />
				<input type="hidden" name="TYPE" value="AUTH" />
	            <label class="input"><?=GetMessage("AUTH_LOGIN")?> <input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" size="17" /></label>
	            <label class="input"><?=GetMessage("AUTH_PASSWORD")?> <input type="password" name="USER_PASSWORD" maxlength="50" size="17" /></label>
	            <label class="check"><input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" /> <?echo GetMessage("AUTH_REMEMBER_SHORT")?></label>
	            <div class="btn alt"><input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" /></div>
	            <a href="<?=SITE_DIR?>auth/?forgot_password=yes" id="remind"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
	            <?if($arResult["AUTH_SERVICES"]):?>
					<div class="bx-auth-lbl"><?=GetMessage("socserv_as_user_form")?></div>
					<?
					$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "icons",
						array(
							"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
							"SUFFIX"=>"form",
						),
						$component,
						array("HIDE_ICONS"=>"Y")
					);
					?>
				<?endif?>
	        </form>
	    </div>
	    <?if($arResult["AUTH_SERVICES"]):?>
			<?
			$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
				array(
					"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
					"AUTH_URL"=>SITE_DIR.'auth/',
					"POST"=>$arResult["POST"],
					"POPUP"=>"Y",
					"SUFFIX"=>"form",
				),
				$component,
				array("HIDE_ICONS"=>"Y")
			);
			?>
		<?endif?>
	</div>
<?else:?>
	<div class="relative right">
		<div id="enter" class="right btn"><a href="<?=SITE_DIR?>?logout=yes"><?=GetMessage("AUTH_LOGOUT_BUTTON")?></a></div>
		<div id="reg_ext" class="right"><a href="<?=SITE_DIR?>personal/"><?=GetMessage("AUTH_PROFILE")?></a></div>
	</div>
<?endif?>
