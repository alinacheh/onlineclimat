<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
$is_section = true;
$sid = Refs::getSectionByCode($arResult["VARIABLES"]['SECTION_CODE'], $arParams["IBLOCK_ID"]);
$APPLICATION->SetPageProperty("SECTION_ID", $sid);
?>
<?$APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "catalog_filter", Array(
	"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],	// ��� ���������
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],	// ��������
	"SECTION_ID" => $sid,	// ID ������� ���������
	"FILTER_NAME" => "arrFilter",	// ��� ���������� ������� ��� ����������
	"CACHE_TYPE" => "A",	// ��� �����������
	"CACHE_TIME" => "36000000",	// ����� ����������� (���.)
	"CACHE_NOTES" => "",
	"CACHE_GROUPS" => "Y",	// ��������� ����� �������
	"SAVE_IN_SESSION" => "N",	// ��������� ��������� ������� � ������ ������������
	"PRICE_CODE" => array(	// ��� ����
		0 => "main",
	)
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID2" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE2" => $arResult["VARIABLES"]["SECTION_CODE"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
		"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
	),
	$component
);?>
<?include "_section.php"?>