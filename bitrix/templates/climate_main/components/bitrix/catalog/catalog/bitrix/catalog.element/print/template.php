<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
	if ($arResult['PRICES']['main']['DISCOUNT_VALUE'] < $arResult['PRICES']['main']['VALUE']) {
		$p1 = number_format($arResult['PRICES']['main']['DISCOUNT_VALUE'], 0, '.', ' ');
		$p3 = number_format($arResult['PRICES']['main']['VALUE'], 0, '.', ' ');
	} else {
		$p1 = number_format($arResult['PRICES']['main']['VALUE'], 0, '.', ' ');
		$p3 = 0;
	}
	if (CBXFeatures::IsFeatureEnabled('CatMultiPrice') && $arResult['PRICES']['other']['VALUE']) {
		$p2 = number_format($arResult['PRICES']['other']['VALUE'], 0, '.', ' ');
	} else $p2 = 0;
	if (isset($arResult['DISPLAY_PROPERTIES']['FILES']["FILE_VALUE"]['ID']))
		$arResult['DISPLAY_PROPERTIES']['FILES']["FILE_VALUE"] = array($arResult['DISPLAY_PROPERTIES']['FILES']["FILE_VALUE"]);
?>
<div class="title">
    <h1><?=$arResult["NAME"]?></h1>
    <?if($arResult['PROPERTIES']['ARTICUL']['VALUE']):?><div class="articul"><?=GetMessage("ASTDESIGN_CLIMATE_ARTIKUL")?><?=$arResult['PROPERTIES']['ARTICUL']['VALUE']?></div><?endif?>
</div>
<div id="tovar">
	<div class="box left">
		<div class="img big">
	        <a href="<?=$arResult['IMAGES'][0]['B']['SRC']?>"><img src="<?=$arResult['IMAGES'][0]['M']['SRC']?>" height="290" width="290" alt="<?=$arResult["NAME"]?>"></a>
	        <div class="labels">
	            <?if($p3):?><div class="discount"><?=GetMessage("ASTDESIGN_CLIMATE_SKIDKA")?><b></b></div><?endif?>
	            <?if($arResult['PROPERTIES']['IS_SPECIAL']['VALUE']):?><div class="discount"><?=GetMessage("ASTDESIGN_CLIMATE_SPEC")?><b></b></div><?endif?>
	            <?if($arResult['PROPERTIES']['IS_POP']['VALUE']):?><div class="hit"><?=GetMessage("ASTDESIGN_CLIMATE_HIT")?><b></b></div><?endif?>
	            <?if($arResult['PROPERTIES']['IS_NEW']['VALUE']):?><div class="new"><?=GetMessage("ASTDESIGN_CLIMATE_NOVINKA")?><b></b></div><?endif?>
	        </div>
	    </div>
	</div>
	<div class="box2 right">
		<?if($arResult['PROPERTIES']['ARTICUL']['VALUE']):?><div class="articul"><?=GetMessage("ASTDESIGN_CLIMATE_ARTIKUL")?><?=$arResult['PROPERTIES']['ARTICUL']['VALUE']?></div><?endif?>
	    <div class="compareBlock">
	    	<?if($arResult['CATALOG_QUANTITY'] > 0):?>
            	<div class="nal left yes"><?=GetMessage("ASTDESIGN_CLIMATE_ESTQ_V_NALICII")?></div>
            <?else:?>
            	<div class="nal left no"><?=GetMessage("ASTDESIGN_CLIMATE_NET_V_NALICII")?></div>
            <?endif?>
	    </div>
	    <div class="line"></div>
	    <div class="priceBlock">
	        <div class="price"><?=GetMessage("ASTDESIGN_CLIMATE_CENA")?><span><?=$p1?> .-</span></div>
	        <?if($p2):?><div class="price rozn"><?=$arResult["CAT_PRICES"]['other']['TITLE']?>:<span><?=$p2?> .-</span></div><?endif?>
	        <?if($p3):?><div class="price old"><?=GetMessage("ASTDESIGN_CLIMATE_STARAA_CENA")?><span><?=$p3?> .-</span></div><?endif?>
	    </div>
	</div>
	<div class="clear"></div>
</div>
<?if($arResult['DETAIL_TEXT']):?>
	<h2><?=GetMessage("ASTDESIGN_CLIMATE_OPISANIE")?><?=$arResult['NAME']?>:</h2>
	<p><?=$arResult['DETAIL_TEXT']?></p>
	<div class="line"></div>
<?endif?>
<h2 class="ma"><?=GetMessage("ASTDESIGN_CLIMATE_HARAKTERISTIKI")?><?=$arResult['NAME']?>:</h2>
<table class="feature">
	<?foreach ($arResult['SHOW_PROPS'] as $pid):
		$val = $arResult['PROPERTIES2ID'][$pid]['VALUE'];
		if (empty($val)) continue;
		if ($arResult['PROPERTIES2ID'][$pid]["PROPERTY_TYPE"] == 'N' && !is_array($val)) $val=floatval($val)?>
		<tr class="fill">
			<th><?=$arResult['PROPERTIES2ID'][$pid]['NAME']?>:</th>
			<td><?=is_array($val)?join('<br>',$val):$val?></td>
		</tr>
	<?endforeach?>
</table>
<?if(CBXFeatures::IsFeatureEnabled('CatMultiStore')):?>
	<div class="line"></div>
	<h2 class="ma"><?=GetMessage("ASTDESIGN_CLIMATE_NALICIE_V_MAGAZINAH")?></h2>
	<?$APPLICATION->IncludeComponent("bitrix:catalog.store.amount", ".default", array(
		"PER_PAGE" => $arParams["STORE_PER_PAGE"],
		"USE_STORE_PHONE" => $arParams["STORE_USE_STORE_PHONE"],
		"SCHEDULE" => $arParams["STORE_USE_STORE_SCHEDULE"],
		"USE_MIN_AMOUNT" => $arParams["STORE_USE_MIN_AMOUNT"],
		"MIN_AMOUNT" => $arParams["STORE_MIN_AMOUNT"],
		"ELEMENT_ID" => $arResult['ID'],
		"STORE_PATH"  =>  $arParams["STORE_STORE_PATH"],
		"MAIN_TITLE"  =>  $arParams["STORE_MAIN_TITLE"],
		),
		$component
	);?>
<?endif?>