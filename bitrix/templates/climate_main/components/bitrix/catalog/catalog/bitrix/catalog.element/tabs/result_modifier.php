<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule('iblock');
$arResult['IMAGES'] = array();
if ($arResult['DETAIL_PICTURE']) {
	$arResult['IMAGES'][] = array(
		'S' => Refs::resize_image($arResult['DETAIL_PICTURE'], 70, 65),
		'M' => Refs::resize_image($arResult['DETAIL_PICTURE'], 290),
		'B' => Refs::resize_image($arResult['DETAIL_PICTURE'], 900),
	);
}
foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $f) {
	$arResult['IMAGES'][] = array(
		'S' => Refs::resize_image($f, 70, 65),
		'M' => Refs::resize_image($f, 290),
		'B' => Refs::resize_image($f, 900),
	);
}
if (!count($arResult['IMAGES'])) {
	$arResult['IMAGES'][] = array(
		'S' => Refs::resize_image($arResult['DETAIL_PICTURE'], 70, 65),
		'M' => Refs::resize_image($arResult['DETAIL_PICTURE'], 290),
		'B' => Refs::resize_image($arResult['DETAIL_PICTURE'], 900),
	);
}
$res = CIBlockSectionPropertyLink::GetArray($arResult['IBLOCK_ID'], $arResult['IBLOCK_SECTION_ID']);
$arResult['SHOW_PROPS'] = array();
foreach ($res as $prop) {
	if ($prop['INHERITED_FROM'] != 0) {
		$arResult['SHOW_PROPS'][] = $prop['PROPERTY_ID'];
	}
}
if (count($arResult['SHOW_PROPS'])) {
	foreach ($arResult['PROPERTIES'] as $code=>&$p) {
		$arResult['PROPERTIES2ID'][$p['ID']] = $p;
	}
	unset($p);
}

$sect = CIBlockSection::GetList(array('ID'=>'ASC'), array('IBLOCK_ID'=>$arResult['IBLOCK_ID'],'ID'=>$arResult['IBLOCK_SECTION_ID']),false,array('UF_*'))->Fetch();
$arResult['SECTION']['UF_ACC'] = $sect['UF_ACC'];