<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
function showmenu($data, $first) {
	if ($first) $root_sid = $data['C'];
	else $root_sid = $data['CP'];
	$i=0;
	foreach($data['H'][$root_sid] as $sid):
		$arSection = $data['SECTIONS'][$sid];
		$i++;
    	?><span <?if($sid==$data['C']):?>class="current"<?endif?>><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a></span><?
    endforeach;
    if (!$i && $first) showmenu($data, false);
}?>
<div class="category clear"><?showmenu($arResult['DATA'], true)?></div>