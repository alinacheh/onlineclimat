<?
	IncludeTemplateLangFile(__FILE__);
	$is_main_page = ($APPLICATION->GetCurPage(true) == '/index.php');
	$is_print = $_REQUEST['print'] == 'Y';
?>
				<?if(!$is_print):?>
        			</div>
        			<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
						"AREA_FILE_SHOW" => "sect",
						"AREA_FILE_SUFFIX" => "right",
						"AREA_FILE_RECURSIVE" => "Y",
						"EDIT_TEMPLATE" => ""
						),
						false
					);?>
			        <div class="clear"></div>
		        <?endif?>
		</section>
		<?if(!$is_print):?>
			<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "about",
				"AREA_FILE_RECURSIVE" => "N",
				"EDIT_TEMPLATE" => ""
				),
				false
			);?>
			<footer class="container">
				<div class="tel right"><?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR."phone.inc.php",
					"EDIT_TEMPLATE" => ""
					),
					false
				);?></div>
				<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
					"ROOT_MENU_TYPE" => "top",	// ��� ���� ��� ������� ������
					"MENU_CACHE_TYPE" => "N",	// ��� �����������
					"MENU_CACHE_TIME" => "3600",	// ����� ����������� (���.)
					"MENU_CACHE_USE_GROUPS" => "Y",	// ��������� ����� �������
					"MENU_CACHE_GET_VARS" => "",	// �������� ���������� �������
					"MAX_LEVEL" => "1",	// ������� ����������� ����
					"CHILD_MENU_TYPE" => "left",	// ��� ���� ��� ��������� �������
					"USE_EXT" => "N",	// ���������� ����� � ������� ���� .���_����.menu_ext.php
					"DELAY" => "Y",	// ����������� ���������� ������� ����
					"ALLOW_MULTI_SELECT" => "N",	// ��������� ��������� �������� ������� ������������
					),
					false
				);?>

				<div class="clear"></div>
				<div class="line"></div>

				<div id="copy" style="height: 52px;">

				</div>
			</footer>
			<div id="shadow"></div>
			<div id="comparator" class="window2 compareList com">
				<div class="close"></div>
				<div class="title"><?=GetMessage("ASTDESIGN_CLIMATE_SPISOK_SRAVNENIA")?></div>
				<div id="comparator_inner"><?include $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'ajax/addToCompare.php'?></div>
			</div>
			<div id="qorder" class="window com enter">
	            <div class="close"></div>
	            <div class="title"><?=GetMessage("ASTDESIGN_CLIMATE_BYSTRYY_ZAKAZ")?></div>
	            <form method="post" id="qorder_form">
            		<input type="hidden" id="qorder_items" name="items" value="" />
	                <label class="input"><?=GetMessage("ASTDESIGN_CLIMATE_IMA")?><input required type="text" name="name" value="<?=$GLOBALS['USER']->GetFullName()?>"></label>
	                <label class="input"><?=GetMessage("ASTDESIGN_CLIMATE_TELEFON")?><input required type="text" class="phone" name="phone" value=""></label>
	                <div class="btn_a alt"><button onclick="$(this).parents('form').submit(); return false;"><?=GetMessage("ASTDESIGN_CLIMATE_OTPRAVITQ")?></button></div>
	            </form>
	        </div>
	        <?if(!$GLOBALS['USER']->IsAuthorized()):?>
		        <div class="window wait" id="popup_form_notify">
					<div class="close"></div>
					<div class="title"><?=GetMessage("ASTDESIGN_CLIMATE_UVEDOMITQ_O_POSTUPLE")?></div>
					<form id="popup_form_notify_form" method="post">
						<div id="popup_n_error" style="color:red;"></div>
						<input id="notify_id" type="hidden" name="id" value="" />
						<input type="hidden" name="subscribe" value="Y" />
						<label class="input"><?=GetMessage("ASTDESIGN_CLIMATE_VAS")?> email <input type="text" value="" name="user_mail" size="25" class="text-placeholder"></label>
						<small id="success_notify" style="display:none"><br /><?=GetMessage("ASTDESIGN_CLIMATE_SPASIBO_KOGDA_TOVAR")?><br /><br /></small>
						<div class="btn alt"><button type="submit" onclick="$(this).parents('form').submit();return false;"><?=GetMessage("ASTDESIGN_CLIMATE_OTPRAVITQ1")?></button></div>
					</form>
				</div>
			<?endif?>
			<div class="window wait" id="popup_form_notify_success" style="display:none">
				<div class="close"></div>
				<div class="title"><?=GetMessage("ASTDESIGN_CLIMATE_UVEDOMITQ_O_POSTUPLE")?></div>
				<small><br /><?=GetMessage("ASTDESIGN_CLIMATE_SPASIBO_KOGDA_TOVAR")?><br /><br /></small>
			</div>
			<div class="window wait" id="popup_form_notify_already" style="display:none">
				<div class="close"></div>
				<div class="title"><?=GetMessage("ASTDESIGN_CLIMATE_UVEDOMITQ_O_POSTUPLE")?></div>
				<small class="success"><br /><?=GetMessage("ASTDESIGN_CLIMATE_ETOT_TOVAR_UJE_DOBAV")?><br /><br /></small>
			</div>
			<div id="add_in_basket" class="add right" style="display:none"><?=GetMessage("ASTDESIGN_CLIMATE_TOVAR_DOBAVLEN")?></div>
		<?endif?>
	</body>
</html>