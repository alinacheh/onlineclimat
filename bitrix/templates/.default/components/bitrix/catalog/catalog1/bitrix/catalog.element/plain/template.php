<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
	if ($arResult['2PRICES']['main']['DISCOUNT_VALUE'] < $arResult['2PRICES']['main']['VALUE']) {
		$p1 = FormatCurrency($arResult['2PRICES']['main']['DISCOUNT_VALUE'], $arResult['2PRICES']['main']['CURRENCY']);
		$p3 = FormatCurrency($arResult['2PRICES']['main']['VALUE'], $arResult['2PRICES']['main']['CURRENCY']);
	} else {
		$p1 = FormatCurrency($arResult['2PRICES']['main']['VALUE'], $arResult['2PRICES']['main']['CURRENCY']);
		$p3 = 0;
	}
	if (CBXFeatures::IsFeatureEnabled('CatMultiPrice') && $arResult['2PRICES']['other']['VALUE']) {
		$p2 = FormatCurrency($arResult['2PRICES']['other']['VALUE'], $arResult['2PRICES']['other']['CURRENCY']);
	} else $p2 = 0;
	if (isset($arResult['DISPLAY_PROPERTIES']['FILES']["FILE_VALUE"]['ID']))
		$arResult['DISPLAY_PROPERTIES']['FILES']["FILE_VALUE"] = array($arResult['DISPLAY_PROPERTIES']['FILES']["FILE_VALUE"]);
?>
<div class="line"></div>
<div id="tovar" class="productBlock">
    <div class="box left">
        <div class="left">
        	<div class="img big">
	            <a href="<?=$arResult['IMAGES'][0]['B']['SRC']?>"><img src="<?=$arResult['IMAGES'][0]['M']['SRC']?>" title="<?=$arResult["NAME"]?>" alt="<?=$arResult["NAME"]?>"></a>
	            <div class="labels">
	                <?if($p3):?><div class="discount"><?=GetMessage("ASTDESIGN_CLIMATE_SKIDKA")?><b></b></div><?endif?>
	                <?if($arResult['PROPERTIES']['IS_SPECIAL']['VALUE']):?><div class="discount"><?=GetMessage("ASTDESIGN_CLIMATE_SPEC")?><b></b></div><?endif?>
	                <?if($arResult['PROPERTIES']['IS_POP']['VALUE']):?><div class="hit"><?=GetMessage("ASTDESIGN_CLIMATE_HIT")?><b></b></div><?endif?>
	                <?if($arResult['PROPERTIES']['IS_NEW']['VALUE']):?><div class="new"><?=GetMessage("ASTDESIGN_CLIMATE_NOVINKA")?><b></b></div><?endif?>
	            </div>
	        </div>
        </div>
        <ul class="images jcarousel-skin-tango">
        	<?foreach ($arResult['IMAGES'] as $i=>$img):?>
            	<li class="img <?if(!$i):?>current<?endif?>"><a href="<?=$img['B']['SRC']?>" data-src="<?=$img['M']['SRC']?>"><img src="<?=$img['S']['SRC']?>" height="65" width="70" title="<?=$arResult["NAME"]?>" alt="<?=$arResult["NAME"]?>"></a></li>
            <?endforeach?>
        </ul>
    </div>
    <div class="box2 right">
        <div class="title">
            <h1><?=$arResult["NAME"]?></h1>
            <?if($arResult['PROPERTIES']['ARTICUL']['VALUE']):?><div class="articul"><?=GetMessage("ASTDESIGN_CLIMATE_ARTIKUL")?><?=$arResult['PROPERTIES']['ARTICUL']['VALUE']?></div><?endif?>
        </div>
        <div class="compareBlock">
            <div class="compareList right active"><a href="#" class="show_comparator"><?=GetMessage("ASTDESIGN_CLIMATE_SPISOK_SRAVNENIA")?></a></div>
            <?if($arResult['CATALOG_QUANTITY'] > 0):?>
            	<div class="nal left yes"><?=GetMessage("ASTDESIGN_CLIMATE_ESTQ_V_NALICII")?></div>
            <?else:?>
            	<?if ($arResult['CATALOG_CAN_BUY_ZERO']=='Y'):?>
            		<div class="nal left no podzakaz"><?=GetMessage("ASTDESIGN_CLIMATE_POD_ZAKAZ")?></div>
            	<?else:?>
            		<div class="nal left no nenal"><?=GetMessage("ASTDESIGN_CLIMATE_NET_V_NALICII")?></div>
            	<?endif?>
            <?endif?>
            <label><input type="checkbox" data-id="<?=$arResult["ID"]?>" class="compare compare<?=$arResult["ID"]?>" > <?=GetMessage("ASTDESIGN_CLIMATE_SRAVNITQ")?></label>
        </div>
        <div class="line"></div>
        <?if($arResult['CATALOG_QUANTITY'] > 0):?>
	        <div class="priceBlock">
	            <div class="price"><?=GetMessage("ASTDESIGN_CLIMATE_CENA")?><span><?=$p1?></span></div>
	            <?if($p2):?><div class="price rozn"><?=$arResult["CAT_PRICES"][$arResult['2PRICES']['other']['CODE']]['TITLE']?>:<span><?=$p2?></span></div><?endif?>
	            <?if($p3):?><div class="price old"><?=GetMessage("ASTDESIGN_CLIMATE_STARAA_CENA")?><span><?=$p3?></span></div><?endif?>
                <?if(!empty($arResult['SERTIFICATE'])):?>
                    <div class="sertificate-detail">
                        <a href="<?=$arResult['SERTIFICATE']['big'];?>"><img src="<?=$arResult['SERTIFICATE']['small'];?>" alt="Сертикифат"></a>
                    </div>
                <?endif;?>
	        </div>
	        <p class="countbasket<?=$arResult['ID']?>"><?=GetMessage("ASTDESIGN_CLIMATE_KOLICESTVO")?></p>
	        <div class="actionBlock clear">
	            <div class="countBlock left countbasket<?=$arResult['ID']?>">
	                <div class="pm right">
	                    <a href="#" class="p"></a>
	                    <a href="#" class="m"></a>
	                </div>
	                <input type="text" class="count right" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1" size="5">
	                <a data-id="<?=$arResult['ID']?>" href="#" class="cart left"></a>
	            </div>
	            <div class="btn buy left">
	                <a href="#" data-class="actionBlock" data-id="<?=$arResult['ID']?>" class="blockview add2basketq add2basket<?=$arResult['ID']?>" onclick="return submitwith('ADD2BASKET');"><?=GetMessage("ASTDESIGN_CLIMATE_KUPITQ")?><b></b></a>
	            </div>
	            <div class="btn alt right">
	                <a href="#" data-pid='<?=$arResult['ID']?>:1' class="qorder"><?=GetMessage("ASTDESIGN_CLIMATE_BYSTRYY_ZAKAZ")?><b></b></a>
	            </div>
	        </div>
	    <?elseif ($arResult['CATALOG_CAN_BUY_ZERO']=='Y'):?>
	    	<div class="priceBlock">
	            <div class="price"><?=GetMessage("ASTDESIGN_CLIMATE_CENA")?><span><?=$p1?></span></div>
	            <?if($p2):?><div class="price rozn"><?=$arResult["CAT_PRICES"][$arResult['2PRICES']['other']['CODE']]['TITLE']?>:<span><?=$p2?></span></div><?endif?>
	            <?if($p3):?><div class="price old"><?=GetMessage("ASTDESIGN_CLIMATE_STARAA_CENA")?><span><?=$p3?></span></div><?endif?>
	        </div>
			<div class="actionBlock clear">
				<div class="btn buy left viewP">
	                <a href="#" data-class="actionBlock" data-id="<?=$arResult['ID']?>" class="viewP2 allowfororder add2basket add2basket<?=$arResult['ID']?>" onclick="return submitwith('ADD2BASKET');"><?=GetMessage("ASTDESIGN_CLIMATE_ZAKAZATQ")?><b></b></a>
	            </div>
	    	</div>
	    <?else:?>
	    	<div class="actionBlock clear">
				<div class="left clear">
					<span class="wait btn" ><a data-id="<?=$arResult["ID"]?>" class="notify notify<?=$arResult["ID"]?>" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_A_JDU")?></a></span>
				</div>
	    	</div>
	    <?endif?>
    </div>
    <div class="clear"></div>
    <div class="viewBlock" id="setEview">
        <span class="first"><a href="#" data-t="tabs"><?=GetMessage("ASTDESIGN_CLIMATE_VKLADKI")?></a></span>
        <span class="current last"><a href="#" data-t="plain"><?=GetMessage("ASTDESIGN_CLIMATE_SPISOK")?></a></span>
    </div>
    <div class="clear"></div>
</div>
<div class="line"></div>
<div id="no_tabs_wrapper">
	<?if($arResult['DETAIL_TEXT']):?>
		<div class="tabs con_tab active" id="con_tab0">
			<a name="atab0"></a>
			<h2><?=GetMessage("ASTDESIGN_CLIMATE_OPISANIE")?><?=$arResult['NAME']?>:</h2>
			<p><?=$arResult['DETAIL_TEXT']?></p>
			<div class="line"></div>
		</div>
		<div class="tabs con_tab" id="con_tab1">
	<?else:?>
		<div class="tabs con_tab active" id="con_tab1">
	<?endif?>
		<a name="atab1"></a>
		<h2 class="ma"><?=GetMessage("ASTDESIGN_CLIMATE_HARAKTERISTIKI")?><?=$arResult['NAME']?>:</h2>
		<table class="feature">
			<?foreach ($arResult['SHOW_PROPS'] as $pid):
				$val = $arResult['PROPERTIES2ID'][$pid]['VALUE'];
				if (empty($val)) continue;
				if ($arResult['PROPERTIES2ID'][$pid]["PROPERTY_TYPE"] == 'N' && !is_array($val)) $val=floatval($val)?>
			    <tr class="fill">
			        <th><?=$arResult['PROPERTIES2ID'][$pid]['NAME']?>:</th>
			        <td><?=is_array($val)?join('<br>',$val):$val?></td>
			    </tr>
		    <?endforeach?>
		</table>
		<div class="line"></div>
	</div>
	<?if(count($arResult["SECTION"]['UF_ACC'])):?>
		<div class="tabs con_tab" id="con_tab2">
			<a name="atab2"></a>
			<h2 class="ma"><?=GetMessage("ASTDESIGN_CLIMATE_AKSSESUARY")?><?=$arResult['NAME']?>:</h2>
			<?$GLOBALS['arrFilter']['SECTION_ID'] = $arResult["SECTION"]['UF_ACC'];?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:catalog.section",
				'accessories',
				Array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"ELEMENT_SORT_FIELD" => 'RAND',
					"ELEMENT_SORT_ORDER" => 'ASC',
					"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
					"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
					"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
					"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
					"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
					"BASKET_URL" => $arParams["BASKET_URL"],
					"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
					"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
					"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
					"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
					"FILTER_NAME" => 'arrFilter',
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_FILTER" => $arParams["CACHE_FILTER"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"SET_TITLE" => $arParams["SET_TITLE"],
					"SET_STATUS_404" => $arParams["SET_STATUS_404"],
					"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
					"PAGE_ELEMENT_COUNT" => $view['COUNT'],
					"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
					"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
					"SHOW_ALL_WO_SECTION" => "Y",
					"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
					"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
					"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
					"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
					"PAGER_TITLE" => $arParams["PAGER_TITLE"],
					"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"]='Y',
					"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
					"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
					"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
					"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

					"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
					"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
					"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
					"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
					"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
					"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

					"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
					"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
					'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
					'CURRENCY_ID' => $arParams['CURRENCY_ID'],
					"MAIN_PRICES" => $arParams['MAIN_PRICES'],
					"OTHER_PRICES" => $arParams['OTHER_PRICES'],
				),
				$component
			);
			?>
			<div class="line"></div>
		</div>
	<?endif?>
	<div class="tabs con_tab" id="con_tab3">
		<a name="atab3"></a>
		<h2 class="ma"><?=GetMessage("ASTDESIGN_CLIMATE_OTZYVY")?></h2>
		<div id="reviews">
			<?$GLOBALS['reviewFilter']['PROPERTY_ITEM'] = $arResult['ID'];?>
			<?$APPLICATION->IncludeComponent("bitrix:news.list", "reviews", Array(
				"IBLOCK_TYPE" => "catalog",
				"IBLOCK_ID" => 7,
				"NEWS_COUNT" => "20",
				"SORT_BY1" => "ID",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "reviewFilter",
				"FIELD_CODE" => array(
					0 => "DATE_CREATE",
					1 => "",
				),
				"PROPERTY_CODE" => array(
					0 => "AUTHOR_EMAIL",
					1 => "AUTHOR_NAME",
					2 => "RATE",
					3 => "",
				),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"CACHE_NOTES" => "",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "Y",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => GetMessage("ASTDESIGN_CLIMATE_NOVOSTI"),
				"PAGER_SHOW_ALWAYS" => "Y",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_OPTION_ADDITIONAL" => "",
				),
				false
			);?>
		    <?$APPLICATION->IncludeComponent("custom:empty", "catalog_review", Array(
				"IBLOCK_TYPE" => "",
				"IBLOCK_ID" => "",
				"ITEM" => $arResult['ID'],
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0",
				"CACHE_NOTES" => ""
				),
				false
			);?>
		</div>
		<div class="line"></div>
	</div>
	<?if(CBXFeatures::IsFeatureEnabled('CatMultiStore')):?>
		<div class="tabs con_tab" id="con_tab4">
			<a name="atab4"></a>
			<h2 class="ma"><?=GetMessage("ASTDESIGN_CLIMATE_NALICIE_V_MAGAZINAH")?></h2>
			<?$APPLICATION->IncludeComponent("bitrix:catalog.store.amount", ".default", array(
				"PER_PAGE" => $arParams["STORE_PER_PAGE"],
				"USE_STORE_PHONE" => $arParams["STORE_USE_STORE_PHONE"],
				"SCHEDULE" => $arParams["STORE_USE_STORE_SCHEDULE"],
				"USE_MIN_AMOUNT" => $arParams["STORE_USE_MIN_AMOUNT"],
				"MIN_AMOUNT" => $arParams["STORE_MIN_AMOUNT"],
				"ELEMENT_ID" => $arResult['ID'],
				"STORE_PATH"  =>  $arParams["STORE_STORE_PATH"],
				"MAIN_TITLE"  =>  $arParams["STORE_MAIN_TITLE"],
				),
				$component
			);?>
			<div class="line"></div>
		</div>
	<?endif?>
	<?if(count($arResult['DISPLAY_PROPERTIES']['FILES']["FILE_VALUE"])):?>
		<div class="tabs con_tab" id="con_tab5">
			<a name="atab5"></a>
			<h2 class="ma"><?=GetMessage("ASTDESIGN_CLIMATE_FAYLY")?></h2>
			<div class="files">
				<?foreach ($arResult['DISPLAY_PROPERTIES']['FILES']["FILE_VALUE"] as $file):?>
		    		<span class="file<?=($file["CONTENT_TYPE"]=='application/zip')?'2':'1'?>">
		    			<a target="_blank" href="<?=$file["SRC"]?>"><?=$file["DESCRIPTION"]?$file["DESCRIPTION"]:$file["ORIGINAL_NAME"]?></a>
		    		</span>
			    <?endforeach?>
			</div>
		</div>
	<?endif?>
</div>
