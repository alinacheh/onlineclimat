<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetPageProperty("BRAND_IBLOCK", $arResult["PROPERTIES"]["BRAND_IBLOCK"]['VALUE']);
$APPLICATION->SetPageProperty("BRAND", $arResult["PROPERTIES"]["BRAND"]['VALUE']);
if ($v = $arResult["PROPERTIES"]['TITLE']['VALUE']) {
	$APPLICATION->SetTitle($v);
	$APPLICATION->SetPageProperty('header', $v);
	$APPLICATION->SetPageProperty("TITLE_REWRITE", $v);
}
if ($v = $arResult["PROPERTIES"]['KEYWORDS']['VALUE']) {
	$APPLICATION->SetPageProperty('keywords', $v);
}
if ($v = $arResult["PROPERTIES"]['DESCRIPTION']['VALUE']) {
	$APPLICATION->SetPageProperty('description', $v);
}?>