<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$sections = $arParams['SECTION_ID_FILTER'];
if (!count($sections)) {
	$code = $arParams['SECTION_CODE2'];
	$id   = intval($arParams['SECTION_ID2']);
	$data = array(
		'H' => array(),
		'C' => 0,
		'CP' => 0,
		'SECTIONS' => array()
	);
	foreach ($arResult["SECTIONS"] as &$sect) {
		$sid = intval($sect['ID']);
		$data['SECTIONS'][$sid] = $sect;
		$data['H'][intval($sect['IBLOCK_SECTION_ID'])][] = $sid;
		$data['H_N'][intval($sect['IBLOCK_SECTION_ID'])][] = $sid . '_' . $sect['NAME'];
		if (
			($id && $sid == $id)
			||
			($code && $sect['CODE']==$code)
		) {
			$data['C'] = $sid;
			$data['CP'] = intval($sect['IBLOCK_SECTION_ID']);
		}
	}
	unset($sect);
} else {
	$data = array(
		'H' => array(),
		'C' => 0,
		'CP' => 0,
		'SECTIONS' => array()
	);
	foreach ($arResult["SECTIONS"] as &$sect) {
		$sid = intval($sect['ID']);
		if (!isset($sections[$sid])) continue;
		$data['SECTIONS'][$sid] = $sect;
		$data['H'][0][] = $sid;
		$data['H_N'][0][] = $sid . '_' . $sect['NAME'];
	}
}
$arResult['DATA'] = $data;