<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule('iblock');
$arResult['IMAGES'] = array();
if ($arResult['DETAIL_PICTURE']) {
	$arResult['IMAGES'][] = array(
		'S' => Refs::resize_image($arResult['DETAIL_PICTURE'], 70, 65),
		'M' => Refs::resize_image($arResult['DETAIL_PICTURE'], 290),
		'B' => Refs::resize_image($arResult['DETAIL_PICTURE'], 900),
	);
}
foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $f) {
	$arResult['IMAGES'][] = array(
		'S' => Refs::resize_image($f, 70, 65),
		'M' => Refs::resize_image($f, 290),
		'B' => Refs::resize_image($f, 900),
	);
}
if (!count($arResult['IMAGES'])) {
	$arResult['IMAGES'][] = array(
		'S' => Refs::resize_image($arResult['DETAIL_PICTURE'], 70, 65),
		'M' => Refs::resize_image($arResult['DETAIL_PICTURE'], 290),
		'B' => Refs::resize_image($arResult['DETAIL_PICTURE'], 900),
	);
}
$res = Refs::getLinkProps($arResult['IBLOCK_ID'], $arResult['IBLOCK_SECTION_ID']);
$arResult['SHOW_PROPS'] = array();
foreach ($res as $prop) {
	if ($prop['INHERITED_FROM'] != 0) {
		$arResult['SHOW_PROPS'][] = $prop['PROPERTY_ID'];
	}
}
if (count($arResult['SHOW_PROPS'])) {
	foreach ($arResult['PROPERTIES'] as $code=>&$p) {
		$arResult['PROPERTIES2ID'][$p['ID']] = $p;
	}
	unset($p);
}


$sect = CIBlockSection::GetList(array('ID'=>'ASC'), array('IBLOCK_ID'=>$arResult['IBLOCK_ID'],'ID'=>$arResult['IBLOCK_SECTION_ID']),false,array('UF_*'))->Fetch();
$arResult['SECTION']['UF_ACC'] = $sect['UF_ACC'];

//prices
$arResult['2PRICES'] = array();
$min_price = array(PHP_INT_MAX, PHP_INT_MAX, '');
foreach ($arParams['MAIN_PRICES'] as $code) {
	$price = $arResult['PRICES'][$code];
	//DISCOUNT_VALUE or VALUE
	if ($price["CAN_ACCESS"] == 'Y' && $price["CAN_BUY"] == 'Y' && ($price['VALUE'] < $min_price[0] || $price['DISCOUNT_VALUE'] < $min_price[1])) {
		$min_price[0] = $price['VALUE'];
		$min_price[1] = $price['DISCOUNT_VALUE'];
		$min_price[2] = $code;
	}
}
$arResult['2PRICES']['main'] = $arResult['PRICES'][$min_price[2]];
$arResult['2PRICES']['main']['CODE'] = $min_price[2];

$min_price = array(PHP_INT_MAX, PHP_INT_MAX, '');
foreach ($arParams['OTHER_PRICES'] as $code) {
	$price = $arResult['PRICES'][$code];
	//DISCOUNT_VALUE or VALUE
	if ($price["CAN_ACCESS"] == 'Y' && ($price['VALUE'] < $min_price[0] || $price['DISCOUNT_VALUE'] < $min_price[1])) {
		$min_price[0] = $price['VALUE'];
		$min_price[1] = $price['DISCOUNT_VALUE'];
		$min_price[2] = $code;
	}
}
$arResult['2PRICES']['other'] = $arResult['PRICES'][$min_price[2]];
$arResult['2PRICES']['other']['CODE'] = $min_price[2];

if(!empty($arResult['PROPERTIES']['BRAND_IBLOCK']['VALUE'])) {
    $sert = CIBlockElement::GetList(array(), array("ID" => $arResult['PROPERTIES']['BRAND_IBLOCK']['VALUE']), false, false, array("PROPERTY_SERTIFICATE"))->Fetch();
    if(!empty($sert['PROPERTY_SERTIFICATE_VALUE']))
        $arResult['SERTIFICATE'] = array(
            "small" => Refs::get_resize_src($sert['PROPERTY_SERTIFICATE_VALUE'], 50, 70),
            "big" => Refs::get_resize_src($sert['PROPERTY_SERTIFICATE_VALUE'], 570, 800),
        );
}