<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

function showmenu($data, $first) {
$arMatchesReplace = array(
  "Радиаторы",
  "Стальные трубчатые радиаторы Arbonia",
  "биметалические ",
  "",
  "Электрические накопительные водонагреватели Electrolux серии ",
  "Накопительные водонагреватели ",
  "Внутрипольные конвекторы ",
  "Внутрипольный конвекторт ",
  "Инверторные сплит системы ",
  "Инверторные мульти сплит системы ",
  " со встроенным термовентилем"
);
//table width=770 = 3*250 + 3*2 + 7*2
echo "<table style=\"width: 100%;\" ><tr>";
	if ($first) $root_sid = $data['C'];
	else $root_sid = $data['CP'];
	$i=0;
	foreach($data['H'][$root_sid] as $sid)
	{
		$arSection = $data['SECTIONS'][$sid];
		$i++;

      $arSelect = Array("DETAIL_PICTURE");    
      $arFilter = Array("IBLOCK_ID"=>"1","SECTION_ID"=>$arSection['ID'],"INCLUDE_SUBSECTIONS" => "Y");      
      $resi = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
      $obi = $resi->GetNextElement();
			if($obi==FALSE)
					{$img_src = "http://on-lineclimat.ru/noimage.jpg";
						$description = NULL;
					}
      else {
        $arFieldsi = $obi->GetFields();	        
        $img_src = CFile::ResizeImageGet($arFieldsi["DETAIL_PICTURE"], 210)['src'];
      }
					
      $subCats = 0;
      $rsParentSectionCol = CIBlockSection::GetByID($data['C']);
      if ($arParentSection = $rsParentSectionCol->GetNext())
      {            
        $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],'>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']); // выберет потомков без учета активности
        $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);

        $rsSect->NavStart();
        $subCats = $rsSect->NavRecordCount;
      }    
      
    if (0 < $subCats || $first) {
          
      echo "<td class=\"section-list-item\" ><a class=\"section-list-imgblock\" href=".$arSection['SECTION_PAGE_URL'].">"."<img src =".$img_src." alt=\"".$arSection["NAME"]."\" title=\"".$arSection["NAME"]."\" /></a><a href=\"".$arSection['SECTION_PAGE_URL']."\" class=\"section-list-titleblock\">".$arSection["NAME"]."</a>";
            
      $rsParentSection = CIBlockSection::GetByID($arSection["ID"]);
      if ($data['C'] > 0 && ($arParentSection = $rsParentSection->GetNext()))
      {
        echo "<div class='subsection-linkblock'>";
        $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],'>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL'], '<DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']+2, 'ACTIVE' => 'Y'); // выберет потомков без учета активности
        $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);
        while ($arSect = $rsSect->GetNext())
        {
        	echo '<a href="'. $arSection['SECTION_PAGE_URL'] . $arSect["CODE"] .'/" class="subsection-link">'.str_replace($arMatchesReplace, '', $arSect["NAME"]).'</a>';
        }
        echo "</div>";
      }      
      echo "</td>";
      
      if(bcmod($i,'3')==0)
      {
        echo "</tr><tr>";
      }
      
    }
	}
	
				echo "</tr></table>";
			
    if (!$i && $first && $root_sid!=222) showmenu($data, false);

}

?>

<div class="category clear"><?showmenu($arResult['DATA'], true)?></div>
