<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);?>
<?
    CModule::IncludeModule('iblock');
    $filter = array('IBLOCK_ID'=>$arParams["IBLOCK_ID"]);
    if ($sid = intval($arResult["VARIABLES"]["SECTION_ID"])) {
        $filter['ID'] = $sid;
    } else {
        $filter['CODE'] = $arResult["VARIABLES"]["SECTION_CODE"];
    }
    $select = array('UF_*');
    $res = CIBlockSection::GetList(array('ID'=>'ASC'), $filter, false, $select)->Fetch();
    $res['ENUMS'] = Refs::getXMLIDByEnumId(array($res['UF_LIST_VIEW'], $res['UF_PAGE_VIEW'], $res['UF_USE_COUNT']));
    $view = Refs::getCatalogView(false);
    if (!$view['VIEW']) {
        $view['VIEW'] = $res['UF_LIST_VIEW'] ? $res['ENUMS'][$res['UF_LIST_VIEW']] : $view['VIEW_FULL'][0];
        $view['COUNT'] = $view['COUNT_FULL'][$view['VIEW']];
    }
    $arBasePrice = CCatalogGroup::GetBaseGroup();
    $priceName = "CATALOG_PRICE_".$arBasePrice["ID"];
?>
<div class="line"></div>
<div id="view" class="right">
    <span class="view2 <?if($view['VIEW']=='view1'):?>current<?endif?>"><a data-view="view1" href="#"></a></span>
    <span class="view1 <?if($view['VIEW']=='view2'):?>current<?endif?>"><a data-view="view2" href="#"></a></span>
    <span class="view3 <?if($view['VIEW']=='view3'):?>current<?endif?>"><a data-view="view3" href="#"></a></span>
</div>
<div id="pagination" class="clear"></div>
<div class="line"></div>
<div id="sort2" class="clear">
    <div class="text left"><?=GetMessage("ASTDESIGN_CLIMATE_SORTIROVATQ_PO")?></div>
    <div class="compareList right active"><a href="#" class="show_comparator"><?=GetMessage("ASTDESIGN_CLIMATE_SPISOK_SRAVNENIA")?></a></div>
    <div class="left how">
        <span class="<?if($view['SORT'][0]==$priceName):?>current<?endif?> <?if(($view['SORT'][0]==$priceName && $view['SORT'][1]=='ASC')||$view['SORT'][0]!=$priceName):?>rev<?endif?>">
            <a data-f="<?=$priceName?>" data-s="<?=($view['SORT'][0]==$priceName && $view['SORT'][1]=='ASC')?'DESC':'ASC'?>" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_CENE")?></a>
        </span>
        <span class="<?if($view['SORT'][0]=='NAME'):?>current<?endif?> <?if(($view['SORT'][0]=='NAME' && $view['SORT'][1]=='ASC')||$view['SORT'][0]!='NAME'):?>rev<?endif?>">
            <a data-f="NAME" data-s="<?=($view['SORT'][0]=='NAME' && $view['SORT'][1]=='ASC')?'DESC':'ASC'?>" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_NAZVANIU")?></a>
        </span>
    </div>
</div>
<?
//$view['SORT'][0] = str_replace("PRICE","PRICE_SCALE",$view['SORT'][0]);

$APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    $view['VIEW'],
    Array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ELEMENT_SORT_FIELD" => $view['SORT'][0],
        "ELEMENT_SORT_ORDER" => $view['SORT'][1],
        "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
        "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
        "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
        "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
        "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
        "BASKET_URL" => $arParams["BASKET_URL"],
        "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
        "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
        "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
        "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
        "FILTER_NAME" => $arParams["FILTER_NAME"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_FILTER" => $arParams["CACHE_FILTER"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "SET_TITLE" => $arParams["SET_TITLE"],
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
        "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
        "PAGE_ELEMENT_COUNT" => $view['COUNT'],
        "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
        "PRICE_CODE" => $arParams["PRICE_CODE"],
        "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
        "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
        "SHOW_ALL_WO_SECTION" => "Y",
        "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
        "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
        "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE" => $arParams["PAGER_TITLE"],
        "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"]='Y',
        "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
        "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

        "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
        "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
        "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
        "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
        "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
        "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
        "ADD_SECTIONS_CHAIN" => $is_section ? 'Y' : 'N',
        "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
        "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
        "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
        'CURRENCY_ID' => $arParams['CURRENCY_ID'],
        'SECTION_INFO' => $res,
		"MAIN_PRICES" => $arParams['MAIN_PRICES'],
		"OTHER_PRICES" => $arParams['OTHER_PRICES'],
    ),
    $component
);
?>
<?if ($is_section) {
    global $APPLICATION;
    if ($v = $res['UF_SEC_TITLE']) {
        $APPLICATION->SetPageProperty('title', $v); //TITLE_REWRITE
    } 
    if ($v = $res['UF_SEC_KEYWORDS']) {
        $APPLICATION->SetPageProperty('keywords', $v);
    }
    if ($v = $res['UF_SEC_DESCRIPTION']) {
        $APPLICATION->SetPageProperty('description', $v);
    }
}