<?
$MESS["ASTDESIGN_CLIMATE_RAZRABOTKA_SAYTA"] = "Разработка сайта";
$MESS["ASTDESIGN_CLIMATE_INTERNET_AGENTSTVO"] = "Интернет-агентство";
$MESS["ASTDESIGN_CLIMATE_ASTDIZAYN"] = "«АстДизайн»";
$MESS["ASTDESIGN_CLIMATE_SPISOK_SRAVNENIA"] = "Список сравнения";
$MESS["ASTDESIGN_CLIMATE_BYSTRYY_ZAKAZ"] = "Быстрый заказ";
$MESS["ASTDESIGN_CLIMATE_BYSTRYY_ZAKAZ_SERVICE"] = "Заказать услугу";
$MESS["ASTDESIGN_CLIMATE_IMA"] = "Имя* ";
$MESS["ASTDESIGN_CLIMATE_TELEFON"] = "Телефон* ";
$MESS["ASTDESIGN_CLIMATE_OTPRAVITQ"] = "Отправить";
$MESS["ASTDESIGN_CLIMATE_UVEDOMITQ_O_POSTUPLE"] = "Уведомить о поступлении";
$MESS["ASTDESIGN_CLIMATE_VAS"] = "Ваш";
$MESS["ASTDESIGN_CLIMATE_SPASIBO_KOGDA_TOVAR"] = "Спасибо! Когда товар будет в наличии, мы Вам обязательно сообщим.";
$MESS["ASTDESIGN_CLIMATE_OTPRAVITQ1"] = "отправить";
$MESS["ASTDESIGN_CLIMATE_ETOT_TOVAR_UJE_DOBAV"] = "Этот товар уже добавлен в список ожидания.";
$MESS["ASTDESIGN_CLIMATE_TOVAR_DOBAVLEN"] = "Товар добавлен";
?>