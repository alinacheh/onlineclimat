<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$d=intval($arParams['TOP_DEPTH']); if($d>3) $d=3; elseif ($d<1) $d=1;?>
<script>
$(document).ready(function() {
$('.catalog_left .am-nav-btn').click(function() {
		$(this).toggleClass('open');
		$(this).parents('.category-item-sublist').toggleClass('open'); 
	});
var location = document.location.href;
$('.catalog_left div').each(function () {
 
var link = "http://on-lineclimat.ru"+$(this).find('a').attr('href');
 
if(location == link) $(this).addClass('current');
 
});
$('.catalog_left li').each(function () {
 
var link = "http://on-lineclimat.ru"+$(this).find('a').attr('href');
 
if(location == link) {$(this).addClass('current');
$(this).parents('.category-item-sublist').toggleClass('current'); 
}
 
});
});
</script>
<div class="catalog_left">
    <?foreach($arResult['DATA']['H'][0] as $i=>$sid):
    	$arSection = $arResult['DATA']['SECTIONS'][$sid];?>
        <div class="category-item-wrap">
	        <div class="category-item">
                <!--<img src="<?=Refs::get_resize_src($arSection['PICTURE'], 50, 50);?>" alt="<?=$arSection["NAME"]?>">-->
                <div class="category-item-sublist">
                    <div class="category-item-title">
                        <a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a><a class="am-nav-btn"><span></span></a>
                    </div>
	    	        <?if ($d>1):?>
                        <ul class="category-item-menu">
                            <?foreach($arResult['DATA']['H'][$sid] as $ssid):
	    		                $arSection = $arResult['DATA']['SECTIONS'][$ssid];?>
                                <li class="second-level"><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a>
									<?if ($d>2):?>
										<ul>
											<?foreach($arResult['DATA']['H'][$ssid] as $sssid):
												$arSection2 = $arResult['DATA']['SECTIONS'][$sssid];?>
												<li class="third-level"><a href="<?=$arSection2["SECTION_PAGE_URL"]?>"><?=$arSection2["NAME"]?></a>

                                                    <?if ($d>3):?>
                                                        <ul>
                                                            <?foreach($arResult['DATA']['H'][$sssid] as $ssssid):
                                                                $arSection3 = $arResult['DATA']['SECTIONS'][$ssssid];?>
                                                                <li><a href="<?=$arSection3["SECTION_PAGE_URL"]?>"><?=$arSection3["NAME"]?></a>
                                                                    <?if ($d>4):?>
                                                                        <ul>
                                                                            <?foreach($arResult['DATA']['H'][$ssssid] as $sssssid):
                                                                                $arSection4 = $arResult['DATA']['SECTIONS'][$sssssid];?>
                                                                                <li><a href="<?=$arSection4["SECTION_PAGE_URL"]?>"><?=$arSection4["NAME"]?></a> </li>
                                                                            <?endforeach;?>
                                                                        </ul>
                                                                    <?endif;?>
                                                                </li>
                                                            <?endforeach;?>
                                                        </ul>
                                                    <?endif;?>

                                                </li>
											<?endforeach;?>
										</ul>
									<?endif;?>
                                </li>
                            <?endforeach;?>
                        </ul>
                    <?endif;?>
                </div>
	        </div>
    	</div>
    <?endforeach?>
</div>
