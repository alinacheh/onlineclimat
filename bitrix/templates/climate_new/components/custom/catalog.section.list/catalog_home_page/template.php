<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$d=intval($arParams['TOP_DEPTH']); if($d>3) $d=3; elseif ($d<1) $d=1;?>
<div class="window catalog" style="display:block;">
    <div class="close"></div>
    <?foreach($arResult['DATA']['H'][0] as $i=>$sid):
    	$arSection = $arResult['DATA']['SECTIONS'][$sid];?>
        <div class="category-popup-item-wrap">
	        <div class="category-popup-item">
                <img src="<?=Refs::get_resize_src($arSection['PICTURE'], 50, 50);?>" alt="<?=$arSection["NAME"]?>">
                <div class="category-popup-item-sublist">
                    <div class="category-popup-item-title">
                        <a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a>
                    </div>
	    	        <?if ($d>1):?>
                        <ul class="category-popup-item-menu">
                            <?foreach($arResult['DATA']['H'][$sid] as $ssid):
	    		                $arSection = $arResult['DATA']['SECTIONS'][$ssid];?>
                                <li><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a> (<?=$arSection['ELEMENT_CNT'];?>)
									<?if ($d>2):?>
										<ul>
											<?foreach($arResult['DATA']['H'][$ssid] as $sssid):
												$arSection2 = $arResult['DATA']['SECTIONS'][$sssid];?>
												<li><a href="<?=$arSection2["SECTION_PAGE_URL"]?>"><?=$arSection2["NAME"]?></a> (<?=$arSection2['ELEMENT_CNT'];?>)</li>
											<?endforeach;?>
										</ul>
									<?endif;?>
                                </li>
                            <?endforeach;?>
                        </ul>
                    <?endif;?>
                </div>
	        </div>
    	</div>
    <?endforeach?>
</div>
