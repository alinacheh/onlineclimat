<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
    $(function(){
        $('#review_form').submit(function(){
            var err = false;
            $(this).find('.required').each(function(){
                if (!$(this).val()) {$(this).addClass('error'); err=true;}
                else $(this).removeClass('error');
            });
            if (!err) {
                $.post(SITE_DIR() + 'ajax/addReview.php', $(this).serialize(), function(data){
                    if (data != 'OK') alert(data);
                    else{
                        alert('<?=GetMessage("ASTDESIGN_CLIMATE_THANK_YOU")?>');
                        location.reload();
                    }
                })
            }
            return false;
        })
    })
</script>
<form id="review_form" method="post">
    <h3><?=GetMessage("ASTDESIGN_CLIMATE_OSTAVITQ_OTZYV")?></h3>
    <input type="hidden" name="product" value="<?=$arParams['ITEM']?>" />
    <label class="left"><?=GetMessage("ASTDESIGN_CLIMATE_VASE_IMA")?><span>*</span> <input class="required input" name="name" type="text" value="<?=$USER->GetFullName()?>"></label>
    <label class="right">E-mail: <span>*</span> <input class="required input" name="email" type="text" value="<?=$USER->GetEmail()?>"></label>
    <div class="clear"></div>
    <div class="left"><?=GetMessage("ASTDESIGN_CLIMATE_OCENKA_TOVARA")?><span>*</span></div>
    <div class="left">
        <label><input type="radio" name="rate" value="1"> 1</label>
        <label><input type="radio" name="rate" value="2"> 2</label>
        <label><input type="radio" name="rate" value="3"> 3</label>
        <label><input type="radio" name="rate" value="4"> 4</label>
        <label><input type="radio" name="rate" value="5"> 5</label>
    </div>
    <div class="clear"></div>
    <div class="left"><?=GetMessage("ASTDESIGN_CLIMATE_SOOBSENIE")?><span>*</span></div>
    <textarea name="review" class="required right"></textarea>
    <div class="clear"></div>
    <?global $USER; if(!$USER->IsAuthorized()):
        include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
        $cpt = new CCaptcha();
        $captchaPass = COption::GetOptionString("main", "captcha_password", "");
        if (strlen($captchaPass) <= 0) {
            $captchaPass = randString(10);
            COption::SetOptionString("main", "captcha_password", $captchaPass);
        }
        $cpt->SetCodeCrypt($captchaPass);?>
        <br>
        <div class="left"><?=GetMessage("ASTDESIGN_CLIMATE_UKAJITE_KOD")?><span>*</span></div>
        <input type="hidden" name="captcha_code" value="<?=htmlspecialchars($cpt->GetCodeCrypt());?>">
        <div class="captcha left"><img src="/bitrix/tools/captcha.php?captcha_code=<?=htmlspecialchars($cpt->GetCodeCrypt());?>"></div>
        <input name="captcha_word" class="required captcha input" type="text">
    <?endif?>
    <div class="btn alt right"><button onclick="yaCounter37461485.reachGoal('klik_otzyv');ga('send', 'pageview', '/klik_otzyv'); return true;"><?=GetMessage("ASTDESIGN_CLIMATE_OTPRAVITQ")?></button></div>
    <div class="clear"></div>
</form>