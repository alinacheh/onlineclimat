<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); $arCompare=array()?>
<?
$arTmp = array();
$arCopy = $arResult["SHOW_PROPERTIES"];
foreach($arResult["SHOW_PROPERTIES"] as $k=>$v) $arTmp[$k] = $v["SORT"];
unset($arResult["SHOW_PROPERTIES"]);
asort($arTmp);
foreach($arTmp as $k=>$v) $arResult["SHOW_PROPERTIES"][$k] = $arCopy[$k];
?>
    <div class="left">
        <div class="options">
            <div class="current"><a data-diff="0" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_VSE_PARAMETRY")?></a></div>
            <div><a data-diff="1" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_OTLICAUSIESA")?></a></div>
        </div>
        <table class="info">
            <tr>
                <td data-name="1"><div><b><?=GetMessage("ASTDESIGN_CLIMATE_CENA")?></b></div></td>
            </tr>
            <?$i=2; foreach($arResult["SHOW_PROPERTIES"] as $code=>$arProperty):?>
                <tr class="trow trow<?=$i?>">
                    <td data-name="<?=$i?>"><div><?=$arProperty["NAME"]?></div></td>
                </tr>
            <?$i++; endforeach?>
        </table>
        <div class="clear"></div>
    </div>
    <div class="right">
        <?foreach($arResult["ITEMS"] as $arElement):
			$ar_price = array(PHP_INT_MAX, 'main');
			foreach ($arElement["PRICES"] as $code => $price) {
				if ($price['CAN_BUY'] == 'Y' && $price['DISCOUNT_VALUE'] < $ar_price[0]) {
					$ar_price = array($price['DISCOUNT_VALUE'], $code);
				}
			}
        ?>
            <article>
                <div class="img">
                    <a data-id="<?=$arElement["ID"]?>" class="tableview active reload close compare compare<?=$arElement["ID"]?>" href="#"></a>
                    <a href="<?=$arElement['DETAIL_PAGE_URL']?>"><img src="<?=Refs::get_resize_src($arElement['DETAIL_PICTURE'], 141, 130)?>"></a>
                </div>
                <a href="<?=$arElement['DETAIL_PAGE_URL']?>" class="name"><?=$arElement['NAME']?></a>
                <table class="info">
                    <tr>
                        <td data-name="1"><div><b><?=FormatCurrency($arElement["PRICES"][$ar_price[1]]["DISCOUNT_VALUE"], $arElement["PRICES"][$ar_price[1]]['CURRENCY'])?></b></div></td>
                    </tr>
                    <?$i=2; foreach($arResult["SHOW_PROPERTIES"] as $code=>$arProperty):
                        if ($arElement["DISPLAY_PROPERTIES"][$code]['PROPERTY_TYPE']=='N' && !is_array($arElement["DISPLAY_PROPERTIES"][$code]["VALUE"])) {
                            $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"] = floatval($arElement["DISPLAY_PROPERTIES"][$code]["VALUE"]);
                            if (!$arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]) $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"] = '';
                        }
                        $arPropertyValue = $arElement["DISPLAY_PROPERTIES"][$code]["VALUE"];
                        if(is_array($arPropertyValue)) {
                            sort($arPropertyValue);
                            $arPropertyValue = implode(" / ", $arPropertyValue);
                        }
                        $arCompare[$i][] = $arPropertyValue;?>
                        <tr class="trow trow<?=$i?>">
                            <td data-name="<?=$i?>"><div>
                                <?=(is_array($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])? implode("/ ", $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]): $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])?></div></td>
                        </tr>
                    <?$i++; endforeach?>
                </table>
            </article>
        <?endforeach?>
    </div>

<?$arSame = array(); $arEmpty = array();
if (count($arResult["ITEMS"])>1) foreach($arCompare as $c=>$arr) {
    $same = 1;
    foreach ($arr as $v) if ($v!=$arr[0]) {$same=0;break;}
    if ($same) $arSame[] = $c;
}
foreach($arCompare as $c=>$arr) {
    $same = 1;
    foreach ($arr as $v) if ($v!=$arr[0]) {$same=0;break;}
    if ($same && !$v) $arEmpty[] = $c;
}?>
<script>
    sames = [<?=join(',', $arSame)?>];
    empties = [<?=join(',', $arEmpty)?>];
    for (var i=0; i<empties.length; i++) {
        $('.trow' + empties[i]).hide();
    }
</script>
<?/*$(function(){
    var sames = [<?=join(',', $arSame)?>];
    <?if($_REQUEST['DIFFERENT']=='Y'):?>
        for (var i=0; i<sames.length; i++) {
            $('.trow' + sames[i]).hide();
        }
    <?endif?>
})*/?>
