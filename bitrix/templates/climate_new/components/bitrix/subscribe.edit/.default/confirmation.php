<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//*************************************
//show confirmation form
//*************************************
?>
<form action="<?=$arResult["FORM_ACTION"]?>" method="get">
<div class="subscription-title">
    <b class="r2"></b><b class="r1"></b><b class="r0"></b>
    <div class="subscription-title-inner"><?echo GetMessage("subscr_title_confirm")?></div>
</div>
<div class="subscription-form">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
<tr valign="top">
	<td width="40%">
		<p><?echo GetMessage("subscr_conf_code")?><span class="starrequired">*</span><br />
		<input type="text" name="CONFIRM_CODE" value="<?echo $arResult["REQUEST"]["CONFIRM_CODE"];?>" size="20" /></p>
		<p><?echo GetMessage("subscr_conf_date")?></p>
		<p><?echo $arResult["SUBSCRIPTION"]["DATE_CONFIRM"];?></p>
	</td>
	<td width="60%">
		<?echo GetMessage("subscr_conf_note1")?> <a title="<?echo GetMessage("adm_send_code")?>" href="<?echo $arResult["FORM_ACTION"]?>?ID=<?echo $arResult["ID"]?>&amp;action=sendcode&amp;<?echo bitrix_sessid_get()?>"><?echo GetMessage("subscr_conf_note2")?></a>.
	</td>
</tr>
<tfoot><tr><td colspan="2"><input type="submit" name="confirm" value="<?echo GetMessage("subscr_conf_button")?>" /></td></tr></tfoot>
</table>
<input type="hidden" name="ID" value="<?echo $arResult["ID"];?>" />
<?echo bitrix_sessid_post();?>
<br />
</div>
</form>

