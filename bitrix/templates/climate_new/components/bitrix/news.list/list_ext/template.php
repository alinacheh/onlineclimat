<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$i=0; foreach($arResult["ITEMS"] as $arItem):?>
	<article <?if(!$i):?>class="first"<?endif?>>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
			<span class="date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
		<?endif?>
	    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="title">
	    	<?=$arItem["NAME"]?>
	    </a>
	    <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<p><?=$arItem["PREVIEW_TEXT"];?></p>
		<?endif;?>
	</article>
<?$i++; endforeach?>