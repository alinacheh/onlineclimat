<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<pre class="hide">
		<?
			print_r($arItem);
		?>
	</pre>

	<div class="r-single-review review-<?=$arItem['ID']?>">
		<div class="r-cloud"></div>
		<div class="r-quotes"></div>
		<div class="r-name"><?=$arItem['NAME']?></div>
		<div class="r-rate">
			<?
				for ($x=0; $x<$arItem['PROPERTIES']['R_RATE']['VALUE']; $x++) echo '<div class="rate-star-r checkedStar"></div>';
				for ($x=0; $x<(5-$arItem['PROPERTIES']['R_RATE']['VALUE']); $x++) echo '<div class="rate-star-r"></div>';
			?>
				<?//=$arItem['PROPERTIES']['R_RATE']['VALUE']?>

			
		</div>
		<div class="r-review-text"><?=$arItem['PREVIEW_TEXT']?></div>
	</div>
	<div class="r-answer">
		<div class="r-str-answer"></div>
		<div class="r-answer-text">
			<div class="bold">Представитель компании</div>
				<div class="r-a-t"><?=$arItem['PROPERTIES']['R_ANSWER']['VALUE']['TEXT']?></div>
		</div>
	</div>


	
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
