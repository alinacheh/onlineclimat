<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
IncludeTemplateLangFile(__FILE__);
$is_section = true;
if (empty($arResult["VARIABLES"])) {
    $is_section = false;
}
$sid = Refs::getSectionByCode($arResult["VARIABLES"]['SECTION_CODE'], $arParams["IBLOCK_ID"]);
$APPLICATION->SetPageProperty("SECTION_ID", $sid);


?>
<?
$this->SetViewTarget('new_filter');
$APPLICATION->IncludeComponent(
    "bitrix:catalog.smart.filter",
    "visual_vertical",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "TEMPLATE_THEME" => "blue",
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "SECTION_ID" => $sid,
        "SECTION_CODE" => "",
        "FILTER_NAME" => "arrFilter",
        "HIDE_NOT_AVAILABLE" => "N",
        "SEF_MODE" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_GROUPS" => "Y",
        "SAVE_IN_SESSION" => "N",
        "INSTANT_RELOAD" => "Y",
        "PAGER_PARAMS_NAME" => "arrPager",
        "PRICE_CODE" => $arParams["PRICE_CODE"],
        "CONVERT_CURRENCY" => "Y",
        "XML_EXPORT" => "N",
        "SECTION_TITLE" => "",
        "SECTION_DESCRIPTION" => "",
        "POPUP_POSITION" => "left",
        "SEF_RULE" => "/examples/books/#SECTION_ID#/filter/#SMART_FILTER_PATH#/apply/",
        "SECTION_CODE_PATH" => "",
        "SMART_FILTER_PATH" => $_REQUEST["SMART_FILTER_PATH"],
        "CURRENCY_ID" => $arParams['CURRENCY_ID']
    ),
    $component
);
$this->EndViewTarget();
?>


<br>

<?
if ($is_section) {
    $_section = CIBlockSection::GetByID($sid)->GetNext();
    $_section = CIBlockSection::GetList([], ['ID' => $sid, 'IBLOCK_ID' => $arParams["IBLOCK_ID"]], false,
        ['DEPTH_LEVEL', 'UF_*'])->GetNext();

    $count = CIBlockSection::GetList(array(),
        array(
            'IBLOCK_ID' => $arParams["IBLOCK_ID"],
            'SECTION_ID' => $sid
        ))->SelectedRowsCount(); //<<< тут явно что то так, но все работает

//по тупому
    if (($_section['DEPTH_LEVEL'] >= 2 && $count > 0) && !$_section['UF_SECT_VIEW_NORM']) {
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "",
            Array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "SECTION_ID2" => $arResult["VARIABLES"]["SECTION_ID"],
                "SECTION_CODE2" => $arResult["VARIABLES"]["SECTION_CODE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
                "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
                "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
            ),
            $component
        );
        include "clear_section.php";
    } else {
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "normal_view",
            Array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "SECTION_ID2" => $arResult["VARIABLES"]["SECTION_ID"],
                "SECTION_CODE2" => $arResult["VARIABLES"]["SECTION_CODE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
                "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
                "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
            ),
            $component
        );
        include "_section.php";
    }

    $time = time();
    $sesTime = (int)$_SESSION['AST_POPUP_' . $sid];

    $ten = 10 * 60;
    $ses = false;
    if($time - $sesTime >= $ten){
        $ses = true;
    }

    if ($_section['UF_POPUP_ACTIVE'] && $ses) {
//    if ($_section['UF_POPUP_ACTIVE']) {
        $arPopUp = [
            'hello' => trim($_section['~UF_POPUP_HELLO']),
            'header' => trim($_section['~UF_POPUP_HEADER']),
            'message' => trim($_section['~UF_POPUP_MSG']),
        ];


        if ($_section['UF_POPUP_IMG']) {
            $arPopUp['background'] = CFile::GetPath($_section['UF_POPUP_IMG']);
        }
    }


}


?>

<? if (!empty($arPopUp)): ?>
    <div id="ast_sect_popup">
        <div id="ast_sect_popup_bg"></div>
        <div class="ast_sect_popup_close"></div>
        <div id="ast_sect_popup_window"
             style="<?= $arPopUp['background'] ? 'background-image: url(' . $arPopUp['background'] . ')' : ''; ?>">
            <form method="post" action="#">
                <div class="MRP_HEADER"><?= $arPopUp['hello'] ?></div>
                <div class="P_BODY">
                    <div class="MRP_SUBJECT"><?= $arPopUp['header'] ?></div>
                    <div class="MRP_MESSAGE"><?= $arPopUp['message'] ?></div>
                </div>
                <div class="PROPS" style="color: rgb(255, 255, 255);">
                    <div class="mrp_input">
                        <input id="MRP_PLACEHOLDER" required="required" type="text"
                               name="phone" placeholder="Телефон">
                    </div>
                    <div class="mrp_input">
                        <input id="MRP_PROP_1" type="text" name="name" placeholder="Имя">
                    </div>
                    <div class="MRP_BUTTON">
                        <button type="submit" name="MRP_BUTTON_SEND" value="1"
                                style="background-color: rgb(255, 229, 84); color: rgb(0, 0, 0);">Отправить
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <script>
        $(document).ready(function () {

            var height = $('html').height() / 3;

            $(window).scroll(function () {
                if ($(this).scrollTop() >= height) {
                    $('#ast_sect_popup').fadeIn(1000);
                }
            })

            $('.ast_sect_popup_close, #ast_sect_popup_bg').on('click', function () {
                $('#ast_sect_popup').fadeOut(1000);
                $.ajax({
                    type: 'POST',
                    url: '/ajax/setSess.php',
                    data: {'nameSess': '<?='AST_POPUP_' . $sid?>'}
                })
                setTimeout(function () {
                    $('#ast_sect_popup').remove();
                }, 1500);

            })

            $("#ast_sect_popup_window form").submit(function (e) {
                e.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: '/ajax/callMe.php',
                    data: $("#ast_sect_popup_window form").serialize(),
                    dataType: 'html',
                    success: function () {
                        $('.ast_sect_popup_close').click();
                    }
                })
            })
        })
    </script>
<? endif; ?>
