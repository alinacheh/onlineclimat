<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $USER;
if ($USER->IsAdmin()) {
    //echo '<pre>';
    foreach ($arResult['SECTIONS'] as $item) {
        if ($item['DEPTH_LEVEL'] == 4) {
            //echo $item['NAME'];
            //echo '<br>';
        }
    }
}
function showmenu($data, $first)
{
    $arMatchesReplace = array(
        "Радиаторы",
        "Стальные трубчатые радиаторы Arbonia",
        "биметалические ",
        "",
        "Электрические накопительные водонагреватели Electrolux серии ",
        "Накопительные водонагреватели ",
        "Внутрипольные конвекторы ",
        "Внутрипольный конвекторт ",
        "Инверторные сплит системы ",
        "Инверторные мульти сплит системы ",
        " со встроенным термовентилем"
    );
//table width=770 = 3*250 + 3*2 + 7*2
    echo "<table style=\"width: 100%;\" ><tr>";
    global $USER;
    if ($USER->IsAdmin()) {
        echo "</tr>";
    }
    if ($first) $root_sid = $data['C'];
    else $root_sid = $data['CP'];
    $i = 0;
    foreach ($data['H'][$root_sid] as $sid) {
        $arSection = $data['SECTIONS'][$sid];
        $i++;

        $arSelect = Array("DETAIL_PICTURE");
        $arFilter = Array("IBLOCK_ID" => "1", "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y");
        $resi = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        $obi = $resi->GetNextElement();
        if ($obi == FALSE) {
            $img_src = "http://on-lineclimat.ru/noimage.jpg";
            $description = NULL;
        } else {
            $arFieldsi = $obi->GetFields();
            $img_src = CFile::ResizeImageGet($arFieldsi["DETAIL_PICTURE"], 210)['src'];
        }

        $subCats = 0;
        $rsParentSectionCol = CIBlockSection::GetByID($data['C']);
        if ($arParentSection = $rsParentSectionCol->GetNext()) {
            $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'], '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'], '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'], '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']); // выберет потомков без учета активности
            $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter);

            $rsSect->NavStart();
            $subCats = $rsSect->NavRecordCount;
        }

        if (0 < $subCats || $first) {
            global $USER;
            if ($USER->IsAdmin() && ($arSection['DEPTH_LEVEL'] == 3 || $arSection['DEPTH_LEVEL'] == 4)) { ?>

                <? echo "    <tr><td class=\"ast-section-list-item section-list-item\" ><div class=\"  series-img-block\"><a class=\"ast-section-list-imgblock section-list-imgblock\" style=''  tag='1' href=" . $arSection['SECTION_PAGE_URL'] . ">" . "<img src =" . $img_src . " alt=\"" . $arSection["NAME"] . "\" title=\"" . $arSection["NAME"] . "\" /></a> </div>";
                // image
                echo " <a style='display: inline-block; ' href=\"" . $arSection['SECTION_PAGE_URL'] . "\" class=\"ast-section-list-titleblock section-list-titleblock\">" . $arSection["NAME"] . "</a>";
                // name
                echo "<div style='display: inline-block; ' class='ast-product-text product-text'>" . $arSection["DESCRIPTION"] . "</div>";
                //descr
                $arFilter = Array(
                    'IBLOCK_ID' => 1,
                    "SECTION_ID" => $arSection['ID'],
                    "ACTIVE" => "Y",
                    'INCLUDE_SUBSECTIONS' => 'Y'
                );
                $arSelects = Array("ID", 'NAME','*', 'PRICE', 'DETAIL_PAGE_URL', 'PROPERTY_SS_POWER', 'PROPERTY_S_AREA', 'IBLOCK_SECTION_ID');
                $res = CIBlockElement::GetList(Array("SORT" => "ASC", "PROPERTY_PRIORITY" => "ASC"), $arFilter, false, Array("nTopCount" => 30), $arSelects);
                //начало таблицы сравнений
                echo '<table  class=\'table-view\'><tr><th>Название</th><th>Мощность, кВт</th><th>Площадь, м<sup>2</sup></th><th class="center">Цена, руб.</th>';
                $i = 0;
                while ($ar_fields = $res->GetNext()) {
                    $i++;
                    //echo  '<pre>';
                    //print_r($ar_fields);
                    $ar_price = GetCatalogProductPrice($ar_fields['ID'], 1);
                    if (isset($ar_price['CURRENCY']) && $ar_price['CURRENCY'] != "RUB") $ar_price['PRICE'] = CCurrencyRates::ConvertCurrency($ar_price['PRICE'], $ar_price["CURRENCY"], "RUB");
                    $price = $ar_price['PRICE'];
                    echo "<tr><td><a style='font-size: 12px;' href='" . $ar_fields['DETAIL_PAGE_URL'] . "'>" . $ar_fields['NAME'] . "</td><td class=\"center\">" . $ar_fields['PROPERTY_SS_POWER_VALUE'] . "</td><td class=\"center\">" . $ar_fields['PROPERTY_S_AREA_VALUE'] . "</td><td class=\"center\">" . $price . "</td></tr>";
                }
                echo '</table>'; ?>



                <?if( $i>3):?>
                <div class="button-prices-more">Показать больше товаров</div>
                    <?endif;?>
            <? } else {
                echo "<td class=\"section-list-item\" ><a class=\"section-list-imgblock\" tag='1' href=" . $arSection['SECTION_PAGE_URL'] . ">" . "<img src =" . $img_src . " alt=\"" . $arSection["NAME"] . "\" title=\"" . $arSection["NAME"] . "\" /></a><a href=\"" . $arSection['SECTION_PAGE_URL'] . "\" class=\" section-list-titleblock\">" . $arSection["NAME"] . "</a>";
            }
            $rsParentSection = CIBlockSection::GetByID($arSection["ID"]);
            if (!$USER->IsAdmin() && $arSection['DEPTH_LEVEL'] != 3 && $data['C'] > 0 && ($arParentSection = $rsParentSection->GetNext())) {
                echo "<div class='subsection-linkblock'>";
                $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'], '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'], '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'], '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL'], '<DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL'] + 2, 'ACTIVE' => 'Y'); // выберет потомков без учета активности
                $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter);
                while ($arSect = $rsSect->GetNext()) {
                    echo '<a href="' . $arSection['SECTION_PAGE_URL'] . $arSect["CODE"] . '/" class="subsection-link">' . str_replace($arMatchesReplace, '', $arSect["NAME"]) . '</a>';
                }
                echo "</div>";
            }
            if ($USER->IsAdmin() && ($arSection['DEPTH_LEVEL'] == 3 || $arSection['DEPTH_LEVEL'] == 4)) {
                echo "</tr> ";
            } else {
                echo "</td>";
            }


            if (bcmod($i, '3') == 0) {
                if ($USER->IsAdmin() && ($arSection['DEPTH_LEVEL'] == 3 || $arSection['DEPTH_LEVEL'] == 4)) {
                    echo "</td>";
                } else {
                    echo "</tr><tr>";
                }
            }

        }
    }

    echo "</tr></table>";

    if (!$i && $first && $root_sid != 222) showmenu($data, false);

}

?>

<script>

    $(document).ready(function () {
        /*        var blocks =$("div.wr-hid:gt(2)");
         blocks.hide();*/

        $('.section-list-item').each(function (i, el) {
            /*console.log(el);*/
            /* if (!$(el).hasClass('clearfix')) {*/
            $(el).find('.table-view>tbody>tr:gt(4)').hide();
            /* }*/
        });
        var flag = true;
        $(document).on('click', '.button-prices-more', function (e) {
            var text = '';
            flag ? text = 'Показать меньше товаров' : text = 'Показать больше товаров';
            $(e.target).html(text);
            if (flag) {
                $(this).parent().find(".table-view>tbody>tr:gt(4)").show();
            }
            else {
                $(this).parent().find(".table-view>tbody>tr:gt(4)").hide();
            }
            flag ? flag = false : flag = true;
        });
    });

</script>
<div class="category clear"><? showmenu($arResult['DATA'], true) ?></div>
