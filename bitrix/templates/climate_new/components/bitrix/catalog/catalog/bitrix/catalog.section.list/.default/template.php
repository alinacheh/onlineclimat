<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

function showmenu($data, $first)
{
    $arMatchesReplace = array(
        "Радиаторы",
        "Стальные трубчатые радиаторы Arbonia",
        "биметалические ",
        "",
        "Электрические накопительные водонагреватели Electrolux серии ",
        "Накопительные водонагреватели ",
        "Внутрипольные конвекторы ",
        "Внутрипольный конвекторт ",
        "Инверторные сплит системы ",
        "Инверторные мульти сплит системы ",
        " со встроенным термовентилем"
    );


    ?>

    <?
    if ($first) {
        $root_sid = $data['C'];
    } else {
        $root_sid = $data['CP'];
    };
    echo '<div style="margin-bottom:10px;">';
    foreach ($data['H'][$root_sid] as $sid):
        $arSection = $data['SECTIONS'][$sid];
        if ($arSection['SHOW_BRAND']):?>
            <a style='display: inline-block !important; margin-right: 9px; margin-bottom: 10px;' href="<?= $arSection['SECTION_PAGE_URL'] ?>"
               class=" how-button ast-section-list-titleblock section-list-titleblock"><?= $arSection["NAME"] ?></a>
        <?endif ?>

    <?endforeach; ?>
    <div style="clear:both"></div>
    <?
    echo '</div>';
    echo "<table style=\"width: 100%;\" ><tr>";

        echo "</tr>";

    if ($first) {
        $root_sid = $data['C'];
    } else {
        $root_sid = $data['CP'];
    }
    $i = 0;
    foreach ($data['H'][$root_sid] as $sid) {
        $arSection = $data['SECTIONS'][$sid];
        $i++;
//var_dump('<pre>',$arSection,'</pre>');
//die();

        if(!$arSection['PICTURE']['ID']){
            $arSelect = Array("DETAIL_PICTURE");
            $arFilter = Array("IBLOCK_ID" => "1", "SECTION_ID" => $arSection['ID'], "INCLUDE_SUBSECTIONS" => "Y");
            $resi = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            $obi = $resi->GetNextElement();
            if ($obi == false) {
                $img_src = "http://on-lineclimat.ru/noimage.jpg";
                $description = null;
            } else {
                $arFieldsi = $obi->GetFields();
                $img_src = CFile::ResizeImageGet($arFieldsi["DETAIL_PICTURE"], 210)['src'];
            }
        }else{
            $img_src = CFile::ResizeImageGet($arSection['PICTURE']['ID'], 210)['src'];
        }


        $subCats = 0;
        $rsParentSectionCol = CIBlockSection::GetByID($data['C']);
        if ($arParentSection = $rsParentSectionCol->GetNext()) {
            $arFilter = array(
                'IBLOCK_ID' => $arParentSection['IBLOCK_ID'],
                '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
                '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],
                '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']
            ); // выберет потомков без учета активности
            $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter);

            $rsSect->NavStart();
            $subCats = $rsSect->NavRecordCount;
        }

        if (0 < $subCats || $first) {

            if ($arSection['DEPTH_LEVEL'] >=3) : ?>

                <tr>
 <td class="ast-section-list-item section-list-item" >

 <div class="series-img-block">

 <a class="ast-section-list-imgblock section-list-imgblock" style=''  tag='1' href="<?=$arSection['SECTION_PAGE_URL']?> ">
 <img src ="<?=$img_src?>" alt="<?=$arSection["NAME"]?>"title="<?=$arSection["NAME"]?>" />
 </a>
 </div>
<?// image?>

         <a style='display: inline-block;' href="<?=$arSection['SECTION_PAGE_URL']?>" class="ast-section-list-titleblock section-list-titleblock"><?=$arSection["NAME"]?></a>
<?// name?>
               <div style='display: inline-block;' class='ast-product-text product-text'><?=$arSection["DESCRIPTION"]?></div> 
<?//descr?>
            <?
                $arFilter = Array(
                    'IBLOCK_ID' => 1,
                    "SECTION_ID" => $arSection['ID'],
                    "ACTIVE" => "Y",
                    'INCLUDE_SUBSECTIONS' => 'Y'
                );
                $arSelects = Array(
                    "ID",
                    'NAME',
                    '*',
                    'PRICE',
                    'DETAIL_PAGE_URL',
                    'IBLOCK_SECTION_ID'
                );
                foreach ($arSection['ELEMENT_PROP']['PROPID'] as $propID) {
                    array_push($arSelects, 'PROPERTY_' . $propID);
                }

                $res = CIBlockElement::GetList(Array("SORT" => "ASC", "PROPERTY_PRIORITY" => "ASC"), $arFilter, false,
                    Array("nTopCount" => 30), $arSelects);
//начало таблицы сравнений
                ?>
                <table class='table-view'>
                    <tr>
                        <th>Название</th>
                        <? foreach ($arSection['ELEMENT_PROP']['NAME'] as $name): ?>
                            <th><?= $name ?></th>
                        <? endforeach; ?>
                        <th class="center">Цена, руб.</th>
                    </tr>

                    <?

                    $elementCount = $res->SelectedRowsCount();
                    while ($ar_fields = $res->GetNext()):
                        $ar_price = GetCatalogProductPrice($ar_fields['ID'], 1);
                        if (isset($ar_price['CURRENCY']) && $ar_price['CURRENCY'] != "RUB") {
                            $ar_price['PRICE'] = CCurrencyRates::ConvertCurrency($ar_price['PRICE'],
                                $ar_price["CURRENCY"],
                                "RUB");
                        }
                        $price = CCurrencyLang::CurrencyFormat($ar_price['PRICE'],'RUB');
//                        $price = $ar_price['PRICE'];

                        ?>
                        <tr>
                            <td><a style='font-size: 12px;'
                                   href='<?= $ar_fields['DETAIL_PAGE_URL'] ?>'><?= $ar_fields['NAME'] ?>
                            </td>
                            <? foreach ($arSection['ELEMENT_PROP']['NAME'] as $k => $name): ?>
                                <td class="center"><?= $ar_fields["PROPERTY_{$k}_VALUE"] ?></td>
                            <? endforeach; ?>

                            <td class="center"><?= $price ?></td>
                        </tr>
                    <? endwhile; ?>
                </table>


                <? if ($elementCount > 4): ?>
                    <div class="button-prices-more">Показать больше товаров</div>
                <? endif; ?>


            <? else: ?>
                <td class="section-list-item">
                <a class="section-list-imgblock" tag='1' href="<?= $arSection['SECTION_PAGE_URL'] ?> ">
                    <img src="<?= $img_src ?>" alt="<?= $arSection["NAME"] ?>" title="<?= $arSection["NAME"] ?>"></a>
                <a href="<?= $arSection['SECTION_PAGE_URL'] ?>"
                   class="section-list-titleblock"><?= $arSection["NAME"] ?></a>;
            <? endif; ?>
            <?

            if ($arSection['DEPTH_LEVEL'] >=3) :?>
               </tr>
            <? else:?>
                </td>
            <?endif; ?>

            <? if (bcmod($i, '3') == 0): ?>
               <? if ($arSection['DEPTH_LEVEL'] >=3):?>
                   </td>
                <?else:?>
                    </tr><tr>
                <?endif;?>
            <?endif;?>
<?
        }
    }

?>
</tr>
    </table>
<?
    if (!$i && $first && $root_sid != 222) {
        showmenu($data, false);
    }

}

?>

<script>
    $(document).ready(function () {
        $('.section-list-item').each(function (i, el) {
            $(el).find('.table-view>tbody>tr:gt(4)').hide();
        });
        $(document).on('click', '.button-prices-more', function (e) {
       if($(this).html() == 'Показать больше товаров'){
           $(this).html('Показать меньше товаров')
       }else{
           $(this).html('Показать больше товаров')
       }
                $(this).parent().find(".table-view>tbody>tr:gt(4)").toggle();
        });
    });
</script>
<div class="category clear"><? showmenu($arResult['DATA'], true) ?></div>
