<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arTemplateParameters = array(
	"COMMENTS_BLOCK_ID"=>array(
		"NAME" => "COMMENTS_BLOCK_ID",
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	"SECTION_INFO"=>array(
		"NAME" => "SECTION_INFO",
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
);
?>