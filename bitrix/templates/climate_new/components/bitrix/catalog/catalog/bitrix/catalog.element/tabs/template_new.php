<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?
if ($arResult['2PRICES']['main']['DISCOUNT_VALUE'] < $arResult['2PRICES']['main']['VALUE']) {
    $p1 = FormatCurrency($arResult['2PRICES']['main']['DISCOUNT_VALUE'], $arResult['2PRICES']['main']['CURRENCY']);
    $p3 = FormatCurrency($arResult['2PRICES']['main']['VALUE'], $arResult['2PRICES']['main']['CURRENCY']);
} else {
    $p1 = FormatCurrency($arResult['2PRICES']['main']['VALUE'], $arResult['2PRICES']['main']['CURRENCY']);
    $p3 = 0;
}
if (CBXFeatures::IsFeatureEnabled('CatMultiPrice') && $arResult['PRICES']['other']['VALUE']) {
    $p2 = FormatCurrency($arResult['2PRICES']['other']['VALUE'], $arResult['2PRICES']['other']['CURRENCY']);
} else {
    $p2 = 0;
}
if (isset($arResult['DISPLAY_PROPERTIES']['FILES']["FILE_VALUE"]['ID'])) {
    $arResult['DISPLAY_PROPERTIES']['FILES']["FILE_VALUE"] = array($arResult['DISPLAY_PROPERTIES']['FILES']["FILE_VALUE"]);
}
?>
<script>
    function submitwith(p) {
        $('#hidden_param_basket').attr('name', '<?=$arParams["ACTION_VARIABLE"]?>' + p).parents('form').submit();
        return false;
    }
</script>
<div class="line"></div>

<!-- COL LEFT -->
<div class="col-left left" style="width:820px;">
	<div id="tovar" class="productBlock">
		<div class="box left">
			<div class="left">
				<div class="img big">
					<a href="<?= $arResult['IMAGES'][0]['B']['SRC'] ?>"><img src="<?= $arResult['IMAGES'][0]['B']['SRC'] ?>"
																			title="<?= $arResult["NAME"] ?>"
																			alt="<?= $arResult["NAME"] ?>"></a>
					<div class="labels">
						<? if ($p3): ?>
							<div class="discount">
								<?= GetMessage("ASTDESIGN_CLIMATE_SKIDKA") ?><b></b></div>
						<? endif ?>
						<? if ($arResult['PROPERTIES']['IS_SPECIAL']['VALUE']): ?>
							<div class="discount">
								<?= GetMessage("ASTDESIGN_CLIMATE_SPEC") ?><b></b></div>
						<? endif ?>
						<? if ($arResult['PROPERTIES']['IS_POP']['VALUE']): ?>
							<div class="hit">
								<?= GetMessage("ASTDESIGN_CLIMATE_HIT") ?><b></b></div>
						<? endif ?>
						<? if ($arResult['PROPERTIES']['IS_NEW']['VALUE']): ?>
							<div class="new">
								<?= GetMessage("ASTDESIGN_CLIMATE_NOVINKA") ?><b></b></div>
						<? endif ?>
					</div>
				</div>
			</div>
			<ul class="images jcarousel-skin-tango">
				<? foreach ($arResult['IMAGES'] as $i => $img): ?>
					<li class="img <? if (!$i): ?>current<? endif ?>"><a href="<?= $img['B']['SRC'] ?>"
																		data-src="<?= $img['M']['SRC'] ?>"><img
									src="<?= $img['S']['SRC'] ?>" height="65" width="70" title="<?= $arResult["NAME"] ?>"
									alt="<?= $arResult["NAME"] ?>"></a></li>
				<? endforeach ?>
			</ul>
			
			<div class="soc">
				<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
				<script src="//yastatic.net/share2/share.js"></script>
				<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter" style="margin-top: 5px;"></div>
			</div>
		</div>
		<div class="box2 left" style="width:350px !important;">
			
			<div class="info1">
				<div class="articul left"><span>Код товара: <?=$arResult['ID']?></span></div>
				
				<? if ($arResult['CATALOG_QUANTITY'] > 0): ?>
					<div class="nal right yes">
						<?= GetMessage("ASTDESIGN_CLIMATE_ESTQ_V_NALICII") ?>
					</div>
				<? else: ?>
					<? if ($arResult['CATALOG_CAN_BUY_ZERO'] == 'Y'): ?>
						<div class="nal right no podzakaz">
							<?= GetMessage("ASTDESIGN_CLIMATE_POD_ZAKAZ") ?>
						</div>
					<? else: ?>
						<div class="nal right no nenal">
							<?= GetMessage("ASTDESIGN_CLIMATE_NET_V_NALICII") ?>
						</div>
					<? endif ?>
				<? endif ?>
			</div>
			
			<div class="title">
				<h1><?= $arResult["NAME"] ?></h1>
				<? if ($arResult['PROPERTIES']['ARTICUL']['VALUE']): ?>
					<div class="articul">
						<?= GetMessage("ASTDESIGN_CLIMATE_ARTIKUL") ?>
						<?= $arResult['PROPERTIES']['ARTICUL']['VALUE'] ?>
					</div>
				<? endif ?>
			</div>
	

			<? if ($arResult['CATALOG_QUANTITY'] > 0): ?>
				<div class="priceBlock">
					<div class="price">
						<?/*= GetMessage("ASTDESIGN_CLIMATE_CENA") */?><span><?= $p1 ?></span></div>
					<? if ($p2): ?>
						<div class="price rozn">
							<?= $arResult["CAT_PRICES"][$arResult['2PRICES']['other']['CODE']]['TITLE'] ?>
							:<span><?= $p2 ?></span></div>
					<? endif ?>
					<? if ($p3): ?>
						<div class="price old">
							<?= GetMessage("ASTDESIGN_CLIMATE_STARAA_CENA") ?><span><?= $p3 ?></span></div>
					<? endif ?>
					<? if (!empty($arResult['SERTIFICATE'])): ?>
						<div class="sertificate-detail">
							<a href="<?= $arResult['SERTIFICATE']['big']; ?>"><img
										src="<?= $arResult['SERTIFICATE']['small']; ?>" alt="Сертикифат"></a>
						</div>
					<? endif; ?>

					<div class="countBlock right countbasket<?= $arResult['ID'] ?>">
						<div class="pm right">
							<a href="#" class="p"></a>
							<a href="#" class="m"></a>
						</div>
						<input type="text" class="count right" name="<? echo $arParams[" PRODUCT_QUANTITY_VARIABLE "] ?>"
							value="1" size="5">
						<a data-id="<?= $arResult['ID'] ?>" href="#" class="cart left"></a>
					</div>
				</div>
				<?/*<p class="countbasket<?= $arResult['ID'] ?>">
					<?= GetMessage("ASTDESIGN_CLIMATE_KOLICESTVO") ?>
				</p>*/ ?>
				<div class="actionBlock clear">
					<div class="btn alt left">
						<a onclick="ga('send', 'pageview', '/bystryj_zakaz');" href="#" data-pid='<?= $arResult['ID'] ?>:1'
						class="qorder">
							<?= GetMessage("ASTDESIGN_CLIMATE_BYSTRYY_ZAKAZ") ?><b></b></a>
					</div>
					<div class="btn buy right">
						<a href="#" data-class="actionBlock" data-id="<?= $arResult['ID'] ?>"
						class="blockview add2basketq add2basket<?= $arResult['ID'] ?>"
						onclick="yaCounter37461485.reachGoal('klik_kupit');ga('send', 'pageview', '/klik_kupit');return submitwith('ADD2BASKET');">
							<?= GetMessage("CATALOG_ADD_TO_BASKET") ?><b></b></a>
					</div>
				</div>
			<? elseif ($arResult['CATALOG_CAN_BUY_ZERO'] == 'Y'): ?>
				<div class="priceBlock">
					<div class="price">
						<?= GetMessage("ASTDESIGN_CLIMATE_CENA") ?><span><?= $p1 ?></span></div>
					<? if ($p2): ?>
						<div class="price rozn">
							<?= $arResult["CAT_PRICES"][$arResult['2PRICES']['other']['CODE']]['TITLE'] ?>
							:<span><?= $p2 ?></span></div>
					<? endif ?>
					<? if ($p3): ?>
						<div class="price old">
							<?= GetMessage("ASTDESIGN_CLIMATE_STARAA_CENA") ?><span><?= $p3 ?></span></div>
					<? endif ?>
				</div>
				<div class="actionBlock clear">
					<div class="btn buy left viewP2">
						<a href="#" data-class="actionBlock" data-id="<?= $arResult['ID'] ?>"
						class="viewP2 add2basket add2basket<?= $arResult['ID'] ?>"
						onclick="return submitwith('ADD2BASKET');">
							<?= GetMessage("ASTDESIGN_CLIMATE_ZAKAZATQ") ?><b></b></a>
					</div>
				</div>
			<? else: ?>
				<div class="actionBlock clear">
					<div class="left clear">
						<span class="wait btn"><a data-id="<?= $arResult["ID"] ?>"
												class="notify notify<?= $arResult["ID"] ?>"
												href="#"><?= GetMessage("ASTDESIGN_CLIMATE_A_JDU") ?></a></span>
					</div>
				</div>
			<? endif ?>

			<div class="line"></div>
			
			<div class="compareBlock">
				<div class="compareList left active">
					<a href="#" class="show_comparator">
						<?= GetMessage("ASTDESIGN_CLIMATE_SPISOK_SRAVNENIA") ?>
					</a>
				</div>
				
				<div class="compareList right">
					<label class="add">
						<input type="checkbox" data-id="<?= $arResult[" ID "] ?>"
							class="compare compare<?= $arResult[" ID "] ?>">
						<?= GetMessage("ASTDESIGN_CLIMATE_SRAVNITQ") ?>
					</label>
				</div>
			</div>
			
		</div>
		<div class="clear"></div>
		<div class="viewBlock" id="setEview">
			<span class="current first"><a href="#" data-t="tabs"><?= GetMessage("ASTDESIGN_CLIMATE_VKLADKI") ?></a></span>
			<span class="last"><a href="#" data-t="plain"><?= GetMessage("ASTDESIGN_CLIMATE_SPISOK") ?></a></span>
		</div>
		<div class="clear"></div>
	</div>
	
	<?/*<div class="line"></div>*/?>
	
	<div id="tabs_wrapper">
		<div class="category clear">
			<? if ($arResult['DETAIL_TEXT']): ?>
				<span class="current tabs"><a class="tabs" id="tab0"
											href="#atab0"><?= GetMessage("ASTDESIGN_CLIMATE_OPISANIE") ?></a></span>
			<? endif ?>
			<? if (count($arResult["SECTION"]['UF_ACC'])): ?><span class="tabs"><a class="tabs" id="tab2"
																				href="#atab2"><?= GetMessage("ASTDESIGN_CLIMATE_AKSESSUARY") ?></a>
				</span>
			<? endif ?>
			<span class="tabs"><a class="tabs" id="tab3"
								href="#atab3"><?= GetMessage("ASTDESIGN_CLIMATE_OTZYVY") ?></a></span>
			<? if (CBXFeatures::IsFeatureEnabled('CatMultiStore')): ?><span class="tabs"><a class="tabs" id="tab4"
																							href="#atab4"><?= GetMessage("ASTDESIGN_CLIMATE_NALICIE_V_MAGAZINAH") ?></a>
				</span>
			<? endif ?>
			<? if (count($arResult['DISPLAY_PROPERTIES']['FILES']["FILE_VALUE"])): ?><span class="tabs"><a class="tabs"
																										id="tab5"
																										href="#atab5"><?= GetMessage("ASTDESIGN_CLIMATE_FAYLY") ?></a>
				</span>
			<? endif ?>
		</div>





		<div class="tratata" style="border: 1px solid #ccc; border-radius: 5px; padding: 0px 25px 25px 25px;">
			<h2 class="ma"> <?= GetMessage("ASTDESIGN_CLIMATE_OPISANIE1") ?></h2>

			<? if ($arResult['PREVIEW_TEXT']): ?>
				<?= $arResult['PREVIEW_TEXT'] ?>
			<? endif ?>
			<?= $arResult['DETAIL_TEXT'] ?>
		</div>

		<? if (count($arResult["SECTION"]['UF_ACC'])): ?>
			<div class="clear"></div>
			<div class="tabs con_tab" id="con_tab2">
				<a name="atab2"></a>
				<h2 class="ma"><?= GetMessage("ASTDESIGN_CLIMATE_AKSSESUARY") ?><?/*= $arResult['NAME'] */?></h2>
				<? $GLOBALS['arrFilter']['SECTION_ID'] = $arResult["SECTION"]['UF_ACC']; ?>
				<? $APPLICATION->IncludeComponent(
					"bitrix:catalog.section",
					'viewAccessories',
					Array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"ELEMENT_SORT_FIELD" => 'RAND',
						"ELEMENT_SORT_ORDER" => 'ASC',
						"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
						"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
						"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
						"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
						"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
						"BASKET_URL" => $arParams["BASKET_URL"],
						"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
						"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
						"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
						"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
						"FILTER_NAME" => 'arrFilter',
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_FILTER" => $arParams["CACHE_FILTER"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"SET_TITLE" => $arParams["SET_TITLE"],
						"SET_STATUS_404" => $arParams["SET_STATUS_404"],
						"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
						"PAGE_ELEMENT_COUNT" => $view['COUNT'],
						"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
						"PRICE_CODE" => $arParams["PRICE_CODE"],
						"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
						"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
						"SHOW_ALL_WO_SECTION" => "Y",
						"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
						"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
						"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
						"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
						"PAGER_TITLE" => $arParams["PAGER_TITLE"],
						"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"] = 'Y',
						"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
						"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
						"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
						"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

						"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
						"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
						"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
						"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
						"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
						"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

						"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
						"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
						"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
						"DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
						'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
						'CURRENCY_ID' => $arParams['CURRENCY_ID'],
						"MAIN_PRICES" => $arParams['MAIN_PRICES'],
						"OTHER_PRICES" => $arParams['OTHER_PRICES'],
					),
					$component
				);
				?>
				<div class="line"></div>
			</div>
		<? elseif (!empty($arResult['PROPERTIES']['ACCESSORIES']['VALUE'])): ?>
			<div class="clear"></div>
				<a name="atab2"></a>
				<h2 class="ma"><?= GetMessage("ASTDESIGN_CLIMATE_AKSSESUARY") ?><?= $arResult['NAME'] ?>:</h2>
				<? $GLOBALS['arrFilter']['ID'] = $arResult['PROPERTIES']['ACCESSORIES']['VALUE']; ?>
				<? $APPLICATION->IncludeComponent(
					"bitrix:catalog.section",
					'viewAccessories',
					Array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"ELEMENT_SORT_FIELD" => 'RAND',
						"ELEMENT_SORT_ORDER" => 'ASC',
						"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
						"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
						"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
						"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
						"INCLUDE_SUBSECTIONS" => 'Y',
						"BASKET_URL" => $arParams["BASKET_URL"],
						"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
						"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
						"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
						"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
						"FILTER_NAME" => 'arrFilter',
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_FILTER" => $arParams["CACHE_FILTER"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"SET_TITLE" => $arParams["SET_TITLE"],
						"SET_STATUS_404" => $arParams["SET_STATUS_404"],
						"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
						"PAGE_ELEMENT_COUNT" => $view['COUNT'], 
						"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
						"PRICE_CODE" => $arParams["PRICE_CODE"],
						"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
						"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
						"SHOW_ALL_WO_SECTION" => "Y",
						"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
						"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
						"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
						"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
						"PAGER_TITLE" => $arParams["PAGER_TITLE"],
						"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"] = 'Y',
						"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
						"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
						"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
						"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

						"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
						"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
						"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
						"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
						"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
						"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

						"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
						"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
						"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
						"DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
						'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
						'CURRENCY_ID' => $arParams['CURRENCY_ID'],
						"MAIN_PRICES" => $arParams['MAIN_PRICES'],
						"OTHER_PRICES" => $arParams['OTHER_PRICES'],
					),
					$component
				);
				?>
				<div class="line"></div>

		<? endif ?>

		<div class="tabs con_tab" id="con_tab3">

			<a name="atab3"></a>

			<div id="reviews">
				<h2 class="ma"><?= GetMessage("ASTDESIGN_CLIMATE_OTZYVY") ?></h2>
				<? $GLOBALS['reviewFilter']['PROPERTY_ITEM'] = $arResult['ID']; ?>
				<? $APPLICATION->IncludeComponent("bitrix:news.list", "reviews", Array(
					"IBLOCK_TYPE" => "catalog",
					"IBLOCK_ID" => 7,
					"NEWS_COUNT" => "20",
					"SORT_BY1" => "ID",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "reviewFilter",
					"FIELD_CODE" => array(
						0 => "DATE_CREATE",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "AUTHOR_EMAIL",
						1 => "AUTHOR_NAME",
						2 => "RATE",
						3 => "",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"CACHE_NOTES" => "",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "Y",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "N",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => GetMessage("ASTDESIGN_CLIMATE_NOVOSTI"),
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
				),
					false
				); ?>
				<? $APPLICATION->IncludeComponent("custom:empty", "catalog_review", Array(
					"IBLOCK_TYPE" => "",
					"IBLOCK_ID" => "",
					"ITEM" => $arResult['ID'],
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "0",
					"CACHE_NOTES" => ""
				),
					false
				); ?>
			</div>
			<div class="line"></div>
		</div>
		<? if (CBXFeatures::IsFeatureEnabled('CatMultiStore')): ?>
			<div class="tabs con_tab" id="con_tab4">
				<a name="atab4"></a>
				<h2 class="ma"><?= GetMessage("ASTDESIGN_CLIMATE_NALICIE_V_MAGAZINAH") ?></h2>
				<? $APPLICATION->IncludeComponent("bitrix:catalog.store.amount", ".default", array(
					"PER_PAGE" => $arParams["STORE_PER_PAGE"],
					"USE_STORE_PHONE" => $arParams["STORE_USE_STORE_PHONE"],
					"SCHEDULE" => $arParams["STORE_USE_STORE_SCHEDULE"],
					"USE_MIN_AMOUNT" => $arParams["STORE_USE_MIN_AMOUNT"],
					"MIN_AMOUNT" => $arParams["STORE_MIN_AMOUNT"],
					"ELEMENT_ID" => $arResult['ID'],
					"STORE_PATH" => $arParams["STORE_STORE_PATH"],
					"MAIN_TITLE" => $arParams["STORE_MAIN_TITLE"],
				),
					$component
				); ?>
				<div class="line"></div>
			</div>
		<? endif ?>
		<? if (count($arResult['DISPLAY_PROPERTIES']['FILES']["FILE_VALUE"])): ?>
			<div class="tabs con_tab" id="con_tab5">
				<a name="atab5"></a>
				<h2 class="ma"><?= GetMessage("ASTDESIGN_CLIMATE_FAYLY") ?></h2>
				<div class="files">
					<? foreach ($arResult['DISPLAY_PROPERTIES']['FILES']["FILE_VALUE"] as $file): ?>
						<span class="file<?= ($file[" CONTENT_TYPE "] == 'application/zip') ? '2' : '1' ?>">
							<a target="_blank"
							href="<?= $file["SRC"] ?>"><?= $file["DESCRIPTION"] ? $file["DESCRIPTION"] : $file["ORIGINAL_NAME"] ?></a>
						</span>
					<? endforeach ?>
				</div>
			</div>
		<? endif ?>

		<? $APPLICATION->IncludeComponent("bitrix:sale.viewed.product", "template1", Array(
			"VIEWED_COUNT" => "3",    // Количество элементов
			"VIEWED_NAME" => "Y",    // Показывать наименование
			"VIEWED_IMAGE" => "Y",    // Показывать изображение
			"VIEWED_PRICE" => "N",    // Показывать цену
			"VIEWED_CANBUY" => "N",    // Показать "Купить"
			"VIEWED_CANBUSKET" => "Y",    // Показать "В корзину"
			"VIEWED_IMG_HEIGHT" => "130",    // Высота изображения
			"VIEWED_IMG_WIDTH" => "130",    // Ширина изображения
			"BASKET_URL" => "/personal/basket.php",    // URL, ведущий на страницу с корзиной покупателя
			"ACTION_VARIABLE" => "action",    // Название переменной, в которой передается действие
			"PRODUCT_ID_VARIABLE" => "id",    // Название переменной, в которой передается код товара для покупки
			"SET_TITLE" => "N",    // Устанавливать заголовок страницы
		),
			false
		); ?>
	</div>

</div>
<!-- /COL LEFT -->

<!-- COL RIGHT -->
<div class="col-right right" style="width:430px;">
		<div class="tabs con_tab active" id="con_tab1">

			<h2 class="ma"><?= GetMessage("ASTDESIGN_CLIMATE_HARAKTERISTIKI1") ?></h2>
			<table class="feature">
				<? foreach ($arResult['SHOW_PROPS'] as $pid):
					$val = $arResult['PROPERTIES2ID'][$pid]['VALUE'];
					if (empty($val) || $arResult['PROPERTIES2ID'][$pid]["CODE"] == 'ACCESSORIES') {
						continue;
					}
					if ($arResult['PROPERTIES2ID'][$pid]["PROPERTY_TYPE"] == 'N' && !is_array($val)) $val = floatval($val) ?>
					<tr class="fill">
						<th>
							<?= $arResult['PROPERTIES2ID'][$pid]['NAME'] ?>:
						</th>
						<td><?= is_array($val) ? join('<br>', $val) : $val ?></td>
					</tr>
				<? endforeach ?>
			</table>
		</div>
</div>
<!-- /COL RIGHT -->
