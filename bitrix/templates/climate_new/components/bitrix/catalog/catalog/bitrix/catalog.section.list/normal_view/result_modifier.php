<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$sections = $arParams['SECTION_ID_FILTER'];


if (!count($sections)) {
    $code = $arParams['SECTION_CODE2'];
    $id = intval($arParams['SECTION_ID2']);
    $data = array(
        'H' => array(),
        'C' => 0,
        'CP' => 0,
        'SECTIONS' => array()
    );
    foreach ($arResult["SECTIONS"] as &$sect) {

        $sid = intval($sect['ID']);
        $data['SECTIONS'][$sid] = $sect;
        $data['H'][intval($sect['IBLOCK_SECTION_ID'])][] = $sid;
        $data['H_N'][intval($sect['IBLOCK_SECTION_ID'])][] = $sid . '_' . $sect['NAME'];
        if (
            ($id && $sid == $id)
            ||
            ($code && $sect['CODE'] == $code)
        ) {
            $data['C'] = $sid;
            $data['CP'] = intval($sect['IBLOCK_SECTION_ID']);
        }
    }
    unset($sect);
} else {
    $data = array(
        'H' => array(),
        'C' => 0,
        'CP' => 0,
        'SECTIONS' => array()
    );
    foreach ($arResult["SECTIONS"] as &$sect) {
        $sid = intval($sect['ID']);
        if (!isset($sections[$sid])) {
            continue;
        }
        $data['SECTIONS'][$sid] = $sect;
        $data['H'][0][] = $sid;
        $data['H_N'][0][] = $sid . '_' . $sect['NAME'];
    }
}
$arResult['DATA'] = $data;

foreach ($arResult['DATA']["SECTIONS"] as $k => $section) {
    $propElements = getProp($section['ID'], $section['IBLOCK_ID']);
    if ($propElements) {
        $arResult['DATA']["SECTIONS"][$k]['ELEMENT_PROP'] = $propElements;
    }

    $arResult['DATA']["SECTIONS"][$k]['SHOW_BRAND'] = showBrand($section['ID'], $section['IBLOCK_ID']);


}

function getProp($sectionID, $iblockId)
{
    $section = CIBlockSection::GetList(array(), array('ID' => $sectionID, 'IBLOCK_ID' => $iblockId), false,
        array('IBLOCK_SECTION_ID', 'UF_ELEMENT_PROP'))->Fetch();
    if ($section['UF_ELEMENT_PROP']) {
        $_data = array();
        foreach ($section['UF_ELEMENT_PROP'] as $k => $propId) {
            $prop = CIBlockProperty::GetByID($propId, $iblockId)->Fetch();
            $_data['NAME'][$propId] = $prop['NAME'];
            $_data['PROPID'][] = $propId;
        }
        return $_data;
    } elseif (!$section['UF_ELEMENT_PROP'] && $section['IBLOCK_SECTION_ID']) {
        return getProp($section['IBLOCK_SECTION_ID'], $iblockId);
    } else {
        return false;
    }

}

function showBrand($sectionID, $iblockId)
{
    $section = CIBlockSection::GetList(array(), array('ID' => $sectionID, 'IBLOCK_ID' => $iblockId), false,
        array('UF_SHOW_BRAND'))->Fetch();
    if ($section['UF_SHOW_BRAND']) {
        if ($section['UF_SHOW_BRAND']) {
            return true;
        }
    }
    return false;

}
