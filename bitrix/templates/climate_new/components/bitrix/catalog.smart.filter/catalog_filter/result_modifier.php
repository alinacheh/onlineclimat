<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arResult["ITEMS_ORDER"] = array(
    0 => 0,
    1 => 1
);
foreach($arResult["ITEMS"] as $pid => $parr) {

    if(isset($GLOBALS[$arParams["FILTER_NAME"]])){
        if($parr["PROPERTY_TYPE"] == "N"){
            if(
                floatval($GLOBALS[$arParams["FILTER_NAME"]]["><PROPERTY_".$parr["ID"]][0]) == floatval($parr["VALUES"]["MIN"]["VALUE"])&&
                floatval($GLOBALS[$arParams["FILTER_NAME"]]["><PROPERTY_".$parr["ID"]][1]) == floatval($parr["VALUES"]["MAX"]["VALUE"])
            )
            unset($GLOBALS[$arParams["FILTER_NAME"]]["><PROPERTY_".$parr["ID"]]);
        }
    }

    if ($parr['PRICE']) $arResult["ITEMS_ORDER"][0] = $pid;
    elseif ($parr['CODE'] == 'BRAND_IBLOCK') $arResult["ITEMS_ORDER"][1] = $pid;
    else $arResult["ITEMS_ORDER"][] = $pid;
}