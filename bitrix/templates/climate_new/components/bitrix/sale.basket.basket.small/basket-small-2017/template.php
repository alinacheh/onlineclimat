<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

	<div class="title"><a title="Перейти в корзину" <?if($arResult['QUANTITY']):?>href="<?=$arParams['PATH_TO_BASKET']?>"<?endif?>><?=GetMessage("ASTDESIGN_CLIMATE_KORZINA")?></a></div>
	<div class="clear">
		<div class="inf left">Сумма: <span><?=FormatCurrency($arResult['PRICE'], COption::GetOptionString('sale', 'default_currency'))?></span></div>
		<div class="inf left">Товаров: <span><?=$arResult['QUANTITY']?></span></div>
	</div>
	
<script>
	in_basket = [<?foreach ($arResult["ITEMS"] as $v) if ($v["DELAY"]=="N" && $v["CAN_BUY"]=="Y") echo $v['PRODUCT_ID'].','?>0];
	in_subscribe = [<?foreach ($arResult["ITEMS"] as $v) if ($v["SUBSCRIBE"]=="Y") echo $v['PRODUCT_ID'].','?>0];
</script>