<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="title"><a <?if($arResult['QUANTITY']):?>href="<?=$arParams['PATH_TO_BASKET']?>"<?endif?>><?=GetMessage("ASTDESIGN_CLIMATE_KORZINA")?></a></div>
<div class="clear">
    <div class="info"><span><?=$arResult['QUANTITY']?></span> товаров на <span><?=FormatCurrency($arResult['PRICE'], COption::GetOptionString('sale', 'default_currency'))?></span></div>
    
    <div class="link-to-order"><a href="<?= SITE_DIR ?>personal/cart/" onclick="yaCounter37461485.reachGoal('klik_oformlenie_zakaza'); return true;"><?= GetMessage("ASTDESIGN_CLIMATE_OFORMITQ_ZAKAZ") ?></a></div>
</div>
<script>
	in_basket = [<?foreach ($arResult["ITEMS"] as $v) if ($v["DELAY"]=="N" && $v["CAN_BUY"]=="Y") echo $v['PRODUCT_ID'].','?>0];
	in_subscribe = [<?foreach ($arResult["ITEMS"] as $v) if ($v["SUBSCRIBE"]=="Y") echo $v['PRODUCT_ID'].','?>0];
</script>
