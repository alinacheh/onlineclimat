<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$i=0; foreach($arResult as $arItem):
	if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;?>
	<article <?if(!$i):?>class="first"<?endif?>>
	    <a href="<?=$arItem["LINK"]?>" class="title">
	    	<?=$arItem["TEXT"]?>
	    </a>
	</article>
<?$i++; endforeach?>