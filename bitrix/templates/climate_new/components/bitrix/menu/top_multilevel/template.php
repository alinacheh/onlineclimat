<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
//echo '<pre>'; print_r($arResult); echo '</pre>';
$prevDepth = 0;
if (!empty($arResult)):?>
<nav class="menu left">
	<?foreach($arResult as $key => $arItem):?>
        <?while($arItem['DEPTH_LEVEL'] < $prevDepth):?>
            </ul>
        <?$prevDepth--; endwhile;?>
        <?if($key > 0 ):?></span><?endif;?>
        <?if($arItem['DEPTH_LEVEL'] == 1):?>
            <span>
			    <a href="<?=$arItem["LINK"]?>"<?if($arItem["SELECTED"]):?> class="selected"<?endif;?>><?=$arItem["TEXT"]?></a>
                <?if($arItem['IS_PARENT']):?><ul class="submenu"><?endif;?>
        <?else:?>
            <li><span><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></span></li>
        <?endif;?>
        <?$prevDepth = $arItem['DEPTH_LEVEL'];?>
	<?endforeach?>
</nav>
<?endif?>