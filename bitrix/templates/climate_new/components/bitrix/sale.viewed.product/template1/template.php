<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (count($arResult) > 0):?>
	<div class="watch">
		<h3><?=GetMessage("VIEW_HEADER");?></h3>
		<?foreach($arResult as $arItem):?>
			<article class="productBlock">
	            <?if($arParams["VIEWED_IMAGE"]=="Y" && is_array($arItem["PICTURE"])):?>
		            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="img">
            			<img src="<?=$arItem["PICTURE"]["src"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>">
		            </a>
		        <?endif?>
		        <?if($arParams["VIEWED_NAME"]=="Y"):?>
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="title"><?=$arItem["NAME"]?></a>
				<?endif?>
				<?if($arParams["VIEWED_PRICE"]=="Y" && $arItem["CAN_BUY"]=="Y"):?>
					<div><?=$arItem["PRICE_FORMATED"]?></div>
				<?endif?>
				<div class="btn">
					<a class="blockview" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("PRODUCT_BUY")?></a>
				</div>
	        </article>
		<?endforeach;?>
	</div>
<?endif;?>