<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
foreach($arResult as $key => $val)
{
	$img = "";
	if ($val["DETAIL_PICTURE"] > 0)
		$img = $val["DETAIL_PICTURE"];
	elseif ($val["PREVIEW_PICTURE"] > 0)
		$img = $val["PREVIEW_PICTURE"];
	else $img = $val["DETAIL_PICTURE"];

	$file = Refs::resize_image($img, $arParams["VIEWED_IMG_WIDTH"], $arParams["VIEWED_IMG_HEIGHT"]);

	$val["PICTURE"] = $file;
	$arResult[$key] = $val;
}
?>