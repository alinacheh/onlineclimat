<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
if(!count($arResult["ITEMS"])){
    return;
}
?>

<h2>Используемые товары в этой работе:</h2>
<div class="our-works-products">
    <? foreach ($arResult["ITEMS"] as $arElement) { ?>
        <div class="our-works-products__item">
            <a href="<?= $arElement["DETAIL_PAGE_URL"] ?>" class="our-works-products__item-link">
                <div class="our-works-products__item-img">
                    <img src="<?= Refs::get_resize_src($arElement["DETAIL_PICTURE"], 220, 150) ?>" alt="">
                </div>
                <div class="our-works-products__item-title">
                    <?= $arElement["NAME"] ?>
                </div>
                <div class="our-works-products__item-price">
                    <?= FormatCurrency($arElement['2PRICES']['main']['DISCOUNT_VALUE'],
                        $arElement['2PRICES']['main']['CURRENCY']); ?>
                </div>
            </a>
        </div>
        <?php
    }
    ?>
</div>