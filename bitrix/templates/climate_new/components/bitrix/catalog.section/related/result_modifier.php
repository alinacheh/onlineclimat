<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule('iblock');
$cache = array();
foreach($arResult["ITEMS"] as &$arElement) {
	if (!isset($cache[$arElement['IBLOCK_SECTION_ID']])) {
		//$res = CIBlockSectionPropertyLink::GetArray($arElement['IBLOCK_ID'], $arElement['IBLOCK_SECTION_ID']);
		$res = Refs::getLinkProps($arResult['IBLOCK_ID'], $arResult['IBLOCK_SECTION_ID']);
		$cache[$arElement['IBLOCK_SECTION_ID']] = array();
		foreach ($res as $prop) {
			if ($prop['INHERITED_FROM'] != 0) {
				$cache[$arElement['IBLOCK_SECTION_ID']][] = $prop['PROPERTY_ID'];
			}
		}
	}
	$db_props = CIBlockElement::GetProperty($arElement['IBLOCK_ID'], $arElement['ID']);
	while ($prop = $db_props->Fetch()) {
		$arElement['PROPERTIES'][$prop['ID']] = $prop;
	}
	$arElement['PROPS'] = $cache[$arElement['IBLOCK_SECTION_ID']];

	//prices
	$arElement['2PRICES'] = array();
	$min_price = array(PHP_INT_MAX, PHP_INT_MAX, '');
	foreach ($arParams['MAIN_PRICES'] as $code) {
		$price = $arElement['PRICES'][$code];
		//DISCOUNT_VALUE or VALUE
		if ($price["CAN_ACCESS"] == 'Y' && $price["CAN_BUY"] == 'Y' && ($price['VALUE'] < $min_price[0] || $price['DISCOUNT_VALUE'] < $min_price[1])) {
			$min_price[0] = $price['VALUE'];
			$min_price[1] = $price['DISCOUNT_VALUE'];
			$min_price[2] = $code;
		}
	}
	$arElement['2PRICES']['main'] = $arElement['PRICES'][$min_price[2]];
	$arElement['2PRICES']['main']['CODE'] = $min_price[2];

	$min_price = array(PHP_INT_MAX, PHP_INT_MAX, '');
	foreach ($arParams['OTHER_PRICES'] as $code) {
		$price = $arElement['PRICES'][$code];
		//DISCOUNT_VALUE or VALUE
		if ($price["CAN_ACCESS"] == 'Y' && ($price['VALUE'] < $min_price[0] || $price['DISCOUNT_VALUE'] < $min_price[1])) {
			$min_price[0] = $price['VALUE'];
			$min_price[1] = $price['DISCOUNT_VALUE'];
			$min_price[2] = $code;
		}
	}
	$arElement['2PRICES']['other'] = $arElement['PRICES'][$min_price[2]];
	$arElement['2PRICES']['other']['CODE'] = $min_price[2];
}
unset($arElement);

$arResult['SECTIONS'] = array();
if (count($cache)) {
	$res = CIBlockSection::GetList(array('ID'=>'ASC'), array('ID'=>array_keys($cache)), false, array('ID', 'NAME', 'SECTION_PAGE_URL'));
	while ($r = $res->GetNext()) $arResult['SECTIONS'][$r['ID']] = $r;
}