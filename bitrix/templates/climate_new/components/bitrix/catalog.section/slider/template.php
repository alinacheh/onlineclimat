<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="<?=SITE_DIR?>js/slides.js"></script>
<div id="slides" class="container">
	<?foreach($arResult["ITEMS"] as $arElement):?>
		<div class="slide">
    		<img src="<?=Refs::get_resize_src($arElement['PROPERTIES']['SLIDER']['VALUE'], 930, 373)?>" alt="<?=$arElement['NAME']?>"/>
		    <div class="box">
        		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="title"><?=$arElement['NAME']?></a>
        		<p class="text"><?=strip_tags($arElement['PREVIEW_TEXT'])?></p>
		        <span class="price"><?=FormatCurrency($arElement['2PRICES']['main']["DISCOUNT_VALUE"], $arElement['2PRICES']['main']['CURRENCY'])?></span>
		        <div class="btn right">
		        	<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=GetMessage("ASTDESIGN_CLIMATE_PODROBNEE")?><b></b></a></div>
		    </div>
		</div>
	<?endforeach?>
</div>
<script>$(function(){
	if ($('#slides .slide').length > 1) {
		$("#slides").slides({
			width: 930,
			height: 366,
			startAtSlide: 0,
			playInterval: <?=$arParams['INTERVAL']?$arParams['INTERVAL']:'5000'?>
		});
		$("#slides").slides("play");
	}
})</script>