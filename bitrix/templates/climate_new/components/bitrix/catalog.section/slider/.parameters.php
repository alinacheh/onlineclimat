<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$prices = array();
CModule::IncludeModule('catalog');
$res = GetCatalogGroups('NAME', 'asc');
while ($r = $res->Fetch()) $prices[$r['NAME']] = $r['NAME'];
$arTemplateParameters = array(
	"INTERVAL" => array(
		"NAME" => GetMessage("ASTDESIGN_INTERVAL"),
      	"TYPE" => "STRING",
      	'DEFAULT_VALUE' => '5000',
	),
	"MAIN_PRICES" => Array(
      "NAME" => GetMessage("ASTDESIGN_MAIN_PRICES"),
      "TYPE" => "LIST",
      'MULTIPLE' => 'Y',
      "VALUES" => $prices
   ),
   "OTHER_PRICES" => Array(
      "NAME" => GetMessage("ASTDESIGN_OTHER_PRICES"),
      "TYPE" => "LIST",
      'MULTIPLE' => 'Y',
      "VALUES" => $prices
   ),

);
?>