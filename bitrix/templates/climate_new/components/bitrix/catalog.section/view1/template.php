<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>




<?

//USE <hr class="system-pagebreak" />
if (preg_match('%(.*?)<hr class="system-pagebreak.*>(.*?)%siU',$arResult['DESCRIPTION'], $pregText)){
    $topText = $pregText[1];
    $bottomText = $pregText[2];
} else {
    $topText = '';
    $bottomText = $arResult['DESCRIPTION'];
}
?>
<div class="seo-text top">
    <?
    // if (!isset($_GET['PAGEN_6']) && !isset($_GET['PAGEN_7']) && !isset($_GET['PAGEN_4'])){
    if(!preg_match("#(.*)PAGEN(.*)#siU", $_SERVER['REQUEST_URI'])){
        echo $topText;
    }
    ?>
</div>



<div id="pagination_hidden" style="display:none"><?=$arResult["NAV_STRING"]?></div>
<div class="items">
    <?$i=0; foreach($arResult["ITEMS"] as $arElement): $i++;
		/* if( $USER->IsAdmin() )
			echo '<pre>' . print_r($arElement['2PRICES'], true) . '</pre>';
 */
	?>
        <article class="productBlock <?if($i%3==0):?>last<?endif?>">
            <?if(!$arParams['IS_SEARCH_RESULT']):?>
                <label><input data-id="<?=$arElement["ID"]?>" class="compare compare<?=$arElement["ID"]?>" type="checkbox"> <?=GetMessage('CATALOG_COMPARE')?></label>
            <?endif?>
            <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="img">
                <!-- resize Refs::get_resize_src($arElement["DETAIL_PICTURE"], 210) -->
                <div class="block-img"><img src="<?=$arElement["DETAIL_PICTURE"]["SRC"]?>" title="<?=$arElement["NAME"]?>" alt="<?=$arElement["NAME"]?>"></div>
            </a>
            <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="title"><?=$arElement["NAME"]?></a>
            <?if($arElement["CAN_BUY"] && $arElement['CATALOG_QUANTITY']):?>
                <span class="price"><?=FormatCurrency($arElement['2PRICES']['main']['DISCOUNT_VALUE'], $arElement['2PRICES']['main']['CURRENCY'])?></span>
                        <div class="btn alt right cat">
                            <a href="#" data-pid='<?=$arElement['ID']?>:1' class="qorder" onclick="yaCounter37461485.reachGoal('klik_kupit');ga('send', 'pageview', '/klik_kupit'); return true;">
                                Купить<!--b></b--></a>
                        </div>
                <?$section = $arResult['SECTIONS'][$arElement['~IBLOCK_SECTION_ID']];
                if ($section['UF_USE_COUNT']):?>
                    <div class="btn clear">
                        <input type="hidden" class="count right" value="1">
                        <div class="have right add2basket<?=$arElement['ID']?>" style="display: none;"><?=GetMessage("ASTDESIGN_CLIMATE_TOVAR_UJE_V_KORZINE")?></div>
                        <a data-id="<?=$arElement["ID"]?>" class="add2basketq viewP3 add2basket<?=$arElement["ID"]?>" href="#" rel="nofollow" onclick="yaCounter37461485.reachGoal('quick'); return true;"><?=GetMessage("ASTDESIGN_CLIMATE_KUPITQ")?></a>
                    </div>
                <?else:?>
                    <div class="clear">
                        <div class="pm right">
                            <a href="#" class="p"></a>
                            <a href="#" class="m"></a>
                        </div>
                        <input type="text" class="count right" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1" size="5">
                        <div class="have right add2basket<?=$arElement['ID']?>" style="display: none;"><?=GetMessage("ASTDESIGN_CLIMATE_TOVAR_UJE_V_KORZINE")?></div>
                        <a href="#" data-id="<?=$arElement['ID']?>" class="add2basketq add2basket<?=$arElement['ID']?> cart left"></a>
                    </div>
                <?endif?>
            <?elseif ($arElement['CATALOG_CAN_BUY_ZERO']=='Y'):?>
                <span class="price"><?=FormatCurrency($arElement['2PRICES']['main']['DISCOUNT_VALUE'], $arElement['2PRICES']['main']['CURRENCY'])?></span>
                <div class="relative viewP btn clear">
                    <a data-id="<?=$arElement["ID"]?>" class="viewP2 add2basket add2basket<?=$arElement["ID"]?>" href="#"onclick="yaCounter37461485.reachGoal('klik_kupit');ga('send', 'pageview', '/klik_kupit');"><?=GetMessage("ASTDESIGN_CLIMATE_ZAKAZATQ")?></a>
                </div>
            <?else:?>
                <span class="price wait viewPWait"><?=GetMessage("CATALOG_NOT_AVAILABLE")?></span>
                <div class="relative clear">
                    <span class="wait btn" ><a data-id="<?=$arElement["ID"]?>" class="notify notify<?=$arElement["ID"]?>" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_A_JDU")?></a></span>
                </div>
            <?endif?>
        </article>
        <?if($i%3==0):?><div class="clear"></div><?endif?>
    <?endforeach?>
    <div class="clear"></div>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]){?><div class="line"></div><div id="pagination" class="clear"><?=$arResult["NAV_STRING"]?></div><div class="line"></div><?}?>
<?if(!$i):?><div id="no_items"><?=GetMessage("ASTDESIGN_CLIMATE_IZVINITE_NICEGO_NE")?></div><?endif?>
<?if($arParams['ADD_SECTIONS_CHAIN'] == 'Y' && $arResult['DESCRIPTION']):?>
    <br /><p><?//=$arResult['DESCRIPTION']?></p>
    <div class="seo-text bottom">
        <?
        if(!preg_match("#(.*)PAGEN(.*)#siU", $_SERVER['REQUEST_URI'])){
            echo $bottomText;
        }
        ?>
    </div>
<?endif?>
