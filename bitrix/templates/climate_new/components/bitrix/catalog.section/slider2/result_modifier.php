<?php
foreach($arResult["ITEMS"] as &$arElement) {
	//prices
	$arElement['2PRICES'] = array();
	$min_price = array(PHP_INT_MAX, PHP_INT_MAX, '');
	foreach ($arParams['MAIN_PRICES'] as $code) {
		$price = $arElement['PRICES'][$code];
		//DISCOUNT_VALUE or VALUE
		if ($price["CAN_ACCESS"] == 'Y' && $price["CAN_BUY"] == 'Y' && ($price['VALUE'] < $min_price[0] || $price['DISCOUNT_VALUE'] < $min_price[1])) {
			$min_price[0] = $price['VALUE'];
			$min_price[1] = $price['DISCOUNT_VALUE'];
			$min_price[2] = $code;
		}
	}
	$arElement['2PRICES']['main'] = $arElement['PRICES'][$min_price[2]];
	$arElement['2PRICES']['main']['CODE'] = $min_price[2];

	$min_price = array(PHP_INT_MAX, PHP_INT_MAX, '');
	foreach ($arParams['OTHER_PRICES'] as $code) {
		$price = $arElement['PRICES'][$code];
		//DISCOUNT_VALUE or VALUE
		if ($price["CAN_ACCESS"] == 'Y' && ($price['VALUE'] < $min_price[0] || $price['DISCOUNT_VALUE'] < $min_price[1])) {
			$min_price[0] = $price['VALUE'];
			$min_price[1] = $price['DISCOUNT_VALUE'];
			$min_price[2] = $code;
		}
	}
	$arElement['2PRICES']['other'] = $arElement['PRICES'][$min_price[2]];
	$arElement['2PRICES']['other']['CODE'] = $min_price[2];
}
unset($arElement);