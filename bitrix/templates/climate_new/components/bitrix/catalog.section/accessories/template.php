<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="items view2">
	<?$sects = array(); $i=0; $max=count($arResult["ITEMS"]);
	foreach($arResult["ITEMS"] as $arElement): $i++;
		if (isset($sects[$arElement['IBLOCK_SECTION_ID']])) continue;
		$sects[$arElement['IBLOCK_SECTION_ID']] = 1;
		$sect = $arElement['IBLOCK_SECTION_ID'];?>
		<article class="productBlock <?=($i==$max)?'last':''?>">
            <div class="altBody left" style="padding:10px">
                <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="img left">
                	<img src="<?=Refs::get_resize_src($arElement["DETAIL_PICTURE"], 210)?>">
                </a>
            </div>
            <div class="altBody right">
            	<?
            	$p1 = FormatCurrency($arElement['2PRICES']['main']['VALUE'], $arElement['2PRICES']['main']['CURRENCY']);
            	$p2 = 0;
            	if ($arElement['2PRICES']['main']['DISCOUNT_VALUE'] < $arElement['2PRICES']['main']['VALUE']) {
					$p1 = FormatCurrency($arElement['2PRICES']['main']['DISCOUNT_VALUE'], $arElement['2PRICES']['main']['CURRENCY']);
					$p2 = FormatCurrency($arElement['2PRICES']['main']['VALUE'], $arElement['2PRICES']['main']['CURRENCY']);
				}?>
                <span class="price">
                    <span><?=GetMessage("ASTDESIGN_CLIMATE_CENA")?></span>
                    <?=$p1?>
                    <?if($p2):?><span class="old"><?=$p2?></span><?endif?>
                </span>
                <div class="clear">
		            <div class="pm right">
		                <a href="#" class="p"></a>
		                <a href="#" class="m"></a>
		            </div>
		            <input type="text" class="count right" value="1">
		            <div class="have right add2basket<?=$arElement['ID']?>" style="display: none;"><?=GetMessage("ASTDESIGN_CLIMATE_TOVAR_UJE_V_KORZINE")?></div>
		            <a href="#" data-id="<?=$arElement['ID']?>" class="cart add2basketq add2basket<?=$arElement['ID']?> left"></a>
		        </div>
            </div>
            <div class="body">
                <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="title"><?=$arElement["NAME"]?></a>
                <ul class="simple">
                	<?$pc=0; foreach($arElement['PROPS'] as $pid) if ($arElement['PROPERTIES'][$pid]['VALUE']):
                		$p = $arElement['PROPERTIES'][$pid];?>
						<li><?=$p['NAME']?>: <?=$p['VALUE_ENUM']?$p['VALUE_ENUM']:$p['VALUE']?></li>
                	<?$pc++; if($pc>4) break; endif?>
                </ul>
            </div>
            <div class="clear"></div>
            
        </article>
        <div class="line"></div>
	<?endforeach?>
</div>
