<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$cache = array();
$section_ids = array();
$view2_text_prop_id = 0;
foreach($arResult["ITEMS"] as &$arElement) {
    $sid = $arElement['~IBLOCK_SECTION_ID'] ? $arElement['~IBLOCK_SECTION_ID'] : $arElement['IBLOCK_SECTION_ID'];
    if (!isset($cache[$sid])) {
        //$res = CIBlockSectionPropertyLink::GetArray($arElement['IBLOCK_ID'], $sid);
        $res = Refs::getLinkProps($arResult['IBLOCK_ID'], $sid);
        $cache[$sid] = array();
        foreach ($res as $prop) {
            if ($prop['INHERITED_FROM'] != 0) {
                if ($prop["SMART_FILTER"] == 'Y') $pids[0][] = $prop['PROPERTY_ID'];
                else $pids[1][] = $prop['PROPERTY_ID'];
            }
        }
        if(!empty($pids[0])) $cache[$sid] += $pids[0];
        if(!empty($pids[1])) $cache[$sid] += $pids[1];
        $section_ids[$sid] = $sid;
    }
    $db_props = CIBlockElement::GetProperty($arElement['IBLOCK_ID'], $arElement['ID']);
    while ($prop = $db_props->Fetch()) {
        if (isset($arElement['PROPERTIES'][$prop['ID']])) {
			//M-property
			$p = &$arElement['PROPERTIES'][$prop['ID']];
			if ($p["VALUE"]) {
				if (!is_array($p['VALUE'])) $p["VALUE"] = array($p['VALUE']);
				$p['VALUE'][] = $prop['VALUE'];
			} elseif ($p["VALUE_ENUM"]) {
				if (!is_array($p['VALUE_ENUM'])) $p["VALUE_ENUM"] = array($p['VALUE_ENUM']);
				$p['VALUE_ENUM'][] = $prop['VALUE_ENUM'];
			}
			unset($p);
        } else $arElement['PROPERTIES'][$prop['ID']] = $prop;
        if (!$view2_text_prop_id && $prop['CODE'] == 'VIEW2_TEXT') $view2_text_prop_id = $prop['ID'];
    }
    $arElement['PROPS'] = $cache[$sid];

    //prices
	$arElement['2PRICES'] = array();
	$min_price = array(PHP_INT_MAX, PHP_INT_MAX, '');
	foreach ($arParams['MAIN_PRICES'] as $code) {
		$price = $arElement['PRICES'][$code];
		//DISCOUNT_VALUE or VALUE
		if ($price["CAN_ACCESS"] == 'Y' && $price["CAN_BUY"] == 'Y' && ($price['VALUE'] < $min_price[0] || $price['DISCOUNT_VALUE'] < $min_price[1])) {
			$min_price[0] = $price['VALUE'];
			$min_price[1] = $price['DISCOUNT_VALUE'];
			$min_price[2] = $code;
		}
	}
	$arElement['2PRICES']['main'] = $arElement['PRICES'][$min_price[2]];
	$arElement['2PRICES']['main']['CODE'] = $min_price[2];

	$min_price = array(PHP_INT_MAX, PHP_INT_MAX, '');
	foreach ($arParams['OTHER_PRICES'] as $code) {
		$price = $arElement['PRICES'][$code];
		//DISCOUNT_VALUE or VALUE
		if ($price["CAN_ACCESS"] == 'Y' && ($price['VALUE'] < $min_price[0] || $price['DISCOUNT_VALUE'] < $min_price[1])) {
			$min_price[0] = $price['VALUE'];
			$min_price[1] = $price['DISCOUNT_VALUE'];
			$min_price[2] = $code;
		}
	}
	$arElement['2PRICES']['other'] = $arElement['PRICES'][$min_price[2]];
	$arElement['2PRICES']['other']['CODE'] = $min_price[2];
}
unset($arElement);

$arResult['VIEW2_TEXT_PROP_CODE'] = $view2_text_prop_id;
$arResult['SECTIONS'] = array();
if (count($section_ids)) {
    $res = CIBlockSection::GetList(
        array('ID'=>'ASC'),
        array("IBLOCK_ID" => $arParams["IBLOCK_ID"],"ID" => $section_ids),
        false,
        array("ID", "DEPTH_LEVEL", "SECTION_PAGE_URL", "UF_USE_COUNT", )
    );
    while ($r = $res->Fetch()) {
        $arResult['SECTIONS'][$r['ID']] = $r;
    }
}
if($USER->IsAdmin()){
    $res = CIBlockSection::GetNavChain($arParams["IBLOCK_ID"], $arResult['ID'], array("ID", "NAME"));

    while ($r = $res->Fetch()) {
        $sect = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ID" => $r['ID']), false, array("ID", "UF_SHOWPROPERTIES"))->Fetch();
        if(!empty($sect['UF_SHOWPROPERTIES']))
            $arResult['SHOWPROPERTIES'] = $sect['UF_SHOWPROPERTIES'];
    }
    
    //echo '<pre>'; print_r($arResult['SHOWPROPERTIES']); echo '</pre>';
    //$dbProps = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ID" => $arResult['SHOWPROPERTIES']));
    //while($arProp = $dbProps->Fetch()) {
    /*foreach($arResult['ITEMS'][0]['PROPERTIES'] AS $arProp){
        if(in_array($arProp['ID'], $arShoP)){
            $arResult['SHOWPROPERTIES'][] = $arProp['CODE'];
            //echo '<pre>'; print_r($arProp); echo '</pre>';
        }
            
    }
     
     echo '<pre>'; print_r($arResult['ITEMS'][0]['PROPERTIES']); echo '</pre>';*/
}

//echo '<pre>'; print_r($arResult); echo '</pre>';