<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?

//USE <hr class="system-pagebreak" />
if (preg_match('%(.*?)<hr class="system-pagebreak.*>(.*?)%siU',$arResult['DESCRIPTION'], $pregText)){
    $topText = $pregText[1];
    $bottomText = $pregText[2];
} else {
    $topText = '';
    $bottomText = $arResult['DESCRIPTION'];
}
?>
<div class="seo-text top">
    <?
    if(!preg_match("#(.*)PAGEN(.*)#siU", $_SERVER['REQUEST_URI'])){
        echo $topText;
    }
    ?>
</div>



<div id="pagination_hidden" style="display:none"><?=$arResult["NAV_STRING"]?></div>
<div class="items view2">
	<?$i=0; $max=count($arResult["ITEMS"]); foreach($arResult["ITEMS"] as $arElement): $i++;?>
		<article class="productBlock <?=($i==$max)?'last':''?>">
            <div class="altBody left" style="padding:10px">
                <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="img">
                	<img src="<?=Refs::get_resize_src($arElement["DETAIL_PICTURE"], 210)?>" title="<?=$arElement["NAME"]?>" alt="<?=$arElement["NAME"]?>">
                </a>
            	<label class="left"><input data-id="<?=$arElement["ID"]?>" class="compare compare<?=$arElement["ID"]?>" type="checkbox"> <?=GetMessage("ASTDESIGN_CLIMATE_SRAVNITQ")?></label>
            </div>
            <div class="altBody right">
            	<?if($arElement["CAN_BUY"] && $arElement['CATALOG_QUANTITY']):?>
            		<?
            		$p1 = FormatCurrency($arElement['2PRICES']['main']['VALUE'], $arElement['2PRICES']['main']['CURRENCY']);
            		$p2 = 0;
            		if ($arElement['2PRICES']['main']['DISCOUNT_VALUE'] < $arElement['2PRICES']['main']['VALUE']) {
						$p1 = FormatCurrency($arElement['2PRICES']['main']['DISCOUNT_VALUE'], $arElement['2PRICES']['main']['CURRENCY']);
						$p2 = FormatCurrency($arElement['2PRICES']['main']['VALUE'], $arElement['2PRICES']['main']['CURRENCY']);
					}?>
					<span class="price wait nal"><?=GetMessage("ASTDESIGN_CLIMATE_V_NAL")?></span><br />
	                <span class="price">
	                    <span><?=GetMessage("ASTDESIGN_CLIMATE_CENA")?></span>
	                    <?=$p1?>
	                    <?if($p2):?><span class="old"><?=$p2?></span><?endif?>
	                </span>
	                <?$section = $arResult['SECTIONS'][$arElement['~IBLOCK_SECTION_ID']];
		    		if ($section['UF_USE_COUNT']):?>
						<div class="btn clear">
						    <input type="hidden" class="count right" value="1">
						    <div class="have right add2basket<?=$arElement['ID']?>" style="display: none;"><?=GetMessage("ASTDESIGN_CLIMATE_TOVAR_UJE_V_KORZINE")?></div>
						    <a data-id="<?=$arElement["ID"]?>" class="add2basketq viewP3 add2basket<?=$arElement["ID"]?>" href="#" rel="nofollow"><?=GetMessage("ASTDESIGN_CLIMATE_KUPITQ")?></a>
						</div>
					<?else:?>
		                <div class="clear">
				            <div class="pm right">
				                <a href="#" class="p"></a>
				                <a href="#" class="m"></a>
				            </div>
				            <input type="text" class="count right" value="1">
				            <div class="have right add2basket<?=$arElement['ID']?>" style="display: none;"><?=GetMessage("ASTDESIGN_CLIMATE_TOVAR_UJE_V_KORZINE")?></div>
				            <a href="#" data-id="<?=$arElement['ID']?>" class="cart add2basketq add2basket<?=$arElement['ID']?> left"></a>
				        </div>
				    <?endif?>
			    <?elseif ($arElement['CATALOG_CAN_BUY_ZERO']=='Y'):?>
			    	<?
			    	$p1 = FormatCurrency($arElement['2PRICES']['main']['VALUE'], $arElement['2PRICES']['main']['CURRENCY']);
			    	$p2 = 0;
            		if ($arElement['2PRICES']['main']['DISCOUNT_VALUE'] < $arElement['2PRICES']['main']['VALUE']) {
						$p1 = FormatCurrency($arElement['2PRICES']['main']['DISCOUNT_VALUE'], $arElement['2PRICES']['main']['CURRENCY']);
						$p2 = FormatCurrency($arElement['2PRICES']['main']['VALUE'], $arElement['2PRICES']['main']['CURRENCY']);
					}?>
					<span class="price wait podzakaz"><?=GetMessage("ASTDESIGN_CLIMATE_POD_ZAKAZ")?></span><br />
	                <span class="price">
	                    <span><?=GetMessage("ASTDESIGN_CLIMATE_CENA")?></span>
	                    <?=$p1?>
	                    <?if($p2):?><span class="old"><?=$p2?></span><?endif?>
	                </span>
	        		<div class="relative viewP btn clear">
						<a data-id="<?=$arElement["ID"]?>" class="viewP2 add2basket add2basket<?=$arElement["ID"]?>" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_ZAKAZATQ")?></a>
					</div>
			    <?else:?>
					<span class="price wait nenal"><?=GetMessage("CATALOG_NOT_AVAILABLE")?></span>
					<div class="relative clear">
						<span class="wait btn" ><a data-id="<?=$arElement["ID"]?>" class="notify notify<?=$arElement["ID"]?>" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_A_JDU")?></a></span>
					</div>
			    <?endif?>
            </div>
            <div class="body">
                <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="title"><?=$arElement["NAME"]?></a>
                <?if (COption::GetOptionString("astdesign.climate", "VIEW2_MODETEXT") == "Y"):?>
					<?=$arElement['PROPERTIES'][$arResult['VIEW2_TEXT_PROP_CODE']]['VALUE']['TEXT'];?>
                <?else:?>
	                <ul class="simple">
                		<?/*$pc=0; foreach($arElement['PROPS'] as $pid) if ($arElement['PROPERTIES'][$pid]['VALUE']):
                			$p = $arElement['PROPERTIES'][$pid];?>
							<li><?=$p['NAME']?>: <?=$p['VALUE_ENUM']?(is_array($p['VALUE_ENUM'])?join(', ', $p['VALUE_ENUM']):$p['VALUE_ENUM']):(is_array($p['VALUE'])?join(', ', $p['VALUE']):$p['VALUE'])?></li>
                		<?$pc++; if($pc>4) break; endif*/?>
                        <?$pc=0; foreach($arResult['SHOWPROPERTIES'] as $pid) if ($arElement['PROPERTIES'][$pid]['VALUE']):
                            $p = $arElement['PROPERTIES'][$pid];?>
                            <li><?=$p['NAME']?>: <?=$p['VALUE_ENUM']?(is_array($p['VALUE_ENUM'])?join(', ', $p['VALUE_ENUM']):$p['VALUE_ENUM']):(is_array($p['VALUE'])?join(', ', $p['VALUE']):$p['VALUE'])?></li>
                        <?$pc++; if($pc>5) break; endif?>
	                </ul>
                <?endif?>
            </div>
            <div class="clear"></div>
        </article>
        <div class="line"></div>
	<?endforeach?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]){?><div class="line"></div><div id="pagination" class="clear"><?=$arResult["NAV_STRING"]?></div><div class="line"></div><?}?>
<?if(!$i):?><div id="no_items"><?=GetMessage("ASTDESIGN_CLIMATE_IZVINITE_NICEGO_NE")?></div><?endif?>
<?if($arParams['ADD_SECTIONS_CHAIN'] == 'Y' && $arResult['DESCRIPTION']):?>
	<br /><p><?//=$arResult['DESCRIPTION']?></p>
	<div class="seo-text bottom">
        <?
        if(!preg_match("#(.*)PAGEN(.*)#siU", $_SERVER['REQUEST_URI'])){
            echo $bottomText;
        }
        ?>
    </div>
<?endif?>