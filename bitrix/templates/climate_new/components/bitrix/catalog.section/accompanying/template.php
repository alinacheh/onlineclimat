<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="access">
    <span class="title">Сопутствующие товары</span>
    <?foreach($arResult["ITEMS"] as $arElement):?>
        <div class="access-items">
            <div class="access-item">
                <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="access-item-title"><?=$arElement["NAME"]?></a>
                <a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=Refs::get_resize_src($arElement["DETAIL_PICTURE"], 130, 130)?>" alt="<?=$arElement["NAME"]?>"></a>
                <span class="access-price"><?=FormatCurrency($arElement['2PRICES']['main']['DISCOUNT_VALUE'], $arElement['2PRICES']['main']['CURRENCY']);?></span>
                <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="access-add-to-cart">Купить</a>
            </div>
        </div>
    <?endforeach;?>
</div>