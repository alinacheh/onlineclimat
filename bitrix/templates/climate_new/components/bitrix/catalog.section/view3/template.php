<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="pagination_hidden" style="display:none"><?=$arResult["NAV_STRING"]?></div>
<table class="items view3">
    <tr>
        <th class="name"><?=GetMessage("CATALOG_TITLE")?></th>
        <th class="price"><?=$arResult["PRICES"]['main']["TITLE"]?></th>
        <th class="info">&nbsp;</th>
    </tr>
    <?$i=0; foreach($arResult["ITEMS"] as $arElement): $i++?>
        <tr>
            <td class="name">
                <a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a>
                <br />
                <?if($arElement['CATALOG_QUANTITY'] > 0):?>
                    <span class="nal"><?=GetMessage("ASTDESIGN_CLIMATE_V_NAL")?></span>
                <?elseif($arElement['CATALOG_CAN_BUY_ZERO']=='Y'):?>
                    <span class="podzakaz"><?=GetMessage("ASTDESIGN_CLIMATE_POD_ZAKAZ")?></span>
                <?else:?>
                    <span class="nenal"><?=GetMessage("CATALOG_NOT_AVAILABLE")?></span>
                <?endif?>
            </td>
            <td class="price">
                <?$arPrice = $arElement["2PRICES"]['main'];
                if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
                    <?=FormatCurrency($arPrice["DISCOUNT_VALUE"], $arPrice['CURRENCY'])?>
                    <span class="old"><?=FormatCurrency($arPrice["VALUE"], $arPrice['CURRENCY'])?></span>
                <?else:?>
                    <?=FormatCurrency($arPrice["VALUE"], $arPrice['CURRENCY'])?>
                <?endif?>
            </td>
            <td class="info">
                <?if($arElement["CAN_BUY"] && $arElement['CATALOG_QUANTITY']):?>
                    <a href="#" data-id="<?=$arElement['ID']?>" class="tableview buy add2basket add2basket<?=$arElement['ID']?> action"><?=GetMessage("ASTDESIGN_CLIMATE_KUPITQ")?></a>
                <?elseif ($arElement['CATALOG_CAN_BUY_ZERO']=='Y'):?>
                    <a href="#" data-id="<?=$arElement['ID']?>" class="tableview viewP2 buy add2basket add2basket<?=$arElement['ID']?> action"><?=GetMessage("ASTDESIGN_CLIMATE_ZAKAZATQ")?></a>
                <?else:?>
                    <div class="relative left clear">
                        <span class="wait btn" ><a data-id="<?=$arElement["ID"]?>" class="notify notify<?=$arElement["ID"]?>" href="#"><?=GetMessage("ASTDESIGN_CLIMATE_A_JDU")?></a></span>
                    </div>
                <?endif?>
                <a data-id="<?=$arElement["ID"]?>" href="#" class="compare tableview compare<?=$arElement["ID"]?> action"><?=GetMessage("ASTDESIGN_CLIMATE_SRAVNITQ")?></a>
            </td>
        </tr>
    <?endforeach?>
</table>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]){?><div class="line"></div><div id="pagination" class="clear"><?=$arResult["NAV_STRING"]?></div><div class="line"></div><?}?>
<?if(!$i):?><div id="no_items"><?=GetMessage("ASTDESIGN_CLIMATE_IZVINITE_NICEGO_NE")?></div><?endif?>
<?if($arParams['ADD_SECTIONS_CHAIN'] == 'Y' && $arResult['DESCRIPTION']):?>
    <p class="describe"><?=$arResult['DESCRIPTION']?></p>
<?endif?>