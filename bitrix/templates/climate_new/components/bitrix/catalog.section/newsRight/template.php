<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (count($arResult) > 0):?>
	<div class="watch inNews">
		<?foreach($arResult["ITEMS"] as $arElement):?>
			<article class="productBlock">
	            <?if(is_array($arElement["DETAIL_PICTURE"])):?>
		            <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="img">
            			<img src="<?=Refs::get_resize_src($arElement["DETAIL_PICTURE"], 130)?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" />
		            </a>
		        <?endif?>
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="title"><?=$arElement["NAME"]?></a>
				<div class="btn">
					<a class="blockview" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=GetMessage("PRODUCT_BUY")?></a>
				</div>
	        </article>
		<?endforeach;?>
	</div>
<?endif;?>