<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

foreach ($arResult['ITEMS'] as $item){
	?>
	<div class="item">
		<div class="img"><img src="<?=$item['DETAIL_PICTURE']['SRC']?>" alt="" /></div>
		<a href="<?=$item['DETAIL_PAGE_URL']?>"><?=$item['NAME']?></a>
	</div>
	<?
}
?>
