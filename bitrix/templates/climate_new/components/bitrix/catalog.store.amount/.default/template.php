<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(strlen($arResult["ERROR_MESSAGE"])>0) ShowError($arResult["ERROR_MESSAGE"]);?>
<?if(count($arResult["STORES"]) > 0):?>
	<table class="items view3 nal">
		<tr>
		    <th><?=GetMessage("ASTDESIGN_CLIMATE_ADRES")?></th>
		    <th><?=GetMessage("ASTDESIGN_CLIMATE_REJIM_RABOTY")?></th>
		    <th><?=GetMessage("ASTDESIGN_CLIMATE_NALICIE")?></th>
		</tr>
		<?foreach($arResult["STORES"] as $pid=>$arProperty):?>
			<tr>
			    <td>
			    	<?=$arProperty["TITLE"]?>
			    	<?if($arParams['USE_STORE_PHONE']=='Y'):?>
			    		<br /><b><?=GetMessage("ASTDESIGN_CLIMATE_TELEFON")?></b>: <?=$arProperty["PHONE"]?>
			    	<?endif?>
			    </td>
			    <td><?=$arProperty["SCHEDULE"]?></td>
			    <td>
			    	<?if($arProperty["AMOUNT"] >= 5):?>
						<span class="high"><?=GetMessage("ASTDESIGN_CLIMATE_MNOGO")?></span>
			    	<?elseif($arProperty["AMOUNT"] > 2):?>
						<span class="medium"><?=GetMessage("ASTDESIGN_CLIMATE_SREDNE")?></span>
			    	<?elseif($arProperty["AMOUNT"] > 0):?>
						<span class="low"><?=GetMessage("ASTDESIGN_CLIMATE_MALO")?></span>
			    	<?else:?>
			    		<span class="no"><?=GetMessage("ASTDESIGN_CLIMATE_NET")?></span>
			    	<?endif?>
			    </td>
			</tr>
		<?endforeach?>
	</table>
<?endif;?>