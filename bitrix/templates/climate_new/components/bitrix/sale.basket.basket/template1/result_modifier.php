<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule('catalog');
function getImageById($id) {
    CModule::IncludeModule('catalog');
    //$res = CCatalogProduct::GetByIDEx($id);
    CModule::IncludeModule('iblock');
    $elem = false;
    $elem = CIBlockElement::GetByID($id)->GetNextElement();
    if($elem) {
        $res = $elem->GetFields();
        $res['INSTALL'] = $elem->GetProperty('INSTALL');
        $res['ARTICUL'] = $elem->GetProperty('ARTICUL');
        $res['PRODUCT'] = CCatalogProduct::GetByID($id);
        return $res;
    }
    return false;
}
$ids = array(); $install_ids = array(); $install_iblock = 0;
foreach (array('AnDelCanBuy', 'DelDelCanBuy', 'nAnCanBuy', 'ProdSubscribe') as $code) {
    foreach ($arResult["ITEMS"][$code] as $i=>$item) {
        $elem = getImageById($item['PRODUCT_ID']);
        $arResult["ITEMS"][$code][$i]['DETAIL_PICTURE'] = $elem['DETAIL_PICTURE'];
        $arResult["ITEMS"][$code][$i]['INSTALL'] = $elem['INSTALL']['VALUE'];
        $arResult["ITEMS"][$code][$i]['ARTICUL'] = $elem['ARTICUL']['VALUE'];
        $arResult["ITEMS"][$code][$i]['PRODUCT'] = $elem['PRODUCT'];
        if (!$install_iblock) $install_iblock = intval($elem['INSTALL']['LINK_IBLOCK_ID']);
        foreach ($elem['INSTALL']['VALUE'] as $id) $install_ids[$id]=$id;
    }
}
$arResult['I'] = array();
if (count($install_ids)) {
    $filter = array('IBLOCK_ID'=>$install_iblock, 'ID'=>$install_ids);
    $res = CIBlockElement::GetList(array('NAME'=>'ASC'), $filter, false, false, array('ID','NAME','PROPERTY_PRICE'));
    while ($r = $res->Fetch()) $arResult['I'][$r['ID']] = $r;
}