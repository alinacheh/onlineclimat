<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (StrLen($arResult["ERROR_MESSAGE"])<=0)
{
	if(is_array($arResult["WARNING_MESSAGE"]) && !empty($arResult["WARNING_MESSAGE"]))
	{
		foreach($arResult["WARNING_MESSAGE"] as $v)
		{
			echo ShowError($v);
		}
	}
	?>
	<script>$(function(){
		$('#tabs_wrapper .sort span:last').addClass('last');
		$('#wide').prepend($('h1').css('margin-bottom', '23px'));
	})</script>
	<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="basket_form">
		<div id="tabs_wrapper">
			<div class="sort left">
		        <span class="first current tabs"><a class="tabs" id="tab1" href="#atab1"><?=GetMessage('SALE_READY_TITLE')?></a></span>
		        <?if($arResult["ShowDelay"]=='Y'):?><span class="tabs"><a class="tabs" id="tab2" href="#atab2"><?=GetMessage('SALE_OTLOG_TITLE')?></a></span><?endif?>
		        <?if($arResult["ShowNotAvail"]=='Y'):?><span class="tabs"><a class="tabs" id="tab3" href="#atab3"><?=GetMessage('SALE_UNAVAIL_TITLE')?></a></span><?endif?>
		        <?if($arResult["ShowSubscribe"]=='Y'):?><span class="tabs"><a class="tabs" id="tab4" href="#atab4"><?=GetMessage('SALE_NOTIFY_TITLE')?></a></span><?endif?>
		    </div>
		    <div class="clear"></div>
			<div class="tabs con_tab active" id="con_tab1">
				<a name="atab1"></a>
				<?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items.php");?>
				<div class="line"></div>
			</div>
			<?if($arResult["ShowDelay"]=='Y'):?>
				<div class="tabs con_tab" id="con_tab2">
					<a name="atab2"></a>
					<?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_delay.php");?>
					<div class="line"></div>
				</div>
			<?endif?>
			<?if($arResult["ShowNotAvail"]=='Y'):?>
				<div class="tabs con_tab" id="con_tab3">
					<a name="atab3"></a>
					<?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_notavail.php");?>
					<div class="line"></div>
				</div>
			<?endif?>
			<?if($arResult["ShowSubscribe"]=='Y'):?>
				<div class="tabs con_tab" id="con_tab4">
					<a name="atab4"></a>
					<?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_subscribe.php");?>
					<div class="line"></div>
				</div>
			<?endif?>
		</div>
		<input type="hidden" value="Y" name="BasketRefresh" id="BasketRefreshBtn">
	</form>
	<?
} else ShowError($arResult["ERROR_MESSAGE"]);
?>