<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<table class="items basket view3">
    <tr>
        <th class="name"><?=GetMessage("ASTDESIGN_CLIMATE_TOVAR")?></th>
        <th class="count2"><?=GetMessage("ASTDESIGN_CLIMATE_KOL_VO")?></th>
        <th class="price"><?=GetMessage("ASTDESIGN_CLIMATE_CENA")?></th>
    </tr>
    <?foreach($arResult["ITEMS"]["nAnCanBuy"] as $arBasketItems):?>
        <tr class="basketitem">
            <input type="hidden" name="DELETE_<?=$arBasketItems["ID"]?>" id="DELETE_<?=$arBasketItems["ID"]?>" value="">
            <td class="tovar clear item<?=$arBasketItems["ID"]?>">
                <a href="<?=$arBasketItems["DETAIL_PAGE_URL"]?>" class="left img"><img src="<?=Refs::get_resize_src($arBasketItems['DETAIL_PICTURE'],130,130)?>"></a>
                <div class="relative left">
                    <a href="<?=$arBasketItems["DETAIL_PAGE_URL"]?>" class="title2"><?=$arBasketItems["NAME"]?></a>
                    <div class="clear"></div>
                </div>
            </td>
            <td class="count2 clear item<?=$arBasketItems["ID"]?>">
                <input type="text" readonly="readonly" class="count left" name="QUANTITY_<?=$arBasketItems["ID"] ?>" value="<?=$arBasketItems["QUANTITY"]?>">
            </td>
            <td class="price relative">
                <div style="position: relative;">
                    <?=FormatCurrency($arBasketItems["PRICE"], $arBasketItems['CURRENCY'])?>
                    <a data-id="<?=$arBasketItems["ID"]?>" href="#" class="close"></a>
                </div>
            </td>
        </tr>
    <?endforeach?>
</table>