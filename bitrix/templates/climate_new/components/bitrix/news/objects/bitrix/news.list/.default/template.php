<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="our-works-list">
    <?php
    foreach($arResult["ITEMS"] as $arItem) {
        ?>
        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="our-works-list__item">
            <div class="our-works-list__item-wrapper">
                <img src="<?=Refs::get_resize_src($arItem['DETAIL_PICTURE'],315,230)?>" alt="">
                <div>
                    <?=$arItem['NAME']?>
                </div>
            </div>
        </a>
        <?php
    }
    ?>
</div>
<div id="pagination"><?=$arResult["NAV_STRING"]?></div>