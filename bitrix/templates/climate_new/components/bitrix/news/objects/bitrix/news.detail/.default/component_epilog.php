<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$GLOBALS['otherFilter']['!ID'] = $arResult['ID'];
$GLOBALS['newsrelatedproducts']['SHOW_RELATED'] = false;
$GLOBALS['newsrelatedproducts']['SHOW_RELATED_BLOCK_ID'] = 0;
if (is_array($arResult['PRODUCTS']) && count($arResult['PRODUCTS'])) {
    $GLOBALS['newsrelatedproducts']['SHOW_RELATED_BLOCK_ID'] = 1;
    $GLOBALS['newsrelatedproducts']['SHOW_RELATED'] = true;
    $GLOBALS['newsrelatedproductsFilter'] = array('ID' => $arResult['PRODUCTS']);
}
$APPLICATION->AddHeadScript('//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/assets/PhotoSwipe/photoswipe.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/assets/PhotoSwipe/photoswipe-ui-default.min.js');

if ($arResult['MORE_PHOTO']) {
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.js-our-works-item__gallery-main-slider').slick({
                arrows: false,
                asNavFor: '.js-our-works-item__gallery-add-slider'
            });

            $('.js-our-works-item__gallery-add-slider').slick({
                arrows: false,
                centerMode: true,
                focusOnSelect: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.js-our-works-item__gallery-main-slider'
            });

            const pswpElement = document.querySelectorAll('.pswp')[0];

            const items = [
                <?php
                    $cnt = count($arResult['MORE_PHOTO']) - 1;
                foreach ($arResult['MORE_PHOTO'] as $k => $photo){
                ?>
                {
                    src: '<?=$photo['src']?>',
                    w: <?=$photo['width']?>,
                    h: <?=$photo['height']?>
                }<?=$cnt==$k?"":","?>
                <?php
                }
                ?>
            ];

            const mainPhotos = document.querySelectorAll('.js-our-works-item__gallery-main-slider-item');
            [].forEach.call(mainPhotos, function (photo, index) {

                photo.addEventListener('click', function () {
                    const options = {
                        index: index - 1,
                        shareEl: false,
                    };

                    const gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
                    gallery.init();
                });
            })
        });
    </script>
    <?php
}