<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<div class="our-works-item">
    <?php
    if (count($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'])) {
        ?>
        <div class="our-works-item__gallery">
            <div class="our-works-item__gallery-main-slider js-our-works-item__gallery-main-slider">
                <?php
                foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $fileId) {
                    $file = Refs::get_resize_src($fileId, 1200, 800);
                    ?>
                    <div>
                        <div class="our-works-item__gallery-main-slider-item js-our-works-item__gallery-main-slider-item">
                            <img src="<?= $file ?>" alt="">
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="our-works-item__gallery-add-slider js-our-works-item__gallery-add-slider">
                <?php
                foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $fileId) {
                    $file = Refs::get_resize_src($fileId, 1200, 800);
                    ?>
                    <div>
                        <div class="our-works-item__gallery-add-slider-item">
                            <div>
                                <img src="<?= $file ?>" alt="">
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="our-works-item__content">
        <?php
        if ($arResult['PREVIEW_TEXT']) {
            ?>
            <p>
                <?= $arResult['PREVIEW_TEXT'] ?>
            </p>
            <?php
        }
        ?>
        <a href="#" class="our-works-item__content-button js-order-service" data-id="<?=$arResult["ID"]?>">
            Заказать услугу
        </a>
        <?php
        if ($arResult['DETAIL_TEXT']) {
            ?>
            <h2>Услуги, которые были оказаны:</h2>
            <?= $arResult['DETAIL_TEXT'] ?>
            <?php
        }
        ?>
    </div>
</div>