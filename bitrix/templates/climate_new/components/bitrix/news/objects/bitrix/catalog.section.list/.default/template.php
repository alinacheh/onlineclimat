<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
if (!count($arResult['SECTIONS'])) {
    return;
}
?>
<div class="our-works-categories">
    <?php
    foreach ($arResult['SECTIONS'] as $arSection) {
        ?>
        <div>
            <a href="<?= $arSection['SECTION_PAGE_URL'] ?>">
                <?= $arSection['NAME'] ?>
            </a>
        </div>
        <?php
    }
    ?>
</div>