<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arResult['PRODUCTS'] = $arResult['PROPERTIES']['PRODUCTS']['VALUE'];
if (count($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'])) {
    $arResult['MORE_PHOTO'] = [];
    foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $fileId) {
        $file = Refs::get_resize_src($fileId, 1200, 800);
        $sizes = getimagesize($_SERVER['DOCUMENT_ROOT'] . $file);
        $arResult['MORE_PHOTO'][] = [
            'src' => $file,
            'width' => $sizes[0],
            'height' => $sizes[1]
        ];
    }
}
$this->__component->setResultCacheKeys(['PRODUCTS', 'MORE_PHOTO']);