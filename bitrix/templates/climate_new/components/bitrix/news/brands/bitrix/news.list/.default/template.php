<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="news">
	<?foreach($arResult["ITEMS"] as $arItem):
		$use_date = ($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]);?>
		<article class="<?if(!$use_date):?>nodate<?endif?>">
			<?if($arItem['DETAIL_PICTURE']):?>
				<img class="img_ext left" src="<?=Refs::get_resize_src($arItem['DETAIL_PICTURE'],130,130,$mode)?>" width="130" alt="<?=$arItem["NAME"]?>"/>
			<?endif?>
	        <?if($use_date):?>
        		<span class="date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
	        <?endif?>
	        <!-- a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="title"><?=$arItem["NAME"]?></a -->
	        <div class="toggle-n"><?=$arItem["NAME"]?></div>
	        <div class="text-toggle-this" id="info-<?=$arItem['ID']?>">
	        	<?=$arItem['DETAIL_TEXT'];?>
	        </div> 
	        <p><?=$arItem["PREVIEW_TEXT"]?strip_tags($arItem["PREVIEW_TEXT"]):mb_substr(strip_tags($arItem["DETAIL_TEXT"]), 0, 500);?></p>
	        <div class="clear"></div>
	    </article>
	<?endforeach;?>
</div>
<div id="pagination"><?=$arResult["NAV_STRING"]?></div>