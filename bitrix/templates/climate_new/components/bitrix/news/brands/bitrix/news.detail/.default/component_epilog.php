<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$GLOBALS['otherFilter']['!ID'] = $arResult['ID'];
$GLOBALS['newsrelatedproducts']['SHOW_RELATED'] = false;
$GLOBALS['newsrelatedproducts']['SHOW_RELATED_BLOCK_ID'] = 0;
if (is_array($arResult['PROPERTIES']['LINK_ITEMS']['VALUE']) && count($arResult['PROPERTIES']['LINK_ITEMS']['VALUE'])) {
	$GLOBALS['newsrelatedproducts']['SHOW_RELATED_BLOCK_ID'] = $arResult['PROPERTIES']['LINK_ITEMS']['LINK_IBLOCK_ID'];
	$GLOBALS['newsrelatedproducts']['SHOW_RELATED'] = true;
	$GLOBALS['newsrelatedproducts']['SHOW_RELATED_PLACE'] = $arResult['PROPERTIES']['LINK_ITEMS_PLACE']['VALUE_XML_ID'];
	$GLOBALS['newsrelatedproductsFilter'] = array('ID' => $arResult['PROPERTIES']['LINK_ITEMS']['VALUE']);
}