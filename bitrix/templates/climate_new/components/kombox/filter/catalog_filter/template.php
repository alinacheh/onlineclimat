<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="smart_filter_hidden" style="display:none">
	<div class="filter">
		<div class="h3"><?echo GetMessage("CT_BCSF_FILTER_TITLE")?></div>
		<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
			<?foreach($arResult["HIDDEN"] as $arItem):?>
				<input
					type="hidden"
					name="<?echo $arItem["CONTROL_NAME"]?>"
					id="<?echo $arItem["CONTROL_ID"]?>"
					value="<?echo $arItem["HTML_VALUE"]?>"
				/>
			<?endforeach;?>
			<div class="filtren">
				<?$pids=0; foreach($arResult["ITEMS_ORDER"] as $ind => $pid):
					$arItem=$arResult["ITEMS"][$pid];
					if($arItem["PROPERTY_TYPE"] == "N" || isset($arItem["PRICE"])):
						$min = intval($arItem["VALUES"]["MIN"]["VALUE"]);
			            $max = intval($arItem["VALUES"]["MAX"]["VALUE"]);
			            if ($min == $max) continue; $pids++?>
						<div class="acc">
							<a href="#" onclick="BX.toggle(BX('ul_<?echo $arItem["ID"]?>')); return false;" class="<?if($ind<2):?>current<?endif?> showchild"><?=$arItem["NAME"]?>:</a>
							<div id="ul_<?echo $arItem["ID"]?>">
			                    <div data-id="<?=$arItem["ID"]?>" class="formCost formPrice<?=$arItem["ID"]?>">
			                        <label for="min-price"><?echo GetMessage("CT_BCSF_FILTER_FROM")?>: </label>
			                        <input
										class="min-price min-price<?=$arItem["ID"]?>"
										type="text"
										data-id="<?=$arItem["ID"]?>"
										name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
										id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
										value="<?=$min?>"
										size="5"
										onkeyup="smartFilter.keyup(this)"
									/>
			                        <label for="max-price"><?echo GetMessage("CT_BCSF_FILTER_TO")?>: </label>
			                        <input
										class="max-price max-price<?=$arItem["ID"]?>"
										type="text"
										data-id="<?=$arItem["ID"]?>"
										name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
										id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
										value="<?=$max?>"
										size="5"
										onkeyup="smartFilter.keyup(this)"
									/>
			                    </div>
			                    <?if(1||isset($arItem['PRICE'])):?>
				                    <div class="sliderCont"><div data-id="<?=$arItem["ID"]?>" id="slider<?=$arItem["ID"]?>" class="slider slider<?=$arItem["ID"]?>"></div></div>
				                    <span class="mark left"><?=number_format($min,0,'.',' ')?></span>
				                    <span class="mark right"><?=number_format($max,0,'.',' ')?></span>
				                    <?if($arItem["VALUES"]["MIN"]["HTML_VALUE"] || $arItem["VALUES"]["MAX"]["HTML_VALUE"]):?>
					                    <script>$(function(){
											<?if($v=intval($arItem["VALUES"]["MIN"]["HTML_VALUE"])):?>
												$('input.min-price<?=$arItem["ID"]?>').val(<?=$v?>).change();
											<?endif?>
											<?if($v=intval($arItem["VALUES"]["MAX"]["HTML_VALUE"])):?>
												$('input.max-price<?=$arItem["ID"]?>').val(<?=$v?>).change();
											<?endif?>
					                    })</script>
				                    <?endif?>
			                    <?endif?>
			                    <div class="clear"></div>
			                </div>
						</div>
					<?elseif(!empty($arItem["VALUES"])):$pids++;?>
						<div class="acc">
							<a href="#" onclick="BX.toggle(BX('ul_<?echo $arItem["ID"]?>')); return false;" class="<?if($ind<2):?>current<?endif?> showchild"><?=$arItem["NAME"]?>:</a>
							<div id="ul_<?echo $arItem["ID"]?>" class="made">
								<?foreach($arItem["VALUES"] as $val => $ar):?>
									<label for="<?echo $ar["CONTROL_ID"]?>"><input
										type="checkbox"
										value="<?echo $ar["HTML_VALUE"]?>"
										name="<?echo $ar["CONTROL_NAME"]?>"
										id="<?echo $ar["CONTROL_ID"]?>"
										<?echo $ar["CHECKED"]? 'checked="checked"': ''?>
										onclick="smartFilter.click(this)"
									/> <?echo $ar["VALUE"];?></label>
								<?endforeach;?>
							</div>
						</div>
					<?endif;?>
				<?endforeach;?>
				<div class="btn">
					<input type="submit" id="set_filter" name="set_filter" value="<?=GetMessage("CT_BCSF_SET_FILTER")?>" />
					<br />
					<input type="submit" id="del_filter" name="del_filter" value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>" />
				</div>
				<?/*<div class="modef" style="display:none" id="modef">
					<?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval(count($arResult["ITEMS"])).'</span>'));?>
					<a href="<?echo $arResult["FILTER_URL"]?>" class="showchild"><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
					<!--<span class="ecke"></span>-->
				</div>*/?>
			</div>
		</form>
	</div>
</div>
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($APPLICATION->GetCurPageParam())?>');
	$(function(){$('#smart_filter_hidden > div').insertAfter('#smart_filter');})
</script>