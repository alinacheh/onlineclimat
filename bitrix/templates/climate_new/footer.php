<?
$bScriptInFooter = !$USER->isAuthorized();
if ($bScriptInFooter) {
    $APPLICATION->ShowHeadStrings();
    $APPLICATION->ShowHeadScripts();
}
	IncludeTemplateLangFile(__FILE__);
	$is_main_page = ($APPLICATION->GetCurPage(true) == '/index.php');
	$is_print = $_REQUEST['print'] == 'Y';
?>





				<?if(!$is_print):?>
        			</div>
        			<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
						"AREA_FILE_SHOW" => "sect",
						"AREA_FILE_SUFFIX" => "right",
						"AREA_FILE_RECURSIVE" => "Y",
						"EDIT_TEMPLATE" => ""
						),
						false
					);?>
			        <div class="clear"></div>
		        <?endif?>
				</div>
				<div class="clear"></div>
		</section>
		<?if(!$is_print):?>
			<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR."sect_about.php",
                    "EDIT_TEMPLATE" => ""
                    ),
                    false
                );?>
            <?/*$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "sect",
				//"AREA_FILE_SUFFIX" => "about",
                "PATH" => SITE_DIR."sect_about.php",
				//"AREA_FILE_RECURSIVE" => "N",
				"EDIT_TEMPLATE" => ""
				),
				false
			);*/?>
			<footer class="container" >
				
				<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
					"ROOT_MENU_TYPE" => "top",
					"MENU_CACHE_TYPE" => "N",
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_CACHE_GET_VARS" => "",
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "left",
					"USE_EXT" => "N",
					"DELAY" => "Y",
					"ALLOW_MULTI_SELECT" => "N",
					),
					false
				);?>

				<div class="clear"></div>
				<div class="line"></div>
				<div id="astdesign" class="right">
					
				</div>
				<div id="copy">
					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR."copy.inc.php",
						"EDIT_TEMPLATE" => ""
						),
						false
					);?>
					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR."copy_under.inc.php",
						"EDIT_TEMPLATE" => ""
						),
						false
					);?>
				</div>
				
				<div class="f-menu">
					<?$APPLICATION->IncludeComponent("bitrix:menu", "left_big3", array(
	"ROOT_MENU_TYPE" => "top",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "N",
	"DELAY" => "Y",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
				</div>
				
				</div>
				<div class="tel right"><?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR."phone.inc.php",
					"EDIT_TEMPLATE" => ""
					),
					false
				);?><div style="text-align: center; margin: 0px 0px 10px;" class=""><img src="/img/email.png" style="float: left;
    margin: 13px -12px 0px 14px;"><a href="mailto:mail@on-lineclimat.ru" class="email_bottom">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR."email.inc.php",
					"EDIT_TEMPLATE" => ""
					),
					false
				);?></a></div></div>
				
				<div style="clear: both;"></div>
				<p style="text-align: center;">© 2017 «Онлайн-Климат»</p>
			</footer>
            <div id="goToUp"></div>
			<div id="shadow"></div>
			<div id="comparator" class="window2 compareList com">
				<div class="close"></div>
				<div class="title"><?=GetMessage("ASTDESIGN_CLIMATE_SPISOK_SRAVNENIA")?></div>
				<div id="comparator_inner"><?include $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'ajax/addToCompare.php'?></div>
			</div>
			<div id="qorder" class="window com enter">
	            <div class="close"></div>
	            <div class="title"><?=GetMessage("ASTDESIGN_CLIMATE_BYSTRYY_ZAKAZ")?></div>
	            <form method="post" id="qorder_form" onsubmit="yaCounter37461485.reachGoal('bystryj_zakaz_otpravit');ga('send', 'pageview', '/bystryj_zakaz_otpravit');">
            		<input type="hidden" id="qorder_items" name="items" value="" />
            		<input type="hidden" id="qorder_page" name="page" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" />
	                <label class="input"><?=GetMessage("ASTDESIGN_CLIMATE_IMA")?><input required type="text" name="name" value="<?=$GLOBALS['USER']->GetFullName()?>"></label>
	                <label class="input"><?=GetMessage("ASTDESIGN_CLIMATE_TELEFON")?><input required type="text" class="phone" name="phone" value=""></label>
	                
					<div class="agreement">
						<input autocomplete="off" type="checkbox" checked />
						<label>Я прочитал(а) и принимаю условия <a href="/polzovatelskoe-soglashenie/" target="_blank">пользовательского соглашения</a></label>
					</div>
	                
	                <div class="btn_a alt"><button onclick="yaCounter37461485.reachGoal('bystryj_zakaz');ga('send', 'pageview', '/bystryj_zakaz_otpravit');$(this).parents('form').submit(); return false;"><?=GetMessage("ASTDESIGN_CLIMATE_OTPRAVITQ")?></button></div>
	            </form>
	        </div>
            <div id="qorderService" class="window com enter">
                <div class="close"></div>
                <div class="title"><?=GetMessage("ASTDESIGN_CLIMATE_BYSTRYY_ZAKAZ_SERVICE")?></div>
                <form method="post" id="qorder_form_service" onsubmit="yaCounter37461485.reachGoal('bystryj_zakaz_service_otpravit');ga('send', 'pageview', '/bystryj_zakaz_service_otpravit');">
                    <input type="hidden" id="qorder_items_service" name="items" value="" />
                    <label class="input"><?=GetMessage("ASTDESIGN_CLIMATE_IMA")?><input required type="text" name="name" value="<?=$GLOBALS['USER']->GetFullName()?>"></label>
                    <label class="input"><?=GetMessage("ASTDESIGN_CLIMATE_TELEFON")?><input required type="text" class="phone" name="phone" value=""></label>
                    <div class="btn_a alt"><button onclick="yaCounter37461485.reachGoal('bystryj_zakaz_service');ga('send', 'pageview', '/bystryj_zakaz_service_otpravit');$(this).parents('form').submit(); return false;"><?=GetMessage("ASTDESIGN_CLIMATE_OTPRAVITQ")?></button></div>
                </form>
            </div>
	        <?if(!$GLOBALS['USER']->IsAuthorized()):?>
		        <div class="window wait" id="popup_form_notify">
					<div class="close"></div>
					<div class="title"><?=GetMessage("ASTDESIGN_CLIMATE_UVEDOMITQ_O_POSTUPLE")?></div>
					<form id="popup_form_notify_form" method="post">
						<div id="popup_n_error" style="color:red;"></div>
						<input id="notify_id" type="hidden" name="id" value="" />
						<input type="hidden" name="subscribe" value="Y" />
						<label class="input"><?=GetMessage("ASTDESIGN_CLIMATE_VAS")?> email <input type="text" value="" name="user_mail" size="25" class="text-placeholder"></label>
						<small id="success_notify" style="display:none"><br /><?=GetMessage("ASTDESIGN_CLIMATE_SPASIBO_KOGDA_TOVAR")?><br /><br /></small>
						<div class="btn alt"><button type="submit" onclick="$(this).parents('form').submit();return false;"><?=GetMessage("ASTDESIGN_CLIMATE_OTPRAVITQ1")?></button></div>
					</form>
				</div>
			<?endif?>
			<div class="window wait" id="popup_form_notify_success" style="display:none">
				<div class="close"></div>
				<div class="title"><?=GetMessage("ASTDESIGN_CLIMATE_UVEDOMITQ_O_POSTUPLE")?></div>
				<small><br /><?=GetMessage("ASTDESIGN_CLIMATE_SPASIBO_KOGDA_TOVAR")?><br /><br /></small>
			</div>
			<div class="window wait" id="popup_form_notify_already" style="display:none">
				<div class="close"></div>
				<div class="title"><?=GetMessage("ASTDESIGN_CLIMATE_UVEDOMITQ_O_POSTUPLE")?></div>
				<small class="success"><br /><?=GetMessage("ASTDESIGN_CLIMATE_ETOT_TOVAR_UJE_DOBAV")?><br /><br /></small>
			</div>
			<div id="add_in_basket" class="add right" style="display:none"><?=GetMessage("ASTDESIGN_CLIMATE_TOVAR_DOBAVLEN")?></div>
		<?endif?>
       <?
            include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
            $cpt = new CCaptcha();
            $captchaPass = COption::GetOptionString("main", "captcha_password", "");
            if(strlen($captchaPass) <= 0)
            {
                $captchaPass = randString(10);
                COption::SetOptionString("main", "captcha_password", $captchaPass);
            }
            $cpt->SetCodeCrypt($captchaPass);
       ?>
        <div class="popup-online">
            <div class="close"></div>
            <span class="title">Online заказ товара</span>
            <form action="javascript:;" id="onlineorder" enctype="multipart/form-data">

                <div class="popup-line">
                    <span>Ваше имя</span>
                    <input name="name" required type="text">
                </div>
                <div class="popup-line">
                    <span>E-mail или Телефон</span>
                    <input type="text" required name="email" placeholder="example@yandex.ru">
                </div>
                <div class="popup-line">
                    <span>Заказ</span>
                    <textarea type="text" name="message" placeholder="Напишите сюда список оборудования который хотелы бы заказать, и другую необходимую информацию."></textarea>
                </div>
                <div class="popup-line">
                    <span>Файл</span>
                    <input type="file" name="file" value="Прикрепить файл">
                </div>
                <div class="popup-line captcha">
                    <span>Проверочный код</span>
                    <input name="captcha_code" value="<?=htmlspecialchars($cpt->GetCodeCrypt());?>" type="hidden">
                    <input id="captcha_word" required name="captcha_word" type="text">
                    <img src="/bitrix/tools/captcha.php?captcha_code=<?=htmlspecialchars($cpt->GetCodeCrypt());?>">
                </div>
                <div class="popup-line">
                    <span></span>
                    <input type="submit" name="send" value="Отправить">
                </div>
            </form>
        </div>
        <div class="overlay-black"></div>
<!-- BEGIN JIVOSITE CODE {literal} --> 
<script type='text/javascript'> 
(function(){ var widget_id = '160007'; 
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script> 
<!-- {/literal} END JIVOSITE CODE -->



<?
if(preg_match('#/brands/.*html#siU', $_SERVER['REQUEST_URI'])){
	$tit = $APPLICATION->GetTitle();
	$newTit = 'Товары производителя '.$tit;
	$APPLICATION->SetTitle($newTit);
}



$TitGen = array(
'/catalog/konditsionery/nastennye_split_sistemy/aux/',
'/catalog/konditsionery/nastennye_split_sistemy/s_ballu/',
// '/catalog/konditsionery/nastennye_split_sistemy/daikin/',
'/catalog/konditsionery/nastennye_split_sistemy/s_electrolux/e_portofino/',
// '/catalog/konditsionery/nastennye_split_sistemy/mitsubishi_electric/',
'/catalog/konditsionery/nastennye_split_sistemy/panasonic/',
'/catalog/konditsionery/nastennye_split_sistemy/?PAGEN_7=2',
'/catalog/konditsionery/nastennye_split_sistemy/?PAGEN_7=3',
'/catalog/konditsionery/nastennye_split_sistemy/?PAGEN_7=4',
'/catalog/konditsionery/nastennye_split_sistemy/?PAGEN_7=5',
'/catalog/konditsionery/nastennye_split_sistemy/s_electrolux/',
'/catalog/konditsionery/nastennye_split_sistemy/?PAGEN_7=17',
'/catalog/konditsionery/nastennye_split_sistemy/aux/?arrFilter_4_2974987658=Y&set_filter=Y',
'/catalog/konditsionery/nastennye_split_sistemy/s_ballu/?arrFilter_4_1842515611=Y&set_filter=Y',
'/catalog/konditsionery/nastennye_split_sistemy/daikin/?arrFilter_4_450215437=Y&set_filter=Y',
'/catalog/konditsionery/nastennye_split_sistemy/s_electrolux/?arrFilter_4_2280940908=Y&set_filter=Y',
'/catalog/konditsionery/nastennye_split_sistemy/aux/?PAGEN_7=2',
'/catalog/konditsionery/nastennye_split_sistemy/aux/?PAGEN_7=3',
'/catalog/konditsionery/nastennye_split_sistemy/daikin/?PAGEN_7=2',
'/catalog/konditsionery/nastennye_split_sistemy/daikin/?PAGEN_7=3',
'/catalog/konditsionery/nastennye_split_sistemy/mitsubishi_electric/?PAGEN_7=2',
'/catalog/konditsionery/nastennye_split_sistemy/mitsubishi_electric/?PAGEN_7=3',
'/catalog/konditsionery/nastennye_split_sistemy/mitsubishi_electric/?PAGEN_7=4',
'/catalog/konditsionery/nastennye_split_sistemy/?PAGEN_7=6',
'/catalog/konditsionery/nastennye_split_sistemy/?PAGEN_7=7',
'/catalog/konditsionery/nastennye_split_sistemy/s_electrolux/?PAGEN_2=2',
'/catalog/konditsionery/nastennye_split_sistemy/s_electrolux/?PAGEN_2=3',
'/catalog/konditsionery/nastennye_split_sistemy/?PAGEN_7=16',
'/catalog/konditsionery/nastennye_split_sistemy/?PAGEN_7=13',
'/catalog/konditsionery/nastennye_split_sistemy/?PAGEN_7=14',
'/catalog/konditsionery/nastennye_split_sistemy/?PAGEN_7=15',


);

if (in_array($_SERVER['REQUEST_URI'], $TitGen)){
	$tit = $APPLICATION->GetTitle();
	$APPLICATION->SetPageProperty('title','Купить настенную сплит систему '.$tit.' в москве недорого | Лучшие цены на сплит системы для квартир и офисов');
}

/*if(isset($_GET['PAGEN_7'])){
	$tit = $APPLICATION->GetPageProperty('title');
	$desc = $APPLICATION->GetPageProperty('description');
	$APPLICATION->SetPageProperty('description',$desc. '. Страница № '.$_GET['PAGEN_7']);
	$APPLICATION->SetPageProperty('title',$tit. '. Страница № '.$_GET['PAGEN_7']);
}*/




$GenDesc = array(
'/brands/acv.html',
'/brands/advantix.html',
'/brands/air_o_swiss.html',
'/brands/akvastorozh.html',
'/brands/arbonia_.html',
'/brands/atlantic.html',
'/brands/atol.html',
'/brands/atoll.html',
'/brands/aux.html',
'/brands/ballu.html',
'/brands/baxi.html',
'/brands/beretta.html',
'/brands/boneco.html',
'/brands/buderus.html',
'/brands/chekhiya.html',
'/brands/cordivari_srl.html',
'/brands/daikin.html',
'/brands/daniya.html',
'/brands/dospel.html',
'/brands/drazice.html',
'/brands/electrolux.html',
'/brands/exemet.html',
'/brands/faral.html',
'/brands/germaniya.html',
'/brands/global_.html',
'/brands/grundfos.html',
'/brands/hitachi.html',
'/brands/honeywell.html',
'/brands/irsap.html',
'/brands/italiya.html',
'/brands/itermic.html',
'/brands/kermi_raya.html',
'/brands/kermi.html',
'/brands/kiturami_.html',
'/brands/lg.html',
'/brands/margaroli_srl_.html',
'/brands/minib_.html',
'/brands/mitsubishi_heavy.html',
'/brands/mitsubishi-electric.html',
'/brands/neptun.html',
'/brands/noirot.html',
'/brands/norvegiya.html',
'/brands/oventrop.html',
'/brands/panasonic.html',
'/brands/pioneer_.html',
'/brands/polsha.html',
'/brands/primoclima.html',
'/brands/protherm.html',
'/brands/rifar.html',
'/brands/rinnai.html',
'/brands/rommer.html',
'/brands/rossiya_germaniya.html',
'/brands/rossiya.html',
'/brands/royal_clima.html',
'/brands/royal_thermo_.html',
'/brands/sira.html',
'/brands/sr_rubinetterie.html',
'/brands/stiebel_eltron_.html',
'/brands/tece.html',
'/brands/teplolyuks.html',
'/brands/terma.html',
'/brands/terminus.html',
'/brands/toshiba_.html',
'/brands/vaillant.html',
'/brands/varmann.html',
'/brands/viessmann.html',
'/brands/zanussi.html',
);

if (in_array($_SERVER['REQUEST_URI'], $GenDesc)){
	$tit = $APPLICATION->GetTitle();

	$seoTitle=$APPLICATION->GetPageProperty("title");

	$APPLICATION->SetPageProperty('title', $tit.' | '.$seoTitle);



	$APPLICATION->SetPageProperty('description','Компания "Онлайн Климат" предлагает купить '.$tit.' в Москве с доставкой по всей россии. Каталог - большой выбор по низким ценам. Мы обеспечиваем высокое качество продукции и работаем только с надежными поставщиками.');
}


if(preg_match('#/brands/.*#siU', $_SERVER['REQUEST_URI'])){
	if(isset($_GET['PAGEN_1'])){
		$APPLICATION->SetPageProperty('description','Компания "Онлайн Климат" предлагает ассортимент товаров от разных производителей в Москве с доставкой по всей России | Наш телефон +7 (495) 720-36-51 - стр '.$_GET['PAGEN_1']);
	}
}



// генерация title для ВСЕХ страниц пагинации по типу title = title - стр N
// генерация description для ВСЕХ страниц пагинации по типу description = description + - стр N
foreach($_GET as $paramName => $paramValue) {
	if(stristr($paramName,'PAGEN_')!==FALSE):
		if(intval($paramValue)>1):
			$curTitle = $APPLICATION->GetPageProperty('title');
			$curDesc = $APPLICATION->GetPageProperty('description');
			$APPLICATION->SetPageProperty('title', $curTitle . ' - стр ' . $paramValue);
			$APPLICATION->SetPageProperty('description', $curDesc . ' - стр ' . $paramValue);
			break;
		endif;
	endif;
}

?>

<? /*
<!-- НАЧАЛО Нужно только для детальной страницы Наши работы -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="pswp__bg"></div>
  <div class="pswp__scroll-wrap">
    <div class="pswp__container">
      <div class="pswp__item"></div>
      <div class="pswp__item"></div>
      <div class="pswp__item"></div>
    </div>
    <div class="pswp__ui pswp__ui--hidden">
      <div class="pswp__top-bar">
        <div class="pswp__counter"></div>
        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
        <button class="pswp__button pswp__button--share" title="Share"></button>
        <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
        <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
        <div class="pswp__preloader">
          <div class="pswp__preloader__icn">
            <div class="pswp__preloader__cut">
              <div class="pswp__preloader__donut"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
        <div class="pswp__share-tooltip"></div>
      </div>
      <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
      </button>
      <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
      </button>
      <div class="pswp__caption">
        <div class="pswp__caption__center"></div>
      </div>
    </div>
  </div>
</div>
<!-- КОНЕЦ Нужно только для детальной страницы Наши работы -->
*/ ?>

<!-- Top100 (Kraken) Widget -->
<span id="top100_widget"></span>
<!-- END Top100 (Kraken) Widget -->

<!-- Top100 (Kraken) Counter -->
<script>
    (function (w, d, c) {
    (w[c] = w[c] || []).push(function() {
        var options = {
            project: 4509201,
            element: 'top100_widget',
        };
        try {
            w.top100Counter = new top100(options);
        } catch(e) { }
    });
    var n = d.getElementsByTagName("script")[0],
    s = d.createElement("script"),
    f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src =
    (d.location.protocol == "https:" ? "https:" : "http:") +
    "//st.top100.ru/top100/top100.js";

    if (w.opera == "[object Opera]") {
    d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(window, document, "_top100q");
</script>
<noscript>
  <img src="//counter.rambler.ru/top100.cnt?pid=4509201" alt="Топ-100" />
</noscript>
<!-- END Top100 (Kraken) Counter -->

<?//echo($_SERVER['REQUEST_URI']);?>

    </body> 
</html>
