<?
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;

CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
CModule::IncludeModule("iblock");
CModule::IncludeModule("currency");



$arSelect = Array("ID", "IBLOCK_ID", "NAME","IBLOCK_SECTION_ID", "DATE_ACTIVE_FROM","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>1, "SECTION_ID" => $_POST['section_id'], "INCLUDE_SUBSECTIONS" => "Y", "PROPERTY_BRAND_IBLOCK" => $_POST['brands'], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("SORT" => "DESC"), $arFilter, false, Array("nPageSize"=>550), $arSelect);
while($ob = $res->GetNextElement()){
$arFields = $ob->GetFields();
$arProps = $ob->GetProperties();
//echo '<pre>';
//print_r($arFields);
//print_r($arProps['ATT_MONTAGNOE_POADK']['VALUE']);
//echo '</pre>';
$arrPodkl[]=$arProps['PROP_MATERIAL_50']['VALUE'];
}    

// echo '<pre>';
// print_r($_POST['countrooms']);
// echo '</pre>';

$arrCalcImg = array_unique($arrPodkl);
?>
<h4 class="go_call_back">Укажите площадь помещений и выберите тип внутреннего блока</h4>
<div class="calc_split_div_bottom_second">
    <?for($i=1;$i<(1 + $_POST['countrooms'][0]);$i++){?>

    <div class="room_div_container" id="room<?=$i?>">
        <div>
            <ul>
                <li>Комната <?=$i?></li>
                <li><p>Площадь: </p><input type="text" value="" class="m2_room"> <span> м2</span></li>
            </ul>
            <?if($i % 2 != 0){?>
                <b>+</b>
            <?}?>
            
        </div>

        <div onclick="Choise_type(this)">
            <img class="roompict<?=$i?> variants" src="/img/ch2.png">
        </div>

            <div class="type_cond_choise" style="<?if($i % 2 == 0){?>left: calc(-102% - 0px);<?}?>" id="rch<?=$i?>">
<i class="close<?=$i?>">✖</i>
        <?foreach($arrCalcImg as $k => $v){
            if($v == ''){continue;}
            $arSelect2 = Array("ID", "IBLOCK_ID", "NAME","IBLOCK_SECTION_ID", "PREVIEW_PICTURE","PROPERTY_*");
            $arFilter2 = Array("IBLOCK_ID"=>34, "NAME" => $v, "ACTIVE"=>"Y");
            $res2 = CIBlockElement::GetList(Array("SORT" => "DESC"), $arFilter2, false, Array("nPageSize"=>50), $arSelect2);
            $ob2 = $res2->GetNextElement();
            $arFields2 = $ob2->GetFields();
            //$arProps2 = $ob2->GetProperties();

            // echo '<pre>';
            // print_r($arFields2);
            // echo '</pre>';
            $PathImg2 = CFile::GetPath($arFields2['PREVIEW_PICTURE']);
            ?>
                <div>
                    <img src="<?=$PathImg2?>" alt="" title="<?=$v?>">
                    <p><?=$arFields2['NAME']?></p>
                </div>
            <?}?>
  
        </div>



        <script>

        $("#rch<?=$i?> img").click(function() {//подставляем картинку в комнату
var imgcrc = $(this).attr('src');
var title = $(this).attr('title');
  $("#room<?=$i?> .roompict<?=$i?>").attr("src", imgcrc);
  $("#room<?=$i?> .roompict<?=$i?>").attr("title", title);
  $('#rch<?=$i?>').css({ 'display': 'none'});

  var m2_room = $("#room<?=$i?> .m2_room").val();//красная рамка если пусто моле м2
    if(m2_room == ''){$('#room<?=$i?> .m2_room').css({ 'border': '1px solid red'});}
});

$('#room<?=$i?> .m2_room').keyup(function(){//отслеживаю ввод в инпут м2
    var innerInput = $("#room<?=$i?> .m2_room").val();
    $('#room<?=$i?> .m2_room').css({ 'border': 'none'});
    $("#room<?=$i?> .m2_room").attr("title", 1);
    if(innerInput == ''){
        $('#room<?=$i?> .m2_room').css({ 'border': '1px solid red'});
        $("#room<?=$i?> .m2_room").attr("title", 0);
        }
    
});
$(".close<?=$i?>").click(function() {
    $('#rch<?=$i?>').css({ 'display': 'none'});
});
        </script>



    </div>

    <?}?>
</div>








<!-- вывожу варианты подобраные -->
<div class="results_container rooms_blocks2"></div>





<script>

function Choise_type(a){//выбираем тип кондея
var id = a.parentNode.id;
 $('#'+id+' .type_cond_choise').css({ 'display': 'flex'});
}
</script>











<script>

$('.calc_split_div_bottom_second input').keyup(function(){//отслеживаю ввод в инпут м2

    var checkboxes = [];//собираю чекнутые бренды в массив
  $('.check_brands input:checkbox:checked').each(function(){
    checkboxes.push(this.value);
  });
if(checkboxes == ''){
    var checkboxes = $(".check_brands input").map(function(){return $(this).val();}).get();
}


var countChoiseRooms = <?=$i?>;
var sect_id = <?=$_POST['section_id']?>;
var countInputTitle = $(".m2_room").map(function(){return $(this).attr('title');}).get();
var podklVariants = $(".variants").map(function(){return $(this).attr('title');}).get();
var countM2 = $(".m2_room").map(function(){return $(this).val();}).get();

var sum=0;
for(var i=0;i<countInputTitle.length;i++){//складываю все значения title у инпута
    sum = sum + parseInt(countInputTitle[i]);
}

//alert(countM2);
if(podklVariants.length == +(countChoiseRooms - 1) && sum == +(countChoiseRooms - 1)){

      $.ajax({//в аджакс передаем данные из обратного звонка
          type: 'POST',
          url: '/bitrix/templates/aspro_optimus/ajax/calc_ajax_level2.php',
          
          data: {'countChoiseRooms': countChoiseRooms,
                 'section_id': sect_id,
                 'podklVariants': podklVariants,
                 'countM2': countM2,
                 'brands': checkboxes
          },
          success: function(data) {
              $('.rooms_blocks2').html(data);
          }
      });
}
});



$('.type_cond_choise div img').click(function() {
    

    var checkboxes = [];//собираю чекнутые бренды в массив
  $('.check_brands input:checkbox:checked').each(function(){
    checkboxes.push(this.value);
  });
if(checkboxes == ''){
    var checkboxes = $(".check_brands input").map(function(){return $(this).val();}).get();
}


    var countChoiseRooms = <?=$i?>;
var sect_id = <?=$_POST['section_id']?>;
var countInputTitle = $(".m2_room").map(function(){return $(this).attr('title');}).get();
var podklVariants = $(".variants").map(function(){return $(this).attr('title');}).get();
var countM2 = $(".m2_room").map(function(){return $(this).val();}).get();



var sum=0;
for(var i=0;i<countInputTitle.length;i++){//складываю все значения title у инпута
    sum = sum + parseInt(countInputTitle[i]);
}

//alert(countM2);
if(podklVariants.length == +(countChoiseRooms - 1) && sum == +(countChoiseRooms - 1)){
      $.ajax({//в аджакс передаем данные из обратного звонка
          type: 'POST',
          url: '/bitrix/templates/aspro_optimus/ajax/calc_ajax_level2.php',
          
          data: {'countChoiseRooms': countChoiseRooms,
                 'section_id': sect_id,
                 'podklVariants': podklVariants,
                 'countM2': countM2,
                 'brands': checkboxes
          },
          success: function(data) {
              $('.rooms_blocks2').html(data);
          }
      });
    }
});


</script>