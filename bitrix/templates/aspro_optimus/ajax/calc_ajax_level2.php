<?
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;

CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
CModule::IncludeModule("iblock");
CModule::IncludeModule("currency");


// echo $_POST['section_id'];
//print_r($_POST['podklVariants']);
//print_r($_POST['brands']);

$arrBrands = $_POST['brands'];

for($h=0;$h<count($arrBrands);$h++){

    $arSelect2 = Array("ID", "IBLOCK_ID", "NAME","IBLOCK_SECTION_ID", "DATE_ACTIVE_FROM");
    $arFilter2 = Array("IBLOCK_ID"=>2, "ID" => $arrBrands[$h], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res2 = CIBlockElement::GetList(Array("SORT" => "DESC"), $arFilter2, false, Array("nPageSize"=>50), $arSelect2);
    $ob2 = $res2->GetNextElement();
    $arFields2 = $ob2->GetFields();
?>



<div class="border_div block<?=$h?>">
<h4>Вариант <?=$arFields2['NAME']?>
<p class="rooms_blocksjj"></p></h4>

    <h5>Внутренние блоки</h5>
    <div class="top_results_div">

<?for($u=1;$u<$_POST['countChoiseRooms'];$u++){

//print_r($_POST['podklVariants']);
$arSelect = Array("ID", "IBLOCK_ID", "NAME","IBLOCK_SECTION_ID", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "DATE_ACTIVE_FROM","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>1, "SECTION_ID" => 576, "INCLUDE_SUBSECTIONS" => "Y", 
"PROPERTY_PROP_MATERIAL_50" => $_POST['podklVariants'][$u-1], 
"PROPERTY_BRAND_IBLOCK" => $arrBrands[$h],
">=PROPERTY_ATT_PLOCHAD_OBS" => $_POST['countM2'][$u-1],
"ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("PROPERTY_ATT_PLOCHAD_OBS" => "ASC"), $arFilter, false, Array("nPageSize"=>1000), $arSelect);

$ob = $res->GetNextElement();
if (is_object($ob)) {
$arFields = $ob->GetFields();
$arProps = $ob->GetProperties();

$PathImg = CFile::GetPath($arFields['PREVIEW_PICTURE']);
$rsPrices = CPrice::GetList(array(), array('PRODUCT_ID' => $arFields['ID'], 'CATALOG_GROUP_ID' => 1));
$arPrice = $rsPrices->Fetch();


if($arPrice['CURRENCY'] == 'EUR'){
    $arFilterCur = array("CURRENCY" => "EUR");
    $byCur = "date";
    $orderCur = "desc";
    
    $rowCur = CCurrencyRates::GetList($byCur, $orderCur, $arFilterCur)->Fetch();

    $arPricet['PRICE'] = $arPrice['PRICE'] * $rowCur['RATE'];

}


if($arPrice['CURRENCY'] == 'USD'){
    $arFilterCur = array("CURRENCY" => "USD");
    $byCur = "date";
    $orderCur = "desc";
    
    $rowCur = CCurrencyRates::GetList($byCur, $orderCur, $arFilterCur)->Fetch();

    $arPrice['PRICE'] = $arPrice['PRICE'] * $rowCur['RATE'];

}

?>

<div class="results_produkt_div" id="<?=$u.$arFields['ID']?>">

            <div><img class="img_ch_room" src="<?=$PathImg?>" alt=""></div>
            <div>
                <ul>
                    <li>Комната <?=$u?></li>
                    <li><a href="<?=$arFields['DETAIL_PAGE_URL']?>" target="_blank"><?=$arFields['NAME']?></a></li>
                    <li>Производитель: <i><?=$arFields2['NAME']?></i></li>
                    <li>Тип: <?=$_POST['podklVariants'][$u-1]?></li>
                    <li>Подключение: <?=$arProps['ATT_MONTAGNOE_POADK']['VALUE'][0]?></li>
                    <li>Рабочая площадь: <i><?=$arProps['ATT_PLOCHAD_OBS']['VALUE']?></i> м2</li>
                    
                    <li><p style="text-align: left;">Цена: <span class="priceCom<?=$h?>"><?=number_format($arPrice['PRICE'], 0, ',', ' ');?></span> руб.</p></li>
                    
                    <input type="hidden" class="prodID" value="<?=$arFields['ID']?>">
                </ul>
            </div>
            <?if($u % 2 != 0){?>
                <b>+</b>
            <?}?>


            


    <div class="after_podbor_change_block">
    <i class="closeup">✖</i>
        <section id="demos">
        <div class="row">
            <div class="large-12 columns">
            <div class="owl-carousel owl-theme">
                    <?//формирую доступные модели для замены в вариантах
                    $arSelectCH = Array("ID", "IBLOCK_ID", "NAME","IBLOCK_SECTION_ID", "DETAIL_PAGE_URL", "PREVIEW_PICTURE","PROPERTY_*");
                    $arFilterCH = Array("IBLOCK_ID"=>1, "SECTION_ID" => 576, "INCLUDE_SUBSECTIONS" => "Y", 
                    "PROPERTY_PROP_MATERIAL_50" => $_POST['podklVariants'][$u-1], 
                    "PROPERTY_BRAND_IBLOCK" => $arrBrands[$h],
                    ">=PROPERTY_ATT_PLOCHAD_OBS" => $_POST['countM2'][$u-1],
                    "ACTIVE"=>"Y");
                    $resCH = CIBlockElement::GetList(Array("PROPERTY_ATT_PLOCHAD_OBS" => "ASC"), $arFilterCH, false, Array("nPageSize"=>50), $arSelectCH);
                    while($obCH = $resCH->GetNextElement()){
                    $arFieldsCH = $obCH->GetFields();
                    $arPropsCH = $obCH->GetProperties();
                    //if($arFieldsCH['ID'] == $arFields['ID']){continue;}
                    $count[]=$arFieldsCH['ID'];
                    // echo '<pre>';
                    // print_r($arFieldsCH);
                    // print_r($arPropsCH['ATT_MONTAGNOE_POADK']['VALUE']);
                    // print_r($arPropsCH['ATT_PLOCHAD_OBS']['VALUE']);
                    // echo '</pre>';

                    $PathImgCH = CFile::GetPath($arFieldsCH['PREVIEW_PICTURE']);
                    $rsPricesCH = CPrice::GetList(array(), array('PRODUCT_ID' => $arFieldsCH['ID'], 'CATALOG_GROUP_ID' => 1));
                    $arPriceCH = $rsPricesCH->Fetch();
                    ?>


                        <div class="item id<?=$arFieldsCH['ID']?>" data-id="<?=$arFieldsCH['ID']?>">
                            <img src="<?=$PathImgCH?>" alt="">
                            <input class="input_hid_name" type="hidden" value="<?=$arFieldsCH['NAME']?>">
                            <input class="input_hid_m2" type="hidden" value="<?=$arPropsCH['ATT_PLOCHAD_OBS']['VALUE']?>">
                            <input class="input_hid_price" type="hidden" value="<?=$arPriceCH['PRICE']?>">
                            <input class="input_hid_id" type="hidden" value="<?=$arFieldsCH['ID']?>">
                            <input class="input_hid_href" type="hidden" value="<?=$arFieldsCH['DETAIL_PAGE_URL']?>">
                            <p><?=$arFieldsCH['NAME']?></p>
                            <p>Производитель: <?=$arFields2['NAME']?></p>
                            <p>Рабочая площадь: <?=$arPropsCH['ATT_PLOCHAD_OBS']['VALUE']?> м2</p>
                            
                            <p class="fly_price">Цена: <?=number_format($arPriceCH['PRICE'], 0, ',', ' ');?></p>
                        </div>


                    <?}?>

                    <script>
        $("#<?=$u.$arFields['ID']?> .item").click(function() {//подставляем картинку в комнату и все остальные атрибуты к замене товара
var id = $(this).attr('data-id');
var imgcrc = $(".id"+id+" img").attr('src');
var chName = $(".id"+id+" .input_hid_name").val();
var chM2 = $(".id"+id+" .input_hid_m2").val();
var input_hid_id = $(".id"+id+" .input_hid_id").val();
var chPrice = $(".id"+id+" .input_hid_price").val();
var chHref = $(".id"+id+" .input_hid_href").val();

function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ' ' + '$2');
    }
    return x1 + x2;
}

chPrice = addCommas(chPrice).split(".");
$("#<?=$u.$arFields['ID']?> .img_ch_room").attr("src", imgcrc);
$("#<?=$u.$arFields['ID']?> a").attr("href", chHref);
$("#<?=$u.$arFields['ID']?> a").html(chName);
$("#<?=$u.$arFields['ID']?> .prodID").val(input_hid_id);
$("#<?=$u.$arFields['ID']?> div ul i").html(chM2);
$("#<?=$u.$arFields['ID']?> div ul span").html(chPrice[0]);

$("#<?=$u.$arFields['ID']?> .after_podbor_change_block").css({ 'display': 'none'});


var allPrices = $(".priceCom<?=$h?>").map(function(){return $(this).html();}).get();//собираю все цены "в комнате" для вывода суммы общей
    allPrices = allPrices.map(function(item){//убираю пробелы в суммах
	return item.replace(/\s+/g,'');
});


var sum=0;
for(var i=0;i<allPrices.length;i++){
    sum = sum + parseInt(allPrices[i]);

}

$(".priceIn<?=$h?>").html(addCommas(sum));//вставляю тотал новую сумм
});
</script>

                </div>
                </div>
            </div>
        </section>
    </div>
    
<?if(count($count) > 0){?>
<p class="result_in_cart" onclick="Change_choise(this)">Заменить модель</p>
<?}?>
</div>

<?

$arrTotalPrice[]=$arPrice['PRICE'];
unset($arAllM2ploch);
unset($count);
}else{?>
    
    <div class="results_produkt_div">
            
            <p>Комната <?=$u?><br><br><span>У производителя <?=$arFields2['NAME']?> отсутствуют внутренние блоки подходящие по заданным параметрам. <?//=$_POST['podklVariants'][$u-1]?></span><br><br>
            
            </p>
          
            <?if($u % 2 != 0){?>
                <b>+</b>
            <?}?>
    </div>

<?}}?>





    </div>

    <h5>Внешний блок</h5>
    <div class="bottom_results_div">
                <?

                $arSelectUot = Array("ID", "IBLOCK_ID", "NAME","IBLOCK_SECTION_ID", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "DATE_ACTIVE_FROM","PROPERTY_*");
                $arFilterUot = Array("IBLOCK_ID"=>1, "SECTION_ID" => 575, "INCLUDE_SUBSECTIONS" => "Y",
                "PROPERTY_BRAND_IBLOCK" => $arrBrands[$h],
                ">=PROPERTY_ATT_PLOCHAD_OBS" => array_sum($_POST['countM2']),
                "ACTIVE"=>"Y");
                $resUot = CIBlockElement::GetList(Array("PROPERTY_ATT_PLOCHAD_OBS" => "ASC"), $arFilterUot, false, Array("nPageSize"=>1), $arSelectUot);
                $obUot = $resUot->GetNextElement();

                if (is_object($obUot)) {
                $arFieldsUot = $obUot->GetFields();
                $arPropsUot = $obUot->GetProperties();
                $PathImgUot = CFile::GetPath($arFieldsUot['PREVIEW_PICTURE']);

                $rsPricesUot = CPrice::GetList(array(), array('PRODUCT_ID' => $arFieldsUot['ID'], 'CATALOG_GROUP_ID' => 1));
                $arPriceUot = $rsPricesUot->Fetch();

                // echo '<pre>';
                // print_r($arPriceUot);
                // echo '</pre>';

                if($arPriceUot['CURRENCY'] == 'EUR'){
                    $arFilterCur = array("CURRENCY" => "EUR");
                    $byCur = "date";
                    $orderCur = "desc";
                    
                    $rowCur = CCurrencyRates::GetList($byCur, $orderCur, $arFilterCur)->Fetch();

                    $arPriceUot['PRICE'] = $arPriceUot['PRICE'] * $rowCur['RATE'];
                
                }

                
                if($arPriceUot['CURRENCY'] == 'USD'){
                    $arFilterCur = array("CURRENCY" => "USD");
                    $byCur = "date";
                    $orderCur = "desc";
                    
                    $rowCur = CCurrencyRates::GetList($byCur, $orderCur, $arFilterCur)->Fetch();

                    $arPriceUot['PRICE'] = $arPriceUot['PRICE'] * $rowCur['RATE'];
                
                }

         
                




                $arrTotalPrice[]=$arPriceUot['PRICE'];
              
                ?>

<div class="bottom_results_single">
            <div>
                <div class="results_single_1">
                    <p>Кол-во комнат: <b><?=$u-1?></b></p>
                    <p>Общая площадь: <b><?=array_sum($_POST['countM2'])?> м2</b></p>
                </div>

                <div class="results_single_2">
                    <img src="<?=$PathImgUot?>" alt="">
                    
                </div>
                <div class="results_single_3">
                    <!-- <img src="/bitrix/templates/aspro_optimus/images/imgmitsu2.jpg" alt=""> -->
                    <h6><a href="<?=$arFieldsUot['DETAIL_PAGE_URL']?>" target="_blank"><?=$arFieldsUot['NAME']?></a></h6>
                    <p>Производитель: <?=$arFields2['NAME']?></p>
                    <p>Обслуживаемая площадь: <?=$arPropsUot['ATT_PLOCHAD_OBS']['VALUE']?> м2</p>
                    
                    <p>Цена: <b class="priceCom<?=$h?>"><?=number_format($arPriceUot['PRICE'], 0, ',', ' ');?></b> руб.</p>
                </div>
            </div>
            <div>
                <ul id="block<?=$h?>">
                <input type="hidden" class="prodID" value="<?=$arFieldsUot['ID']?>">
                    <li>Стоимость комплекта</li>
                    <li><b class="priceIn<?=$h?>"><?=number_format(array_sum($arrTotalPrice), 0, ',', ' ');?></b> руб.</li>
                    <li>Итого:</li>
                    <li><b class="priceIn<?=$h?>"><?=number_format(array_sum($arrTotalPrice), 0, ',', ' ');?></b> руб.</li>
                    <li class="result_in_cart" onclick="Inkart(this)">В корзину</li>
                    <li class="one_click_result" onclick="Fast_click_kart(this)">Купить в 1 клик</li>
                </ul>
            </div>
        </div>
       

      
            </div>

        </div>
        
    </div>


    </div>
 <?}else{?>
    <div class="non_out_block">
                    <h6>У производителя <?=$arFields2['NAME']?> отсутствуют внешние блоки подходящие по заданным параметрам. <?//=array_sum($_POST['countM2'])?></h6>
                </div>
    <?}?>
    </div>
<?
unset($arrTotalPrice);
}?>










<div class="overley_desh overley_desh_calc">
    <div class="desh_container">
        <h4>Быстрый заказ<i onclick="CloseUp(this)" class="fa fa-times"></i></h4>
        <div class="dinamik_products">

        </div>
        <ul>
        <li><p class="wnt_name">Ваше имя: *</p> <input id="wnt_name" value="" type="text"></li>
        <li><p class="wnt_phone">Ваш телефон: *</p> <input class="phone" id="wnt_phone" value="" type="text"></li>
        <li><p>E-mail:</p> <input id="wnt_email" value="" type="text"></li>


        </ul>
        <p class="wnt_results"></p>
        <div class="politika_konfidencial">Нажимая кнопку "Отправить" Вы соглашаетесь на&nbsp;<a href="/politika-konfidentsialnosti.php">обработку персональных данных</a></div>
        <p></p>
        <span id="wnt_go" onclick="Goformajax(this)">Отправить</span>
    </div>
</div>

<script>
    function Fast_click_kart(a){//тут собираем товары для отображения в форме по клику на быструю покупку
 var id = a.parentNode.id;
 var listId = $("."+id+" .prodID").map(function(){return $(this).val();}).get();

 $.ajax({
          type: 'POST',
          url: '/bitrix/templates/aspro_optimus/ajax/form_fast_ajax.php',
          
          data: {'listId': listId},
          success: function(data) {
              $('.dinamik_products').html(data);
          }
      });

      $( ".overley_desh_calc" ).css({'display': 'block'});
}



</script>

<script>
function CloseUp(a){
    $( ".overley_desh_calc" ).css({'display': 'none'});
}
</script>
<script>
//форма нашли дешевле
function Goformajax(e){//собираем данные из формы быстрого заказа и передаем в аджакс
    var wnt_name = $("#wnt_name").val();
    var wnt_phone = $("#wnt_phone").val();
    var wnt_email = $("#wnt_email").val();


    $.ajax({
        type: 'POST',
        url: '/bitrix/templates/aspro_optimus/ajax/fast_click_calc.php',
        data: 'wnt_name='+wnt_name+'&wnt_phone='+wnt_phone+'&wnt_email='+wnt_email+'',
        success: function(data) {
           $('.wnt_results').html(data);
        }
    });
}
</script>























<script>
function Inkart(a){//тут передаем в аджакс айдишники для добавления их в корзину при клике на кнопку в корзину
 var id = a.parentNode.id;
 var listId = $("."+id+" .prodID").map(function(){return $(this).val();}).get();

 $.ajax({
          type: 'POST',
          url: '/bitrix/templates/aspro_optimus/ajax/add_calc_axaj.php',
          
          data: {'listId': listId},
          success: function(data) {
              $('.rooms_blocksjj').html(data);
          }
      });

}
</script>







<script>
function Change_choise(a){
 var id = a.parentNode.id;
$("#"+id+" .after_podbor_change_block").slideToggle(100); 
	
}
</script>

<script>
$(".closeup").click(function() {
    $('.after_podbor_change_block').css({ 'display': 'none'});
});
</script>


<script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                loop: false,
                margin: 5,
                nav:true,
                navRewind: true,
                autoplay: false,
                dots: true,
                responsive: {
                  300: {
                    items: 2
                  },
                  1000: {
                    items: 2
                  }
                }
              })
            })
          </script>