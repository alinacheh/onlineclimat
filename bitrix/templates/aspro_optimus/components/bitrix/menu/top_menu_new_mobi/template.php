<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<?if( !empty( $arResult ) ){?>
	 <ul class="dop-menu">
		<?foreach( $arResult as $key => $arItem ){?>
			<li class="dop-menu__item <?if( $arItem["SELECTED"] ):?>current<?endif?>"> 
				<a href="<?=$arItem["LINK"]?>" class="dop-menu__link"><?=$arItem["TEXT"]?></a></li>
			</li>
		<?}?>
	</ul>
<?}?>