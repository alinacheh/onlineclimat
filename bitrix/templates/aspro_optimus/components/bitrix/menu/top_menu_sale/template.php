<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
        <ul id="menu" class="top-menu">
            <?
            $previousLevel = 0;
            foreach ($arResult as $arItem): ?>
            <? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
            <? endif ?><? if ($arItem["IS_PARENT"]): ?><? if ($arItem["DEPTH_LEVEL"] == 1): ?>
            <li class="top-menu__item"><a href="<?= $arItem["LINK"] ?>"
                   class="<? if ($arItem["SELECTED"]):?>root-item-selected top-menu__link<? else:?>top-menu__link<? endif ?>"><?= $arItem["TEXT"] ?></a>
                <ul><? else: ?>
                    <li <? if ($arItem["SELECTED"]):?> class="item-selected top-menu__item"<? endif ?>><a href="<?= $arItem["LINK"] ?>"
                                                                                          class="parent"><?= $arItem["TEXT"] ?></a>
                        <ul><? endif ?>
                            <? else:?><? if ($arItem["PERMISSION"] > "D"):?>
                                <? if ($arItem["DEPTH_LEVEL"] == 1):?>
                                    <li class="top-menu__item"><a href="<?= $arItem["LINK"] ?>"
                                           class="<? if ($arItem["SELECTED"]):?>root-item-selected<? else:?>top-menu__link<? endif ?>"><?= $arItem["TEXT"] ?></a>
                                    </li>
                                <? else:?>
                                    <li<? if ($arItem["SELECTED"]):?> class="item-selected top-menu__item"<? endif ?>><a
                                            href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
                                <? endif ?><? else:?><? if ($arItem["DEPTH_LEVEL"] == 1):?>
                                <li class="top-menu__item"><a href=""
                                       class="<? if ($arItem["SELECTED"]):?>root-item-selected<? else:?>top-menu__link<? endif ?>"
                                       title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a>
                                </li>
                            <? else:?>
                                <li class="top-menu__item"><a href="" class="denied"
                                       title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a>
                                </li>
                            <? endif ?><? endif ?><? endif ?><? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>
                            <? endforeach ?><? if ($previousLevel > 1)://close last item tags?>
                                <?= str_repeat("</ul></li>", ($previousLevel - 1)); ?>
                            <? endif ?>
                        </ul>
    <div class="menu-clear-left"></div>
<? endif ?>
<script type="text/javascript">
    $(function () {
        if ($.browser.msie && $.browser.version.substr(0, 1) < 7) {
            $('li').has('ul').mouseover(function () {
                $(this).children('ul').css('visibility', 'visible');
            }).mouseout(function () {
                $(this).children('ul').css('visibility', 'hidden');
            })
        }
    });
</script>
