<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<?if( !empty( $arResult ) ){?>
	<ul class="top-menu">
		<?foreach( $arResult as $key => $arItem ){?>
			<li class="top-menu__item <?if( $arItem["SELECTED"] ):?>current<?endif?>"> 
				<a href="<?=$arItem["LINK"]?>" class="top-menu__link"><?=$arItem["TEXT"]?></a></li>
			</li>
		<?}?>
	</ul>
<?}?>