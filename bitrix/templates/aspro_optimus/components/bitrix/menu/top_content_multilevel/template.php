<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<?if( !empty( $arResult ) ){?>
	<ul class="menu top menu_top_block catalogfirst visible_on_ready">
		<?foreach( $arResult as $key => $arItem ){

// echo '<pre>';
// print_r($arItem['TEXT']);
// echo '</pre>';




			if(isset($arItem["PARAMS"]["NOT_VISIBLE"]) && $arItem["PARAMS"]["NOT_VISIBLE"]=="Y")
				continue;?>

<?
    if($arItem['TEXT'] == 'Услуги'){?>
    <li class="  has-child">
    <?}else{?>
    <li class="<?=($arItem["SELECTED"] ? "current" : "");?> <?=($arItem['PARAMS']['CLASS'] ? $arItem['PARAMS']['CLASS'] : "");?> <?=($arItem["CHILD"] ? "has-child" : "");?>">
    <?}?>
				<a class="<?=($arItem["CHILD"] ? "icons_fa parent" : "");?>" href="<?=$arItem["LINK"]?>" ><?=$arItem["TEXT"]?></a>

                
        <?if($arItem['TEXT'] == 'Услуги'){?>
            <ul class="dropdown">
                <li>
                    <a href="/services/montazh_i_demontazh_konditsionerov/">Монтаж и демонтаж кондиционеров</a>
                </li>
                <li>
                    <a href="/services/montazh_sistemy_ventilyatsii/">Монтаж системы вентиляции</a>
                </li>
                <li>
                    <a href="/services/servisnoe_i_tekhnicheskoe_obluzhivanie/">Сервисное и техническое облуживание</a>
                </li>
            </ul>
        <?}?>

				<?if($arItem["CHILD"]){?>
					<ul class="dropdown">
						<?foreach($arItem["CHILD"] as $arChildItem){?>
							<li class="<?=($arChildItem["CHILD"] ? "has-child" : "");?> <?if($arChildItem["SELECTED"]){?> current <?}?>">
								<a class="<?=($arChildItem["CHILD"] ? "icons_fa parent" : "");?>" href="<?=$arChildItem["LINK"];?>"><?=$arChildItem["TEXT"];?></a>
								<?if($arChildItem["CHILD"]){?>
									<ul class="dropdown">
										<?foreach($arChildItem["CHILD"] as $arChildItem1){?>
											<li class="menu_item1 <?if($arChildItem1["SELECTED"]){?> current <?}?>">
												<a href="<?=$arChildItem1["LINK"];?>"><span class="text"><?=$arChildItem1["TEXT"];?></span></a>
											</li>
										<?}?>
									</ul>
								<?}?>
							</li>
						<?}?>
					</ul>


				<?}?>
			</li>
		<?}?>
		<li class="more">
			<a href="javascript:;" rel="nofollow"></a>
			<ul class="dropdown"></ul>
		</li>
	</ul>
	<div class="mobile_menu_wrapper">
		<ul class="mobile_menu">
			<?foreach( $arResult as $key => $arItem ){?>

                <?if($arItem['TEXT'] == 'Услуги'){?>

                    

    <li id="aa<?=$key?>" class="icons_fa"><b class="has-plus"></b>
    
<script>
	  $( "#aa<?=$key?> b" ).click(function(){ // задаем функцию при нажатиии на элемент с классом slide-toggle
	    $( "#aa<?=$key?> .dropdown" ).slideToggle(); // плавно скрываем, или отображаем все элементы <div>
	  });
</script>
    <?}else{?>
    <li id="aa<?=$key?>" class="icons_fa"><?if($arItem["CHILD"] ? "has-child" : ""){?><b class="has-plus"></b>
        
<script>
	  $( "#aa<?=$key?> b" ).click(function(){ // задаем функцию при нажатиии на элемент с классом slide-toggle
	    $( "#aa<?=$key?> .dropdown" ).slideToggle(); // плавно скрываем, или отображаем все элементы <div>
	  });
</script>
        <?}?>
    <?}?>
					<a class="dark_link <?=($arItem["CHILD"] ? "parent" : "");?>" href="<?=$arItem["LINK"]?>" ><?=$arItem["TEXT"]?></a>




        <?if($arItem['TEXT'] == 'Услуги'){?>
            <ul class="dropdown">
                <li>
                    <a href="/services/montazh_i_demontazh_konditsionerov/">Монтаж и демонтаж кондиционеров</a>
                </li>
                <li>
                    <a href="/services/montazh_sistemy_ventilyatsii/">Монтаж системы вентиляции</a>
                </li>
                <li>
                    <a href="/services/servisnoe_i_tekhnicheskoe_obluzhivanie/">Сервисное и техническое облуживание</a>
                </li>
            </ul>
        <?}?>



					<?if($arItem["CHILD"]){?>
						<ul class="dropdown">
							<?foreach($arItem["CHILD"] as $arChildItem){?>
								<li class="full <?if($arChildItem["SELECTED"]){?> current <?}?>">
									<a class="icons_fa <?=($arChildItem["CHILD"] ? "parent" : "");?>" href="<?=($arChildItem["SECTION_PAGE_URL"] ? $arChildItem["SECTION_PAGE_URL"] : $arChildItem["LINK"] );?>"><?=($arChildItem["NAME"] ? $arChildItem["NAME"] : $arChildItem["TEXT"] );?></a>
								</li>
							<?}?>
						</ul>
					<?}?>
				</li>
			<?}?>
			<li class="search">
				<div class="search-input-div">
					<input class="search-input" type="text" autocomplete="off" maxlength="50" size="40" placeholder="<?=GetMessage("CT_BST_SEARCH_BUTTON")?>" value="" name="q">
				</div>
				<div class="search-button-div">
					<button class="button btn-search btn-default" value="<?=GetMessage("CT_BST_SEARCH2_BUTTON")?>" name="s" type="submit"><?=GetMessage("CT_BST_SEARCH2_BUTTON")?></button>
				</div>
			</li>
		</ul>
	</div>
<?}?>