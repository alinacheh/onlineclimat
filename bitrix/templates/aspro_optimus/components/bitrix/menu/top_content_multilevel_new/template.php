<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<?if( !empty( $arResult ) ){?>
<nav class="mainmenu-block mainmenu-desktop">
	<div class="wrapper_inner">
		<nav>
		<ul class="mainmenu">
		<?foreach( $arResult as $key => $arItem ){


 //echo '<pre>';
 //print_r($arItem["PARAMS"]["UF_BRAND_BLOCK"]);
 //echo '</pre>';

            if(isset($arItem["PARAMS"]["NOT_VISIBLE"]) && $arItem["PARAMS"]["NOT_VISIBLE"]=="Y")
				continue;?>


	<li class="mainmenu__item">
		<a href="<?=$arItem["LINK"]?>" data="<?=$arItem["PARAMS"]["DATA"]?>" class="mainmenu__link <?if($arItem["TEXT"] == "Каталог"){?>catalogLink<?}?> <?=($arItem["CHILD"] ? "icons_fa parent" : "");?>"> 
			<?if($arItem["TEXT"] == "Каталог"){?><img src="<?=SITE_TEMPLATE_PATH?>/images/template/icon_burger.png" width="15" alt=""><?}?>
			<?=$arItem["TEXT"]?>
		</a>    

			<?if($arItem["CHILD"]){?>
				
				<?if(empty($arItem["PARAMS"]["DIV"])){?>
					<div class="submenu2-brands">
					<ul class="<?=$arItem["PARAMS"]["CLASS_PARENT"]?> <?=$arItem["PARAMS"]["DATA"]?> submenu2">
						<?foreach($arItem["CHILD"] as $arChildItem){
							
							if($arChildItem["PARAMS"]["IMG_NO"] != "Y"){
								$arChildItem["PARAMS"]["ICON_PATH"] = (!empty($arChildItem["PARAMS"]["ICON_PATH"]) ? $arChildItem["PARAMS"]["ICON_PATH"] : SITE_TEMPLATE_PATH."/images/template/submenu1.png");
								?>
								<li class="<?=($arChildItem["CHILD"] ? "has-child2" : "");?> <?if($arChildItem["SELECTED"]){?> current <?}?>">
									<a href="<?=$arChildItem["LINK"];?>">
									<img src="<?=$arChildItem["PARAMS"]["ICON_PATH"]?>" alt="Иконка <?=$arChildItem["TEXT"];?>" /> <?=$arChildItem["TEXT"];?>
									</a>
								</li>							
								<?if($arChildItem["CHILD"] && false){?>							
									<li class="<?=($arChildItem["CHILD"] ? "has-child" : "");?> <?if($arChildItem["SELECTED"]){?> current <?}?>">
										<a class="<?=($arChildItem["CHILD"] ? "icons_fa parent" : "");?>" href="<?=$arChildItem["LINK"];?>"><?=$arChildItem["TEXT"];?> <span>(<?=$arChildItem["PARAMS"]["COUNT_ELEMENT"]?>)</span></a>
										<?if($arChildItem["CHILD"]){?>
											<ul class="submenu">
												<?foreach($arChildItem["CHILD"] as $arChildItem1){?>
													<li class="menu_item1 <?if($arChildItem1["SELECTED"]){?> current <?}?>">
														<a href="<?=$arChildItem1["LINK"];?>"><span class="text"><?=$arChildItem1["TEXT"];?></span></a>
													</li>
												<?}?>
											</ul>
										<?}?>
									</li>
								<?}?>
														
							<?}else{?>
								<?if(!$arChildItem["CHILD"]){?>	
									<li class="<?=($arChildItem["CHILD"] ? "has-child2" : "");?> <?if($arChildItem["SELECTED"]){?> current <?}?>">
										<a href="<?=$arChildItem["LINK"];?>"><?=$arChildItem["TEXT"];?></span></a>
									</li>	
								<?}else{?>						
									<li class="<?=($arChildItem["CHILD"] ? "has-child" : "has-child");?> <?if($arChildItem["SELECTED"]){?> current <?}?>">
										<a class="<?=($arChildItem["CHILD"] ? "icons_fa parent" : "");?>" href="<?=$arChildItem["LINK"];?>"><?=$arChildItem["TEXT"];?></a>
										<?if($arChildItem["CHILD"]){?>
											<ul class="submenu3">
												<?foreach($arChildItem["CHILD"] as $arChildItem1){?>
													<li class="menu_item1 <?if($arChildItem1["SELECTED"]){?> current <?}?>">
														<a href="<?=$arChildItem1["LINK"];?>"><span class="text"><?=$arChildItem1["TEXT"];?></span></a>
													</li>
												<?}?>
											</ul>
										<?}?>
									</li>
								<?}?>							
							<?}?>
														
						<?}?>
						
						</ul>
					<?}else{?>
						<div class="<?=$arItem["PARAMS"]["DIV"]?>">
							<ul class="<?=$arItem["PARAMS"]["CLASS_PARENT"]?> <?=$arItem["PARAMS"]["DATA"]?>">
								<?foreach($arItem["CHILD"] as $arChildItem){
									//echo "<pre>";
									//var_dump($arChildItem);
									//echo "</pre>";?>			
					
									<?if(!$arChildItem["CHILD"]){?>	
										<li class="CHILDCHILD <?=($arChildItem["CHILD"] ? "has-child2" : "");?> <?if($arChildItem["SELECTED"]){?> current <?}?>">
											<a href="<?=$arChildItem["LINK"];?>"><?=$arChildItem["TEXT"];?></span></a>
										</li>	
									<?}else{?>						
										<li class="<?=($arChildItem["CHILD"] ? "has-child" : "has-child");?> <?if($arChildItem["SELECTED"]){?> current <?}?>">
											<a class="<?=($arChildItem["CHILD"] ? "icons_fa parent" : "");?>" href="<?=$arChildItem["LINK"];?>"><?=$arChildItem["TEXT"];?></a>
											<?if($arChildItem["CHILD"]){?>
												<ul class="submenu3">
													<?foreach($arChildItem["CHILD"] as $arChildItem1){?>
														<li class="submenu3333<?if($arChildItem1["SELECTED"]){?> current <?}?>">
															<a href="<?=$arChildItem1["LINK"];?>"><span class="text"><?=$arChildItem1["TEXT"];?></span></a>
														</li>
													<?}?>
												</ul>
											<?}?>
										</li>
									<?}?>														
								<?}?>
						</ul>	
						
						
						<?if(!empty($arItem["PARAMS"]["UF_BRAND_BLOCK"])){?>
							 <div class="brands-menu"> 							
									<?
									$arBrand[$arItem["PARAMS"]["SECTION_ID"]] = [];
									$dbItems = \Bitrix\Iblock\ElementTable::getList(array(
										'order' => array('SORT' => 'asc'),
										'select' => array('ID', 'NAME', 'IBLOCK_ID', "CODE", "DETAIL_PICTURE"),
										'filter' => array('IBLOCK_ID' => IBLOCK_ID_BRAND, 'ID' => $arItem["PARAMS"]["UF_BRAND_BLOCK"]),
										 'cache' => array(
											'ttl' => 60,
											'cache_joins' => true,
										)
									));
									while ($arItemB = $dbItems->fetch()){
										$arBrand[$arItem["PARAMS"]["SECTION_ID"]][$arItemB["ID"]]["NAME"] = $arItemB["NAME"];
										$arBrand[$arItem["PARAMS"]["SECTION_ID"]][$arItemB["ID"]]["DETAIL_PICTURE"] = CFile::GetPath($arItemB["DETAIL_PICTURE"]);
										$arBrand[$arItem["PARAMS"]["SECTION_ID"]][$arItemB["ID"]]["URL_FILTER"] = $arItem["LINK"]."filter/brand_iblock-is-".$arItemB["CODE"]."/apply/";
									}
									foreach($arBrand[$arItem["PARAMS"]["SECTION_ID"]] as $arBrandElem){
									?>
										<a title="<?=$arBrandElem["NAME"]?>" class="brands-menu__item" href="<?=$arBrandElem["URL_FILTER"]?>">
											<img src="<?=$arBrandElem["DETAIL_PICTURE"]?>" alt="<?=$arBrandElem["NAME"]?>" class="b-header-menu__brands-item-img">
										</a>			
									<?}?>
								</div>									
							<?}?>
						</div>					
					<?}?>
					
				<?}?>
				
			</li>
		<?}?>
	</ul>
          </nav>
	</div>
</nav>
<?}?>