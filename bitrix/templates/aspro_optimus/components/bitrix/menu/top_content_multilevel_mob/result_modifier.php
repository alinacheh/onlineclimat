<?
if($arResult){
	$arResult = COptimus::getChilds($arResult);
	$isCatalog=false;
	$arCatalogItem=array();
	foreach($arResult as $key=>$arItem){
		if(isset($arItem["PARAMS"]["CLASS"]) && $arItem["PARAMS"]["CLASS"]=="catalog"){
			$isCatalog=true;
			$arCatalogItem=$arItem;
			unset($arResult[$key]);
		}
		if(isset($arItem["PARAMS"]["NOT_SHOW"]) && $arItem["PARAMS"]["NOT_SHOW"]=="Y"){
			unset($arResult[$key]);
		}
		if($arItem["CHILD"]){
			foreach($arItem["CHILD"] as $key2=>$arChild){
				if(isset($arChild["PARAMS"]["NOT_SHOW"]) && $arChild["PARAMS"]["NOT_SHOW"]=="Y"){
					unset($arResult[$key]["CHILD"][$key2]);
				}
				if($arChild["PARAMS"]["PICTURE"]){
					$img=CFile::ResizeImageGet($arChild["PARAMS"]["PICTURE"], Array('width'=>50, 'height'=>50), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					$arResult[$key]["CHILD"][$key2]["PARAMS"]["IMAGES"]=$img;
				}
			}
		}
	}
	if($isCatalog){
		$catalog_id=\Bitrix\Main\Config\Option::get("aspro.optimus", "CATALOG_IBLOCK_ID", COptimusCache::$arIBlocks[SITE_ID]['aspro_optimus_catalog']['aspro_optimus_catalog'][0]);
		$arSections = COptimusCache::CIBlockSection_GetList(array('SORT' => 'ASC', 'ID' => 'ASC', 'CACHE' => array('TAG' => COptimusCache::GetIBlockCacheTag($catalog_id), 'GROUP' => array('ID'))), array('IBLOCK_ID' => $catalog_id, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y', 'ACTIVE_DATE' => 'Y', '<DEPTH_LEVEL' =>\Bitrix\Main\Config\Option::get("aspro.optimus", "MAX_DEPTH_MENU", 2)), false, array("ID", "NAME", "PICTURE", "LEFT_MARGIN", "RIGHT_MARGIN", "DEPTH_LEVEL", "SECTION_PAGE_URL", "IBLOCK_SECTION_ID"));
		$arTmpResult = array();
		if($arSections){

			$cur_page = $GLOBALS['APPLICATION']->GetCurPage(true);
			$cur_page_no_index = $GLOBALS['APPLICATION']->GetCurPage(false);

			foreach($arSections as $ID => $arSection){
				$arSections[$ID]['SELECTED'] = CMenu::IsItemSelected($arSection['SECTION_PAGE_URL'], $cur_page, $cur_page_no_index);
				if($arSection['IBLOCK_SECTION_ID']){
					if(!isset($arSections[$arSection['IBLOCK_SECTION_ID']]['CHILD'])){
						$arSections[$arSection['IBLOCK_SECTION_ID']]['CHILD'] = array();
					}
					$arSections[$arSection['IBLOCK_SECTION_ID']]['CHILD'][] = &$arSections[$arSection['ID']];
				}

				if($arSection['DEPTH_LEVEL'] == 1){
					$arTmpResult[] = &$arSections[$arSection['ID']];
				}
			}
		}
		array_unshift($arResult, $arCatalogItem);
		$arResult[0]["CHILD"]=$arTmpResult;
	}
	

	
	foreach($arResult as $key=>$arItem){
			//echo "<pre>";
			//var_dump($arItem);
			//echo "</pre>";
			
	
			
		if( $arItem["PARAMS"]["SECTION_ID"] > 0  && $arItem["DEPTH_LEVEL"] == 1){
			
			$aMenuLinks = [];
			
			$rsParentSection = CIBlockSection::GetByID($arItem["PARAMS"]["SECTION_ID"]);
			if ($arParentSection = $rsParentSection->GetNext())
			{
			   $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],
								"UF_HEADER_MENU_CHILD" => 1, "ACTIVE" => "Y", '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
								'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']
							); 
			   $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter, false, array("ID", "NAME", "UF_HEADER_MENU_CHILD", 
			   "IBLOCK_SECTION_ID", "SECTION_PAGE_URL" ,"DEPTH_LEVEL"));
			   while ($arSect = $rsSect->GetNext())
			   {
				    $arSect2 = [];
					$arSect2["TEXT"] = $arSect["NAME"];
					$arSect2["LINK"] = $arSect["SECTION_PAGE_URL"];
					$arSect2["SELECTED"] = false;
					$arSect2["DEPTH_LEVEL"] = $arSect["DEPTH_LEVEL"];
					$arSect2["PARAMS"] = array("IMG_NO" => "Y", "ID" => $arSect["ID"], "IBLOCK_SECTION_ID" => $arSect["IBLOCK_SECTION_ID"]);

					if($arSect2["DEPTH_LEVEL"] == 2)
						$aMenuLinks[$arSect["ID"]] = $arSect2;
						
					if($arSect2["DEPTH_LEVEL"] == 3){
						$aMenuLinks[$arSect["IBLOCK_SECTION_ID"]]["IS_PARENT"] = true;
						$aMenuLinks[$arSect["IBLOCK_SECTION_ID"]]["CHILD"][] = $arSect2;
						
					}

			   }
			}

			
			$arResult[$key]["IS_PARENT"] = true;
			$arResult[$key]["CHILD"] = $aMenuLinks;

			

//echo "<pre>";
//var_dump($arResult[$key]);
//echo "</pre>";
			
			//$isCatalog=true;
			//$arCatalogItem=$arItem;
			//unset($arResult[$key]);
		}

		if($arItem["CHILD"]){
			foreach($arItem["CHILD"] as $key2=>$arChild){
				if(isset($arChild["PARAMS"]["NOT_SHOW"]) && $arChild["PARAMS"]["NOT_SHOW"]=="Y"){
					unset($arResult[$key]["CHILD"][$key2]);
				}
				if($arChild["PARAMS"]["PICTURE"]){
					$img=CFile::ResizeImageGet($arChild["PARAMS"]["PICTURE"], Array('width'=>50, 'height'=>50), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					$arResult[$key]["CHILD"][$key2]["PARAMS"]["IMAGES"]=$img;
				}
			}
		}
	}
	
	
}?>