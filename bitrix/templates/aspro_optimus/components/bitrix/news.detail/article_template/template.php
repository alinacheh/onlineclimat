<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<?
/*set array props for component_epilog*/
$templateData = array(
	'PRICE' => $arResult['PROPERTIES'][$arParams["PRICE_PROPERTY"]]['VALUE'],
);

/**/
?>
<div class="news_detail_wrapp big item">
	<?if( !empty($arResult["DETAIL_PICTURE"])):?>
		<div class="detail_picture_block clearfix">
			<?$img = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array( "width" => 270, "height" => 185 ), BX_RESIZE_IMAGE_PROPORTIONAL, true, array() );?>
			<a href="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" class="fancy">
				<img src="<?=$img["src"]?>" alt="<?=($arResult["DETAIL_PICTURE"]["ALT"] ? $arResult["DETAIL_PICTURE"]["ALT"] : $arResult["NAME"])?>" title="<?=($arResult["DETAIL_PICTURE"]["TITLE"] ? $arResult["DETAIL_PICTURE"]["TITLE"] : $arResult["NAME"])?>"/>
			</a>
		</div>
	<?endif;?>
	<?if($arParams["DISPLAY_DATE"]!="N"):?>
			<?if($arResult["PROPERTIES"][$arParams["PERIOD_PROPERTY"]]["VALUE"]):?>
				<div class="news_date_time_detail date_small"><?=$arResult["PROPERTIES"][$arParams["PERIOD_PROPERTY"]]["VALUE"];?></div>	
			<?elseif($arResult["DISPLAY_ACTIVE_FROM"]):?>
				<div class="news_date_time_detail date_small"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></div>	
			<?endif;?>
	<?endif;?>

    <? foreach ($arParams["FIELD_CODE"] as $key => $field) {
        if ($field == "PREVIEW_TEXT") { ?>
            <div class="<? if ($left_side): ?>right_side<? endif; ?> margin preview_text"><?= $arResult["PREVIEW_TEXT"]; ?></div>
            <? break;
        }
    } ?>
		
	<?if ($arResult["DETAIL_TEXT"]):?>
		<div class="detail_text <?=($arResult["DETAIL_PICTURE"] ? "wimg" : "");?>"><?=$arResult["DETAIL_TEXT"]?></div>
	<?endif;?>

    <div class="whatsapp-block">
        <div class="container_new">
            <div class="b-whatsapp">
                <span class="bold whatsapp-title">Появились вопросы? Нужна консультация?</span>
                <a href="https://wa.me/79037203651" target="_blank" class="b-textfield__input whatsapp-input" alt="Написать в Whatsapp" title="Написать в Whatsapp"><img src="/bitrix/templates/aspro_optimus/images/w_icon.png" class="w-icon">Напишите нам</a>
            </div>
        </div>
    </div>

    <?if ($arResult["PROPERTIES"]["TEXT_AFTER_WHATSUP"]["VALUE"]):?>
        <div class="text_after_whatsup"><?=htmlspecialcharsBack($arResult["PROPERTIES"]["TEXT_AFTER_WHATSUP"]["VALUE"]["TEXT"])?></div>
    <?endif;?>

    <div class="subscribe_wrap">
        <?$APPLICATION->IncludeComponent(
            "bitrix:subscribe.form",
            "article",
            array(
                "AJAX_MODE" => "N",
                "SHOW_HIDDEN" => "N",
                "ALLOW_ANONYMOUS" => "Y",
                "SHOW_AUTH_LINKS" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600000",
                "CACHE_NOTES" => "",
                "SET_TITLE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "LK" => "Y",
                "COMPONENT_TEMPLATE" => "main",
                "USE_PERSONALIZATION" => "Y",
                "PAGE" => SITE_DIR."personal/subscribe/",
                "URL_SUBSCRIBE" => SITE_DIR."subscribe/"
            ),
            false
        );?>
    </div>

    <?if ($arResult["PROPERTIES"]["TEXT_AFTER_SUBSCRIBE"]["VALUE"]):?>
        <div class="text_after_subscribe"><?=htmlspecialcharsBack($arResult["PROPERTIES"]["TEXT_AFTER_SUBSCRIBE"]["VALUE"]["TEXT"])?></div>
    <?endif;?>

    <?if ($arResult["PROPERTIES"]["GOODS"]["VALUE"]):?>
        <section class="article-goods-block">
            <?global $GOODS_IN_ARTICLES;
            $GOODS_IN_ARTICLES["ID"] = $arResult["PROPERTIES"]["GOODS"]["VALUE"];
            ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:catalog.section",
                "catalog_block_acessories",
                array(
                    "SHOW_UNABLE_SKU_PROPS" => "Y",
                    "SEF_URL_TEMPLATES" => array(
                        "sections" => "",
                        "section" => "#SECTION_CODE_PATH#/",
                        "element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
                        "compare" => "compare.php?action=#ACTION_CODE#",
                        "smart_filter" => "#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/",
                    ),
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => "1",
                    "SECTION_ID" => "",
                    "SECTION_CODE" => "",
                    "SHOW_ALL_WO_SECTION" => "Y",
                    "SHOW_ARTICLE_SKU" => "Y",
                    "SHOW_MEASURE_WITH_RATIO" => "N",
                    "AJAX_REQUEST" => "N",
                    "ELEMENT_SORT_FIELD" => "ID",
                    "ELEMENT_SORT_ORDER" => "ASC",

                    "FILTER_NAME" => "GOODS_IN_ARTICLES",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "PAGE_ELEMENT_COUNT" => "4",
                    "LINE_ELEMENT_COUNT" => "4",
                    "DISPLAY_TYPE" => "block",
                    "TYPE_SKU" => "TYPE_1",
                    "PROPERTY_CODE" => array(
                        0 => "",
                    ),
                    "SHOW_DISCOUNT_TIME_EACH_SKU" => "N",

                    "OFFERS_FIELD_CODE" => array(
                        0 => "NAME",
                        1 => "CML2_LINK",
                        2 => "DETAIL_PAGE_URL",
                        3 => "",
                    ),
                    "OFFERS_PROPERTY_CODE" => array(
                        0 => "ARTICLE",
                        1 => "VOLUME",
                        2 => "SIZES",
                        3 => "COLOR_REF",
                        4 => "",
                    ),
                    "OFFERS_SORT_FIELD" => "shows",
                    "OFFERS_SORT_ORDER" => "asc",
                    "OFFERS_SORT_FIELD2" => "shows",
                    "OFFERS_SORT_ORDER2" => "asc",
                    'OFFER_TREE_PROPS' => array(
                        0 => "SIZES",
                        1 => "COLOR_REF",
                    ),

                    "OFFERS_LIMIT" => "10",

                    "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                    "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                    "BASKET_URL" => "/basket/",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "PRODUCT_PROPS_VARIABLE" => "prop",

                    "SET_LAST_MODIFIED" => "Y",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600000",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",


                    "HIDE_NOT_AVAILABLE" => "Y",
                    "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
                    "USE_COMPARE" => "Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "SHOW_404" => "N",
                    "MESSAGE_404" => "",
                    "FILE_404" => "",
                    "PRICE_CODE" => array(
                        0 => "main",
                    ),
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRICE_VAT_SHOW_VALUE" => "N",


                    "USE_PRODUCT_QUANTITY" => "Y",
                    "OFFERS_CART_PROPERTIES" => array(
                        0 => "VOLUME",
                        1 => "SIZES",
                        2 => "COLOR_REF",
                    ),
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",

                    "PAGER_TITLE" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "N",
                    "PAGER_SHOW_ALL" => "N",

                    "AJAX_OPTION_ADDITIONAL" => "goods",
                    "ADD_CHAIN_ITEM" => "N",
                    "SHOW_QUANTITY" => "Y",
                    "SHOW_COUNTER_LIST" => "Y",
                    "SHOW_QUANTITY_COUNT" => "Y",
                    "SHOW_DISCOUNT_PERCENT" => "Y",
                    "SHOW_DISCOUNT_TIME" => "N",
                    "SHOW_OLD_PRICE" => "Y",
                    "CONVERT_CURRENCY" => "Y",
                    "CURRENCY_ID" => "RUB",
                    "USE_STORE" => "N",
                    "MAX_AMOUNT" => "20",
                    "MIN_AMOUNT" => "10",
                    "USE_MIN_AMOUNT" => "N",
                    "USE_ONLY_MAX_AMOUNT" => "Y",
                    "DISPLAY_WISH_BUTTONS" => "N",
                    "LIST_DISPLAY_POPUP_IMAGE" => "Y",
                    "DEFAULT_COUNT" => "1",
                    "SHOW_MEASURE" => "Y",
                    "SHOW_HINTS" => "Y",
                    "OFFER_HIDE_NAME_PROPS" => "N",
                    "SHOW_SECTIONS_LIST_PREVIEW" => $arParams["SHOW_SECTIONS_LIST_PREVIEW"],
                    "SECTIONS_LIST_PREVIEW_PROPERTY" => "DESCRIPTION",
                    "SHOW_SECTION_LIST_PICTURES" => "Y",
                    "USE_MAIN_ELEMENT_SECTION" => "N",
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "PARTIAL_PRODUCT_PROPERTIES" => "Y",
                    "PRODUCT_PROPERTIES" => array(),
                    "SALE_STIKER" => "SALE_TEXT",
                    "SHOW_RATING" => "Y",
                    "COMPATIBLE_MODE" => "Y",
                    "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                )
            ); ?>
        </section>
    <?endif;?>

    <?if ($arResult["PROPERTIES"]["RECOMENDED_ARTICLES"]["VALUE"]):?>
        <section class="recomended-articles-block">
            <h3><?=GetMessage("RECOMENDED_ARTICLES_TITLE")?></h3>
            <?global $RECOMENDED_ARTICLES;
            $RECOMENDED_ARTICLES["ID"] = $arResult["PROPERTIES"]["RECOMENDED_ARTICLES"]["VALUE"];
            ?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "article_on_main",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "info",
                    "IBLOCK_ID" => "4",
                    "NEWS_COUNT" => "3",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "RECOMENDED_ARTICLES",
                    "FIELD_CODE" => array("NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DATE_ACTIVE_FROM"),
                    "PROPERTY_CODE" => array(),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "150",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "STRICT_SECTION_CHECK" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "Y",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "SET_STATUS_404" => "N",
                    "SEF_FOLDER" => "/info/articles/",
                    "SHOW_404" => "N",
                    "MESSAGE_404" => "",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "IS_VERTICAL" => "N",
                    "BIG_IMG" => "N"
                ),
                false
            );?>
        </section>
    <?endif;?>
	
	<?/*if($arParams["SHOW_PRICE"]=="Y" && $arResult["PROPERTIES"][$arParams["PRICE_PROPERTY"]]["VALUE"]):?>
		<div class="price_block">
			<div class="price"><?=GetMessage("SERVICE_PRICE")?> <?=$arResult["PROPERTIES"][$arParams["PRICE_PROPERTY"]]["VALUE"];?></div>
		</div>
	<?endif;*/?>

	<div class="clear"></div>
	<?if($arParams["SHOW_GALLERY"]=="Y" && $arResult["PROPERTIES"][$arParams["GALLERY_PROPERTY"]]["VALUE"]){?>
		<div class="row galley">
			<ul class="module-gallery-list">
				<?
					$files = $arResult["PROPERTIES"][$arParams["GALLERY_PROPERTY"]]["VALUE"];		
					if(array_key_exists('SRC', $files)) 
					{
					   $files['SRC'] = '/' . substr($files['SRC'], 1);
					   $files['ID'] = $arResult["PROPERTIES"][$arParams["GALLERY_PROPERTY"]]["VALUE"][0];
					   $files = array($files);
					}
				?>
				<?	foreach($files as $arFile):?>
					<?
						$img = CFile::ResizeImageGet($arFile, array('width'=>230, 'height'=>155), BX_RESIZE_IMAGE_EXACT, true);
						
						$img_big = CFile::ResizeImageGet($arFile, array('width'=>800, 'height'=>600), BX_RESIZE_IMAGE_PROPORTIONAL, true);
						$img_big = $img_big['src'];
					?>
					<li class="item_block">
						<a href="<?=$img_big;?>" class="fancy" data-fancybox-group="gallery">
							<img src="<?=$img["src"];?>" alt="<?=$arResult["NAME"];?>" title="<?=$arResult["NAME"];?>" />
						</a>				  
					</li>
				<?endforeach;?>
			</ul>
			<div class="clear"></div>
		</div>
	<?}?>


	<?/*if ($arParams["SHOW_FAQ_BLOCK"]=="Y"):?>
		<div class="ask_big_block">
			<div class="ask_btn_block">
				<a class="button vbig_btn wides ask_btn"><span><?=GetMessage("ASK_QUESTION")?></span></a>
			</div>
			<div class="description">
				<?$APPLICATION->IncludeFile(SITE_DIR."include/ask_block_detail_description.php", Array(), Array("MODE" => "html", "NAME" => GetMessage("ASK_QUESTION_DETAIL_TEXT"), ));?>
			</div>
		</div>
	<?endif;*/?>
</div>