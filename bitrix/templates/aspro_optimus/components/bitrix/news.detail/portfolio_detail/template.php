<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<?/*set array props for component_epilog*/
$templateData = array(
	'CODE' => $arResult['CODE']
);
/**/?>
<div class="news_detail_wrapp portfolio_detail">
    <?$showProp = [];?>
    <?foreach ($arParams["FIELD_CODE"] as $prop) {
        $showProp[] = $prop;
    }?>
	<?if((in_array("DETAIL_PICTURE", $showProp)) && ($arResult["DETAIL_PICTURE"])):?>
		<div class="detail_picture_block clearfix">
			<?$img = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array( "width" => 270, "height" => 175 ), BX_RESIZE_IMAGE_PROPORTIONAL, true, array() );?>
			<a href="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" class="fancy">
				<img src="<?=$img["src"]?>" alt="<?=($arResult["DETAIL_PICTURE"]["ALT"] ? $arResult["DETAIL_PICTURE"]["ALT"] : $arResult["NAME"])?>" title="<?=($arResult["DETAIL_PICTURE"]["TITLE"] ? $arResult["DETAIL_PICTURE"]["TITLE"] : $arResult["NAME"])?>" height="<?=$img["height"]?>" width="<?=$img["width"]?>" />
			</a>
		</div>
	<?endif;?>
	<?if ($arResult["PREVIEW_TEXT"]):?>
		<div class="<?if ($left_side):?>right_side<?endif;?> preview_text"><?=$arResult["PREVIEW_TEXT"];?></div>
	<?endif;?>
	<?if ($arResult["DETAIL_TEXT"]):?>
		<div class="detail_text <?=($arResult["DETAIL_PICTURE"] ? "wimg" : "");?>"><?=$arResult["DETAIL_TEXT"]?></div>
	<?endif;?>
	<div class="clear"></div>
		
	<?if($arParams["SHOW_GALLERY"]=="Y" && $arResult["PROPERTIES"][$arParams["GALLERY_PROPERTY"]]["VALUE"]){?>
		<div class="row galley">
			<ul class="module-gallery-list">
				<?
					$files = $arResult["PROPERTIES"][$arParams["GALLERY_PROPERTY"]]["VALUE"];		
					if(array_key_exists('SRC', $files)) 
					{
					   $files['SRC'] = '/' . substr($files['SRC'], 1);
					   $files['ID'] = $arResult["PROPERTIES"][$arParams["GALLERY_PROPERTY"]]["VALUE"][0];
					   $files = array($files);
					}
				?>
				<?	foreach($files as $arFile):?>
					<?
						$img = CFile::ResizeImageGet($arFile, array('width'=>230, 'height'=>155), BX_RESIZE_IMAGE_PROPORTIONAL, true);
						
						$img_big = CFile::ResizeImageGet($arFile, array('width'=>800, 'height'=>600), BX_RESIZE_IMAGE_PROPORTIONAL, true);
						$img_big = $img_big['src'];
					?>
					<li class="item_block">
						<a href="<?=$img_big;?>" class="fancy" data-fancybox-group="gallery">
							<img src="<?=$img["src"];?>" alt="<?=$arResult["NAME"];?>" title="<?=$arResult["NAME"];?>" />
						</a>				  
					</li>
				<?endforeach;?>

				<?	foreach($arResult['VIDEOS'] as $embed):?>
					<?
					?>
					<li class="item_block">
						<div class="you-block">
						<?$embed_img = str_replace("https://www.youtube.com/watch?v=", "", $embed);?>						
							<a href="<?=$embed?>" style=" background: url(http://img.youtube.com/vi/<?=$embed_img ?>/mqdefault.jpg);" 
								class="popupbox-video" data-fancybox-group="gallery">
								<img class="play-y" src="/images/youtube-play.png">
								<?/*<iframe width="100%"  src="https://www.youtube.com/embed/<?=$embed?>?rel=0&autoplay=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>*/?>
							</a>
						</div>					  
					</li>
				<?endforeach;?>
			</ul>
			<div class="clear"></div>
		</div>
	<?}?>
		
	
</div>