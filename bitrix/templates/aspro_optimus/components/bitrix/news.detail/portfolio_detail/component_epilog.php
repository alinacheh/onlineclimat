<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arParams["SHOW_LINKED_PRODUCTS"] == "Y" && !empty($arResult["LINKED_PRODUCTS_IDS"])):?>
	<?IncludeTemplateLangFile(__FILE__);?>
	<?
	$iblockID = \Bitrix\Main\Config\Option::get("aspro.optimus", "CATALOG_IBLOCK_ID", COptimusCache::$arIBlocks[SITE_ID]["aspro_optimus_catalog"]["aspro_optimus_catalog"][0]);
	$arItems = COptimusCache::CIBLockElement_GetList(array('CACHE' => array("MULTI" =>"Y", "TAG" => COptimusCache::GetIBlockCacheTag($iblockID))), array("IBLOCK_ID" => $iblockID, "ACTIVE"=>"Y", "=ID" => $arResult["LINKED_PRODUCTS_IDS"]), false, false, array("ID"));
	if ($arItems) {
		?>
		<hr class="long"/>
		<div class="similar_products_wrapp">
			<h3><?=GetMessage("PORTFOLIO_PRODUCTS");?></h3>
			<?$GLOBALS[$arParams["CATALOG_FILTER_NAME"]] = array("=ID" => $arResult["LINKED_PRODUCTS_IDS"])?>
			<?include($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'include/news.detail.products_slider.php');?>
		</div>
	<?}?>
<?endif;?>
<?if ($arParams["SHOW_BACK_LINK"]=="Y"):?>
	<?$refer=$_SERVER['HTTP_REFERER'];
	if (strpos($refer, $arResult["LIST_PAGE_URL"])!==false) {?>
		<div class="back"><a class="back" href="javascript:history.back();"><span><?=GetMessage("BACK");?></span></a></div>
	<?}else{?>
		<div class="back"><a class="back" href="<?=$arResult["LIST_PAGE_URL"]?>"><span><?=GetMessage("BACK");?></span></a></div>
	<?}?>
<?endif;?>