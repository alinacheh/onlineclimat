<?use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;?>
<?php
$APPLICATION->IncludeComponent(
    "aku:aku.base",
    "canonical",
    Array(
        'module' => 'canonical',
        'element_vars' => $arResult["VARIABLES"],
        'product_id_variable' => $arParams["PRODUCT_ID_VARIABLE"],
        'iblock_id' => $arParams["IBLOCK_ID"]
    ),
    false
);
?>

<?global $USER;
if($USER->IsAdmin()){
    $temp = "main_new";
}else{
    $temp = "main-adm";
    $temp = "main_new";
}?>

<div class="catalog_detail" itemscope itemtype="http://schema.org/Product">
    <?$ElementID = $APPLICATION->IncludeComponent(
        "bitrix:catalog.element",
        $temp,
        Array(
            "SECTION_TIZER"=>$arSection["UF_TIZERS"],
            "SHOW_UNABLE_SKU_PROPS"=>$arParams["SHOW_UNABLE_SKU_PROPS"],
            "SHOW_DISCOUNT_TIME"=>$arParams["SHOW_DISCOUNT_TIME"],
            "TYPE_SKU" => ($typeSKU ? $typeSKU : $TEMPLATE_OPTIONS["TYPE_SKU"]["CURRENT_VALUE"]),
            "SEF_URL_TEMPLATES" => $arParams["SEF_URL_TEMPLATES"],
            "SHOW_DISCOUNT_TIME_EACH_SKU" => $arParams["SHOW_DISCOUNT_TIME_EACH_SKU"],
            "SHOW_ARTICLE_SKU" => $arParams["SHOW_ARTICLE_SKU"],
            "SHOW_MEASURE_WITH_RATIO" => $arParams["SHOW_MEASURE_WITH_RATIO"],
            "IBLOCK_REVIEWS_TYPE" => $arParams["IBLOCK_REVIEWS_TYPE"],
            "IBLOCK_REVIEWS_ID" => $arParams["IBLOCK_REVIEWS_ID"],
            "SHOW_ONE_CLICK_BUY" => $arParams["SHOW_ONE_CLICK_BUY"],
            "SEF_MODE_BRAND_SECTIONS" => $arParams["SEF_MODE_BRAND_SECTIONS"],
            "SEF_MODE_BRAND_ELEMENT" => $arParams["SEF_MODE_BRAND_ELEMENT"],
            "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
            "PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
            "META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"],
            "META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"],
            "BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"],
            "BASKET_URL" => $arParams["BASKET_URL"],
            "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
            "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
            "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
            'CHECK_SECTION_ID_VARIABLE' => (isset($arParams['DETAIL_CHECK_SECTION_ID_VARIABLE']) ? $arParams['DETAIL_CHECK_SECTION_ID_VARIABLE'] : ''),
            "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
            "SET_TITLE" => $arParams["SET_TITLE"],
            "SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
            "SET_LAST_MODIFIED" => "Y",
            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
            "MESSAGE_404" => $arParams["MESSAGE_404"],
            "SHOW_404" => $arParams["SHOW_404"],
            "FILE_404" => $arParams["FILE_404"],
            "PRICE_CODE" => $arParams["PRICE_CODE"],
            "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
            "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
            "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
            "PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
            "LINK_IBLOCK_TYPE" => $arParams["LINK_IBLOCK_TYPE"],
            "LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"],
            "LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"],
            "LINK_ELEMENTS_URL" => $arParams["LINK_ELEMENTS_URL"],
            "USE_ALSO_BUY" => $arParams["USE_ALSO_BUY"],
            'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
            'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
            "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
            "OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"],
            "OFFERS_PROPERTY_CODE" => $arParams["DETAIL_OFFERS_PROPERTY_CODE"],
            "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
            "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
            "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
            "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
            "SKU_DISPLAY_LOCATION" => $arParams["SKU_DISPLAY_LOCATION"],
            "ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
            "ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
            "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
            "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
            "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
            "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
            "ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
            "ADD_ELEMENT_CHAIN" => $arParams["ADD_ELEMENT_CHAIN"],
            "USE_STORE" => $arParams["USE_STORE"],
            "USE_STORE_PHONE" => $arParams["USE_STORE_PHONE"],
            "USE_STORE_SCHEDULE" => $arParams["USE_STORE_SCHEDULE"],
            "USE_MIN_AMOUNT" => $arParams["USE_MIN_AMOUNT"],
            "MIN_AMOUNT" => $arParams["MIN_AMOUNT"],
            "STORE_PATH" => $arParams["STORE_PATH"],
            "MAIN_TITLE" => $arParams["MAIN_TITLE"],
            "USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
            "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
            "IBLOCK_STOCK_ID" => $arParams["IBLOCK_STOCK_ID"],
            "SEF_MODE_STOCK_SECTIONS" => $arParams["SEF_MODE_STOCK_SECTIONS"],
            "SHOW_QUANTITY" => $arParams["SHOW_QUANTITY"],
            "SHOW_QUANTITY_COUNT" => $arParams["SHOW_QUANTITY_COUNT"],
            "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
            "CURRENCY_ID" => $arParams["CURRENCY_ID"],
            'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
            'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
            'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED'],
            "USE_ELEMENT_COUNTER" => $arParams["USE_ELEMENT_COUNTER"],
            'STRICT_SECTION_CHECK' => (isset($arParams['DETAIL_STRICT_SECTION_CHECK']) ? $arParams['DETAIL_STRICT_SECTION_CHECK'] : ''),
            'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),

            "USE_RATING" => $arParams["USE_RATING"],
            "USE_REVIEW" => $arParams["USE_REVIEW"],
            "FORUM_ID" => $arParams["FORUM_ID"],
            "MAX_AMOUNT" => $arParams["MAX_AMOUNT"],
            "USE_ONLY_MAX_AMOUNT" => $arParams["USE_ONLY_MAX_AMOUNT"],
            "DISPLAY_WISH_BUTTONS" => $arParams["DISPLAY_WISH_BUTTONS"],
            "DEFAULT_COUNT" => $arParams["DEFAULT_COUNT"],
            "SHOW_BRAND_PICTURE" => $arParams["SHOW_BRAND_PICTURE"],
            "PROPERTIES_DISPLAY_LOCATION" => $arParams["PROPERTIES_DISPLAY_LOCATION"],
            "PROPERTIES_DISPLAY_TYPE" => $arParams["PROPERTIES_DISPLAY_TYPE"],
            "SHOW_ADDITIONAL_TAB" => $arParams["SHOW_ADDITIONAL_TAB"],
            "SHOW_ASK_BLOCK" => $arParams["SHOW_ASK_BLOCK"],
            "ASK_FORM_ID" => $arParams["ASK_FORM_ID"],
            "SHOW_MEASURE" => $arParams["SHOW_MEASURE"],
            "SHOW_HINTS" => $arParams["SHOW_HINTS"],
            "OFFER_HIDE_NAME_PROPS" => $arParams["OFFER_HIDE_NAME_PROPS"],
            "SHOW_KIT_PARTS" => $arParams["SHOW_KIT_PARTS"],
            "SHOW_KIT_PARTS_PRICES" => $arParams["SHOW_KIT_PARTS_PRICES"],
            "SHOW_DISCOUNT_PERCENT" => $arParams["SHOW_DISCOUNT_PERCENT"],
            "SHOW_OLD_PRICE" => $arParams["SHOW_OLD_PRICE"],
            'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
            'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
            "SHOW_EMPTY_STORE" => $arParams['SHOW_EMPTY_STORE'],
            "SHOW_GENERAL_STORE_INFORMATION" => $arParams['SHOW_GENERAL_STORE_INFORMATION'],
            "USER_FIELDS" => $arParams['USER_FIELDS'],
            "FIELDS" => $arParams['FIELDS'],
            "STORES" => $arParams['STORES'],
            "BIG_DATA_RCM_TYPE" => $arParams['BIG_DATA_RCM_TYPE'],
            "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
            "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
            "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
            "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
            "SALE_STIKER" => $arParams["SALE_STIKER"],
            "SHOW_RATING" => $arParams["SHOW_RATING"],

            'MAIN_BLOCK_PROPERTY_CODE' => (isset($arParams['DETAIL_MAIN_BLOCK_PROPERTY_CODE']) ? $arParams['DETAIL_MAIN_BLOCK_PROPERTY_CODE'] : ''),
            'MAIN_BLOCK_OFFERS_PROPERTY_CODE' => (isset($arParams['DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE']) ? $arParams['DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE'] : ''),

            "OFFERS_LIMIT" => $arParams["DETAIL_OFFERS_LIMIT"],

            'SHOW_BASIS_PRICE' => (isset($arParams['DETAIL_SHOW_BASIS_PRICE']) ? $arParams['DETAIL_SHOW_BASIS_PRICE'] : 'Y'),
            "DETAIL_PICTURE_MODE" => (isset($TEMPLATE_OPTIONS["DETAIL_PICTURE_MODE"]["CURRENT_VALUE"]) ? $TEMPLATE_OPTIONS["DETAIL_PICTURE_MODE"]["CURRENT_VALUE"] : 'POPUP'),
            'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
            'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
            'SET_VIEWED_IN_COMPONENT' => (isset($arParams['DETAIL_SET_VIEWED_IN_COMPONENT']) ? $arParams['DETAIL_SET_VIEWED_IN_COMPONENT'] : ''),

            'SHOW_SLIDER' => (isset($arParams['DETAIL_SHOW_SLIDER']) ? $arParams['DETAIL_SHOW_SLIDER'] : ''),
            'SLIDER_INTERVAL' => (isset($arParams['DETAIL_SLIDER_INTERVAL']) ? $arParams['DETAIL_SLIDER_INTERVAL'] : ''),
            'SLIDER_PROGRESS' => (isset($arParams['DETAIL_SLIDER_PROGRESS']) ? $arParams['DETAIL_SLIDER_PROGRESS'] : ''),
            'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
            'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),


            "USE_GIFTS_DETAIL" => $arParams['USE_GIFTS_DETAIL']?: 'Y',
            "USE_GIFTS_MAIN_PR_SECTION_LIST" => $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST']?: 'Y',
            "GIFTS_SHOW_DISCOUNT_PERCENT" => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
            "GIFTS_SHOW_OLD_PRICE" => $arParams['GIFTS_SHOW_OLD_PRICE'],
            "GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
            "GIFTS_DETAIL_HIDE_BLOCK_TITLE" => $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'],
            "GIFTS_DETAIL_TEXT_LABEL_GIFT" => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],
            "GIFTS_DETAIL_BLOCK_TITLE" => $arParams["GIFTS_DETAIL_BLOCK_TITLE"],
            "GIFTS_SHOW_NAME" => $arParams['GIFTS_SHOW_NAME'],
            "GIFTS_SHOW_IMAGE" => $arParams['GIFTS_SHOW_IMAGE'],
            "SET_BROWSER_TITLE" => 'N',
            "SET_META_KEYWORDS" => 'N',
            "SET_META_DESCRIPTION" => 'N',
            "GIFTS_MESS_BTN_BUY" => $arParams['GIFTS_MESS_BTN_BUY'],

            "GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
            "GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'],
        ),
        $component
    );?>

</div>
<?COptimus::checkBreadcrumbsChain($arParams, $arSection, $arElement);?>
<div class="clearfix"></div>
<?/*fix title after ajax form start*/
$postfix = '';
global $arSite;
if(\Bitrix\Main\Config\Option::get("aspro.optimus", "HIDE_SITE_NAME_TITLE", "N")=="N")
    $postfix = ' - '.$arSite['SITE_NAME'];
$arAdditionalData = $arNavParams = array();
$arAdditionalData['TITLE'] = htmlspecialcharsback($APPLICATION->GetTitle());
//$arAdditionalData['WINDOW_TITLE'] = htmlspecialcharsback($APPLICATION->GetTitle('title').$postfix);
$arAdditionalData['WINDOW_TITLE'] = htmlspecialcharsback($APPLICATION->GetTitle('title'));

// dirty hack: try to get breadcrumb call params
for ($i = 0, $cnt = count($APPLICATION->buffer_content_type); $i < $cnt; $i++){
    if ($APPLICATION->buffer_content_type[$i]['F'][1] == 'GetNavChain'){
        $arNavParams = $APPLICATION->buffer_content_type[$i]['P'];
    }
}
if ($arNavParams){
    $arAdditionalData['NAV_CHAIN'] = $APPLICATION->GetNavChain($arNavParams[0], $arNavParams[1], $arNavParams[2], $arNavParams[3], $arNavParams[4]);
}
?>

<script type="text/javascript">
    BX.addCustomEvent(window, "onAjaxSuccess", function(){
        var arAjaxPageData = <?=CUtil::PhpToJSObject($arAdditionalData);?>;

        //set title from offers
        if(typeof ItemObj == 'object' && Object.keys(ItemObj).length)
        {
            if('TITLE' in ItemObj && ItemObj.TITLE)
            {
                arAjaxPageData.TITLE = ItemObj.TITLE;
                arAjaxPageData.WINDOW_TITLE = ItemObj.WINDOW_TITLE;
            }
        }

        if (arAjaxPageData.TITLE)
            BX.ajax.UpdatePageTitle(arAjaxPageData.TITLE);
        if (arAjaxPageData.WINDOW_TITLE || arAjaxPageData.TITLE)
            BX.ajax.UpdateWindowTitle(arAjaxPageData.WINDOW_TITLE || arAjaxPageData.TITLE);
        if (arAjaxPageData.NAV_CHAIN)
            BX.ajax.UpdatePageNavChain(arAjaxPageData.NAV_CHAIN);
        $('.catalog_detail .tabs_section .tabs_content .form.inline input[data-sid="PRODUCT_NAME"]').attr('value', $('h1').text());
        // top.BX.ajax.UpdatePageData(arAjaxPageData);
    });

</script>
<?/*fix title after ajax form end*/?>

<?
$arAllValues=$arSimilar=$arAccessories=array();
/*similar goods*/
$arExpValues=COptimusCache::CIBlockElement_GetProperty($arParams["IBLOCK_ID"], $ElementID, array("CACHE" => array("TAG" =>COptimusCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]))), array("CODE" => "EXPANDABLES"));
if($arExpValues)
{
    $arItems = COptimusCache::CIBLockElement_GetList(array('CACHE' => array("MULTI" =>"N", "GROUP" => "ID", "TAG" => COptimusCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]))), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE"=>"Y", "ID" => $arExpValues), false, false, array("ID"));
    if($arItems)
    {
        $arAllValues["EXPANDABLES"] = array_keys($arItems);
    }
    else
    {
        $arExpValues = array();
    }
}
/*accessories goods*/
$arAccessories=COptimusCache::CIBlockElement_GetProperty($arParams["IBLOCK_ID"], $ElementID, array("CACHE" => array("TAG" =>COptimusCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]))), array("CODE" => "ASSOCIATED"));
if($arAccessories)
{
    $arItems = COptimusCache::CIBLockElement_GetList(array('CACHE' => array("MULTI" =>"N", "GROUP" => "ID", "TAG" => COptimusCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]))), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE"=>"Y", "ID" => $arAccessories), false, false, array("ID"));
    if($arItems)
    {
        $arAllValues["ASSOCIATED"] = array_keys($arItems);
    }
    else
    {
        $arAccessories = array();
    }
}
?>



