<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$frame = $this->createFrame()->begin();?>
<div class="subscribe-block">
    <div class="container_new">
        <div class="b-subscribe">
            <span class="bold subscribe-title"><?=GetMessage("TEXT_TITLE_1")?></span>
            <span class="bold subscribe-title"><?=GetMessage("TEXT_TITLE_2")?></span>
        </div>
        <div class="b-subscribe b-subscribe2">
            <div class="b-social__title">
                <form action="<?=$arResult["FORM_ACTION"];?>" class="sform_footer box-sizing">
                    <?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
                        <label for="sf_RUB_ID_<?=$itemValue["ID"]?>" class="hidden">
                            <input type="checkbox" name="sf_RUB_ID[]" id="sf_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>"<?if($itemValue["CHECKED"]) echo " checked"?> /> <?=$itemValue["NAME"]?>
                        </label>
                    <?endforeach;?>
                    <div class="wrap_md">
                        <div class="email_wrap iblock">
                            <input type="email" name="sf_EMAIL" class="b-textfield__input subscribe-input js-textfield__inputmask" required size="20" value="<?=$arResult["EMAIL"]?>" placeholder="<?=GetMessage("subscr_form_email_title")?>" />
                        </div>
                        <div class="button_wrap iblock">
                            <input type="submit" name="OK" class="b-button-s" value="<?=($arResult["EMAIL"] ? GetMessage("subscr_form_button_change") : GetMessage("subscr_form_button"));?>" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready(function(){
		$("form.sform_footer").validate({
			rules:{ "sf_EMAIL": {email: true} }
		});
	})
</script>
<?$frame->end();?>
