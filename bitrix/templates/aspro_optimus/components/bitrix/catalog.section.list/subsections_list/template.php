<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><? $this->setFrameMode( true ); ?>
<?if($arResult["SECTIONS"]){
$arFilter1 = Array('IBLOCK_ID'=>1, "SECTION_ID"=>297, 'GLOBAL_ACTIVE'=>'Y');
$db_list1 = CIBlockSection::GetList(Array(), $arFilter1, true);
$k=0;
while($ar_result1 = $db_list1->GetNext())
{
	$arrayID[$k] = $ar_result1['ID'];
	$k++;
	
}
$arFilter1 = Array('IBLOCK_ID'=>1, "SECTION_ID"=>156, 'GLOBAL_ACTIVE'=>'Y');
$db_list1 = CIBlockSection::GetList(Array(), $arFilter1, true);
while($ar_result1 = $db_list1->GetNext())
{
	$arrayID[$k] = $ar_result1['ID'];
	$k++;
	
}
$arFilter1 = Array('IBLOCK_ID'=>1, "SECTION_ID"=>68, 'GLOBAL_ACTIVE'=>'Y');
$db_list1 = CIBlockSection::GetList(Array(), $arFilter1, true);
while($ar_result1 = $db_list1->GetNext())
{
	$arrayID[$k] = $ar_result1['ID'];
	$k++;
	
}
$arFilter1 = Array('IBLOCK_ID'=>1, "SECTION_ID"=>288, 'GLOBAL_ACTIVE'=>'Y');
$db_list1 = CIBlockSection::GetList(Array(), $arFilter1, true);
while($ar_result1 = $db_list1->GetNext())
{
	$arrayID[$k] = $ar_result1['ID'];
	$k++;
	
}
$arFilter1 = Array('IBLOCK_ID'=>1, "SECTION_ID"=>386, 'GLOBAL_ACTIVE'=>'Y');
$db_list1 = CIBlockSection::GetList(Array(), $arFilter1, true);
while($ar_result1 = $db_list1->GetNext())
{
	$arrayID[$k] = $ar_result1['ID'];
	$k++;
	
}

?>


<?php if($arResult['SECTION']['IBLOCK_SECTION_ID']) { ?>

	<? if($arResult["SECTIONS"][0]["DEPTH_LEVEL"]>=4 || in_array($arResult["SECTIONS"][0]["IBLOCK_SECTION_ID"], $arrayID)){?>

        <ul class="subt"  style="margin-bottom:25px;">
            <?foreach( $arResult["SECTIONS"] as $arItems ){?>
							<li class="item-sub-3" style="font-size:13px; text-align:left;">
									<a style="font-size:14px;" href="<?=$arItems["SECTION_PAGE_URL"]?>"><span><?=$arItems["NAME"]?></span></a>
							</li>
			<?}?>
        </ul>
	<?}else{?>	
        <div style="margin-bottom:25px;">
            <?foreach( $arResult["SECTIONS"] as $arItems ){?>
    			<div class="item-sub-3 item-sub-3-img">
    				<div class="sections_block_on_first-image_wrapper">
    					<?if($arItems["PICTURE"]["SRC"]):?>
    						<?$img = CFile::ResizeImageGet($arItems["PICTURE"]["ID"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
    						<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb">
    							<img class="lazy" data-src="<?=$img["src"]?>" alt="" title="" />
    						</a>
    					<?elseif($arItems["~PICTURE"]):?>
    						<?$img = CFile::ResizeImageGet($arItems["~PICTURE"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
    						<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb">
    							<img class="lazy" data-src="<?=$img["src"]?>" alt="" title="" />
    						</a>
    					<?else:?>
    						<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img style="display: block; margin:auto;" src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_medium.png" alt="" title="" height="90" />
    						</a>
    					<?endif;?>
    				</div>
    				<a style="font-size:14px;" href="<?=$arItems["SECTION_PAGE_URL"]?>"><span><?=$arItems["NAME"]?></span></a>
    			</div>
    		<?}?>
        </div>

	<?}?>

<?php } else { ?>
	<div class="sections_block_on_first">
		<?foreach( $arResult["SECTIONS"] as $arItems ){?>
			<div class="sections_block_on_first-item">
				<div class="sections_block_on_first-image_wrapper">
					<?if($arItems["PICTURE"]["SRC"]):?>
						<?$img = CFile::ResizeImageGet($arItems["PICTURE"]["ID"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
						<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb">
							<img class="lazy" data-src="<?=$img["src"]?>" alt="" title="" />
						</a>
					<?elseif($arItems["~PICTURE"]):?>
						<?$img = CFile::ResizeImageGet($arItems["~PICTURE"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
						<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb">
							<img class="lazy" data-src="<?=$img["src"]?>" alt="" title="" />
						</a>
					<?else:?>
						<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img style="display: block; margin:auto;" class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_medium.png" alt="" title="" height="90" />
						</a>
					<?endif;?>
				</div>
				<a style="font-size:14px;" href="<?=$arItems["SECTION_PAGE_URL"]?>"><span><?=$arItems["NAME"]?></span></a>
			</div>
		<?}?>
	</div>
<?php } ?>





















<div class="catalog_section_list rows_block items section hidden">	
	<?foreach( $arResult["SECTIONS"] as $arItems ){
		$this->AddEditAction($arItems['ID'], $arItems['EDIT_LINK'], CIBlock::GetArrayByID($arItems["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItems['ID'], $arItems['DELETE_LINK'], CIBlock::GetArrayByID($arItems["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<div class="item_block col-2">
			<div class="section_item item" id="<?=$this->GetEditAreaId($arItems['ID']);?>">
			
				
						<?if ($arParams["SHOW_SECTION_LIST_PICTURES"]=="Y"):?>
							<?$collspan = 2;?>
							
								<?if($arItems["PICTURE"]["SRC"]):?>
									<?$img = CFile::ResizeImageGet($arItems["PICTURE"]["ID"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
									<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img class="lazy" data-src="<?=$img["src"]?>" alt="" title="" /></a>
								<?elseif($arItems["~PICTURE"]):?>
									<?$img = CFile::ResizeImageGet($arItems["~PICTURE"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
									<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img class="lazy" data-src="<?=$img["src"]?>" alt="" title="" /></a>
								<?else:?>
									<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img style="display: block; margin:auto;" class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_medium.png" alt="" title="" height="90" /></a>
								<?endif;?>
							
						<?endif;?>
						<br>
							<div class="name name_costom">
									<a href="<?=$arItems["SECTION_PAGE_URL"]?>"><span><?=$arItems["NAME"]?></span></a> 
							</div>
							<?if($arParams["SECTIONS_LIST_PREVIEW_DESCRIPTION"] != 'N'):?>
								<?$arSection = $section=COptimusCache::CIBlockSection_GetList(array('CACHE' => array("MULTI" =>"N", "TAG" => COptimusCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]))), array('GLOBAL_ACTIVE' => 'Y', "ID" => $arItems["ID"], "IBLOCK_ID" => $arParams["IBLOCK_ID"]), false, array("ID", "IBLOCK_ID", $arParams["SECTIONS_LIST_PREVIEW_PROPERTY"]));?>
								<div class="desc" ><span class="desc_wrapp">
									<?if ($arSection[$arParams["SECTIONS_LIST_PREVIEW_PROPERTY"]]):?>
										<?=$arSection[$arParams["SECTIONS_LIST_PREVIEW_PROPERTY"]]?>
									<?else:?>
										<?=$arItems["DESCRIPTION"]?>
									<?endif;?>
								</span></div>
							<?endif;?>
						
				
			</div>
		</div>
	<?}?>
</div>
<script>
	$(document).ready(function(){
		$('.catalog_section_list.rows_block .item .section_info').sliceHeight();
		$('.catalog_section_list.rows_block .item').sliceHeight();
		setTimeout(function(){
			$(window).resize();
		},100)
	});
</script>
<?}?>