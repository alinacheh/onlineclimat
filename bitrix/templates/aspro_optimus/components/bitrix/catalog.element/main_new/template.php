<?use Bitrix\Main\ModuleManager;?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><div class="basket_props_block" id="bx_basket_div_<?=$arResult["ID"];?>" style="display: none;">
    <?if (!empty($arResult['PRODUCT_PROPERTIES_FILL'])){
        foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo){?>
            <input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
            <?if (isset($arResult['PRODUCT_PROPERTIES'][$propID]))
                unset($arResult['PRODUCT_PROPERTIES'][$propID]);
        }
    }
    $arResult["EMPTY_PROPS_JS"]="Y";
    $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
    if (!$emptyProductProperties){
        $arResult["EMPTY_PROPS_JS"]="N";?>
        <div class="wrapper">
            <table>
                <?foreach ($arResult['PRODUCT_PROPERTIES'] as $propID => $propInfo){?>
                    <tr>
                        <td><? echo $arResult['PROPERTIES'][$propID]['NAME']; ?></td>
                        <td>
                            <?if('L' == $arResult['PROPERTIES'][$propID]['PROPERTY_TYPE'] && 'C' == $arResult['PROPERTIES'][$propID]['LIST_TYPE']){
                                foreach($propInfo['VALUES'] as $valueID => $value){?>
                                    <label>
                                        <input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?>
                                    </label>
                                <?}
                            }else{?>
                                <select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]">
                                    <?foreach($propInfo['VALUES'] as $valueID => $value){?>
                                        <option value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option>
                                    <?}?>
                                </select>
                            <?}?>
                        </td>
                    </tr>
                <?}?>
            </table>
        </div>
    <?}?>

</div>
<?
$this->setFrameMode(true);

$currencyList = '';
if (!empty($arResult['CURRENCIES'])){
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList,
    'STORES' => array(
        "USE_STORE_PHONE" => $arParams["USE_STORE_PHONE"],
        "SCHEDULE" => $arParams["SCHEDULE"],
        "USE_MIN_AMOUNT" => $arParams["USE_MIN_AMOUNT"],
        "MIN_AMOUNT" => $arParams["MIN_AMOUNT"],
        "ELEMENT_ID" => $arResult["ID"],
        "STORE_PATH"  =>  $arParams["STORE_PATH"],
        "MAIN_TITLE"  =>  $arParams["MAIN_TITLE"],
        "MAX_AMOUNT"=>$arParams["MAX_AMOUNT"],
        "USE_ONLY_MAX_AMOUNT" => $arParams["USE_ONLY_MAX_AMOUNT"],
        "SHOW_EMPTY_STORE" => $arParams['SHOW_EMPTY_STORE'],
        "SHOW_GENERAL_STORE_INFORMATION" => $arParams['SHOW_GENERAL_STORE_INFORMATION'],
        "USE_ONLY_MAX_AMOUNT" => $arParams["USE_ONLY_MAX_AMOUNT"],
        "USER_FIELDS" => $arParams['USER_FIELDS'],
        "FIELDS" => $arParams['FIELDS'],
        "STORES" => $arParams['STORES'] = array_diff($arParams['STORES'], array('')),
    )
);
unset($currencyList, $templateLibrary);

$arSkuTemplate = array();
if (!empty($arResult['SKU_PROPS'])){
    $arSkuTemplate=COptimus::GetSKUPropsArray($arResult['SKU_PROPS'], $arResult["SKU_IBLOCK_ID"], "list", $arParams["OFFER_HIDE_NAME_PROPS"]);
}
$strMainID = $this->GetEditAreaId($arResult['ID']);

$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

if($GLOBALS['device'] == 'm'){?>
    <style>
        #print_img{
            display: none;
        }
    </style>
<?}
$arResult["strMainID"] = $this->GetEditAreaId($arResult['ID']);
$arItemIDs=COptimus::GetItemsIDs($arResult, "Y");
$totalCount = COptimus::GetTotalCount($arResult);
$arQuantityData = COptimus::GetQuantityArray($totalCount, $arItemIDs["ALL_ITEM_IDS"], "Y");

$arParams["BASKET_ITEMS"]=($arParams["BASKET_ITEMS"] ? $arParams["BASKET_ITEMS"] : array());
$useStores = $arParams["USE_STORE"] == "Y" && $arResult["STORES_COUNT"] && $arQuantityData["RIGHTS"]["SHOW_QUANTITY"];
$showCustomOffer=(($arResult['OFFERS'] && $arParams["TYPE_SKU"] !="N") ? true : false);
if($showCustomOffer){
    $templateData['JS_OBJ'] = $strObName;
}
$strMeasure='';
if($arResult["OFFERS"]){
    $strMeasure=$arResult["MIN_PRICE"]["CATALOG_MEASURE_NAME"];
    $templateData["STORES"]["OFFERS"]="Y";
    foreach($arResult["OFFERS"] as $arOffer){
        $templateData["STORES"]["OFFERS_ID"][]=$arOffer["ID"];
    }
}else{
    if (($arParams["SHOW_MEASURE"]=="Y")&&($arResult["CATALOG_MEASURE"])){
        $arMeasure = CCatalogMeasure::getList(array(), array("ID"=>$arResult["CATALOG_MEASURE"]), false, false, array())->GetNext();
        $strMeasure=$arMeasure["SYMBOL_RUS"];
    }

    $arAddToBasketData = COptimus::GetAddToBasketArray($arResult, $totalCount, $arParams["DEFAULT_COUNT"], $arParams["BASKET_URL"], false, $arItemIDs["ALL_ITEM_IDS"], 'big_btn w_icons button-card-custom', $arParams, true);
}
$arOfferProps = implode(';', $arParams['OFFERS_CART_PROPERTIES']);

// save item viewed
$arFirstPhoto = reset($arResult['MORE_PHOTO']);
$arItemPrices = $arResult['MIN_PRICE'];
if(isset($arResult['PRICE_MATRIX']) && $arResult['PRICE_MATRIX'])
{
    $rangSelected = $arResult['ITEM_QUANTITY_RANGE_SELECTED'];
    $priceSelected = $arResult['ITEM_PRICE_SELECTED'];
    if(isset($arResult['FIX_PRICE_MATRIX']) && $arResult['FIX_PRICE_MATRIX'])
    {
        $rangSelected = $arResult['FIX_PRICE_MATRIX']['RANGE_SELECT'];
        $priceSelected = $arResult['FIX_PRICE_MATRIX']['PRICE_SELECT'];
    }
    $arItemPrices = $arResult['ITEM_PRICES'][$priceSelected];
    $arItemPrices['VALUE'] = $arItemPrices['PRICE'];
    $arItemPrices['PRINT_VALUE'] = \Aspro\Functions\CAsproItem::getCurrentPrice('PRICE', $arItemPrices);
    $arItemPrices['DISCOUNT_VALUE'] = $arItemPrices['BASE_PRICE'];
    $arItemPrices['PRINT_DISCOUNT_VALUE'] = \Aspro\Functions\CAsproItem::getCurrentPrice('BASE_PRICE', $arItemPrices);
}
$arViewedData = array(
    'PRODUCT_ID' => $arResult['ID'],
    'IBLOCK_ID' => $arResult['IBLOCK_ID'],
    'NAME' => $arResult['NAME'],
    'DETAIL_PAGE_URL' => $arResult['DETAIL_PAGE_URL'],
    'PICTURE_ID' => $arResult['PREVIEW_PICTURE'] ? $arResult['PREVIEW_PICTURE']['ID'] : ($arFirstPhoto ? $arFirstPhoto['ID'] : false),
    'CATALOG_MEASURE_NAME' => $arResult['CATALOG_MEASURE_NAME'],
    'MIN_PRICE' => $arItemPrices,
    'CAN_BUY' => $arResult['CAN_BUY'] ? 'Y' : 'N',
    'IS_OFFER' => 'N',
    'WITH_OFFERS' => $arResult['OFFERS'] ? 'Y' : 'N',
);
?>
<script type="text/javascript">
    setViewedProduct(<?=$arResult['ID']?>, <?=CUtil::PhpToJSObject($arViewedData, false)?>);
</script>
<meta itemprop="name" content="<?=$name = strip_tags(!empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) ? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arResult['NAME'])?>" />
<meta itemprop="category" content="<?=$arResult['CATEGORY_PATH']?>" />
<meta itemprop="description" content="<?=(strlen(strip_tags($arResult['PREVIEW_TEXT'])) ? strip_tags($arResult['PREVIEW_TEXT']) : (strlen(strip_tags($arResult['DETAIL_TEXT'])) ? strip_tags($arResult['DETAIL_TEXT']) : $name))?>" />
<div class="item_main_info <?=(!$showCustomOffer ? "noffer" : "");?> <?=($arParams["SHOW_UNABLE_SKU_PROPS"] != "N" ? "show_un_props" : "unshow_un_props");?>" id="<?=$arItemIDs["strMainID"];?>">
    <div class="catalog_detail_tabs">
        <a href="#description">Описание</a>
        <a href="#characteristics">Характеристики</a>
        <a href="#documents">Документация</a>
        <a href="#reviews">Отзывы</a>
        <a href="#accessories">Комплектующие</a>
    </div>
    <div class="img_wrapper">

        <div class="stickers stickers-custom">
            <?php if(isset($arResult["ITEM_PRICES"])) { ?>
                <?php $current_price = current($arResult["ITEM_PRICES"]); ?>
                <?php if($current_price['PERCENT'] or $current_price['DISCOUNT']) { ?>
                    <div class="product-status-icon product-status-percent product-status-percent__custom"></div>
                <?php } ?>
            <?php } ?>
            <?php if(isset($arResult["PROPERTIES"]["IS_FASTENERS"]) and ($arResult["PROPERTIES"]["IS_FASTENERS"]["VALUE"] == 'Y')) { ?>
                <div class="product-status-icon product-status-fasteners"></div>
            <?php } ?>
            <?php if(isset($arResult["PROPERTIES"]["IS_NEW"]) and !empty($arResult["PROPERTIES"]["IS_NEW"]["VALUE"])) { ?>
                <div class="product-status-icon product-status-new product-status-new__custom"></div>
            <?php } ?>
            <?php if(isset($arResult["PROPERTIES"]["IS_POP"]) and !empty($arResult["PROPERTIES"]["IS_POP"]["VALUE"])) { ?>
                <div class="product-status-icon product-status-hit product-status-hit__custom"></div>
            <?php } ?>
            <?php if(isset($arResult["PROPERTIES"]["PROP_MONT_GIFT"]) and !empty($arResult["PROPERTIES"]["PROP_MONT_GIFT"]["VALUE"])) { ?>
                <div class="product-status-icon product-status-montaj product-status-montaj__custom"></div>
            <?php } ?>
            <?php if(isset($arResult["PROPERTIES"]["PROP_FREE_DIV"]) and !empty($arResult["PROPERTIES"]["PROP_FREE_DIV"]["VALUE"])) { ?>
                <div class="product-status-icon product-status-delivery product-status-delivery__custom"></div>
            <?php } ?>
            <?php if(isset($arResult["PROPERTIES"]["IS_PRESENT"]) and !empty($arResult["PROPERTIES"]["IS_PRESENT"]["VALUE"])) { ?>
                <div class="product-status-icon product-status-gift product-status-gift__custom"></div>
            <?php } ?>
        </div>

        <div class="stickers">
            <?if (is_array($arResult["PROPERTIES"]["HIT"]["VALUE_XML_ID"])):?>
                <?foreach($arResult["PROPERTIES"]["HIT"]["VALUE_XML_ID"] as $key=>$class){?>
                    <div><div class="sticker_<?=strtolower($class);?>"><?=$arResult["PROPERTIES"]["HIT"]["VALUE"][$key]?></div></div>
                <?}?>
            <?endif;?>
            <?if($arParams["SALE_STIKER"] && $arResult["PROPERTIES"][$arParams["SALE_STIKER"]]["VALUE"]){?>
                <div><div class="sticker_sale_text"><?=$arResult["PROPERTIES"][$arParams["SALE_STIKER"]]["VALUE"];?></div></div>
            <?}?>
        </div>
        <div class="item_slider item_slider_custom">

            <?if(($arParams["DISPLAY_WISH_BUTTONS"] != "N" || $arParams["DISPLAY_COMPARE"] == "Y") || (strlen($arResult["DISPLAY_PROPERTIES"]["CML2_ARTICLE"]["VALUE"]) || ($arResult['SHOW_OFFERS_PROPS'] && $showCustomOffer))):?>
                <div class="like_wrapper">
                    <?if($arParams["DISPLAY_WISH_BUTTONS"] != "N" || $arParams["DISPLAY_COMPARE"] == "Y"):?>
                        <div class="like_icons iblock">
                            <?if($arParams["DISPLAY_WISH_BUTTONS"] != "N"):?>
                                <?if(!$arResult["OFFERS"]):?>
                                    <div class="wish_item text" data-item="<?=$arResult["ID"]?>" data-iblock="<?=$arResult["IBLOCK_ID"]?>">
                                        <span class="value" title="<?=GetMessage('CT_BCE_CATALOG_IZB')?>" ><i></i></span>
                                        <span class="value added" title="<?=GetMessage('CT_BCE_CATALOG_IZB_ADDED')?>"><i></i></span>
                                    </div>
                                <?elseif($arResult["OFFERS"] && $arParams["TYPE_SKU"] === 'TYPE_1' && !empty($arResult['OFFERS_PROP'])):?>
                                    <div class="wish_item text " data-item="" data-iblock="<?=$arResult["IBLOCK_ID"]?>" <?=(!empty($arResult['OFFERS_PROP']) ? 'data-offers="Y"' : '');?> data-props="<?=$arOfferProps?>">
                                        <span class="value <?=$arParams["TYPE_SKU"];?>" title="<?=GetMessage('CT_BCE_CATALOG_IZB')?>"><i></i></span>
                                        <span class="value added <?=$arParams["TYPE_SKU"];?>" title="<?=GetMessage('CT_BCE_CATALOG_IZB_ADDED')?>"><i></i></span>
                                    </div>
                                <?endif;?>
                            <?endif;?>
                            <?if($arParams["DISPLAY_COMPARE"] == "Y"):?>
                                <?if(!$arResult["OFFERS"] || ($arResult["OFFERS"] && $arParams["TYPE_SKU"] === 'TYPE_1' && !$arResult["OFFERS_PROP"])):?>
                                    <div data-item="<?=$arResult["ID"]?>" data-iblock="<?=$arResult["IBLOCK_ID"]?>" data-href="<?=$arResult["COMPARE_URL"]?>" class="compare_item text <?=($arResult["OFFERS"] ? $arParams["TYPE_SKU"] : "");?>" id="<? echo $arItemIDs["ALL_ITEM_IDS"]['COMPARE_LINK']; ?>">
                                        <span class="value" title="<?=GetMessage('CT_BCE_CATALOG_COMPARE')?>"><i></i></span>
                                        <span class="value added" title="<?=GetMessage('CT_BCE_CATALOG_COMPARE_ADDED')?>"><i></i></span>
                                    </div>
                                <?elseif($arResult["OFFERS"] && $arParams["TYPE_SKU"] === 'TYPE_1'):?>
                                    <div data-item="" data-iblock="<?=$arResult["IBLOCK_ID"]?>" data-href="<?=$arResult["COMPARE_URL"]?>" class="compare_item text <?=$arParams["TYPE_SKU"];?>">
                                        <span class="value" title="<?=GetMessage('CT_BCE_CATALOG_COMPARE')?>"><i></i></span>
                                        <span class="value added" title="<?=GetMessage('CT_BCE_CATALOG_COMPARE_ADDED')?>"><i></i></span>
                                    </div>
                                <?endif;?>
                            <?endif;?>
                        </div>
                    <?endif;?>
                </div>
            <?endif;?>

            <?reset($arResult['MORE_PHOTO']);
            $arFirstPhoto = current($arResult['MORE_PHOTO']);
            $viewImgType=$arParams["DETAIL_PICTURE_MODE"];?>
            <div class="slides">
                <?if($showCustomOffer && !empty($arResult['OFFERS_PROP'])){?>
                    <div class="offers_img wof">
                        <?$alt=$arFirstPhoto["ALT"];
                        $title=$arFirstPhoto["TITLE"];?>
                        <?if($arFirstPhoto["BIG"]["src"]){?>
                            <a href="<?=($viewImgType=="POPUP" ? $arFirstPhoto["BIG"]["src"] : "javascript:void(0)");?>" class="<?=($viewImgType=="POPUP" ? "popup_link" : "line_link");?>" title="<?=$title;?>">
                                <img id="<? echo $arItemIDs["ALL_ITEM_IDS"]['PICT']; ?>" src="<?=$arFirstPhoto['SMALL']['src']; ?>" <?=($viewImgType=="MAGNIFIER" ? 'data-large="" xpreview="" xoriginal=""': "");?> alt="<?=$alt;?>" title="<?=$title;?>" itemprop="image">
                            </a>
                        <?}else{?>
                            <a href="javascript:void(0)" class="" title="<?=$title;?>">
                                <img id="<? echo $arItemIDs["ALL_ITEM_IDS"]['PICT']; ?>" src="<?=$arFirstPhoto['SRC']; ?>" alt="<?=$alt;?>" title="<?=$title;?>" itemprop="image">
                            </a>
                        <?}?>
                    </div>
                <?}else{
                    if($arResult["MORE_PHOTO"]){
                        $bMagnifier = ($viewImgType=="MAGNIFIER");?>
                        <ul>
                            <?foreach($arResult["MORE_PHOTO"] as $i => $arImage){
                                if($i && $bMagnifier):?>
                                    <?continue;?>
                                <?endif;?>
                                <?$isEmpty=($arImage["SMALL"]["src"] ? false : true );?>
                                <?
                                $alt=$arImage["ALT"];
                                $title=$arImage["TITLE"];
                                ?>
                                <li id="photo-<?=$i?>" <?=(!$i ? 'class="current"' : 'style="display: none;"')?>>
                                    <?if(!$isEmpty){?>
                                        <a href="<?=($viewImgType=="POPUP" ? $arImage["BIG"]["src"] : "javascript:void(0)");?>" <?=($bIsOneImage ? '' : 'data-fancybox-group="item_slider"')?> class="<?=($viewImgType=="POPUP" ? "popup_link fancy" : "line_link");?>" title="<?=$title;?>">

                                            <img  src="<?=$arImage["SMALL"]["src"]?>" <?=($viewImgType=="MAGNIFIER" ? "class='zoom_picture'" : "");?> <?=($viewImgType=="MAGNIFIER" ? 'xoriginal="'.$arImage["BIG"]["src"].'" xpreview="'.$arImage["THUMB"]["src"].'"' : "");?> alt="<?=$alt;?>" title="<?=$title;?>"<?=(!$i ? ' itemprop="image"' : '')?> class="drift-zoom" data-zoom="<?=$arImage["BIG"]["src"]?>"/>
                                        </a>
                                    <?}else{?>
                                        <img  src="<?=$arImage["SRC"]?>" alt="<?=$alt;?>" title="<?=$title;?>" />
                                    <?}?>
                                </li>
                            <?}?>
                        </ul>
                    <?}
                }?>
            </div>
            <img id="print_img" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="">
            <?/*thumbs*/?>
            <?if(!$showCustomOffer || empty($arResult['OFFERS_PROP'])){
            if(count($arResult["MORE_PHOTO"]) > 1):?>
                <div class="wrapp_thumbs xzoom-thumbs">
                    <div class="thumbs flexslider" data-plugin-options='{"animation": "slide", "selector": ".slides_block > li", "directionNav": true, "itemMargin":10, "itemWidth": 54, "controlsContainer": ".thumbs_navigation", "controlNav" :false, "animationLoop": true, "slideshow": false}' style="max-width:<?=ceil(((count($arResult['MORE_PHOTO']) <= 4 ? count($arResult['MORE_PHOTO']) : 4) * 64) - 10)?>px;">
                        <ul class="slides_block" id="thumbs">
                            <?foreach($arResult["MORE_PHOTO"]as $i => $arImage):?>
                                <li <?=(!$i ? 'class="current"' : '')?> data-big_img="<?=$arImage["BIG"]["src"]?>" data-small_img="<?=$arImage["SMALL"]["src"]?>">
                                    <span><img class="xzoom-gallery" width="50" xpreview="<?=$arImage["THUMB"]["src"];?>" src="<?=$arImage["THUMB"]["src"]?>" alt="<?=$arImage["ALT"];?>" title="<?=$arImage["TITLE"];?>" /></span>
                                </li>
                            <?endforeach;?>
                        </ul>
                        <span class="thumbs_navigation custom_flex"></span>
                    </div>
                </div>
                <script>
                    $(document).ready(function(){
                        $('.item_slider .thumbs li').first().addClass('current');
                        $('.item_slider .thumbs .slides_block').delegate('li:not(.current)', 'click', function(){
                            var slider_wrapper = $(this).parents('.item_slider'),
                                index = $(this).index();
                            $(this).addClass('current').siblings().removeClass('current')
                            if(arOptimusOptions['THEME']['DETAIL_PICTURE_MODE'] == 'MAGNIFIER')
                            {
                                var li = $(this).parents('.item_slider').find('.slides li');
                                li.find('img').attr('src', $(this).data('small_img'));
                                li.find('img').attr('xoriginal', $(this).data('big_img'));
                            }
                            else
                            {
                                slider_wrapper.find('.slides li').removeClass('current').hide();
                                slider_wrapper.find('.slides li:eq('+index+')').addClass('current').show();
                            }
                        });
                    })
                </script>
            <?endif;?>
            <?}else{?>
                <div class="wrapp_thumbs">
                    <div class="sliders">
                        <div class="thumbs" style="">
                        </div>
                    </div>
                </div>
            <?}?>
        </div>
        <?/*mobile*/?>
        <?if(!$showCustomOffer || empty($arResult['OFFERS_PROP'])){?>
            <div class="item_slider flex flexslider" data-plugin-options='{"animation": "slide", "directionNav": false, "controlNav": true, "animationLoop": false, "slideshow": true, "slideshowSpeed": 10000, "animationSpeed": 600}'>
                <ul class="slides">
                    <?if($arResult["MORE_PHOTO"]){
                        foreach($arResult["MORE_PHOTO"] as $i => $arImage){?>
                            <?$isEmpty=($arImage["SMALL"]["src"] ? false : true );?>
                            <li id="mphoto-<?=$i?>" <?=(!$i ? 'class="current"' : 'style="display: none;"')?>>
                                <?
                                $alt=$arImage["ALT"];
                                $title=$arImage["TITLE"];
                                ?>
                                <?if(!$isEmpty){?>
                                    <a href="<?=$arImage["BIG"]["src"]?>" data-fancybox-group="item_slider_flex" class="fancy" title="<?=$title;?>" >
                                        <img src="<?=$arImage["SMALL"]["src"]?>" alt="<?=$alt;?>" title="<?=$title;?>" class="drift-zoom" data-zoom="<?=$arImage["BIG"]["src"]?>" />
                                    </a>
                                <?}else{?>
                                    <img  src="<?=$arImage["SRC"];?>" alt="<?=$alt;?>" title="<?=$title;?>" />
                                <?}?>
                            </li>
                        <?}
                    }?>
                </ul>
            </div>
        <?}else{?>
            <div class="item_slider flex"></div>
        <?}?>
    </div>
    <div class="right_info">
        <div class="info_item info_item_custom">
            <?$isArticle=(strlen($arResult["DISPLAY_PROPERTIES"]["CML2_ARTICLE"]["VALUE"]) || ($arResult['SHOW_OFFERS_PROPS'] && $showCustomOffer));?>
            <?if($isArticle || $arResult["BRAND_ITEM"] || $arParams["SHOW_RATING"] == "Y" || strlen($arResult["PREVIEW_TEXT"])){?>
                <div class="top_info detail-card-info">
                    <div class="detail-card-info-block">
                        <?$col=1;
                        if($isArticle && $arResult["BRAND_ITEM"] && $arParams["SHOW_RATING"] == "Y"){
                            $col=3;
                        }elseif(($isArticle && $arResult["BRAND_ITEM"]) || ($isArticle && $arParams["SHOW_RATING"] == "Y") || ($arResult["BRAND_ITEM"] && $arParams["SHOW_RATING"] == "Y")){
                            $col=2;
                        }?>
                        <div class="left-info">
                            <?if($arParams["SHOW_RATING"] == "Y"):?>
                                <div class="item_block col-2"><!--<?=$col;?>-->
                                    <?$frame = $this->createFrame('dv_'.$arResult["ID"])->begin('');?>
                                    <div class="rating">
                                        <?$APPLICATION->IncludeComponent(
                                            "bitrix:iblock.vote",
                                            "element_rating",
                                            Array(
                                                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                                "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                                                "ELEMENT_ID" => $arResult["ID"],
                                                "MAX_VOTE" => 5,
                                                "VOTE_NAMES" => array(),
                                                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                                "CACHE_TIME" => $arParams["CACHE_TIME"],
                                                "DISPLAY_AS_RATING" => 'vote_avg'
                                            ),
                                            $component, array("HIDE_ICONS" =>"Y")
                                        );?>
                                    </div>
                                    <?$frame->end();?>
                                </div>
                            <?endif;?>

                            <?if(!empty($arResult["TOVAR_LINK"])){?>
                                <div class="tovar-list-block">
                                    <p><?=(!empty($arResult["TOVAR_LINK_DESCR_NAME"]) ? $arResult["TOVAR_LINK_DESCR_NAME"] : "Размеры")?>:</p>
                                    <select class="select2 select-tovar">
                                        <?foreach($arResult["TOVAR_LINK"] as $arTovar){?>
                                            <option value="<?=$arTovar["DETAIL_PAGE_URL"]?>"<?if($arTovar["ID"] == $arResult["ID"]){?> selected<?}?>><?=$arTovar["NAME"]?></option>
                                        <?}?>
                                    </select>
                                </div>
                            <?}?>
                            <?if($arResult["PROPERTIES"]["COLOR_PALETTE"]["VALUE"]) {
                            $colorPalette = CFile::GetFileArray($arResult["PROPERTIES"]["COLOR_PALETTE"]["VALUE"])?>
                            <a href="" class="detail-element-color detail-element-link">Цветовая палитра RAL<span><img src="<?=$colorPalette["SRC"]?>"/></span></a>
                            <?}?>
                            <? $APPLICATION->IncludeComponent(
                                "aku:aku.base",
                                "characteristics_custom",
                                Array(
                                    'module' => 'characteristics',
                                    'properties' => $arResult["PROPERTIES"],
                                    'element_id' => $arResult['ID']
                                ),
                                false
                            );
                            ?>
                        </div>

                        <div class="right-info">
                            <p class="detail-element-id">ID товара: <? echo $arResult['ID']; ?></p>
                            <script>
                                $(document).ready(function(){
                                    $('.counter_block input').on('blur change', function(){
                                        var input = this;
                                        setTimeout(function(){
                                            var num1 = parseFloat($(input).closest('.info_item_custom').find('.price:not(.discount) .values_wrapper .price_value, .prices .price_matrix_wrapper .price:not(.discount) .values_wrapper .price_value').text().replace(/\s/g, '')),
                                                num2 = parseInt($(input).val().replace('.',',')),
                                                total = num1 * num2;
                                            var deliveryCost;
                                            if (total >= 10000) {
                                                deliveryCost = 0;
                                            } else {
                                                deliveryCost = 700;
                                            }
                                            if (num1 && num2) $('.links-and-titles .delivery_value').text(deliveryCost + " руб.");
                                            //if (num1 && num2) $('.links-and-titles .delivery_value').text((total.toString().indexOf('.') !== -1 ? total.toFixed(2) : total) + " руб.");
                                            $(input).closest('.prices').find('.wrapp_one_click .pi-one-click-btn').attr('data-quantity', num2);
                                        }, 20);
                                    });
                                    $('.counter_block input').trigger('change');
                                    $('.price_matrix_wrapper:not(.head) .price_interval').each(function(){
                                        var quantity = parseInt($(this).text().match(/\d+/)[0]),
                                            price = Math.round(parseFloat($(this).parent().find('.price:not(.discount)').attr('data-value'))),
                                            total = price * quantity;

                                        total = (total.toString().indexOf('.') !== -1 ? total.toFixed(2) : total);
                                        //$(this).parent().append('<div class="price" data-currency="RUB" data-value="' + total + '"><span class="values_wrapper"><span class="price_value">' + total + '</span><span class="price_currency"> руб.</span></span></div>');
                                    });
                                });
                            </script>
                            <div class="right-info-block">
                                <div class="right-info-links-group">
                                    <div class="links-and-titles">
                                        <a href="/help/#self" target="_blank" class="detail-element-link">Самовывоз:</a>
                                        <span>0 руб.</span>
                                    </div>
                                    <div class="links-and-titles">
                                        <a href="/help/#moscow" target="_blank" class="detail-element-link">Доставка:</a>
                                        <span class="delivery_value">0 руб.</span>
                                    </div>
                                    <div class="links-and-titles">
                                        <a href="/services/" target="_blank" class="detail-element-link">Установка:</a>
                                        <span>от 3000</span>
                                    </div>
                                    <?if($arResult["PROPERTIES"]["CONNECTION_TYPE"]["VALUE"]):?>
                                        <div class="links-and-titles">
                                            <a href="<?=$arResult["PROPERTIES"]["CONNECTION_TYPE"]["VALUE"]?>" target="_blank" class="detail-element-link"><?=$arResult["PROPERTIES"]["CONNECTION_TYPE_NAME"]["VALUE"]?></a>
                                        </div>
                                    <?endif?>
                                </div>
                                <div class="link-help">
                                    <div class="link-help-question">
                                        <span class="link-help-img"></span>
                                        <span class="hidden">Мы поможем подобрать</span>
                                    </div>
                                    <a href="#" class="detail-element-link ask_btn"><span>Помощь в подборе</span></a>
                                    <?/*Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("form-faq-block2");*/?><!--
                                    <?/*$APPLICATION->IncludeComponent("bitrix:form.result.new",
                                        "popup",
                                        Array(
                                            "WEB_FORM_ID" => "8",
                                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                                            "USE_EXTENDED_ERRORS" => "Y",
                                            "SEF_MODE" => "N",
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "3600000",
                                            "CACHE_GROUPS" => "N",
                                            "LIST_URL" => "",
                                            "EDIT_URL" => "",
                                            "SUCCESS_URL" => "?send=ok",
                                            "CHAIN_ITEM_TEXT" => "",
                                            "CHAIN_ITEM_LINK" => "",
                                            "VARIABLE_ALIASES" => Array(
                                                "WEB_FORM_ID" => "WEB_FORM_ID",
                                                "RESULT_ID" => "RESULT_ID"
                                            )
                                        )
                                    );*/?>
                                    --><?/*Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("form-faq-block2", "");*/?>
                                </div>
                                <div class="logos-block">
                                    <?if($arResult["PROPERTIES"]["BRAND_IBLOCK"]["VALUE"]):?>
                                        <img class="logo-item" src="<?=$arResult["BRAND_PICTURE"]["SRC"]?>">
                                    <?endif?>
                                    <?if ($arResult["PROPERTIES"]["GARANTY_PICTURE"]["VALUE"]): ?>
                                        <?$garanty = CFile::GetFileArray($arResult["PROPERTIES"]["GARANTY_PICTURE"]["VALUE"])?>
                                        <img class="logo-item" src="<?=$garanty["SRC"]?>">
                                    <?endif?>
                                </div>
                            </div>
                        </div>

                        <div class="detail-card-gifts">
                            <?if ($arResult['CATALOG'] && $arParams['USE_GIFTS_DETAIL'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled("sale"))
                            {
                                $APPLICATION->IncludeComponent("bitrix:sale.gift.product", "card-new", array(
                                    "SHOW_UNABLE_SKU_PROPS"=>$arParams["SHOW_UNABLE_SKU_PROPS"],
                                    'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
                                    'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
                                    'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
                                    'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
                                    'SUBSCRIBE_URL_TEMPLATE' => $arResult['~SUBSCRIBE_URL_TEMPLATE'],
                                    'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
                                    "OFFER_HIDE_NAME_PROPS" => $arParams["OFFER_HIDE_NAME_PROPS"],
                                    'SHOW_DISCOUNT_TIME_EACH_SKU' => $arParams['SHOW_DISCOUNT_TIME_EACH_SKU'],

                                    "SHOW_DISCOUNT_PERCENT" => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
                                    "SHOW_OLD_PRICE" => $arParams['GIFTS_SHOW_OLD_PRICE'],
                                    "PAGE_ELEMENT_COUNT" => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
                                    "LINE_ELEMENT_COUNT" => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
                                    "HIDE_BLOCK_TITLE" => $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'],
                                    "BLOCK_TITLE" => $arParams['GIFTS_DETAIL_BLOCK_TITLE'],
                                    "TEXT_LABEL_GIFT" => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],
                                    "SHOW_NAME" => $arParams['GIFTS_SHOW_NAME'],
                                    "SHOW_IMAGE" => $arParams['GIFTS_SHOW_IMAGE'],
                                    "MESS_BTN_BUY" => $arParams['GIFTS_MESS_BTN_BUY'],

                                    "SHOW_PRODUCTS_{$arParams['IBLOCK_ID']}" => "Y",
                                    "HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
                                    "PRODUCT_SUBSCRIPTION" => $arParams["PRODUCT_SUBSCRIPTION"],
                                    "MESS_BTN_DETAIL" => $arParams["MESS_BTN_DETAIL"],
                                    "MESS_BTN_SUBSCRIBE" => $arParams["MESS_BTN_SUBSCRIBE"],
                                    "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                                    "PRICE_CODE" => $arParams["PRICE_CODE"],
                                    "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
                                    "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                                    "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
                                    "CURRENCY_ID" => $arParams["CURRENCY_ID"],
                                    "BASKET_URL" => $arParams["BASKET_URL"],
                                    "ADD_PROPERTIES_TO_BASKET" => $arParams["ADD_PROPERTIES_TO_BASKET"],
                                    "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                                    "PARTIAL_PRODUCT_PROPERTIES" => $arParams["PARTIAL_PRODUCT_PROPERTIES"],
                                    "USE_PRODUCT_QUANTITY" => 'N',
                                    "OFFER_TREE_PROPS_{$arResult['OFFERS_IBLOCK']}" => $arParams['OFFER_TREE_PROPS'],
                                    "CART_PROPERTIES_{$arResult['OFFERS_IBLOCK']}" => $arParams['OFFERS_CART_PROPERTIES'],
                                    "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                                    "SHOW_DISCOUNT_TIME" => $arParams["SHOW_DISCOUNT_TIME"],
                                    "SALE_STIKER" => $arParams["SALE_STIKER"],
                                    "SHOW_OLD_PRICE" => $arParams["SHOW_OLD_PRICE"],
                                    "SHOW_MEASURE" => $arParams["SHOW_MEASURE"],
                                    "DISPLAY_TYPE" => "block",
                                    "SHOW_RATING" => $arParams["SHOW_RATING"],
                                    "DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
                                    "DISPLAY_WISH_BUTTONS" => $arParams["DISPLAY_WISH_BUTTONS"],
                                    "DEFAULT_COUNT" => $arParams["DEFAULT_COUNT"],
                                    "TYPE_SKU" => "Y",

                                    "POTENTIAL_PRODUCT_TO_BUY" => array(
                                        'ID' => isset($arResult['ID']) ? $arResult['ID'] : null,
                                        'MODULE' => isset($arResult['MODULE']) ? $arResult['MODULE'] : 'catalog',
                                        'PRODUCT_PROVIDER_CLASS' => isset($arResult['PRODUCT_PROVIDER_CLASS']) ? $arResult['PRODUCT_PROVIDER_CLASS'] : 'CCatalogProductProvider',
                                        'QUANTITY' => isset($arResult['QUANTITY']) ? $arResult['QUANTITY'] : null,
                                        'IBLOCK_ID' => isset($arResult['IBLOCK_ID']) ? $arResult['IBLOCK_ID'] : null,

                                        'PRIMARY_OFFER_ID' => isset($arResult['OFFERS'][0]['ID']) ? $arResult['OFFERS'][0]['ID'] : null,
                                        'SECTION' => array(
                                            'ID' => isset($arResult['SECTION']['ID']) ? $arResult['SECTION']['ID'] : null,
                                            'IBLOCK_ID' => isset($arResult['SECTION']['IBLOCK_ID']) ? $arResult['SECTION']['IBLOCK_ID'] : null,
                                            'LEFT_MARGIN' => isset($arResult['SECTION']['LEFT_MARGIN']) ? $arResult['SECTION']['LEFT_MARGIN'] : null,
                                            'RIGHT_MARGIN' => isset($arResult['SECTION']['RIGHT_MARGIN']) ? $arResult['SECTION']['RIGHT_MARGIN'] : null,
                                        ),
                                    )
                                ), $component, array("HIDE_ICONS" => "Y"));
                            }?>
                        </div>
                    </div>
                </div>
            <?}?>
            <div class="middle_info main_item_wrapper">
                <?$frame = $this->createFrame()->begin();?>
                <div class="prices_block">
                    <div class="price_block_left cost prices clearfix">
                        <?if( count( $arResult["OFFERS"] ) > 0 ){?>
                            <div class="with_matrix" style="display:none;">
                                <div class="price price_value_block"><span class="values_wrapper"></span></div>
                                <?if($arParams["SHOW_OLD_PRICE"]=="Y"):?>
                                    <div class="price discount"></div>
                                <?endif;?>
                                <div class="price_block_right">
                                    <?if($arParams["SHOW_DISCOUNT_PERCENT"]=="Y"){?>
                                        <div class="sale_block matrix" style="display:none;">
                                            <div class="sale_wrapper">
                                                <div class="value">-<span></span>%</div>
                                                <div class="text"><span class="title"><?=GetMessage("CATALOG_ECONOMY");?></span>
                                                    <span class="values_wrapper"></span></div>
                                                <div class="clearfix"></div></div>
                                        </div>
                                    <?}?>
                                </div>
                            </div>
                            <?\Aspro\Functions\CAsproSku::showItemPrices($arParams, $arResult, $item_id, $min_price_id, $arItemIDs);?>
                        <?}else{?>
                            <?
                            $item_id = $arResult["ID"];
                            if(isset($arResult['PRICE_MATRIX']) && $arResult['PRICE_MATRIX']) // USE_PRICE_COUNT
                            {
                                if($arResult['PRICE_MATRIX']['COLS'])
                                {
                                    $arCurPriceType = current($arResult['PRICE_MATRIX']['COLS']);
                                    $arCurPrice = current($arResult['PRICE_MATRIX']['MATRIX'][$arCurPriceType['ID']]);
                                    $min_price_id = $arCurPriceType['ID'];?>
                                    <div class="" itemprop="offers" itemscope itemtype="https://schema.org/Offer">
                                        <meta itemprop="price" content="<?=($arResult['MIN_PRICE']['DISCOUNT_VALUE'] ? $arResult['MIN_PRICE']['DISCOUNT_VALUE'] : $arResult['MIN_PRICE']['VALUE'])?>" />
                                        <meta itemprop="priceCurrency" content="<?=$arResult['MIN_PRICE']['CURRENCY']?>" />
                                        <link itemprop="availability" href="http://schema.org/<?=($arResult['PRICE_MATRIX']['AVAILABLE'] == 'Y' ? 'InStock' : 'OutOfStock')?>" />
                                    </div>
                                <?}?>
                                <?if($arResult['ITEM_PRICE_MODE'] == 'Q' && count($arResult['PRICE_MATRIX']['ROWS']) > 1):?>
                                <?=COptimus::showPriceRangeTop($arResult, $arParams, GetMessage("CATALOG_ECONOMY"));?>
                            <?endif;?>
                                <?=COptimus::showPriceMatrix($arResult, $arParams, $strMeasure, $arAddToBasketData);?>
                                <?
                            }
                            else
                            {?>
                                <?\Aspro\Functions\CAsproItem::showItemPrices($arParams, $arResult["PRICES"], $strMeasure, $min_price_id);?>
                            <?}?>
                        <?}?>
                    </div>
                    <?
                    $arSectionSale = [691, 741, 742, 1731, 1732, 692, 1708];
                    $UF_SALE_MITSUBISHI10 = \Bitrix\Main\Config\Option::get( "askaron.settings", "UF_SALE_MITSUBISHI10");
                    if($UF_SALE_MITSUBISHI10 == 1 && in_array($arResult['IBLOCK_SECTION_ID'], $arSectionSale)){?>
                        <div class="item-stock" data="<?=$UF_SALE_MITSUBISHI10?>" data2="<?= $arResult['IBLOCK_SECTION_ID']?>"><span>*Скидка 10% при покупке от 2-х и более штук</span></div>
                    <?}?>


                    <?if($arParams["SHOW_DISCOUNT_TIME"]=="Y"){?>
                    <?$arUserGroups = $USER->GetUserGroupArray();?>
                    <?if($arParams['SHOW_DISCOUNT_TIME_EACH_SKU'] != 'Y' || ($arParams['SHOW_DISCOUNT_TIME_EACH_SKU'] == 'Y' && !$arResult['OFFERS'])):?>
                    <?$arDiscounts = CCatalogDiscount::GetDiscountByProduct($item_id, $arUserGroups, "N", $min_price_id, SITE_ID);
                    $arDiscount=array();
                    if($arDiscounts)
                        $arDiscount=current($arDiscounts);
                    if($arDiscount["ACTIVE_TO"]){?>
                    <div class="view_sale_block <?=($arQuantityData["HTML"] ? '' : 'wq');?>"">
                    <div class="count_d_block">
                        <span class="active_to hidden"><?=$arDiscount["ACTIVE_TO"];?></span>
                        <div class="title"><?=GetMessage("UNTIL_AKC");?></div>
                        <span class="countdown values"></span>
                    </div>
                    <?if($arQuantityData["HTML"]):?>
                        <div class="quantity_block">
                            <div class="title"><?=GetMessage("TITLE_QUANTITY_BLOCK");?></div>
                            <div class="values">
												<span class="item">
													<span class="value" <?=((count( $arResult["OFFERS"] ) > 0 && $arParams["TYPE_SKU"] == 'TYPE_1' && $arResult["OFFERS_PROP"]) ? 'style="opacity:0;"' : '')?>><?=$totalCount;?></span>
													<span class="text"><?=GetMessage("TITLE_QUANTITY");?></span>
												</span>
                            </div>
                        </div>
                    <?endif;?>
                </div>
            <?}?>
            <?else:?>
                <?if($arResult['JS_OFFERS'])
                {

                    foreach($arResult['JS_OFFERS'] as $keyOffer => $arTmpOffer2)
                    {
                        $active_to = '';
                        $arDiscounts = CCatalogDiscount::GetDiscountByProduct( $arTmpOffer2['ID'], $arUserGroups, "N", array(), SITE_ID );
                        if($arDiscounts)
                        {
                            foreach($arDiscounts as $arDiscountOffer)
                            {
                                if($arDiscountOffer['ACTIVE_TO'])
                                {
                                    $active_to = $arDiscountOffer['ACTIVE_TO'];
                                    break;
                                }
                            }
                        }
                        $arResult['JS_OFFERS'][$keyOffer]['DISCOUNT_ACTIVE'] = $active_to;
                    }
                }?>
                <div class="view_sale_block" style="display:none;">
                    <div class="count_d_block">
                        <span class="active_to_<?=$arResult["ID"]?> hidden"><?=$arDiscount["ACTIVE_TO"];?></span>
                        <div class="title"><?=GetMessage("UNTIL_AKC");?></div>
                        <span class="countdown countdown_<?=$arResult["ID"]?> values"></span>
                    </div>
                    <?if($arQuantityData["HTML"]):?>
                        <div class="quantity_block">
                            <div class="title"><?=GetMessage("TITLE_QUANTITY_BLOCK");?></div>
                            <div class="values">
											<span class="item">
												<span class="value"><?=$totalCount;?></span>
												<span class="text"><?=GetMessage("TITLE_QUANTITY");?></span>
											</span>
                            </div>
                        </div>
                    <?endif;?>
                </div>
            <?endif;?>
            <?}?>
                <?if($useStores){?>
                <div class="p_block">
                    <?}?>
                    <?=$arQuantityData["HTML"];?>
                    <?if($totalCount == 0){?>
                        <div class="count0">
                            <?if(!empty($arResult['PROPERTIES']["DELIVERY_OPTIONS"]['VALUE'])){?>
                                <span>Срок доставки: <?=$arResult['PROPERTIES']["DELIVERY_OPTIONS"]['VALUE']?> дней</span><br>
                            <?}?>
                            <?if(!empty($arResult['PROPERTIES']["SALES_NOTES"]['VALUE'])){?>
                                <span><?=$arResult['PROPERTIES']["SALES_NOTES"]['VALUE']?></span>
                            <?}?>
                        </div>
                    <?}?>
                    <?if($useStores){?>
                </div>
            <?}?>
            </div>
            <div class="buy_block buy_block_custom">
                <?if($arResult["OFFERS"] && $showCustomOffer){?>
                    <div class="sku_props">
                        <?if (!empty($arResult['OFFERS_PROP'])){?>
                            <div class="bx_catalog_item_scu wrapper_sku" id="<? echo $arItemIDs["ALL_ITEM_IDS"]['PROP_DIV']; ?>">
                                <?foreach ($arSkuTemplate as $code => $strTemplate){
                                    if (!isset($arResult['OFFERS_PROP'][$code]))
                                        continue;
                                    echo str_replace('#ITEM#_prop_', $arItemIDs["ALL_ITEM_IDS"]['PROP'], $strTemplate);
                                }?>
                            </div>
                        <?}?>
                        <?$arItemJSParams=COptimus::GetSKUJSParams($arResult, $arParams, $arResult, "Y");?>
                        <script type="text/javascript">
                            var <? echo $arItemIDs["strObName"]; ?> = new JCCatalogElement(<? echo CUtil::PhpToJSObject($arItemJSParams, false, true); ?>);
                        </script>
                    </div>
                <?}?>
                <?if(!$arResult["OFFERS"]):?>
                    <script>
                        $(document).ready(function() {
                            $('.catalog_detail .tabs_section .tabs_content .form.inline input[data-sid="PRODUCT_NAME"]').attr('value', $('h1').text());
                        });
                    </script>
                    <div class="counter_wrapp">
                        <?if(($arAddToBasketData["OPTIONS"]["USE_PRODUCT_QUANTITY_DETAIL"] && $arAddToBasketData["ACTION"] == "ADD") && $arResult["CAN_BUY"]):?>
                            <div class="counter_block big_basket" data-offers="<?=($arResult["OFFERS"] ? "Y" : "N");?>" data-item="<?=$arResult["ID"];?>" <?=(($arResult["OFFERS"] && $arParams["TYPE_SKU"]=="N") ? "style='display: none;'" : "");?>>
                                <span class="minus" id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY_DOWN']; ?>">-</span>
                                <input type="text" class="text" id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<?=$arAddToBasketData["MIN_QUANTITY_BUY"]?>" />
                                <span class="plus" id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY_UP']; ?>" <?=($arAddToBasketData["MAX_QUANTITY_BUY"] ? "data-max='".$arAddToBasketData["MAX_QUANTITY_BUY"]."'" : "")?>>+</span>
                            </div>
                        <?endif;?>
                        <div id="<? echo $arItemIDs["ALL_ITEM_IDS"]['BASKET_ACTIONS']; ?>" onclick="yaCounter37461485.reachGoal('buyElement')" class="button_block <?=(($arAddToBasketData["ACTION"] == "ORDER" /*&& !$arResult["CAN_BUY"]*/) || !$arResult["CAN_BUY"] || !$arAddToBasketData["OPTIONS"]["USE_PRODUCT_QUANTITY_DETAIL"] || ($arAddToBasketData["ACTION"] == "SUBSCRIBE" && $arResult["CATALOG_SUBSCRIBE"] == "Y")  ? "wide" : "");?>" >
                            <!--noindex-->
                            <?=$arAddToBasketData["HTML"]?>
                            <!--/noindex-->
                        </div>
                    </div>
                <?if($arAddToBasketData["ACTION"] !== "NOTHING"):?>
                <?if($arAddToBasketData["ACTION"] == "ADD" && $arResult["CAN_BUY"] && $arParams["SHOW_ONE_CLICK_BUY"]!="N"):?>
                    <div class="wrapp_one_click">
									<span class="transparent big_btn type_block button button-card-custom transition_bg one_click" data-item="<?=$arResult["ID"]?>" data-iblockID="<?=$arParams["IBLOCK_ID"]?>" data-quantity="<?=$arAddToBasketData["MIN_QUANTITY_BUY"];?>" onclick="yaCounter37461485.reachGoal('buy1clickElement');  oneClickBuy('<?=$arResult["ID"]?>', '<?=$arParams["IBLOCK_ID"]?>', this)">
										<span class="buy-one-click"><?=GetMessage('ONE_CLICK_BUY')?></span>
									</span>
                    </div>
                <?endif;?>
                <?endif;?>

                <?if(isset($arResult['PRICE_MATRIX']) && $arResult['PRICE_MATRIX']) // USE_PRICE_COUNT
                {?>
                <?if($arResult['ITEM_PRICE_MODE'] == 'Q' && count($arResult['PRICE_MATRIX']['ROWS']) > 1):?>
                <?$arOnlyItemJSParams = array(
                    "ITEM_PRICES" => $arResult["ITEM_PRICES"],
                    "ITEM_PRICE_MODE" => $arResult["ITEM_PRICE_MODE"],
                    "ITEM_QUANTITY_RANGES" => $arResult["ITEM_QUANTITY_RANGES"],
                    "MIN_QUANTITY_BUY" => $arAddToBasketData["MIN_QUANTITY_BUY"],
                    "ID" => $arItemIDs["strMainID"],
                )?>
                    <script type="text/javascript">
                        var <? echo $arItemIDs["strObName"]; ?>el = new JCCatalogOnlyElement(<? echo CUtil::PhpToJSObject($arOnlyItemJSParams, false, true); ?>);
                    </script>
                <?endif;?>
                <?}?>
                <?elseif($arResult["OFFERS"] && $arParams['TYPE_SKU'] == 'TYPE_1'):?>
                    <div class="offer_buy_block buys_wrapp" style="display:none;">
                        <div class="counter_wrapp"></div>
                    </div>
                <?elseif($arResult["OFFERS"] && $arParams['TYPE_SKU'] != 'TYPE_1'):?>
                    <span class="big_btn slide_offer button transition_bg type_block"><i></i><span><?=GetMessage("MORE_TEXT_BOTTOM");?></span></span>
                <?endif;?>
            </div>
            <?$frame->end();?>
        </div>
        <?if(is_array($arResult["STOCK"]) && $arResult["STOCK"]):?>
            <?foreach($arResult["STOCK"] as $key => $arStockItem):?>
                <div class="stock_board">
                    <div class="title"><?=GetMessage("CATALOG_STOCK_TITLE")?></div>
                    <div class="txt"><?=$arStockItem["PREVIEW_TEXT"]?></div>
                    <a class="read_more" href="<?=$arStockItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CATALOG_STOCK_VIEW")?></a>
                </div>
            <?endforeach;?>
        <?endif;?>

        <div><?if($arResult['PROPERTIES']["PROP_MONT_GIFT"]['VALUE']=="Есть"): ?>Монтаж в подарок<?endif;?></div>
        <div><?if($arResult['PROPERTIES']["PROP_FREE_DIV"]['VALUE']=="Есть"): ?>Бесплатная доставка<?endif;?></div>

    </div>
</div>
<?if($arResult['OFFERS']):?>
    <span itemprop="offers" itemscope itemtype="https://schema.org/AggregateOffer" style="display:none;">
			<meta itemprop="lowPrice" content="<?=($arResult['MIN_PRICE']['DISCOUNT_VALUE'] ? $arResult['MIN_PRICE']['DISCOUNT_VALUE'] : $arResult['MIN_PRICE']['VALUE'] )?>" />
		    <meta itemprop="priceCurrency" content="<?=$arResult['MIN_PRICE']['CURRENCY']?>" />
		    <meta itemprop="offerCount" content="<?=count($arResult['OFFERS'])?>" />
			<?foreach($arResult['OFFERS'] as $arOffer):?>
                <?$currentOffersList = array();?>
                <?foreach($arOffer['TREE'] as $propName => $skuId):?>
                    <?$propId = (int)substr($propName, 5);?>
                    <?foreach($arResult['SKU_PROPS'] as $prop):?>
                        <?if($prop['ID'] == $propId):?>
                            <?foreach($prop['VALUES'] as $propId => $propValue):?>
                                <?if($propId == $skuId):?>
                                    <?$currentOffersList[] = $propValue['NAME'];?>
                                    <?break;?>
                                <?endif;?>
                            <?endforeach;?>
                        <?endif;?>
                    <?endforeach;?>
                <?endforeach;?>
                <span itemprop="offers" itemscope itemtype="https://schema.org/Offer">
					<meta itemprop="sku" content="<?=implode('/', $currentOffersList)?>" />
					<a href="<?=$arOffer['DETAIL_PAGE_URL']?>" itemprop="url"></a>
					<meta itemprop="price" content="<?=($arOffer['MIN_PRICE']['DISCOUNT_VALUE']) ? $arOffer['MIN_PRICE']['DISCOUNT_VALUE'] : $arOffer['MIN_PRICE']['VALUE']?>" />
					<meta itemprop="priceCurrency" content="<?=$arOffer['MIN_PRICE']['CURRENCY']?>" />
					<link itemprop="availability" href="http://schema.org/<?=($arOffer['CAN_BUY'] ? 'InStock' : 'OutOfStock')?>" />
				</span>
            <?endforeach;?>
		</span>
    <?unset($arOffer, $currentOffersList);?>
<?else:?>
    <span itemprop="offers" itemscope itemtype="https://schema.org/Offer">
			<meta itemprop="price" content="<?=($arResult['MIN_PRICE']['DISCOUNT_VALUE'] ? $arResult['MIN_PRICE']['DISCOUNT_VALUE'] : $arResult['MIN_PRICE']['VALUE'])?>" />
			<meta itemprop="priceCurrency" content="<?=$arResult['MIN_PRICE']['CURRENCY']?>" />
			<link itemprop="availability" href="http://schema.org/<?=($arResult['MIN_PRICE']['CAN_BUY'] ? 'InStock' : 'OutOfStock')?>" />
		</span>
<?endif;?>
<div class="clearleft"></div>

<?if($arResult["TIZERS_ITEMS"]){?>
    <div class="tizers_block_detail">
        <div class="rows_block">
            <?$count_t_items=count($arResult["TIZERS_ITEMS"]);?>
            <?foreach($arResult["TIZERS_ITEMS"] as $arItem){?>
                <div class="item_block tizer col-<?=$count_t_items;?>">
                    <div class="inner_wrapper">
                        <?if($arItem["UF_LINK"]){?>
                        <a href="<?=$arItem["UF_LINK"];?>" <?=(strpos($arItem["UF_LINK"], "http") !== false ? "target='_blank' rel='nofollow'" : '')?>>
                            <?}?>
                            <?if($arItem["UF_FILE"]){?>
                                <div class="image">
                                    <img src="<?=$arItem["PREVIEW_PICTURE"]["src"];?>" alt="<?=$arItem["UF_NAME"];?>" title="<?=$arItem["UF_NAME"];?>">
                                </div>
                            <?}?>
                            <div class="text">
                                <?=$arItem["UF_NAME"];?>
                            </div>
                            <div class="clearfix"></div>
                            <?if($arItem["UF_LINK"]){?>
                        </a>
                    <?}?>
                    </div>
                </div>
            <?}?>
        </div>
    </div>
<?}?>

<?if($arParams["SHOW_KIT_PARTS"] == "Y" && $arResult["SET_ITEMS"]):?>
    <div class="set_wrapp set_block">
        <div class="title"><?=GetMessage("GROUP_PARTS_TITLE")?></div>
        <ul>
            <?foreach($arResult["SET_ITEMS"] as $iii => $arSetItem):?>
                <li class="item">
                    <div class="item_inner">
                        <div class="image">
                            <a href="<?=$arSetItem["DETAIL_PAGE_URL"]?>">
                                <?if($arSetItem["PREVIEW_PICTURE"]):?>
                                    <?$img = CFile::ResizeImageGet($arSetItem["PREVIEW_PICTURE"], array("width" => 140, "height" => 140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
                                    <img  src="<?=$img["src"]?>" alt="<?=$arSetItem["NAME"];?>" title="<?=$arSetItem["NAME"];?>" />
                                <?elseif($arSetItem["DETAIL_PICTURE"]):?>
                                    <?$img = CFile::ResizeImageGet($arSetItem["DETAIL_PICTURE"], array("width" => 140, "height" => 140), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
                                    <img  src="<?=$img["src"]?>" alt="<?=$arSetItem["NAME"];?>" title="<?=$arSetItem["NAME"];?>" />
                                <?else:?>
                                    <img  src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_small.png" alt="<?=$arSetItem["NAME"];?>" title="<?=$arSetItem["NAME"];?>" />
                                <?endif;?>
                            </a>
                            <?if($arResult["SET_ITEMS_QUANTITY"]):?>
                                <div class="quantity">x<?=$arSetItem["QUANTITY"];?></div>
                            <?endif;?>
                        </div>
                        <div class="item_info">

                            <div class="item-title">
                                <a href="<?=$arSetItem["DETAIL_PAGE_URL"]?>"><span><?=$arSetItem["NAME"]?></span></a>
                            </div>
                            <?if($arParams["SHOW_KIT_PARTS_PRICES"] == "Y"):?>
                                <div class="cost prices clearfix">
                                    <?
                                    $arCountPricesCanAccess = 0;
                                    foreach($arSetItem["PRICES"] as $key => $arPrice){
                                        if($arPrice["CAN_ACCESS"]){
                                            $arCountPricesCanAccess++;
                                        }
                                    }?>
                                    <?foreach($arSetItem["PRICES"] as $key => $arPrice):?>
                                        <?if($arPrice["CAN_ACCESS"]):?>
                                            <?$price = CPrice::GetByID($arPrice["ID"]);?>
                                            <?if($arCountPricesCanAccess > 1):?>
                                                <div class="price_name"><?=$price["CATALOG_GROUP_NAME"];?></div>
                                            <?endif;?>
                                            <?if($arPrice["VALUE"] > $arPrice["DISCOUNT_VALUE"]):?>
                                                <div class="price">
                                                    <?=$arPrice["PRINT_DISCOUNT_VALUE"];?><?if(($arParams["SHOW_MEASURE"] == "Y") && $strMeasure):?><small>/<?=$strMeasure?></small><?endif;?>
                                                </div>
                                                <?if($arParams["SHOW_OLD_PRICE"]=="Y"):?>
                                                    <div class="price discount">
                                                        <span><?=$arPrice["PRINT_VALUE"]?></span>
                                                    </div>
                                                <?endif;?>
                                            <?else:?>
                                                <div class="price">
                                                    <?=$arPrice["PRINT_VALUE"];?><?if(($arParams["SHOW_MEASURE"] == "Y") && $strMeasure):?><small>/<?=$strMeasure?></small><?endif;?>
                                                </div>
                                            <?endif;?>
                                        <?endif;?>
                                    <?endforeach;?>
                                </div>
                            <?endif;?>
                        </div>
                    </div>
                </li>
                <?if($arResult["SET_ITEMS"][$iii + 1]):?>
                    <li class="separator"></li>
                <?endif;?>
            <?endforeach;?>
        </ul>
    </div>
<?endif;?>
<?if($arResult['OFFERS']):?>
    <?if($arResult['OFFER_GROUP']):?>
        <?foreach($arResult['OFFERS'] as $arOffer):?>
            <?if(!$arOffer['OFFER_GROUP']) continue;?>
            <span id="<?=$arItemIDs['ALL_ITEM_IDS']['OFFER_GROUP'].$arOffer['ID']?>" style="display: none;">
					<?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor", "",
                        array(
                            "IBLOCK_ID" => $arResult["OFFERS_IBLOCK"],
                            "ELEMENT_ID" => $arOffer['ID'],
                            "PRICE_CODE" => $arParams["PRICE_CODE"],
                            "BASKET_URL" => $arParams["BASKET_URL"],
                            "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "SHOW_OLD_PRICE" => $arParams["SHOW_OLD_PRICE"],
                            "SHOW_MEASURE" => $arParams["SHOW_MEASURE"],
                            "SHOW_DISCOUNT_PERCENT" => $arParams["SHOW_DISCOUNT_PERCENT"],
                            "CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
                            "CURRENCY_ID" => $arParams["CURRENCY_ID"]
                        ), $component, array("HIDE_ICONS" => "Y")
                    );?>
				</span>
        <?endforeach;?>
    <?endif;?>
<?else:?>
    <?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor", "",
        array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "ELEMENT_ID" => $arResult["ID"],
            "PRICE_CODE" => $arParams["PRICE_CODE"],
            "BASKET_URL" => $arParams["BASKET_URL"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
            "SHOW_OLD_PRICE" => $arParams["SHOW_OLD_PRICE"],
            "SHOW_MEASURE" => $arParams["SHOW_MEASURE"],
            "SHOW_DISCOUNT_PERCENT" => $arParams["SHOW_DISCOUNT_PERCENT"],
            "CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
            "CURRENCY_ID" => $arParams["CURRENCY_ID"]
        ), $component, array("HIDE_ICONS" => "Y")
    );?>
<?endif;?>
</div>

<!--Статьи-->
<?if ($arResult["PROPERTIES"]["ARTICLES"]["VALUE"]):?>
    <section class="advices-items-block">
        <?foreach ($arResult["ARTICLE"] as $article):?>
            <?$articleImg = CFile::GetPath($article["PREVIEW_PICTURE"]);
            $articleName = $article["NAME"];
            $articleUrl = $article["DETAIL_PAGE_URL"];
            ?>
            <div class="advice-item">
                <img class="advice-item-img" src="<?=$articleImg?>" alt="<?=$articleName?>>"/>
                <a class="advice-item-link" href="<?=$articleUrl?>" target="_blank"><?=$articleName?></a>
            </div>
        <?endforeach;?>
    </section>
<?endif;?>


<?if ($arResult["PROPERTIES"]["ACCESSORIES_QUESTION"]["VALUE"] || $arResult["PROPERTIES"]["ACCESSORIES"]["VALUE"]){?>
    <?$imgAccesoriesTab = CFile::ResizeImageGet($arResult["PROPERTIES"]["ACCESSORIES_PICTURE"]["VALUE"], array('width'=> 220, 'height'=> 184), BX_RESIZE_IMAGE_EXACT, true);?>
    <section id="accessories" class="additional-goods-block">
                                    <img src="<?=$imgAccesoriesTab["src"]?>" alt="<?=$arResult["PROPERTIES"]["ACCESSORIES_QUESTION"]["VALUE"]?>" class="additional-goods-img">
                                    <div class="additional-goods-title--block">
                                        <h2 class="additional-goods-title"><?=$arResult["PROPERTIES"]["ACCESSORIES_QUESTION"]["VALUE"]?></h2>
                                    </div>
                                    <p class="additional-goods-text"><?=htmlspecialcharsBack($arResult["PROPERTIES"]["ACCESSORIES_TEXT"]["VALUE"]["TEXT"])?></p>
                                    <?if (($arResult["PROPERTIES"]["ACCESSORIES"]["VALUE"]) ||
                                            ($arResult["PROPERTIES"]["ACCESSORIES_1TAB_NAME"]["VALUE"]) ||
                                            ($arResult["PROPERTIES"]["ACCESSORIES_2TAB_NAME"]["VALUE"]) ||
                                            ($arResult["PROPERTIES"]["ACCESSORIES_3TAB_NAME"]["VALUE"]) ||
                                            ($arResult["PROPERTIES"]["ACCESSORIES_4TAB_NAME"]["VALUE"])) { ?>
                                    <div class="tabs_section tabs-additional">
                                        <ul class="tabs1 main_tabs1 tabs-head">
                                            <?$iTab = 0;?>
                                            <?if($arResult["PROPERTIES"]["ACCESSORIES"]["VALUE"]):?>
                                                <li class="acessoriesTab<?=(!($iTab++) ? ' current' : '')?>">
                                                    <span><?=$arResult["PROPERTIES"]["ACCESSORIES"]["NAME"]?></span>
                                                </li>
                                            <?endif;?>

                                            <?if($arResult["PROPERTIES"]["ACCESSORIES_1TAB_NAME"]["VALUE"]):?>
                                                <li class="acessories1Tab<?=(!($iTab++) ? ' current' : '')?>">
                                                    <span><?=$arResult["PROPERTIES"]["ACCESSORIES_1TAB_NAME"]["VALUE"]?></span>
                                                </li>
                                            <?endif;?>

                                            <?if($arResult["PROPERTIES"]["ACCESSORIES_2TAB_NAME"]["VALUE"]):?>
                                                <li class="acessories2Tab<?=(!($iTab++) ? ' current' : '')?>">
                                                    <span><?=$arResult["PROPERTIES"]["ACCESSORIES_2TAB_NAME"]["VALUE"]?></span>
                                                </li>
                                            <?endif;?>

                                            <?if($arResult["PROPERTIES"]["ACCESSORIES_3TAB_NAME"]["VALUE"]):?>
                                                <li class="acessories3Tab<?=(!($iTab++) ? ' current' : '')?>">
                                                    <span><?=$arResult["PROPERTIES"]["ACCESSORIES_3TAB_NAME"]["VALUE"]?></span>
                                                </li>
                                            <?endif;?>

                                            <?if($arResult["PROPERTIES"]["ACCESSORIES_4TAB_NAME"]["VALUE"]):?>
                                                <li class="acessories4Tab<?=(!($iTab++) ? ' current' : '')?>">
                                                    <span><?=$arResult["PROPERTIES"]["ACCESSORIES_4TAB_NAME"]["VALUE"]?></span>
                                                </li>
                                            <?endif;?>
                                        </ul>
                                        <ul class="tabs_content tabs-body">
                                            <?$show_tabs = false;?>
                                            <?$iTab = 0;?>

                                            <? if ($arResult["PROPERTIES"]["ACCESSORIES"]["VALUE"]): ?>
                                                <li class="oldacessories <?= (!($iTab++) ? 'current' : '') ?>">
                                                    <? global $OPTIMUS_SMART_FILTER_DOP;
                                                    $OPTIMUS_SMART_FILTER_DOP["ID"] = $arResult["DISPLAY_PROPERTIES"]["ACCESSORIES"]["VALUE"];
                                                    ?>

                                                        <? $APPLICATION->IncludeComponent(
                                                            "bitrix:catalog.section",
                                                            "catalog_block_acessories",
                                                            array(
                                                                "SHOW_UNABLE_SKU_PROPS" => "Y",
                                                                "SEF_URL_TEMPLATES" => array(
                                                                    "sections" => "",
                                                                    "section" => "#SECTION_CODE_PATH#/",
                                                                    "element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
                                                                    "compare" => "compare.php?action=#ACTION_CODE#",
                                                                    "smart_filter" => "#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/",
                                                                ),
                                                                "IBLOCK_TYPE" => "catalog",
                                                                "IBLOCK_ID" => "1",
                                                                "SECTION_ID" => "",
                                                                "SECTION_CODE" => "",
                                                                "SHOW_ALL_WO_SECTION" => "Y",
                                                                "SHOW_ARTICLE_SKU" => "Y",
                                                                "SHOW_MEASURE_WITH_RATIO" => "N",
                                                                "AJAX_REQUEST" => "N",
                                                                "ELEMENT_SORT_FIELD" => "ID",
                                                                "ELEMENT_SORT_ORDER" => "ASC",

                                                                "FILTER_NAME" => "OPTIMUS_SMART_FILTER_DOP",
                                                                "INCLUDE_SUBSECTIONS" => "Y",
                                                                "PAGE_ELEMENT_COUNT" => "5",
                                                                "LINE_ELEMENT_COUNT" => "5",
                                                                "DISPLAY_TYPE" => "block",
                                                                // "TYPE_SKU" => $TEMPLATE_OPTIONS["TYPE_SKU"]["CURRENT_VALUE"],
                                                                "TYPE_SKU" => "TYPE_1",
                                                                "PROPERTY_CODE" => array(
                                                                    0 => "54",
                                                                    1 => "64",
                                                                    2 => "66",
                                                                    3 => "130",
                                                                    4 => "133",
                                                                    5 => "134",
                                                                    6 => "204",
                                                                    7 => "205",
                                                                    8 => "206",
                                                                    9 => "209",
                                                                    10 => "211",
                                                                    11 => "214",
                                                                    12 => "215",
                                                                    13 => "216",
                                                                    14 => "218",
                                                                    15 => "220",
                                                                    16 => "221",
                                                                    17 => "231",
                                                                    18 => "236",
                                                                    19 => "238",
                                                                    20 => "239",
                                                                    21 => "240",
                                                                    22 => "241",
                                                                    23 => "242",
                                                                    24 => "243",
                                                                    25 => "245",
                                                                    26 => "246",
                                                                    27 => "248",
                                                                    28 => "249",
                                                                    29 => "251",
                                                                    30 => "COLOR",
                                                                    31 => "ACCESSORIES",
                                                                    32 => "ARTICUL",
                                                                    33 => "CML2_ARTICLE",
                                                                    34 => "WEIGHT_PROP",
                                                                    35 => "VIDEO_YOUTUBE",
                                                                    36 => "PROP_MATERIAL_37",
                                                                    37 => "PROP_MATERIAL_36",
                                                                    38 => "PROP_MATERIAL_26",
                                                                    39 => "YMARKET_DESC",
                                                                    40 => "YMARKET_TIME",
                                                                    41 => "SALE_NOTES",
                                                                    42 => "CML2_ATTRIBUTES",
                                                                    43 => "52",
                                                                    44 => "56",
                                                                    45 => "59",
                                                                    46 => "60",
                                                                    47 => "61",
                                                                    48 => "62",
                                                                    49 => "63",
                                                                    50 => "122",
                                                                    51 => "123",
                                                                    52 => "202",
                                                                    53 => "203",
                                                                    54 => "207",
                                                                    55 => "208",
                                                                    56 => "210",
                                                                    57 => "212",
                                                                    58 => "217",
                                                                    59 => "227",
                                                                    60 => "228",
                                                                    61 => "229",
                                                                    62 => "230",
                                                                    63 => "234",
                                                                    64 => "235",
                                                                    65 => "237",
                                                                    66 => "BRAND",
                                                                    67 => "PROP_2033",
                                                                    68 => "COLOR_REF2",
                                                                    69 => "PROP_159",
                                                                    70 => "PROP_2052",
                                                                    71 => "PROP_2027",
                                                                    72 => "PROP_2053",
                                                                    73 => "PROP_2083",
                                                                    74 => "PROP_2049",
                                                                    75 => "PROP_2026",
                                                                    76 => "PROP_2044",
                                                                    77 => "PROP_162",
                                                                    78 => "PROP_2065",
                                                                    79 => "PROP_2054",
                                                                    80 => "PROP_2017",
                                                                    81 => "PROP_2055",
                                                                    82 => "PROP_2069",
                                                                    83 => "PROP_2062",
                                                                    84 => "PROP_2061",
                                                                    85 => "CML2_LINK",
                                                                    86 => "",
                                                                ),
                                                                "SHOW_DISCOUNT_TIME_EACH_SKU" => "N",

                                                                "OFFERS_FIELD_CODE" => array(
                                                                    0 => "NAME",
                                                                    1 => "CML2_LINK",
                                                                    2 => "DETAIL_PAGE_URL",
                                                                    3 => "",
                                                                ),
                                                                "OFFERS_PROPERTY_CODE" => array(
                                                                    0 => "ARTICLE",
                                                                    1 => "VOLUME",
                                                                    2 => "SIZES",
                                                                    3 => "COLOR_REF",
                                                                    4 => "",
                                                                ),
                                                                "OFFERS_SORT_FIELD" => "shows",
                                                                "OFFERS_SORT_ORDER" => "asc",
                                                                "OFFERS_SORT_FIELD2" => "shows",
                                                                "OFFERS_SORT_ORDER2" => "asc",
                                                                'OFFER_TREE_PROPS' => array(
                                                                    0 => "SIZES",
                                                                    1 => "COLOR_REF",
                                                                ),

                                                                "OFFERS_LIMIT" => "10",

                                                                "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                                                                "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                                                                "BASKET_URL" => "/basket/",
                                                                "ACTION_VARIABLE" => "action",
                                                                "PRODUCT_ID_VARIABLE" => "id",
                                                                "SECTION_ID_VARIABLE" => "SECTION_ID",
                                                                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                                                "PRODUCT_PROPS_VARIABLE" => "prop",

                                                                "SET_LAST_MODIFIED" => "Y",
                                                                "AJAX_MODE" => "N",
                                                                "AJAX_OPTION_JUMP" => "N",
                                                                "AJAX_OPTION_STYLE" => "Y",
                                                                "AJAX_OPTION_HISTORY" => "Y",
                                                                "CACHE_TYPE" => "A",
                                                                "CACHE_TIME" => "3600000",
                                                                "CACHE_FILTER" => "Y",
                                                                "CACHE_GROUPS" => "N",


                                                                "HIDE_NOT_AVAILABLE" => "Y",
                                                                "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
                                                                "USE_COMPARE" => "Y",
                                                                "SET_TITLE" => "N",
                                                                "SET_STATUS_404" => "N",
                                                                "SHOW_404" => "N",
                                                                "MESSAGE_404" => "",
                                                                "FILE_404" => "",
                                                                "PRICE_CODE" => array(
                                                                    0 => "main",
                                                                ),
                                                                "USE_PRICE_COUNT" => "N",
                                                                "SHOW_PRICE_COUNT" => "1",
                                                                "PRICE_VAT_INCLUDE" => "Y",
                                                                "PRICE_VAT_SHOW_VALUE" => "N",


                                                                "USE_PRODUCT_QUANTITY" => "Y",
                                                                "OFFERS_CART_PROPERTIES" => array(
                                                                    0 => "VOLUME",
                                                                    1 => "SIZES",
                                                                    2 => "COLOR_REF",
                                                                ),
                                                                "DISPLAY_TOP_PAGER" => "N",
                                                                "DISPLAY_BOTTOM_PAGER" => "Y",

                                                                "PAGER_TITLE" => "N",
                                                                "PAGER_SHOW_ALWAYS" => "N",
                                                                "PAGER_TEMPLATE" => "load_more",
                                                                "PAGER_DESC_NUMBERING" => "N",
                                                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "N",
                                                                "PAGER_SHOW_ALL" => "N",

                                                                "AJAX_OPTION_ADDITIONAL" => "asseccories",
                                                                "ADD_CHAIN_ITEM" => "N",
                                                                "SHOW_QUANTITY" => "Y",
                                                                "SHOW_COUNTER_LIST" => "Y",
                                                                "SHOW_QUANTITY_COUNT" => "Y",
                                                                "SHOW_DISCOUNT_PERCENT" => "Y",
                                                                "SHOW_DISCOUNT_TIME" => "N",
                                                                "SHOW_OLD_PRICE" => "Y",
                                                                "CONVERT_CURRENCY" => "Y",
                                                                "CURRENCY_ID" => "RUB",
                                                                "USE_STORE" => "N",
                                                                "MAX_AMOUNT" => "20",
                                                                "MIN_AMOUNT" => "10",
                                                                "USE_MIN_AMOUNT" => "N",
                                                                "USE_ONLY_MAX_AMOUNT" => "Y",
                                                                "DISPLAY_WISH_BUTTONS" => "N",
                                                                "LIST_DISPLAY_POPUP_IMAGE" => "Y",
                                                                "DEFAULT_COUNT" => "1",
                                                                "SHOW_MEASURE" => "Y",
                                                                "SHOW_HINTS" => "Y",
                                                                "OFFER_HIDE_NAME_PROPS" => "N",
                                                                "SHOW_SECTIONS_LIST_PREVIEW" => $arParams["SHOW_SECTIONS_LIST_PREVIEW"],
                                                                "SECTIONS_LIST_PREVIEW_PROPERTY" => "DESCRIPTION",
                                                                "SHOW_SECTION_LIST_PICTURES" => "Y",
                                                                "USE_MAIN_ELEMENT_SECTION" => "N",
                                                                "ADD_PROPERTIES_TO_BASKET" => "Y",
                                                                "PARTIAL_PRODUCT_PROPERTIES" => "Y",
                                                                "PRODUCT_PROPERTIES" => array(),
                                                                "SALE_STIKER" => "SALE_TEXT",
                                                                "SHOW_RATING" => "Y",
                                                                "COMPATIBLE_MODE" => "Y",
                                                                "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                                            )
                                                        ); ?>
                                                </li>
                                            <? endif; ?>
                                            <? if ($arResult["PROPERTIES"]["ACCESSORIES_1TAB_ELEMENTS"]["VALUE"]): ?>
                                                <li class="acessories1 <?= (!($iTab++) ? 'current' : '') ?>">
                                                    <? global $OPTIMUS_SMART_FILTER_DOP;
                                                    $OPTIMUS_SMART_FILTER_DOP["ID"] = $arResult["DISPLAY_PROPERTIES"]["ACCESSORIES_1TAB_ELEMENTS"]["VALUE"];
                                                    ?>

                                                    <? $APPLICATION->IncludeComponent(
                                                        "bitrix:catalog.section",
                                                        "catalog_block_acessories",
                                                        array(
                                                            "SHOW_UNABLE_SKU_PROPS" => "Y",
                                                            "SEF_URL_TEMPLATES" => array(
                                                                "sections" => "",
                                                                "section" => "#SECTION_CODE_PATH#/",
                                                                "element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
                                                                "compare" => "compare.php?action=#ACTION_CODE#",
                                                                "smart_filter" => "#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/",
                                                            ),
                                                            "IBLOCK_TYPE" => "catalog",
                                                            "IBLOCK_ID" => "1",
                                                            "SECTION_ID" => "",
                                                            "SECTION_CODE" => "",
                                                            "SHOW_ALL_WO_SECTION" => "Y",
                                                            "SHOW_ARTICLE_SKU" => "Y",
                                                            "SHOW_MEASURE_WITH_RATIO" => "N",
                                                            "AJAX_REQUEST" => "N",
                                                            "ELEMENT_SORT_FIELD" => "ID",
                                                            "ELEMENT_SORT_ORDER" => "ASC",

                                                            "FILTER_NAME" => "OPTIMUS_SMART_FILTER_DOP",
                                                            "INCLUDE_SUBSECTIONS" => "Y",
                                                            "PAGE_ELEMENT_COUNT" => "5",
                                                            "LINE_ELEMENT_COUNT" => "5",
                                                            "DISPLAY_TYPE" => "block",
                                                            // "TYPE_SKU" => $TEMPLATE_OPTIONS["TYPE_SKU"]["CURRENT_VALUE"],
                                                            "TYPE_SKU" => "TYPE_1",
                                                            "PROPERTY_CODE" => array(
                                                                0 => "54",
                                                                1 => "64",
                                                                2 => "66",
                                                                3 => "130",
                                                                4 => "133",
                                                                5 => "134",
                                                                6 => "204",
                                                                7 => "205",
                                                                8 => "206",
                                                                9 => "209",
                                                                10 => "211",
                                                                11 => "214",
                                                                12 => "215",
                                                                13 => "216",
                                                                14 => "218",
                                                                15 => "220",
                                                                16 => "221",
                                                                17 => "231",
                                                                18 => "236",
                                                                19 => "238",
                                                                20 => "239",
                                                                21 => "240",
                                                                22 => "241",
                                                                23 => "242",
                                                                24 => "243",
                                                                25 => "245",
                                                                26 => "246",
                                                                27 => "248",
                                                                28 => "249",
                                                                29 => "251",
                                                                30 => "COLOR",
                                                                31 => "ACCESSORIES",
                                                                32 => "ARTICUL",
                                                                33 => "CML2_ARTICLE",
                                                                34 => "WEIGHT_PROP",
                                                                35 => "VIDEO_YOUTUBE",
                                                                36 => "PROP_MATERIAL_37",
                                                                37 => "PROP_MATERIAL_36",
                                                                38 => "PROP_MATERIAL_26",
                                                                39 => "YMARKET_DESC",
                                                                40 => "YMARKET_TIME",
                                                                41 => "SALE_NOTES",
                                                                42 => "CML2_ATTRIBUTES",
                                                                43 => "52",
                                                                44 => "56",
                                                                45 => "59",
                                                                46 => "60",
                                                                47 => "61",
                                                                48 => "62",
                                                                49 => "63",
                                                                50 => "122",
                                                                51 => "123",
                                                                52 => "202",
                                                                53 => "203",
                                                                54 => "207",
                                                                55 => "208",
                                                                56 => "210",
                                                                57 => "212",
                                                                58 => "217",
                                                                59 => "227",
                                                                60 => "228",
                                                                61 => "229",
                                                                62 => "230",
                                                                63 => "234",
                                                                64 => "235",
                                                                65 => "237",
                                                                66 => "BRAND",
                                                                67 => "PROP_2033",
                                                                68 => "COLOR_REF2",
                                                                69 => "PROP_159",
                                                                70 => "PROP_2052",
                                                                71 => "PROP_2027",
                                                                72 => "PROP_2053",
                                                                73 => "PROP_2083",
                                                                74 => "PROP_2049",
                                                                75 => "PROP_2026",
                                                                76 => "PROP_2044",
                                                                77 => "PROP_162",
                                                                78 => "PROP_2065",
                                                                79 => "PROP_2054",
                                                                80 => "PROP_2017",
                                                                81 => "PROP_2055",
                                                                82 => "PROP_2069",
                                                                83 => "PROP_2062",
                                                                84 => "PROP_2061",
                                                                85 => "CML2_LINK",
                                                                86 => "",
                                                            ),
                                                            "SHOW_DISCOUNT_TIME_EACH_SKU" => "N",

                                                            "OFFERS_FIELD_CODE" => array(
                                                                0 => "NAME",
                                                                1 => "CML2_LINK",
                                                                2 => "DETAIL_PAGE_URL",
                                                                3 => "",
                                                            ),
                                                            "OFFERS_PROPERTY_CODE" => array(
                                                                0 => "ARTICLE",
                                                                1 => "VOLUME",
                                                                2 => "SIZES",
                                                                3 => "COLOR_REF",
                                                                4 => "",
                                                            ),
                                                            "OFFERS_SORT_FIELD" => "shows",
                                                            "OFFERS_SORT_ORDER" => "asc",
                                                            "OFFERS_SORT_FIELD2" => "shows",
                                                            "OFFERS_SORT_ORDER2" => "asc",
                                                            'OFFER_TREE_PROPS' => array(
                                                                0 => "SIZES",
                                                                1 => "COLOR_REF",
                                                            ),

                                                            "OFFERS_LIMIT" => "10",

                                                            "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                                                            "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                                                            "BASKET_URL" => "/basket/",
                                                            "ACTION_VARIABLE" => "action",
                                                            "PRODUCT_ID_VARIABLE" => "id",
                                                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                                                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                                            "PRODUCT_PROPS_VARIABLE" => "prop",

                                                            "SET_LAST_MODIFIED" => "Y",
                                                            "AJAX_MODE" => "N",
                                                            "AJAX_OPTION_JUMP" => "N",
                                                            "AJAX_OPTION_STYLE" => "Y",
                                                            "AJAX_OPTION_HISTORY" => "Y",
                                                            "CACHE_TYPE" => "A",
                                                            "CACHE_TIME" => "3600000",
                                                            "CACHE_FILTER" => "Y",
                                                            "CACHE_GROUPS" => "N",


                                                            "HIDE_NOT_AVAILABLE" => "Y",
                                                            "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
                                                            "USE_COMPARE" => "Y",
                                                            "SET_TITLE" => "N",
                                                            "SET_STATUS_404" => "N",
                                                            "SHOW_404" => "N",
                                                            "MESSAGE_404" => "",
                                                            "FILE_404" => "",
                                                            "PRICE_CODE" => array(
                                                                0 => "main",
                                                            ),
                                                            "USE_PRICE_COUNT" => "N",
                                                            "SHOW_PRICE_COUNT" => "1",
                                                            "PRICE_VAT_INCLUDE" => "Y",
                                                            "PRICE_VAT_SHOW_VALUE" => "N",


                                                            "USE_PRODUCT_QUANTITY" => "Y",
                                                            "OFFERS_CART_PROPERTIES" => array(
                                                                0 => "VOLUME",
                                                                1 => "SIZES",
                                                                2 => "COLOR_REF",
                                                            ),
                                                            "DISPLAY_TOP_PAGER" => "N",
                                                            "DISPLAY_BOTTOM_PAGER" => "Y",

                                                            "PAGER_TITLE" => "N",
                                                            "PAGER_SHOW_ALWAYS" => "N",
                                                            "PAGER_TEMPLATE" => "load_more",
                                                            "PAGER_DESC_NUMBERING" => "N",
                                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "N",
                                                            "PAGER_SHOW_ALL" => "N",

                                                            "AJAX_OPTION_ADDITIONAL" => "asseccories",
                                                            "ADD_CHAIN_ITEM" => "N",
                                                            "SHOW_QUANTITY" => "Y",
                                                            "SHOW_COUNTER_LIST" => "Y",
                                                            "SHOW_QUANTITY_COUNT" => "Y",
                                                            "SHOW_DISCOUNT_PERCENT" => "Y",
                                                            "SHOW_DISCOUNT_TIME" => "N",
                                                            "SHOW_OLD_PRICE" => "Y",
                                                            "CONVERT_CURRENCY" => "Y",
                                                            "CURRENCY_ID" => "RUB",
                                                            "USE_STORE" => "N",
                                                            "MAX_AMOUNT" => "20",
                                                            "MIN_AMOUNT" => "10",
                                                            "USE_MIN_AMOUNT" => "N",
                                                            "USE_ONLY_MAX_AMOUNT" => "Y",
                                                            "DISPLAY_WISH_BUTTONS" => "N",
                                                            "LIST_DISPLAY_POPUP_IMAGE" => "Y",
                                                            "DEFAULT_COUNT" => "1",
                                                            "SHOW_MEASURE" => "Y",
                                                            "SHOW_HINTS" => "Y",
                                                            "OFFER_HIDE_NAME_PROPS" => "N",
                                                            "SHOW_SECTIONS_LIST_PREVIEW" => $arParams["SHOW_SECTIONS_LIST_PREVIEW"],
                                                            "SECTIONS_LIST_PREVIEW_PROPERTY" => "DESCRIPTION",
                                                            "SHOW_SECTION_LIST_PICTURES" => "Y",
                                                            "USE_MAIN_ELEMENT_SECTION" => "N",
                                                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                                                            "PARTIAL_PRODUCT_PROPERTIES" => "Y",
                                                            "PRODUCT_PROPERTIES" => array(),
                                                            "SALE_STIKER" => "SALE_TEXT",
                                                            "SHOW_RATING" => "Y",
                                                            "COMPATIBLE_MODE" => "Y",
                                                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                                        )
                                                    ); ?>
                                                </li>
                                            <? endif; ?>
                                            <? if ($arResult["PROPERTIES"]["ACCESSORIES_2TAB_ELEMENTS"]["VALUE"]): ?>
                                                <li class="acessories2 <?= (!($iTab++) ? ' current' : '') ?>">
                                                    <? global $OPTIMUS_SMART_FILTER_DOP;
                                                    $OPTIMUS_SMART_FILTER_DOP["ID"] = $arResult["DISPLAY_PROPERTIES"]["ACCESSORIES_2TAB_ELEMENTS"]["VALUE"];
                                                    ?>

                                                        <? $APPLICATION->IncludeComponent(
                                                            "bitrix:catalog.section",
                                                            "catalog_block_acessories",
                                                            array(
                                                                "SHOW_UNABLE_SKU_PROPS" => "Y",
                                                                "SEF_URL_TEMPLATES" => array(
                                                                    "sections" => "",
                                                                    "section" => "#SECTION_CODE_PATH#/",
                                                                    "element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
                                                                    "compare" => "compare.php?action=#ACTION_CODE#",
                                                                    "smart_filter" => "#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/",
                                                                ),
                                                                "IBLOCK_TYPE" => "catalog",
                                                                "IBLOCK_ID" => "1",
                                                                "SECTION_ID" => "",
                                                                "SECTION_CODE" => "",
                                                                "SHOW_ALL_WO_SECTION" => "Y",
                                                                "SHOW_ARTICLE_SKU" => "Y",
                                                                "SHOW_MEASURE_WITH_RATIO" => "N",
                                                                "AJAX_REQUEST" => "N",
                                                                "ELEMENT_SORT_FIELD" => "ID",
                                                                "ELEMENT_SORT_ORDER" => "ASC",

                                                                "FILTER_NAME" => "OPTIMUS_SMART_FILTER_DOP",
                                                                "INCLUDE_SUBSECTIONS" => "Y",
                                                                "PAGE_ELEMENT_COUNT" => "5",
                                                                "LINE_ELEMENT_COUNT" => "5",
                                                                "DISPLAY_TYPE" => "block",
                                                                // "TYPE_SKU" => $TEMPLATE_OPTIONS["TYPE_SKU"]["CURRENT_VALUE"],
                                                                "TYPE_SKU" => "TYPE_1",
                                                                "PROPERTY_CODE" => array(
                                                                    0 => "54",
                                                                    1 => "64",
                                                                    2 => "66",
                                                                    3 => "130",
                                                                    4 => "133",
                                                                    5 => "134",
                                                                    6 => "204",
                                                                    7 => "205",
                                                                    8 => "206",
                                                                    9 => "209",
                                                                    10 => "211",
                                                                    11 => "214",
                                                                    12 => "215",
                                                                    13 => "216",
                                                                    14 => "218",
                                                                    15 => "220",
                                                                    16 => "221",
                                                                    17 => "231",
                                                                    18 => "236",
                                                                    19 => "238",
                                                                    20 => "239",
                                                                    21 => "240",
                                                                    22 => "241",
                                                                    23 => "242",
                                                                    24 => "243",
                                                                    25 => "245",
                                                                    26 => "246",
                                                                    27 => "248",
                                                                    28 => "249",
                                                                    29 => "251",
                                                                    30 => "COLOR",
                                                                    31 => "ACCESSORIES",
                                                                    32 => "ARTICUL",
                                                                    33 => "CML2_ARTICLE",
                                                                    34 => "WEIGHT_PROP",
                                                                    35 => "VIDEO_YOUTUBE",
                                                                    36 => "PROP_MATERIAL_37",
                                                                    37 => "PROP_MATERIAL_36",
                                                                    38 => "PROP_MATERIAL_26",
                                                                    39 => "YMARKET_DESC",
                                                                    40 => "YMARKET_TIME",
                                                                    41 => "SALE_NOTES",
                                                                    42 => "CML2_ATTRIBUTES",
                                                                    43 => "52",
                                                                    44 => "56",
                                                                    45 => "59",
                                                                    46 => "60",
                                                                    47 => "61",
                                                                    48 => "62",
                                                                    49 => "63",
                                                                    50 => "122",
                                                                    51 => "123",
                                                                    52 => "202",
                                                                    53 => "203",
                                                                    54 => "207",
                                                                    55 => "208",
                                                                    56 => "210",
                                                                    57 => "212",
                                                                    58 => "217",
                                                                    59 => "227",
                                                                    60 => "228",
                                                                    61 => "229",
                                                                    62 => "230",
                                                                    63 => "234",
                                                                    64 => "235",
                                                                    65 => "237",
                                                                    66 => "BRAND",
                                                                    67 => "PROP_2033",
                                                                    68 => "COLOR_REF2",
                                                                    69 => "PROP_159",
                                                                    70 => "PROP_2052",
                                                                    71 => "PROP_2027",
                                                                    72 => "PROP_2053",
                                                                    73 => "PROP_2083",
                                                                    74 => "PROP_2049",
                                                                    75 => "PROP_2026",
                                                                    76 => "PROP_2044",
                                                                    77 => "PROP_162",
                                                                    78 => "PROP_2065",
                                                                    79 => "PROP_2054",
                                                                    80 => "PROP_2017",
                                                                    81 => "PROP_2055",
                                                                    82 => "PROP_2069",
                                                                    83 => "PROP_2062",
                                                                    84 => "PROP_2061",
                                                                    85 => "CML2_LINK",
                                                                    86 => "",
                                                                ),
                                                                "SHOW_DISCOUNT_TIME_EACH_SKU" => "N",

                                                                "OFFERS_FIELD_CODE" => array(
                                                                    0 => "NAME",
                                                                    1 => "CML2_LINK",
                                                                    2 => "DETAIL_PAGE_URL",
                                                                    3 => "",
                                                                ),
                                                                "OFFERS_PROPERTY_CODE" => array(
                                                                    0 => "ARTICLE",
                                                                    1 => "VOLUME",
                                                                    2 => "SIZES",
                                                                    3 => "COLOR_REF",
                                                                    4 => "",
                                                                ),
                                                                "OFFERS_SORT_FIELD" => "shows",
                                                                "OFFERS_SORT_ORDER" => "asc",
                                                                "OFFERS_SORT_FIELD2" => "shows",
                                                                "OFFERS_SORT_ORDER2" => "asc",
                                                                'OFFER_TREE_PROPS' => array(
                                                                    0 => "SIZES",
                                                                    1 => "COLOR_REF",
                                                                ),

                                                                "OFFERS_LIMIT" => "10",

                                                                "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                                                                "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                                                                "BASKET_URL" => "/basket/",
                                                                "ACTION_VARIABLE" => "action",
                                                                "PRODUCT_ID_VARIABLE" => "id",
                                                                "SECTION_ID_VARIABLE" => "SECTION_ID",
                                                                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                                                "PRODUCT_PROPS_VARIABLE" => "prop",

                                                                "SET_LAST_MODIFIED" => "Y",
                                                                "AJAX_MODE" => "N",
                                                                "AJAX_OPTION_JUMP" => "N",
                                                                "AJAX_OPTION_STYLE" => "N",
                                                                "AJAX_OPTION_HISTORY" => "Y",
                                                                "CACHE_TYPE" => "A",
                                                                "CACHE_TIME" => "3600000",
                                                                "CACHE_FILTER" => "Y",
                                                                "CACHE_GROUPS" => "N",


                                                                "HIDE_NOT_AVAILABLE" => "Y",
                                                                "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
                                                                "USE_COMPARE" => "Y",
                                                                "SET_TITLE" => "N",
                                                                "SET_STATUS_404" => "N",
                                                                "SHOW_404" => "N",
                                                                "MESSAGE_404" => "",
                                                                "FILE_404" => "",
                                                                "PRICE_CODE" => array(
                                                                    0 => "main",
                                                                ),
                                                                "USE_PRICE_COUNT" => "N",
                                                                "SHOW_PRICE_COUNT" => "1",
                                                                "PRICE_VAT_INCLUDE" => "Y",
                                                                "PRICE_VAT_SHOW_VALUE" => "N",


                                                                "USE_PRODUCT_QUANTITY" => "Y",
                                                                "OFFERS_CART_PROPERTIES" => array(
                                                                    0 => "VOLUME",
                                                                    1 => "SIZES",
                                                                    2 => "COLOR_REF",
                                                                ),
                                                                "DISPLAY_TOP_PAGER" => "N",
                                                                "DISPLAY_BOTTOM_PAGER" => "Y",

                                                                "PAGER_TITLE" => "N",
                                                                "PAGER_SHOW_ALWAYS" => "N",
                                                                "PAGER_TEMPLATE" => "load_more",
                                                                "PAGER_DESC_NUMBERING" => "N",
                                                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "N",
                                                                "PAGER_SHOW_ALL" => "N",

                                                                "AJAX_OPTION_ADDITIONAL" => "",
                                                                "ADD_CHAIN_ITEM" => "N",
                                                                "SHOW_QUANTITY" => "Y",
                                                                "SHOW_COUNTER_LIST" => "Y",
                                                                "SHOW_QUANTITY_COUNT" => "Y",
                                                                "SHOW_DISCOUNT_PERCENT" => "Y",
                                                                "SHOW_DISCOUNT_TIME" => "N",
                                                                "SHOW_OLD_PRICE" => "Y",
                                                                "CONVERT_CURRENCY" => "Y",
                                                                "CURRENCY_ID" => "RUB",
                                                                "USE_STORE" => "N",
                                                                "MAX_AMOUNT" => "20",
                                                                "MIN_AMOUNT" => "10",
                                                                "USE_MIN_AMOUNT" => "N",
                                                                "USE_ONLY_MAX_AMOUNT" => "Y",
                                                                "DISPLAY_WISH_BUTTONS" => "N",
                                                                "LIST_DISPLAY_POPUP_IMAGE" => "Y",
                                                                "DEFAULT_COUNT" => "1",
                                                                "SHOW_MEASURE" => "Y",
                                                                "SHOW_HINTS" => "Y",
                                                                "OFFER_HIDE_NAME_PROPS" => "N",
                                                                "SHOW_SECTIONS_LIST_PREVIEW" => $arParams["SHOW_SECTIONS_LIST_PREVIEW"],
                                                                "SECTIONS_LIST_PREVIEW_PROPERTY" => "DESCRIPTION",
                                                                "SHOW_SECTION_LIST_PICTURES" => "Y",
                                                                "USE_MAIN_ELEMENT_SECTION" => "N",
                                                                "ADD_PROPERTIES_TO_BASKET" => "Y",
                                                                "PARTIAL_PRODUCT_PROPERTIES" => "Y",
                                                                "PRODUCT_PROPERTIES" => array(),
                                                                "SALE_STIKER" => "SALE_TEXT",
                                                                "SHOW_RATING" => "Y",
                                                                "COMPATIBLE_MODE" => "Y",
                                                                "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                                            )
                                                        ); ?>
                                                </li>
                                            <? endif; ?>
                                            <? if ($arResult["PROPERTIES"]["ACCESSORIES_3TAB_ELEMENTS"]["VALUE"]): ?>
                                                <li class="acessories3 <?= (!($iTab++) ? ' current' : '') ?>">
                                                    <? global $OPTIMUS_SMART_FILTER_DOP;
                                                    $OPTIMUS_SMART_FILTER_DOP["ID"] = $arResult["DISPLAY_PROPERTIES"]["ACCESSORIES_3TAB_ELEMENTS"]["VALUE"];
                                                    ?>

                                                        <? $APPLICATION->IncludeComponent(
                                                            "bitrix:catalog.section",
                                                            "catalog_block_acessories",
                                                            array(
                                                                "SHOW_UNABLE_SKU_PROPS" => "Y",
                                                                "SEF_URL_TEMPLATES" => array(
                                                                    "sections" => "",
                                                                    "section" => "#SECTION_CODE_PATH#/",
                                                                    "element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
                                                                    "compare" => "compare.php?action=#ACTION_CODE#",
                                                                    "smart_filter" => "#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/",
                                                                ),
                                                                "IBLOCK_TYPE" => "catalog",
                                                                "IBLOCK_ID" => "1",
                                                                "SECTION_ID" => "",
                                                                "SECTION_CODE" => "",
                                                                "SHOW_ALL_WO_SECTION" => "Y",
                                                                "SHOW_ARTICLE_SKU" => "Y",
                                                                "SHOW_MEASURE_WITH_RATIO" => "N",
                                                                "AJAX_REQUEST" => "N",
                                                                "ELEMENT_SORT_FIELD" => "ID",
                                                                "ELEMENT_SORT_ORDER" => "ASC",

                                                                "FILTER_NAME" => "OPTIMUS_SMART_FILTER_DOP",
                                                                "INCLUDE_SUBSECTIONS" => "Y",
                                                                "PAGE_ELEMENT_COUNT" => "5",
                                                                "LINE_ELEMENT_COUNT" => "5",
                                                                "DISPLAY_TYPE" => "block",
                                                                // "TYPE_SKU" => $TEMPLATE_OPTIONS["TYPE_SKU"]["CURRENT_VALUE"],
                                                                "TYPE_SKU" => "TYPE_1",
                                                                "PROPERTY_CODE" => array(
                                                                    0 => "54",
                                                                    1 => "64",
                                                                    2 => "66",
                                                                    3 => "130",
                                                                    4 => "133",
                                                                    5 => "134",
                                                                    6 => "204",
                                                                    7 => "205",
                                                                    8 => "206",
                                                                    9 => "209",
                                                                    10 => "211",
                                                                    11 => "214",
                                                                    12 => "215",
                                                                    13 => "216",
                                                                    14 => "218",
                                                                    15 => "220",
                                                                    16 => "221",
                                                                    17 => "231",
                                                                    18 => "236",
                                                                    19 => "238",
                                                                    20 => "239",
                                                                    21 => "240",
                                                                    22 => "241",
                                                                    23 => "242",
                                                                    24 => "243",
                                                                    25 => "245",
                                                                    26 => "246",
                                                                    27 => "248",
                                                                    28 => "249",
                                                                    29 => "251",
                                                                    30 => "COLOR",
                                                                    31 => "ACCESSORIES",
                                                                    32 => "ARTICUL",
                                                                    33 => "CML2_ARTICLE",
                                                                    34 => "WEIGHT_PROP",
                                                                    35 => "VIDEO_YOUTUBE",
                                                                    36 => "PROP_MATERIAL_37",
                                                                    37 => "PROP_MATERIAL_36",
                                                                    38 => "PROP_MATERIAL_26",
                                                                    39 => "YMARKET_DESC",
                                                                    40 => "YMARKET_TIME",
                                                                    41 => "SALE_NOTES",
                                                                    42 => "CML2_ATTRIBUTES",
                                                                    43 => "52",
                                                                    44 => "56",
                                                                    45 => "59",
                                                                    46 => "60",
                                                                    47 => "61",
                                                                    48 => "62",
                                                                    49 => "63",
                                                                    50 => "122",
                                                                    51 => "123",
                                                                    52 => "202",
                                                                    53 => "203",
                                                                    54 => "207",
                                                                    55 => "208",
                                                                    56 => "210",
                                                                    57 => "212",
                                                                    58 => "217",
                                                                    59 => "227",
                                                                    60 => "228",
                                                                    61 => "229",
                                                                    62 => "230",
                                                                    63 => "234",
                                                                    64 => "235",
                                                                    65 => "237",
                                                                    66 => "BRAND",
                                                                    67 => "PROP_2033",
                                                                    68 => "COLOR_REF2",
                                                                    69 => "PROP_159",
                                                                    70 => "PROP_2052",
                                                                    71 => "PROP_2027",
                                                                    72 => "PROP_2053",
                                                                    73 => "PROP_2083",
                                                                    74 => "PROP_2049",
                                                                    75 => "PROP_2026",
                                                                    76 => "PROP_2044",
                                                                    77 => "PROP_162",
                                                                    78 => "PROP_2065",
                                                                    79 => "PROP_2054",
                                                                    80 => "PROP_2017",
                                                                    81 => "PROP_2055",
                                                                    82 => "PROP_2069",
                                                                    83 => "PROP_2062",
                                                                    84 => "PROP_2061",
                                                                    85 => "CML2_LINK",
                                                                    86 => "",
                                                                ),
                                                                "SHOW_DISCOUNT_TIME_EACH_SKU" => "N",

                                                                "OFFERS_FIELD_CODE" => array(
                                                                    0 => "NAME",
                                                                    1 => "CML2_LINK",
                                                                    2 => "DETAIL_PAGE_URL",
                                                                    3 => "",
                                                                ),
                                                                "OFFERS_PROPERTY_CODE" => array(
                                                                    0 => "ARTICLE",
                                                                    1 => "VOLUME",
                                                                    2 => "SIZES",
                                                                    3 => "COLOR_REF",
                                                                    4 => "",
                                                                ),
                                                                "OFFERS_SORT_FIELD" => "shows",
                                                                "OFFERS_SORT_ORDER" => "asc",
                                                                "OFFERS_SORT_FIELD2" => "shows",
                                                                "OFFERS_SORT_ORDER2" => "asc",
                                                                'OFFER_TREE_PROPS' => array(
                                                                    0 => "SIZES",
                                                                    1 => "COLOR_REF",
                                                                ),

                                                                "OFFERS_LIMIT" => "10",

                                                                "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                                                                "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                                                                "BASKET_URL" => "/basket/",
                                                                "ACTION_VARIABLE" => "action",
                                                                "PRODUCT_ID_VARIABLE" => "id",
                                                                "SECTION_ID_VARIABLE" => "SECTION_ID",
                                                                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                                                "PRODUCT_PROPS_VARIABLE" => "prop",

                                                                "SET_LAST_MODIFIED" => "Y",
                                                                "AJAX_MODE" => "N",
                                                                "AJAX_OPTION_JUMP" => "N",
                                                                "AJAX_OPTION_STYLE" => "N",
                                                                "AJAX_OPTION_HISTORY" => "Y",
                                                                "CACHE_TYPE" => "A",
                                                                "CACHE_TIME" => "3600000",
                                                                "CACHE_FILTER" => "Y",
                                                                "CACHE_GROUPS" => "N",


                                                                "HIDE_NOT_AVAILABLE" => "Y",
                                                                "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
                                                                "USE_COMPARE" => "Y",
                                                                "SET_TITLE" => "N",
                                                                "SET_STATUS_404" => "N",
                                                                "SHOW_404" => "N",
                                                                "MESSAGE_404" => "",
                                                                "FILE_404" => "",
                                                                "PRICE_CODE" => array(
                                                                    0 => "main",
                                                                ),
                                                                "USE_PRICE_COUNT" => "N",
                                                                "SHOW_PRICE_COUNT" => "1",
                                                                "PRICE_VAT_INCLUDE" => "Y",
                                                                "PRICE_VAT_SHOW_VALUE" => "N",


                                                                "USE_PRODUCT_QUANTITY" => "Y",
                                                                "OFFERS_CART_PROPERTIES" => array(
                                                                    0 => "VOLUME",
                                                                    1 => "SIZES",
                                                                    2 => "COLOR_REF",
                                                                ),
                                                                "DISPLAY_TOP_PAGER" => "N",
                                                                "DISPLAY_BOTTOM_PAGER" => "Y",

                                                                "PAGER_TITLE" => "N",
                                                                "PAGER_SHOW_ALWAYS" => "N",
                                                                "PAGER_TEMPLATE" => "load_more",
                                                                "PAGER_DESC_NUMBERING" => "N",
                                                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "N",
                                                                "PAGER_SHOW_ALL" => "N",

                                                                "AJAX_OPTION_ADDITIONAL" => "",
                                                                "ADD_CHAIN_ITEM" => "N",
                                                                "SHOW_QUANTITY" => "Y",
                                                                "SHOW_COUNTER_LIST" => "Y",
                                                                "SHOW_QUANTITY_COUNT" => "Y",
                                                                "SHOW_DISCOUNT_PERCENT" => "Y",
                                                                "SHOW_DISCOUNT_TIME" => "N",
                                                                "SHOW_OLD_PRICE" => "Y",
                                                                "CONVERT_CURRENCY" => "Y",
                                                                "CURRENCY_ID" => "RUB",
                                                                "USE_STORE" => "N",
                                                                "MAX_AMOUNT" => "20",
                                                                "MIN_AMOUNT" => "10",
                                                                "USE_MIN_AMOUNT" => "N",
                                                                "USE_ONLY_MAX_AMOUNT" => "Y",
                                                                "DISPLAY_WISH_BUTTONS" => "N",
                                                                "LIST_DISPLAY_POPUP_IMAGE" => "Y",
                                                                "DEFAULT_COUNT" => "1",
                                                                "SHOW_MEASURE" => "Y",
                                                                "SHOW_HINTS" => "Y",
                                                                "OFFER_HIDE_NAME_PROPS" => "N",
                                                                "SHOW_SECTIONS_LIST_PREVIEW" => $arParams["SHOW_SECTIONS_LIST_PREVIEW"],
                                                                "SECTIONS_LIST_PREVIEW_PROPERTY" => "DESCRIPTION",
                                                                "SHOW_SECTION_LIST_PICTURES" => "Y",
                                                                "USE_MAIN_ELEMENT_SECTION" => "N",
                                                                "ADD_PROPERTIES_TO_BASKET" => "Y",
                                                                "PARTIAL_PRODUCT_PROPERTIES" => "Y",
                                                                "PRODUCT_PROPERTIES" => array(),
                                                                "SALE_STIKER" => "SALE_TEXT",
                                                                "SHOW_RATING" => "Y",
                                                                "COMPATIBLE_MODE" => "Y",
                                                                "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                                            )
                                                        ); ?>
                                                </li>
                                            <? endif; ?>
                                            <? if ($arResult["PROPERTIES"]["ACCESSORIES_4TAB_ELEMENTS"]["VALUE"]): ?>
                                                <li class="acessories4 <?= (!($iTab++) ? ' current' : '') ?>">
                                                    <? global $OPTIMUS_SMART_FILTER_DOP;
                                                    $OPTIMUS_SMART_FILTER_DOP["ID"] = $arResult["DISPLAY_PROPERTIES"]["ACCESSORIES_4TAB_ELEMENTS"]["VALUE"];
                                                    ?>

                                                        <? $APPLICATION->IncludeComponent(
                                                            "bitrix:catalog.section",
                                                            "catalog_block_acessories",
                                                            array(
                                                                "SHOW_UNABLE_SKU_PROPS" => "Y",
                                                                "SEF_URL_TEMPLATES" => array(
                                                                    "sections" => "",
                                                                    "section" => "#SECTION_CODE_PATH#/",
                                                                    "element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
                                                                    "compare" => "compare.php?action=#ACTION_CODE#",
                                                                    "smart_filter" => "#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/",
                                                                ),
                                                                "IBLOCK_TYPE" => "catalog",
                                                                "IBLOCK_ID" => "1",
                                                                "SECTION_ID" => "",
                                                                "SECTION_CODE" => "",
                                                                "SHOW_ALL_WO_SECTION" => "Y",
                                                                "SHOW_ARTICLE_SKU" => "Y",
                                                                "SHOW_MEASURE_WITH_RATIO" => "N",
                                                                "AJAX_REQUEST" => "N",
                                                                "ELEMENT_SORT_FIELD" => "ID",
                                                                "ELEMENT_SORT_ORDER" => "ASC",

                                                                "FILTER_NAME" => "OPTIMUS_SMART_FILTER_DOP",
                                                                "INCLUDE_SUBSECTIONS" => "Y",
                                                                "PAGE_ELEMENT_COUNT" => "5",
                                                                "LINE_ELEMENT_COUNT" => "5",
                                                                "DISPLAY_TYPE" => "block",
                                                                // "TYPE_SKU" => $TEMPLATE_OPTIONS["TYPE_SKU"]["CURRENT_VALUE"],
                                                                "TYPE_SKU" => "TYPE_1",
                                                                "PROPERTY_CODE" => array(
                                                                    0 => "54",
                                                                    1 => "64",
                                                                    2 => "66",
                                                                    3 => "130",
                                                                    4 => "133",
                                                                    5 => "134",
                                                                    6 => "204",
                                                                    7 => "205",
                                                                    8 => "206",
                                                                    9 => "209",
                                                                    10 => "211",
                                                                    11 => "214",
                                                                    12 => "215",
                                                                    13 => "216",
                                                                    14 => "218",
                                                                    15 => "220",
                                                                    16 => "221",
                                                                    17 => "231",
                                                                    18 => "236",
                                                                    19 => "238",
                                                                    20 => "239",
                                                                    21 => "240",
                                                                    22 => "241",
                                                                    23 => "242",
                                                                    24 => "243",
                                                                    25 => "245",
                                                                    26 => "246",
                                                                    27 => "248",
                                                                    28 => "249",
                                                                    29 => "251",
                                                                    30 => "COLOR",
                                                                    31 => "ACCESSORIES",
                                                                    32 => "ARTICUL",
                                                                    33 => "CML2_ARTICLE",
                                                                    34 => "WEIGHT_PROP",
                                                                    35 => "VIDEO_YOUTUBE",
                                                                    36 => "PROP_MATERIAL_37",
                                                                    37 => "PROP_MATERIAL_36",
                                                                    38 => "PROP_MATERIAL_26",
                                                                    39 => "YMARKET_DESC",
                                                                    40 => "YMARKET_TIME",
                                                                    41 => "SALE_NOTES",
                                                                    42 => "CML2_ATTRIBUTES",
                                                                    43 => "52",
                                                                    44 => "56",
                                                                    45 => "59",
                                                                    46 => "60",
                                                                    47 => "61",
                                                                    48 => "62",
                                                                    49 => "63",
                                                                    50 => "122",
                                                                    51 => "123",
                                                                    52 => "202",
                                                                    53 => "203",
                                                                    54 => "207",
                                                                    55 => "208",
                                                                    56 => "210",
                                                                    57 => "212",
                                                                    58 => "217",
                                                                    59 => "227",
                                                                    60 => "228",
                                                                    61 => "229",
                                                                    62 => "230",
                                                                    63 => "234",
                                                                    64 => "235",
                                                                    65 => "237",
                                                                    66 => "BRAND",
                                                                    67 => "PROP_2033",
                                                                    68 => "COLOR_REF2",
                                                                    69 => "PROP_159",
                                                                    70 => "PROP_2052",
                                                                    71 => "PROP_2027",
                                                                    72 => "PROP_2053",
                                                                    73 => "PROP_2083",
                                                                    74 => "PROP_2049",
                                                                    75 => "PROP_2026",
                                                                    76 => "PROP_2044",
                                                                    77 => "PROP_162",
                                                                    78 => "PROP_2065",
                                                                    79 => "PROP_2054",
                                                                    80 => "PROP_2017",
                                                                    81 => "PROP_2055",
                                                                    82 => "PROP_2069",
                                                                    83 => "PROP_2062",
                                                                    84 => "PROP_2061",
                                                                    85 => "CML2_LINK",
                                                                    86 => "",
                                                                ),
                                                                "SHOW_DISCOUNT_TIME_EACH_SKU" => "N",

                                                                "OFFERS_FIELD_CODE" => array(
                                                                    0 => "NAME",
                                                                    1 => "CML2_LINK",
                                                                    2 => "DETAIL_PAGE_URL",
                                                                    3 => "",
                                                                ),
                                                                "OFFERS_PROPERTY_CODE" => array(
                                                                    0 => "ARTICLE",
                                                                    1 => "VOLUME",
                                                                    2 => "SIZES",
                                                                    3 => "COLOR_REF",
                                                                    4 => "",
                                                                ),
                                                                "OFFERS_SORT_FIELD" => "shows",
                                                                "OFFERS_SORT_ORDER" => "asc",
                                                                "OFFERS_SORT_FIELD2" => "shows",
                                                                "OFFERS_SORT_ORDER2" => "asc",
                                                                'OFFER_TREE_PROPS' => array(
                                                                    0 => "SIZES",
                                                                    1 => "COLOR_REF",
                                                                ),

                                                                "OFFERS_LIMIT" => "10",

                                                                "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                                                                "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                                                                "BASKET_URL" => "/basket/",
                                                                "ACTION_VARIABLE" => "action",
                                                                "PRODUCT_ID_VARIABLE" => "id",
                                                                "SECTION_ID_VARIABLE" => "SECTION_ID",
                                                                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                                                "PRODUCT_PROPS_VARIABLE" => "prop",

                                                                "SET_LAST_MODIFIED" => "Y",
                                                                "AJAX_MODE" => "N",
                                                                "AJAX_OPTION_JUMP" => "N",
                                                                "AJAX_OPTION_STYLE" => "N",
                                                                "AJAX_OPTION_HISTORY" => "Y",
                                                                "CACHE_TYPE" => "A",
                                                                "CACHE_TIME" => "3600000",
                                                                "CACHE_FILTER" => "Y",
                                                                "CACHE_GROUPS" => "N",


                                                                "HIDE_NOT_AVAILABLE" => "Y",
                                                                "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
                                                                "USE_COMPARE" => "Y",
                                                                "SET_TITLE" => "N",
                                                                "SET_STATUS_404" => "N",
                                                                "SHOW_404" => "N",
                                                                "MESSAGE_404" => "",
                                                                "FILE_404" => "",
                                                                "PRICE_CODE" => array(
                                                                    0 => "main",
                                                                ),
                                                                "USE_PRICE_COUNT" => "N",
                                                                "SHOW_PRICE_COUNT" => "1",
                                                                "PRICE_VAT_INCLUDE" => "Y",
                                                                "PRICE_VAT_SHOW_VALUE" => "N",


                                                                "USE_PRODUCT_QUANTITY" => "Y",
                                                                "OFFERS_CART_PROPERTIES" => array(
                                                                    0 => "VOLUME",
                                                                    1 => "SIZES",
                                                                    2 => "COLOR_REF",
                                                                ),
                                                                "DISPLAY_TOP_PAGER" => "N",
                                                                "DISPLAY_BOTTOM_PAGER" => "Y",

                                                                "PAGER_TITLE" => "N",
                                                                "PAGER_SHOW_ALWAYS" => "N",
                                                                "PAGER_TEMPLATE" => "load_more",
                                                                "PAGER_DESC_NUMBERING" => "N",
                                                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "N",
                                                                "PAGER_SHOW_ALL" => "N",

                                                                "AJAX_OPTION_ADDITIONAL" => "",
                                                                "ADD_CHAIN_ITEM" => "N",
                                                                "SHOW_QUANTITY" => "Y",
                                                                "SHOW_COUNTER_LIST" => "Y",
                                                                "SHOW_QUANTITY_COUNT" => "Y",
                                                                "SHOW_DISCOUNT_PERCENT" => "Y",
                                                                "SHOW_DISCOUNT_TIME" => "N",
                                                                "SHOW_OLD_PRICE" => "Y",
                                                                "CONVERT_CURRENCY" => "Y",
                                                                "CURRENCY_ID" => "RUB",
                                                                "USE_STORE" => "N",
                                                                "MAX_AMOUNT" => "20",
                                                                "MIN_AMOUNT" => "10",
                                                                "USE_MIN_AMOUNT" => "N",
                                                                "USE_ONLY_MAX_AMOUNT" => "Y",
                                                                "DISPLAY_WISH_BUTTONS" => "N",
                                                                "LIST_DISPLAY_POPUP_IMAGE" => "Y",
                                                                "DEFAULT_COUNT" => "1",
                                                                "SHOW_MEASURE" => "Y",
                                                                "SHOW_HINTS" => "Y",
                                                                "OFFER_HIDE_NAME_PROPS" => "N",
                                                                "SHOW_SECTIONS_LIST_PREVIEW" => $arParams["SHOW_SECTIONS_LIST_PREVIEW"],
                                                                "SECTIONS_LIST_PREVIEW_PROPERTY" => "DESCRIPTION",
                                                                "SHOW_SECTION_LIST_PICTURES" => "Y",
                                                                "USE_MAIN_ELEMENT_SECTION" => "N",
                                                                "ADD_PROPERTIES_TO_BASKET" => "Y",
                                                                "PARTIAL_PRODUCT_PROPERTIES" => "Y",
                                                                "PRODUCT_PROPERTIES" => array(),
                                                                "SALE_STIKER" => "SALE_TEXT",
                                                                "SHOW_RATING" => "Y",
                                                                "COMPATIBLE_MODE" => "Y",
                                                                "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                                            )
                                                        ); ?>
                                                </li>
                                            <? endif; ?>

                                        </ul>
                                    </div>
        <?}?>
                                </section>
<?}?>


<?if($arResult['PROPERTIES']['SIMILAR_PRODUCTS']['VALUE']):?>
    <section class="similar_products_section">
        <div class="top_blocks">
            <ul class="tabs">
                <li data-code="advantages" class='title-category-card'><span><?=GetMessage("ASSOCIATED_TITLE")?></span></li>
                <li class="stretch"></li>
            </ul>
            <p class="specials-text">Вы можете сравнить товары между собой поставив галочки к выбранным товарам и нажав кнопку «Сравнить»</p>
        </div>

        <div class="similar_products">
            <? global $SIMILAR_PRODUCTS;
            $SIMILAR_PRODUCTS["ID"] = $arResult["PROPERTIES"]["SIMILAR_PRODUCTS"]["VALUE"];
            ?>

            <? $APPLICATION->IncludeComponent(
                "bitrix:catalog.section",
                "catalog_block_acessories",
                array(
                    "SHOW_UNABLE_SKU_PROPS" => "Y",
                    "SEF_URL_TEMPLATES" => array(
                        "sections" => "",
                        "section" => "#SECTION_CODE_PATH#/",
                        "element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
                        "compare" => "compare.php?action=#ACTION_CODE#",
                        "smart_filter" => "#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/",
                    ),
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => "1",
                    "SECTION_ID" => "",
                    "SECTION_CODE" => "",
                    "SHOW_ALL_WO_SECTION" => "Y",
                    "SHOW_ARTICLE_SKU" => "Y",
                    "SHOW_MEASURE_WITH_RATIO" => "N",
                    "AJAX_REQUEST" => "N",
                    "ELEMENT_SORT_FIELD" => "ID",
                    "ELEMENT_SORT_ORDER" => "ASC",

                    "FILTER_NAME" => "SIMILAR_PRODUCTS",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "PAGE_ELEMENT_COUNT" => "5",
                    "LINE_ELEMENT_COUNT" => "5",
                    "DISPLAY_TYPE" => "block",
                    // "TYPE_SKU" => $TEMPLATE_OPTIONS["TYPE_SKU"]["CURRENT_VALUE"],
                    "TYPE_SKU" => "TYPE_1",
                    "PROPERTY_CODE" => array(
                        0 => "54",
                        1 => "64",
                        2 => "66",
                        3 => "130",
                        4 => "133",
                        5 => "134",
                        6 => "204",
                        7 => "205",
                        8 => "206",
                        9 => "209",
                        10 => "211",
                        11 => "214",
                        12 => "215",
                        13 => "216",
                        14 => "218",
                        15 => "220",
                        16 => "221",
                        17 => "231",
                        18 => "236",
                        19 => "238",
                        20 => "239",
                        21 => "240",
                        22 => "241",
                        23 => "242",
                        24 => "243",
                        25 => "245",
                        26 => "246",
                        27 => "248",
                        28 => "249",
                        29 => "251",
                        30 => "COLOR",
                        31 => "ACCESSORIES",
                        32 => "ARTICUL",
                        33 => "CML2_ARTICLE",
                        34 => "WEIGHT_PROP",
                        35 => "VIDEO_YOUTUBE",
                        36 => "PROP_MATERIAL_37",
                        37 => "PROP_MATERIAL_36",
                        38 => "PROP_MATERIAL_26",
                        39 => "YMARKET_DESC",
                        40 => "YMARKET_TIME",
                        41 => "SALE_NOTES",
                        42 => "CML2_ATTRIBUTES",
                        43 => "52",
                        44 => "56",
                        45 => "59",
                        46 => "60",
                        47 => "61",
                        48 => "62",
                        49 => "63",
                        50 => "122",
                        51 => "123",
                        52 => "202",
                        53 => "203",
                        54 => "207",
                        55 => "208",
                        56 => "210",
                        57 => "212",
                        58 => "217",
                        59 => "227",
                        60 => "228",
                        61 => "229",
                        62 => "230",
                        63 => "234",
                        64 => "235",
                        65 => "237",
                        66 => "BRAND",
                        67 => "PROP_2033",
                        68 => "COLOR_REF2",
                        69 => "PROP_159",
                        70 => "PROP_2052",
                        71 => "PROP_2027",
                        72 => "PROP_2053",
                        73 => "PROP_2083",
                        74 => "PROP_2049",
                        75 => "PROP_2026",
                        76 => "PROP_2044",
                        77 => "PROP_162",
                        78 => "PROP_2065",
                        79 => "PROP_2054",
                        80 => "PROP_2017",
                        81 => "PROP_2055",
                        82 => "PROP_2069",
                        83 => "PROP_2062",
                        84 => "PROP_2061",
                        85 => "CML2_LINK",
                        86 => "",
                    ),
                    "SHOW_DISCOUNT_TIME_EACH_SKU" => "N",

                    "OFFERS_FIELD_CODE" => array(
                        0 => "NAME",
                        1 => "CML2_LINK",
                        2 => "DETAIL_PAGE_URL",
                        3 => "",
                    ),
                    "OFFERS_PROPERTY_CODE" => array(
                        0 => "ARTICLE",
                        1 => "VOLUME",
                        2 => "SIZES",
                        3 => "COLOR_REF",
                        4 => "",
                    ),
                    "OFFERS_SORT_FIELD" => "shows",
                    "OFFERS_SORT_ORDER" => "asc",
                    "OFFERS_SORT_FIELD2" => "shows",
                    "OFFERS_SORT_ORDER2" => "asc",
                    'OFFER_TREE_PROPS' => array(
                        0 => "SIZES",
                        1 => "COLOR_REF",
                    ),

                    "OFFERS_LIMIT" => "10",

                    "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                    "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                    "BASKET_URL" => "/basket/",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "PRODUCT_PROPS_VARIABLE" => "prop",

                    "SET_LAST_MODIFIED" => "Y",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "AJAX_OPTION_HISTORY" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600000",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",


                    "HIDE_NOT_AVAILABLE" => "Y",
                    "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
                    "USE_COMPARE" => "Y",
                    "DISPLAY_COMPARE" => "Y",
                    "COMPARE_PATH" => "compare.php?action=#ACTION_CODE#",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "SHOW_404" => "N",
                    "MESSAGE_404" => "",
                    "FILE_404" => "",
                    "PRICE_CODE" => array(
                        0 => "main",
                    ),
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRICE_VAT_SHOW_VALUE" => "N",


                    "USE_PRODUCT_QUANTITY" => "Y",
                    "OFFERS_CART_PROPERTIES" => array(
                        0 => "VOLUME",
                        1 => "SIZES",
                        2 => "COLOR_REF",
                    ),
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",

                    "PAGER_TITLE" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "load_more",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "N",
                    "PAGER_SHOW_ALL" => "N",

                    "AJAX_OPTION_ADDITIONAL" => "",
                    "ADD_CHAIN_ITEM" => "N",
                    "SHOW_QUANTITY" => "Y",
                    "SHOW_COUNTER_LIST" => "Y",
                    "SHOW_QUANTITY_COUNT" => "Y",
                    "SHOW_DISCOUNT_PERCENT" => "Y",
                    "SHOW_DISCOUNT_TIME" => "N",
                    "SHOW_OLD_PRICE" => "Y",
                    "CONVERT_CURRENCY" => "Y",
                    "CURRENCY_ID" => "RUB",
                    "USE_STORE" => "N",
                    "MAX_AMOUNT" => "20",
                    "MIN_AMOUNT" => "10",
                    "USE_MIN_AMOUNT" => "N",
                    "USE_ONLY_MAX_AMOUNT" => "Y",
                    "DISPLAY_WISH_BUTTONS" => "N",
                    "LIST_DISPLAY_POPUP_IMAGE" => "Y",
                    "DEFAULT_COUNT" => "1",
                    "SHOW_MEASURE" => "Y",
                    "SHOW_HINTS" => "Y",
                    "OFFER_HIDE_NAME_PROPS" => "N",
                    "SHOW_SECTIONS_LIST_PREVIEW" => $arParams["SHOW_SECTIONS_LIST_PREVIEW"],
                    "SECTIONS_LIST_PREVIEW_PROPERTY" => "DESCRIPTION",
                    "SHOW_SECTION_LIST_PICTURES" => "Y",
                    "USE_MAIN_ELEMENT_SECTION" => "N",
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "PARTIAL_PRODUCT_PROPERTIES" => "Y",
                    "PRODUCT_PROPERTIES" => array(),
                    "SALE_STIKER" => "SALE_TEXT",
                    "SHOW_RATING" => "Y",
                    "COMPATIBLE_MODE" => "Y",
                    "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                )
            ); ?>
        </div>

    </section>
<?endif;?>

<?if($arResult["DISPLAY_PROPERTIES"]["ADVANTAGES"]["VALUE"]){?>
<section class="main-advantages-section">
    <div class="top_blocks">
        <ul class="tabs">
            <li data-code="advantages" class='title-category-card'><span>Основные преимущества</span></li>
            <li class="stretch"></li>
        </ul>
    </div>
    <div class="main-advantages-items">
        <?$advantage = [];?>
        <?foreach ($arResult["DISPLAY_PROPERTIES"]["ADVANTAGES"]["VALUE"] as $item):
            $advantage[] = $item;?>
        <?endforeach;?>
        <?$i=0?>
        <?foreach ($arResult["PROPERTIES"]['ADVANTAGE_PICTURES']['VALUE'] as $pid => $arProperty):
            $arFile = CFile::GetFileArray($arProperty);?>
            <div class="advantage-item">
                <img src="<?=$arFile["SRC"]?>" alt="<?=$advantage[$i]?>"/>
                <span><?=$advantage[$i]?></span>
            </div>
            <?$i++;?>
        <?endforeach;?>
    </div>
</section>
<?}?>


<section class="product-description-section">
    <div class="product-description-tabs">
        <div class="product-description-twotabs">
            <?if(strlen($arResult["DETAIL_TEXT"])):?>
            <div id="description" class="description-tab description">
                <div class="top_blocks">
                    <ul class="tabs">
                        <li data-code="description" class='title-category-card'><span><?=GetMessage("DESCRIPTION_TAB")?></span></li>
                        <li class="stretch"></li>
                    </ul>
                </div>
                <div class="detail_text"><?=$arResult["DETAIL_TEXT"]?></div>
            </div>
            <?endif;?>
            <div id="characteristics" class="description-tab characteristics">
                <div class="top_blocks">
                    <ul class="tabs">
                        <li data-code="advantages" class='title-category-card'><span><?=GetMessage("PROPERTIES_TAB")?></span></li>
                        <li class="stretch"></li>
                    </ul>
                </div>
                <table class="props_list">
                    <?
                    $mainProps = ["PROP_MATERIAL_2", "SECTION_QT", "ATT_GUARANTY", "COLOR", "ATT_PLOCHAD_OBS", "ARTICUL", "130", "133", "134", "NN_SIZES", "NN_PREDNAZ"];
                    $dimensionsProps = ["PROP_WIDTH", "PROP_HEIGHT", "PROP_DEPTH", "ATT_MATERIAL", "WEIGHT_PROP"];
                    $thermalProps = ["248", "249", "251", "PROP_MATERIAL_4", "PROP_MATERIAL_5", "PROP_MATERIAL_6", "PROP_MATERIAL_14", "PROP_MATERIAL_15", "PROP_MATERIAL_16"];
                    $notProps = ["ACCESSORIES", "IS_FASTENERS", "GARANTY_PICTURE", "CONNECTION_TYPE_NAME", "CONNECTION_TYPE", "COLOR_PALETTE", "ACCESSORIES_QUESTION", "ACCESSORIES_TEXT", "ACCESSORIES_PICTURE", "ACCESSORIES_1TAB_NAME", "ACCESSORIES_2TAB_NAME", "ACCESSORIES_3TAB_NAME", "ACCESSORIES_4TAB_NAME", "ACCESSORIES_1TAB_ELEMENTS", "ACCESSORIES_2TAB_ELEMENTS", "ACCESSORIES_3TAB_ELEMENTS", "ACCESSORIES_4TAB_ELEMENTS", "ADVANTAGES", "ADVANTAGE_PICTURES", "ADD_ADVANTAGES", "ADD_ADVANTAGE_TEXTS", "ADD_ADVANTAGE_1_PICTURES", "ADD_ADVANTAGE_2_PICTURES"];
                    ?>
                    <tr>
                        <th colspan="2">Основная информация</th>
                    </tr>
                    <?foreach($arResult["DISPLAY_PROPERTIES"] as $arProp):?>
                            <?if(in_array($arProp["CODE"], $mainProps)):?>
                                <tr itemprop="additionalProperty" itemscope itemtype="https://schema.org/PropertyValue">
                                    <td class="char_name">
                                        <span <?if($arProp["HINT"] && $arParams["SHOW_HINTS"] == "Y"){?>class="whint"<?}?>><?if($arProp["HINT"] && $arParams["SHOW_HINTS"] == "Y"):?><div class="hint"><span class="icon"><i>?</i></span><div class="tooltip"><?=$arProp["HINT"]?></div></div><?endif;?><span itemprop="name"><?=$arProp["NAME"]?></span></span>
                                    </td>
                                    <td class="char_value">
                                <span itemprop="value">
                                    <?if(count($arProp["DISPLAY_VALUE"]) > 1):?>
                                        <?=implode(', ', $arProp["DISPLAY_VALUE"]);?>
                                    <?else:?>
                                        <?=$arProp["DISPLAY_VALUE"];?>
                                    <?endif;?>
                                </span>
                                    </td>
                                </tr>
                                <?endif;?>
                    <?endforeach;?>

                    <tr>
                        <th colspan="2">Габариты</th>
                    </tr>
                    <?foreach($arResult["DISPLAY_PROPERTIES"] as $arProp):?>
                        <?if(in_array($arProp["CODE"], $dimensionsProps)):?>
                            <tr itemprop="additionalProperty" itemscope itemtype="https://schema.org/PropertyValue">
                                <td class="char_name">
                                    <span <?if($arProp["HINT"] && $arParams["SHOW_HINTS"] == "Y"){?>class="whint"<?}?>><?if($arProp["HINT"] && $arParams["SHOW_HINTS"] == "Y"):?><div class="hint"><span class="icon"><i>?</i></span><div class="tooltip"><?=$arProp["HINT"]?></div></div><?endif;?><span itemprop="name"><?=$arProp["NAME"]?></span></span>
                                </td>
                                <td class="char_value">
                                <span itemprop="value">
                                    <?if(count($arProp["DISPLAY_VALUE"]) > 1):?>
                                        <?=implode(', ', $arProp["DISPLAY_VALUE"]);?>
                                    <?else:?>
                                        <?=$arProp["DISPLAY_VALUE"];?>
                                    <?endif;?>
                                </span>
                                </td>
                            </tr>
                        <?endif;?>
                    <?endforeach;?>

                    <tr>
                        <th colspan="2">Тепловые характеристики</th>
                    </tr>
                    <?foreach($arResult["DISPLAY_PROPERTIES"] as $arProp):?>
                        <?if(in_array($arProp["CODE"], $thermalProps)):?>
                            <tr itemprop="additionalProperty" itemscope itemtype="https://schema.org/PropertyValue">
                                <td class="char_name">
                                    <span <?if($arProp["HINT"] && $arParams["SHOW_HINTS"] == "Y"){?>class="whint"<?}?>><?if($arProp["HINT"] && $arParams["SHOW_HINTS"] == "Y"):?><div class="hint"><span class="icon"><i>?</i></span><div class="tooltip"><?=$arProp["HINT"]?></div></div><?endif;?><span itemprop="name"><?=$arProp["NAME"]?></span></span>
                                </td>
                                <td class="char_value">
                                <span itemprop="value">
                                    <?if(count($arProp["DISPLAY_VALUE"]) > 1):?>
                                        <?=implode(', ', $arProp["DISPLAY_VALUE"]);?>
                                    <?else:?>
                                        <?=$arProp["DISPLAY_VALUE"];?>
                                    <?endif;?>
                                </span>
                                </td>
                            </tr>
                        <?endif;?>
                    <?endforeach;?>

                    <tr>
                        <th colspan="2">Другие характеристики</th>
                    </tr>
                    <?foreach($arResult["DISPLAY_PROPERTIES"] as $arProp):?>
                        <?if((!in_array($arProp["CODE"], $mainProps)) &&
                            (!in_array($arProp["CODE"], $dimensionsProps)) &&
                            (!in_array($arProp["CODE"], $thermalProps)) &&
                            (!in_array($arProp["CODE"], $notProps))):?>
                            <tr itemprop="additionalProperty" itemscope itemtype="https://schema.org/PropertyValue">
                                <td class="char_name">
                                    <span <?if($arProp["HINT"] && $arParams["SHOW_HINTS"] == "Y"){?>class="whint"<?}?>><?if($arProp["HINT"] && $arParams["SHOW_HINTS"] == "Y"):?><div class="hint"><span class="icon"><i>?</i></span><div class="tooltip"><?=$arProp["HINT"]?></div></div><?endif;?><span itemprop="name"><?=$arProp["NAME"]?></span></span>
                                </td>
                                <td class="char_value">
                                <span itemprop="value">
                                    <?if(count($arProp["DISPLAY_VALUE"]) > 1):?>
                                        <?=implode(', ', $arProp["DISPLAY_VALUE"]);?>
                                    <?else:?>
                                        <?=$arProp["DISPLAY_VALUE"];?>
                                    <?endif;?>
                                </span>
                                </td>
                            </tr>
                        <?endif;?>
                    <?endforeach;?>
                </table>
            </div>
        </div>
        <?if ($arResult["DISPLAY_PROPERTIES"]["ADD_ADVANTAGES"]["VALUE"]):?>
        <div class="product-description-twotabs">
            <?
            $add_advantages_text = [];
            $arPicture1 = [];
            $arPicture2 = [];
            ?>
            <?foreach($arResult["DISPLAY_PROPERTIES"]["ADD_ADVANTAGE_TEXTS"]["VALUE"] as $text){
                $add_advantages_text[] = $text["TEXT"];
            }?>

            <?foreach ($arResult["PROPERTIES"]['ADD_ADVANTAGE_1_PICTURES']['VALUE'] as $pid => $arProperty) {
                $arPicture1[] = CFile::GetFileArray($arProperty);
            } ?>

            <?foreach ($arResult["PROPERTIES"]['ADD_ADVANTAGE_2_PICTURES']['VALUE'] as $pid => $arProperty) {
                $arPicture2[] = CFile::GetFileArray($arProperty);
            } ?>

            <?$i = 0?>
            <?foreach ($arResult["DISPLAY_PROPERTIES"]["ADD_ADVANTAGES"]["VALUE"] as $add_advantages):?>
            <div class="description-tab temperature">
                <div class="top_blocks">
                    <ul class="tabs">
                        <li data-code="advantages" class='title-category-card'><span><?=$add_advantages?></span></li>
                        <li class="stretch"></li>
                    </ul>
                </div>
                <p><?=$add_advantages_text[$i]?></p>
                <?if($i == 0) {?>
                    <div class="temperature-images">
                        <img src="<?= $arPicture1[0]["SRC"] ?>" alt="<?= $add_advantages ?>">
                        <img src="<?= $arPicture1[1]["SRC"] ?>" alt="<?= $add_advantages ?>">
                    </div>
                <?} elseif($i == 1) {?>
                    <div class="temperature-images">
                        <img src="<?= $arPicture2[0]["SRC"] ?>" alt="<?= $add_advantages ?>">
                        <img src="<?= $arPicture2[1]["SRC"] ?>" alt="<?= $add_advantages ?>">
                    </div>
                <?}?>
            </div>
            <?$i++?>
                <?endforeach;?>
        </div>
        <?endif;?>
        <?if($arResult['PROPERTIES']['ATT_PDF_PRODUCT']['VALUE']):?>
        <div id="documents" class="description-tab documents">
            <div class="top_blocks">
                <ul class="tabs">
                    <li data-code="advantages" class='title-category-card'><span><?=GetMessage("DOCUMENTS_TITLE")?></span></li>
                    <li class="stretch"></li>
                </ul>
            </div>

                <div class="obertkapdf">
                    <?
                    $arSelect = Array("ID", "IBLOCK_ID", "NAME","IBLOCK_SECTION_ID", "DATE_ACTIVE_FROM","PROPERTY_*");
                    $arFilter = Array("IBLOCK_ID"=>33, "ID" => $arResult['PROPERTIES']['ATT_PDF_PRODUCT']['VALUE'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
                    $res = CIBlockElement::GetList(Array("SORT" => "DESC"), $arFilter, false, Array("nPageSize"=>50), $arSelect);
                    while($ob = $res->GetNextElement()){
                        $arFields = $ob->GetFields();
                        $arProps = $ob->GetProperties();

                        $PathImg = CFile::GetPath($arProps['ATT_FILE_PDF']['VALUE']); //получаем путь до картинки
                        ?>

                        <div class="files_block">

                            <div class="wrap_md">
                                <div class="wrapp_docs iblock">
                                    <div class="file_type clearfix pdf">
                                        <i class="icon"></i>
                                        <div class="description">
                                            <a target="_blank" href="<?=$PathImg?>"><?=$arFields["NAME"];?></a>
                                            <span class="size"><?//=GetMessage('CT_NAME_SIZE')?>
                                                <?//=$arFile["FILE_SIZE_FORMAT"];?>
                                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    <?}?>
                </div>

        </div>
    </div>
    <?endif;?>
</section>


<?if($arParams["USE_REVIEW"] == "Y"):?>
    <section id="reviews" class="reviews-section">
        <div class="top_blocks">
            <ul class="tabs">
                <li data-code="advantages" class='title-category-card'><span><?= GetMessage("REVIEW_TAB") ?></span></li>
                <li class="stretch"></li>
            </ul>
        </div>
        <div class="reviews_content_custom">
            <? Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("area"); ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:forum.topic.reviews",
                "main",
                array(
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
                    "USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
                    "FORUM_ID" => $arParams["FORUM_ID"],
                    "ELEMENT_ID" => $arResult["ID"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "AJAX_POST" => $arParams["REVIEW_AJAX_POST"],
                    "SHOW_RATING" => "N",
                    "SHOW_MINIMIZED" => "Y",
                    "SECTION_REVIEW" => "Y",
                    "POST_FIRST_MESSAGE" => "Y",
                    "MINIMIZED_MINIMIZE_TEXT" => GetMessage("HIDE_FORM"),
                    "MINIMIZED_EXPAND_TEXT" => GetMessage("ADD_REVIEW"),
                    "SHOW_AVATAR" => "N",
                    "SHOW_LINK_TO_FORUM" => "N",
                    "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
                ), false
            ); ?>
            <? Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("area", ""); ?>
        </div>
    </section>
<?endif;?>

<script type="text/javascript">
    BX.message({
        QUANTITY_AVAILIABLE: '<? echo COption::GetOptionString("aspro.optimus", "EXPRESSION_FOR_EXISTS", GetMessage("EXPRESSION_FOR_EXISTS_DEFAULT"), SITE_ID); ?>',
        QUANTITY_NOT_AVAILIABLE: '<? echo COption::GetOptionString("aspro.optimus", "EXPRESSION_FOR_NOTEXISTS", GetMessage("EXPRESSION_FOR_NOTEXISTS"), SITE_ID); ?>',
        ADD_ERROR_BASKET: '<? echo GetMessage("ADD_ERROR_BASKET"); ?>',
        ADD_ERROR_COMPARE: '<? echo GetMessage("ADD_ERROR_COMPARE"); ?>',
        ONE_CLICK_BUY: '<? echo GetMessage("ONE_CLICK_BUY"); ?>',
        SITE_ID: '<? echo SITE_ID; ?>'
    })
</script>

<script>
    $(document).ready(function() {
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            loop: false,
            margin: 20,
            nav:true,
            navRewind: true,
            autoplay: false,
            dots: false,
            responsive: {
                300: {
                    items: 2
                },
                1000: {
                    items: 5
                }
            }
        })
    })
</script>

<script type="text/javascript">
    $( ".print" ).click(function(){
        $('#print_img').css({ 'display': 'block'});
        setTimeout(function() {
            $( ".print2" ).click();
        }, 200);
        setTimeout(function() {
            $('#print_img').css({ 'display': 'none'});
        }, 700);
    });


    $( ".print2" ).click(function(){
        window.onload = function () {
            window.print();
        }

    });


</script>


<script>
    $('#product_reviews_tab, .product_ask_tab, .deliveryPay').click(function() {
        $('.props_list').css({ 'display': 'none'});
    });

    $('.descriptTab, .propTab').click(function() {
        $('.props_list').css({ 'display': 'block'});
    });
</script>
<style>
    @media all and (max-width: 600px) {
        .catalog_item .image_wrapper_block{
            display: block;
        }
        .specials_slider > li, .tabs_slider > li, .wrapper_block .wr > li {
            width: 141px !important;
        }
    }
</style>



<pre>
<?//print_r($arResult);?>
</pre>


<script>
    var brand = $(".aku-product-brand-block a").html();

    dataLayer.push({
        "ecommerce": {
            "detail": {
                "products": [
                    {
                        "id": "<?=$arResult['ID']?>",
                        "name" : "<?=$arResult['NAME']?>",
                        "price": <?=$arResult['MIN_PRICE']['VALUE']?>,
                        "brand": brand,
                        "category": "<?=$arResult['SECTION']['NAME']?>"
                    }
                ]
            }
        }
    });
</script>