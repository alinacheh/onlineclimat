/*
You can use this file with your scripts.
It will not be overwritten when you upgrade solution.
*/

$(document).ready(function () {
    /*$(".catalogLink").on('click', function(e) {
        e.preventDefault();
        var data = "." + $(this).attr("data");
        console.log(data);
        //$('.submenu').slideToggle();
        $('.mainmenu-block .submenu').hide();
        $(data).slideToggle();
    })*/

    /*$(".catalogLink").on('click', function(e) {
        e.preventDefault();
        var parent_a = $(this).parents();
        parent_a.find('.submenu').slideToggle();
    })*/


    $(".mobview").on('click', function (e) {
        e.preventDefault();
        $('.mainmenu').slideToggle();
    })

    $(".dopmenuview").on('click', function (e) {
        e.preventDefault();
        $('.left-aside').addClass('active');
    })

    $(".left-asideClose").on('click', function (e) {
        e.preventDefault();
        $('.left-aside').removeClass('active');
    })

    $(".submenu2-brands").hover(function () {
        $(this).parent().addClass('active');
    }, function () {
        $(this).parent().removeClass('active')
    })

    $("body.detail").find(".right_block").removeClass("right_block");

    $(function () {
        $("a[href^='#']").click(function () {
            var _href = $(this).attr("href");
            $("html, body").animate({scrollTop: $(_href).offset().top + "px"});
            return false;
        });
    });
});


// Кнопка показать ещё
$(document).on('click', '.oldacessories.current .load_more', function () {

    var targetContainer = $('.oldacessories.current .loadmore_wrap'),          //  Контейнер, в котором хранятся элементы
        url = $('.oldacessories.current .load_more').attr('data-url');    //  URL, из которого будем брать элементы

    if (url !== undefined) {
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'html',
            success: function (data) {
                //  Удаляем старую навигацию
                $('.oldacessories.current .load_more').remove();

                var elements = $(data).find('.oldacessories').find('.loadmore_item'),  //  Ищем элементы
                    pagination = $(data).find('.oldacessories').find('.load_more');//  Ищем навигацию

                targetContainer.append(elements);   //  Добавляем посты в конец контейнера
                targetContainer.append(pagination); //  добавляем навигацию следом

            }
        })
    }

});

$(document).on('click', '.acessories1.current .load_more', function () {

    var targetContainer = $('.acessories1.current .loadmore_wrap'),          //  Контейнер, в котором хранятся элементы
        url = $('.acessories1.current .load_more').attr('data-url');    //  URL, из которого будем брать элементы

    if (url !== undefined) {
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'html',
            success: function (data) {

                //  Удаляем старую навигацию
                $('.acessories1.current .load_more').remove();

                var elements = $(data).find('.acessories1').find('.loadmore_item'),  //  Ищем элементы
                    pagination = $(data).find('.acessories1').find('.load_more');//  Ищем навигацию

                targetContainer.append(elements);   //  Добавляем посты в конец контейнера
                targetContainer.append(pagination); //  добавляем навигацию следом

            }
        })
    }

});

$(document).on('click', '.acessories1.current .load_more', function () {

    var targetContainer = $('.acessories2.current .loadmore_wrap'),          //  Контейнер, в котором хранятся элементы
        url = $('.acessories2.current .load_more').attr('data-url');    //  URL, из которого будем брать элементы

    if (url !== undefined) {
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'html',
            success: function (data) {

                //  Удаляем старую навигацию
                $('.acessories2.current .load_more').remove();

                var elements = $(data).find('.acessories2').find('.loadmore_item'),  //  Ищем элементы
                    pagination = $(data).find('.acessories2').find('.load_more');//  Ищем навигацию

                targetContainer.append(elements);   //  Добавляем посты в конец контейнера
                targetContainer.append(pagination); //  добавляем навигацию следом

            }
        })
    }

});

$(document).on('click', '.acessories3.current .load_more', function () {

    var targetContainer = $('.acessories3.current .loadmore_wrap'),          //  Контейнер, в котором хранятся элементы
        url = $('.acessories3.current .load_more').attr('data-url');    //  URL, из которого будем брать элементы

    if (url !== undefined) {
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'html',
            success: function (data) {

                //  Удаляем старую навигацию
                $('.acessories3.current .load_more').remove();

                var elements = $(data).find('.acessories3').find('.loadmore_item'),  //  Ищем элементы
                    pagination = $(data).find('.acessories3').find('.load_more');//  Ищем навигацию

                targetContainer.append(elements);   //  Добавляем посты в конец контейнера
                targetContainer.append(pagination); //  добавляем навигацию следом

            }
        })
    }

});

$(document).on('click', '.acessories1.current .load_more', function () {

    var targetContainer = $('.acessories4.current .loadmore_wrap'),          //  Контейнер, в котором хранятся элементы
        url = $('.acessories4.current .load_more').attr('data-url');    //  URL, из которого будем брать элементы

    if (url !== undefined) {
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'html',
            success: function (data) {

                //  Удаляем старую навигацию
                $('.acessories4.current .load_more').remove();

                var elements = $(data).find('.acessories4').find('.loadmore_item'),  //  Ищем элементы
                    pagination = $(data).find('.acessories4').find('.load_more');//  Ищем навигацию

                targetContainer.append(elements);   //  Добавляем посты в конец контейнера
                targetContainer.append(pagination); //  добавляем навигацию следом

            }
        })
    }

});

$(document).on('click', '.similar_products .load_more', function () {

    var targetContainer = $('.similar_products .loadmore_wrap'),          //  Контейнер, в котором хранятся элементы
        url = $('.similar_products .load_more').attr('data-url');    //  URL, из которого будем брать элементы

    if (url !== undefined) {
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'html',
            success: function (data) {

                //  Удаляем старую навигацию
                $('.similar_products .load_more').remove();

                var elements = $(data).find('.similar_products .loadmore_item'),  //  Ищем элементы
                    pagination = $(data).find('.similar_products .load_more');//  Ищем навигацию

                targetContainer.append(elements);   //  Добавляем посты в конец контейнера
                targetContainer.append(pagination); //  добавляем навигацию следом

            }
        })
    }
});