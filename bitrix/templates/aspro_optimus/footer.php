<?if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == "xmlhttprequest") die();?><?IncludeTemplateLangFile(__FILE__);?>
							<?if(!COptimus::IsMainPage()):?>
								</div> <?// .container?>
							<?endif;?>
					<?if(!COptimus::IsMainPage()):?>
						</div> <?// .middle?>
					<?endif;?>
					<?if(!COptimus::IsOrderPage() && !COptimus::IsBasketPage() && !COptimus::IsMainPage()):?>
						</div> <?// .right_block?>
					<?endif;?>
				</div> <?// .wrapper_inner?>				
			</div> <?// #content?>
		</div><?// .wrapper?>
		<footer id="footer">
			<div class="footer_inner <?=strtolower($TEMPLATE_OPTIONS["BGCOLOR_THEME_FOOTER_SIDE"]["CURRENT_VALUE"]);?>">

				<?if($APPLICATION->GetProperty("viewed_show")=="Y" || defined("ERROR_404")):?>
					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
						array(
							"COMPONENT_TEMPLATE" => ".default",
							"PATH" => SITE_DIR."include/footer/comp_viewed.php",
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "",
							"AREA_FILE_RECURSIVE" => "Y",
							"EDIT_TEMPLATE" => "standard.php"
						),
						false
					);?>					
				<?endif;?>
				<div class="wrapper_inner">
					<div class="footer_bottom_inner">
						<div class="left_block">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
								array(
									"COMPONENT_TEMPLATE" => ".default",
									"PATH" => SITE_DIR."include/footer/copyright.php",
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => "standard.php"
								),
								false
							);?>							
							<div id="bx-composite-banner"></div>
						</div>
						<div class="right_block">
							<div class="middle">
								<div class="rows_block">
									<div class="item_block col-75 menus">
										<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_submenu_top", array(
											"ROOT_MENU_TYPE" => "bottom",
											"MENU_CACHE_TYPE" => "Y",
											"MENU_CACHE_TIME" => "3600000",
											"MENU_CACHE_USE_GROUPS" => "N",
											"MENU_CACHE_GET_VARS" => array(),
											"MAX_LEVEL" => "1",
											"USE_EXT" => "N",
											"DELAY" => "N",
											"ALLOW_MULTI_SELECT" => "N"
											),false
										);?>
										<div class="rows_block">
											<div class="item_block col-3">
												<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_submenu", array(
													"ROOT_MENU_TYPE" => "bottom_company",
													"MENU_CACHE_TYPE" => "Y",
													"MENU_CACHE_TIME" => "3600000",
													"MENU_CACHE_USE_GROUPS" => "N",
													"MENU_CACHE_GET_VARS" => array(),
													"MAX_LEVEL" => "1",
													"USE_EXT" => "N",
													"DELAY" => "N",
													"ALLOW_MULTI_SELECT" => "N"
													),false
												);?>
											</div>
											<div class="item_block col-3">
												<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_submenu", array(
													"ROOT_MENU_TYPE" => "bottom_info",
													"MENU_CACHE_TYPE" => "Y",
													"MENU_CACHE_TIME" => "3600000",
													"MENU_CACHE_USE_GROUPS" => "N",
													"MENU_CACHE_GET_VARS" => array(),
													"MAX_LEVEL" => "1",
													"USE_EXT" => "N",
													"DELAY" => "N",
													"ALLOW_MULTI_SELECT" => "N"
													),false
												);?>
											</div>
											<div class="item_block col-3">
												<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_submenu", array(
													"ROOT_MENU_TYPE" => "bottom_help",
													"MENU_CACHE_TYPE" => "Y",
													"MENU_CACHE_TIME" => "3600000",
													"MENU_CACHE_USE_GROUPS" => "N",
													"MENU_CACHE_GET_VARS" => array(),
													"MAX_LEVEL" => "1",
													"USE_EXT" => "N",
													"DELAY" => "N",
													"ALLOW_MULTI_SELECT" => "N"
													),false
												);?>
											</div>
										</div>
									</div>
									<div class="item_block col-4 soc">
										<div class="soc_wrapper">
											<div class="phones">
												<div class="phone_block">
													<span class="phone_wrap">
														<span class="icons fa fa-phone"></span>
														<span>
															<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
																array(
																	"COMPONENT_TEMPLATE" => ".default",
																	"PATH" => SITE_DIR."include/phone.php",
																	"AREA_FILE_SHOW" => "file",
																	"AREA_FILE_SUFFIX" => "",
																	"AREA_FILE_RECURSIVE" => "Y",
																	"EDIT_TEMPLATE" => "standard.php"
																),
																false
															);?>
														</span>
													</span>
													<span class="order_wrap_btn">
														<span class="callback_btn" onclick="yaCounter37461485.reachGoal('waitingCallFooter'); return true;"><?=GetMessage('CALLBACK')?></span>
													</span>
													<?
														/*
														<div class="www">
														<a style=" font-size: 24px;" href="/">
															<i class="fab fa-instagram"></i>
														</a>
													</div>
														*/
													?>
												</div>
											</div>
											<div class="social_wrapper">
												<div class="social">
													<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
														array(
															"COMPONENT_TEMPLATE" => ".default",
															"PATH" => SITE_DIR."include/footer/social.info.optimus.default.php",
															"AREA_FILE_SHOW" => "file",
															"AREA_FILE_SUFFIX" => "",
															"AREA_FILE_RECURSIVE" => "Y",
															"EDIT_TEMPLATE" => "standard.php"
														),
														false
													);?>
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="mobile_copy">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
							array(
								"COMPONENT_TEMPLATE" => ".default",
								"PATH" => SITE_DIR."include/footer/copyright.php",
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "",
								"AREA_FILE_RECURSIVE" => "Y",
								"EDIT_TEMPLATE" => "standard.php"
							),
							false
						);?>
					</div>
					<?$APPLICATION->IncludeFile(SITE_DIR."include/bottom_include1.php", Array(), Array("MODE" => "text", "NAME" => GetMessage("ARBITRARY_1"))); ?>
					<?$APPLICATION->IncludeFile(SITE_DIR."include/bottom_include2.php", Array(), Array("MODE" => "text", "NAME" => GetMessage("ARBITRARY_2"))); ?>
				</div>
			</div>
		</footer>
		<?
		COptimus::setFooterTitle();
		COptimus::showFooterBasket();
		?>

	
    <script src="/js/owl.carousel.min.js"></script>
	<script src="<?php echo SITE_TEMPLATE_PATH ?>/js/modification.js"></script>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>


    <script src="/bitrix/templates/aspro_optimus/js/jquery.lazy.min.js"></script>
<?/*
//2909
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134174534-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-134174534-1');
</script>
*/?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-179167545-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-179167545-1');
</script>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'TkNWyHzdFj';var d=document;var w=window;function l(){
  var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
  s.src = '//code.jivosite.com/script/widget/'+widget_id
    ; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}
  if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}
  else{w.addEventListener('load',l,false);}}})();
</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- Facebook Pixel Code -->

<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script','https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '633293197576535');
	fbq('track', 'PageView');
</script>

<noscript><img height="1" width="1" style="display:none"

src="https://www.facebook.com/tr?id=633293197576535&ev=PageView&noscript=1";

/></noscript>

<!-- End Facebook Pixel Code -->

    <script>
    JS:      $('.lazy').Lazy({
        // your configuration goes here
        scrollDirection: 'vertical',
        effect: 'fadeIn',
        visibleOnly: true
    });
    </script>


<div class="overley_desh">
    <div class="desh_container">
        <h4>Нашли дешевле? <i onclick="CloseUp(this)" class="fa fa-times"></i></h4>
        <p>Мы сделаем скидку, если вы нашли этот товар дешевле в российских интернет-магазинах. Отправьте нам ссылку.</p>
        <ul>
        <li><p class="wnt_name">Ваше имя: *</p> <input id="wnt_name" value="" type="text"></li>
        <li><p class="wnt_phone">Ваш телефон: *</p> <input class="phone" id="wnt_phone" value="" type="text"></li>
        <li><p>E-mail:</p> <input id="wnt_email" value="" type="text"></li>
        <li><p>Какую цену Вы видели:</p> <input id="wnt_price" value="" type="text"></li>
        <li><p>Стоимость доставки:</p> <input id="wnt_deliv" value="" type="text"></li>
        <li><p class="wnt_href">Ссылка на товар: *</p> <input id="wnt_href" value="" type="text"></li><p>

        </ul>
        <p class="wnt_results"></p>
        <div class="politika_konfidencial">Нажимая кнопку "Отправить" Вы соглашаетесь на&nbsp;<a href="/politika-konfidentsialnosti.php">обработку персональных данных</a></div>
        <p>Пожалуйста, убедитесь, что указанный в магазине товар со статусом "в наличии" и продается за указанную цену, при учёте условий доставки.<br><br>

На товары со статусом "под заказ" данное предложение не распространяется.</p>
        <span id="wnt_go" onclick="Goformajax(this)">Отправить</span>
    </div>
</div>

<script>
    $( ".find_desh" ).click(function(){
	    $( ".overley_desh" ).css({'display': 'block'});
	  });

</script>

<script>
function CloseUp(a){
    $( ".overley_desh" ).css({'display': 'none'});
}
</script>

<script>
//форма нашли дешевле
function Goformajax(e){
    var wnt_name = $("#wnt_name").val();
    var wnt_phone = $("#wnt_phone").val();
    var wnt_email = $("#wnt_email").val();
    var wnt_price = $("#wnt_price").val();
    var wnt_deliv = $("#wnt_deliv").val();
    var wnt_href = $("#wnt_href").val();

    $.ajax({
        type: 'POST',
        url: '/bitrix/templates/aspro_optimus/ajax/wnt_sale_form.php',
        data: 'wnt_name='+wnt_name+'&wnt_phone='+wnt_phone+'&wnt_email='+wnt_email+'&wnt_price='+wnt_price+'&wnt_deliv='+wnt_deliv+'&wnt_href='+wnt_href+'',
        success: function(data) {
            $('.wnt_results').html(data);
        }
    });
}
</script>
  <?function containerWidth() {
        global $APPLICATION;
        ob_start();

        if(strlen($APPLICATION->GetProperty('fullWidthContainer')) > 0) {
            echo 'detail';
        }
        $result = ob_get_contents();
        ob_end_clean();
        return $result;
    }?>
	</body>
</html>