<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Highloadblock\HighloadBlockTable;

CModule::IncludeModule('highloadblock');

$arTypes = [];
$arTypeCodes = [];

foreach ($arResult['ITEMS'] as $arItem) {
    $arTypeCodes[] = $arItem["PROPERTIES"]["TYPE"]["VALUE"];
}

$arTypeCodes = array_unique($arTypeCodes);

if (!empty($arTypeCodes)) {
    $arHLBlock = HighloadBlockTable::getById(5)->fetch();
    $obEntity = HighloadBlockTable::compileEntity($arHLBlock);
    $strEntityDataClass = $obEntity->getDataClass();
    $resData = $strEntityDataClass::getList(array(
        'select' => array('UF_FILE','UF_XML_ID'),
        'filter' => array('UF_XML_ID' => $arTypeCodes),
        'order' => array('ID' => 'ASC'),
        'limit' => 1000
    ));

    while ($arType = $resData->fetch()) {
        $arTypes[$arType['UF_XML_ID']] = $arType;
    }
}

if (!empty($arTypes)) {
    foreach ($arResult['ITEMS'] as $key => $arItem) {
        if (!isset($arItem['PROPERTIES']['TYPE'])) {
            continue;
        }
        if (!array_key_exists($arItem['PROPERTIES']['TYPE']['VALUE'], $arTypes)) {
            continue;
        }
        $arResult['ITEMS'][$key]['PROPERTIES']['TYPE']['VALUE'] = array(
            'NAME' => $arItem["PROPERTIES"]["TYPE"]["VALUE"],
            'FILE' => CFile::GetPath($arTypes[$arItem['PROPERTIES']['TYPE']['VALUE']]['UF_FILE'])
        );
    }
}