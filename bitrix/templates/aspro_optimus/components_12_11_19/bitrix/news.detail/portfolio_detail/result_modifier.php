<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

$arLinkedProductsIds = array();

if (!empty($arResult['PROPERTIES'][$arParams['LINKED_PRODUCTS_PROPERTY']]) && !empty($arResult['PROPERTIES'][$arParams['LINKED_PRODUCTS_PROPERTY']]['VALUE'])) {
	if (is_array($arResult['PROPERTIES'][$arParams['LINKED_PRODUCTS_PROPERTY']]['VALUE'])) {
		foreach ($arResult['PROPERTIES'][$arParams['LINKED_PRODUCTS_PROPERTY']]['VALUE'] as $productId) {
			$arLinkedProductsIds[] = $productId;
		}
	}
}

$arResult['LINKED_PRODUCTS_IDS'] = $arLinkedProductsIds;

$this->getComponent()->SetResultCacheKeys(array("LINKED_PRODUCTS_IDS"));