<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<?if($arResult["SECTIONS"]){?>


<?php if($arResult['SECTION']['IBLOCK_SECTION_ID']) { ?>

	<?php if(count( $arResult["SECTIONS"]) < 4) { ?>

		<div class="sections_block_on_first">
			<?foreach( $arResult["SECTIONS"] as $arItems ){?>
				<div class="sections_block_on_first-item">
					<div class="sections_block_on_first-image_wrapper">
						<?if($arItems["PICTURE"]["SRC"]):?>
							<?$img = CFile::ResizeImageGet($arItems["PICTURE"]["ID"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
							<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb">
								<img src="<?=$img["src"]?>" alt="" title="" />
							</a>
						<?elseif($arItems["~PICTURE"]):?>
							<?$img = CFile::ResizeImageGet($arItems["~PICTURE"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
							<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb">
								<img src="<?=$img["src"]?>" alt="" title="" />
							</a>
						<?else:?>
							<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img style="display: block; margin:auto;" src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_medium.png" alt="" title="" height="90" />
							</a>
						<?endif;?>
					</div>
					<a style="font-size:14px;" href="<?=$arItems["SECTION_PAGE_URL"]?>"><span><?=$arItems["NAME"]?></span></a>
				</div>
			<?}?>
		</div>

	<?php } else { ?>

		<div class="bottom_slider sections_slider_wrapp specials tab_slider_wrapp subcat" style="padding-top:0px;">
			<div class="top_blocks">
				<ul class="slider_navigation top custom_flex border" style="top:1px;">
					<li class="tabs_slider_navigation subcat_nav cur" data-code="subcat">
						<ul class="flex-direction-nav">
						</ul>
					</li>
				</ul>
			</div>
			<ul class="tabs_content" style="">
				<li class="tab subcat_wrapp cur" data-code="subcat" data-unhover="522" data-hover="605" style="margin-top: 30px;">
					<div class="flex-viewport" style="overflow: hidden; position: relative;">
						<ul class="tabs_slider subcat_slides 345 wr" style="width: 2000%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
							<?foreach( $arResult["SECTIONS"] as $arItems ){?>
								<li class="catalog_item" id="bx_subcat" style="height:175px;">
								<div class="image_wrapper_block">
								<?if($arItems["PICTURE"]["SRC"]):?>
								<?$img = CFile::ResizeImageGet($arItems["PICTURE"]["ID"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
								<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=$img["src"]?>" alt="" title="" /></a>
								<?elseif($arItems["~PICTURE"]):?>
								<?$img = CFile::ResizeImageGet($arItems["~PICTURE"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
								<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=$img["src"]?>" alt="" title="" /></a>
								<?else:?>
								<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img style="display: block; margin:auto;" src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_medium.png" alt="" title="" height="90" /></a>
								<?endif;?>
								</div>
									<a style="font-size:14px;" href="<?=$arItems["SECTION_PAGE_URL"]?>"><span><?=$arItems["NAME"]?></span></a>
								</li>
							<?}?>
						</ul>
					</div>
				</li>
			</ul>
		</div>


		<script type="text/javascript">
			$(document).ready(function(){

				$('.tab_slider_wrapp.subcat .tabs > li').first().addClass('cur');
				$('.tab_slider_wrapp.subcat .slider_navigation > li').first().addClass('cur');
				$('.tab_slider_wrapp.subcat .tabs_content > li').first().addClass('cur');

				var flexsliderItemWidth = 220;
				var flexsliderItemMargin = 12;

				var sliderWidth = $('.tab_slider_wrapp.subcat').outerWidth();
				var flexsliderMinItems = Math.floor(sliderWidth / (flexsliderItemWidth + flexsliderItemMargin));
				$('.tab_slider_wrapp.subcat .tabs_content > li.cur').flexslider({
					animation: 'slide',
					selector: '.tabs_slider .catalog_item',
					slideshow: false,
					animationSpeed: 600,
					directionNav: true,
					controlNav: false,
					pauseOnHover: true,
					animationLoop: true,
					itemWidth: flexsliderItemWidth,
					itemMargin: flexsliderItemMargin,
					minItems: flexsliderMinItems,
					controlsContainer: '.tab_slider_wrapp .tabs_slider_navigation.cur',
					start: function(slider){
						slider.find('li').css('opacity', 1);
					}
				});

				$('.tab_slider_wrapp.subcat .tabs > li').on('click', function(){
					var sliderIndex = $(this).index();
					if(!$('.tab_slider_wrapp.subcat .tabs_content > li.cur .flex-viewport').length){
						$('.tab_slider_wrapp.subcat .tabs_content > li.cur').flexslider({
							animation: 'slide',
							selector: '.tab_slider_wrapp .tabs_slider .catalog_item',
							slideshow: false,
							animationSpeed: 600,
							directionNav: true,
							controlNav: false,
							pauseOnHover: true,
							animationLoop: true,
							itemWidth: flexsliderItemWidth,
							itemMargin: flexsliderItemMargin,
							minItems: flexsliderMinItems,
							controlsContainer: '.tab_slider_wrapp .tabs_slider_navigation.cur',
						});
						setHeightBlockSlider();
					}

				});

				setHeightBlockSlider();

				$(document).on({
					mouseover: function(e){
						var tabsContentHover = $(this).closest('.tab').attr('data-hover') * 1;
						$(this).closest('.tab_slider_wrapp.tab').fadeTo(100, 1);
						$(this).closest('.tab_slider_wrapp.tab').stop().css({'height': tabsContentHover});
						$(this).find('.tab_slider_wrapp.buttons_block').fadeIn(450, 'easeOutCirc');
					},
					mouseleave: function(e){
						var tabsContentUnhoverHover = $(this).closest('.tab').attr('data-unhover') * 1;
						$(this).closest('.tab_slider_wrapp.tab').stop().animate({'height': tabsContentUnhoverHover}, 100);
						$(this).find('.tab_slider_wrapp.buttons_block').stop().fadeOut(233);
					}
				}, '.tab_slider_wrapp.subcat .tabs_slider > li');
			})
		</script>
	<?php } ?>

<?php } else { ?>
	<div class="sections_block_on_first">
		<?foreach( $arResult["SECTIONS"] as $arItems ){?>
			<div class="sections_block_on_first-item">
				<div class="sections_block_on_first-image_wrapper">
					<?if($arItems["PICTURE"]["SRC"]):?>
						<?$img = CFile::ResizeImageGet($arItems["PICTURE"]["ID"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
						<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb">
							<img src="<?=$img["src"]?>" alt="" title="" />
						</a>
					<?elseif($arItems["~PICTURE"]):?>
						<?$img = CFile::ResizeImageGet($arItems["~PICTURE"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
						<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb">
							<img src="<?=$img["src"]?>" alt="" title="" />
						</a>
					<?else:?>
						<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img style="display: block; margin:auto;" src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_medium.png" alt="" title="" height="90" />
						</a>
					<?endif;?>
				</div>
				<a style="font-size:14px;" href="<?=$arItems["SECTION_PAGE_URL"]?>"><span><?=$arItems["NAME"]?></span></a>
			</div>
		<?}?>
	</div>
<?php } ?>





















<div class="catalog_section_list rows_block items section hidden">	
	<?foreach( $arResult["SECTIONS"] as $arItems ){
		$this->AddEditAction($arItems['ID'], $arItems['EDIT_LINK'], CIBlock::GetArrayByID($arItems["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItems['ID'], $arItems['DELETE_LINK'], CIBlock::GetArrayByID($arItems["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<div class="item_block col-2">
			<div class="section_item item" id="<?=$this->GetEditAreaId($arItems['ID']);?>">
			
				
						<?if ($arParams["SHOW_SECTION_LIST_PICTURES"]=="Y"):?>
							<?$collspan = 2;?>
							
								<?if($arItems["PICTURE"]["SRC"]):?>
									<?$img = CFile::ResizeImageGet($arItems["PICTURE"]["ID"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
									<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=$img["src"]?>" alt="" title="" /></a>
								<?elseif($arItems["~PICTURE"]):?>
									<?$img = CFile::ResizeImageGet($arItems["~PICTURE"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
									<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=$img["src"]?>" alt="" title="" /></a>
								<?else:?>
									<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img style="display: block; margin:auto;" src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_medium.png" alt="" title="" height="90" /></a>
								<?endif;?>
							
						<?endif;?>
						<br>
							<div class="name name_costom">
									<a href="<?=$arItems["SECTION_PAGE_URL"]?>"><span><?=$arItems["NAME"]?></span></a> 
							</div>
							<?if($arParams["SECTIONS_LIST_PREVIEW_DESCRIPTION"] != 'N'):?>
								<?$arSection = $section=COptimusCache::CIBlockSection_GetList(array('CACHE' => array("MULTI" =>"N", "TAG" => COptimusCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]))), array('GLOBAL_ACTIVE' => 'Y', "ID" => $arItems["ID"], "IBLOCK_ID" => $arParams["IBLOCK_ID"]), false, array("ID", "IBLOCK_ID", $arParams["SECTIONS_LIST_PREVIEW_PROPERTY"]));?>
								<div class="desc" ><span class="desc_wrapp">
									<?if ($arSection[$arParams["SECTIONS_LIST_PREVIEW_PROPERTY"]]):?>
										<?=$arSection[$arParams["SECTIONS_LIST_PREVIEW_PROPERTY"]]?>
									<?else:?>
										<?=$arItems["DESCRIPTION"]?>
									<?endif;?>
								</span></div>
							<?endif;?>
						
				
			</div>
		</div>
	<?}?>
</div>
<script>
	$(document).ready(function(){
		$('.catalog_section_list.rows_block .item .section_info').sliceHeight();
		$('.catalog_section_list.rows_block .item').sliceHeight();
		setTimeout(function(){
			$(window).resize();
		},100)
	});
</script>
<?}?>