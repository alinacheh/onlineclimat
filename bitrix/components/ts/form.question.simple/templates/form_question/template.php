<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?=$arResult["ERROR"];?>
<form method="post" enctype="multipart/form-data" action="<?=$APPLICATION->GetCurPage()?>">
	<input type="hidden" name="frmFeedbackSent" value="Y">
	<div class="form form-validate">
		<div class="block-form block-form-name">
			<div class="st-name">Ваше имя<span class="req">*</span></div>
			<input type="text" name="strName" class="input-text" placeholder="">
		</div>
		<div class="block-form block-form-mail">
			<div class="st-mail">Ваш E-Mail<span class="req">*</span></div>
			<input type="text" name="strMail" class="input-text" placeholder="">
		</div>
		<div class="block-form">
			<div class="st-question">Сообщение <span class="req">*</span></div>
			<textarea name="strMessage" placeholder=""></textarea>
		</div>
		<div class="block-rate-s-t">Ваша оценка<span class="req">*</span></div>
		<div class="block-form rate-stars">
			<div class="rate-star-single" data-rate="1"></div>
			<div class="rate-star-single" data-rate="2"></div>
			<div class="rate-star-single" data-rate="3"></div>
			<div class="rate-star-single" data-rate="4"></div>
			<div class="rate-star-single" data-rate="5"></div>
		</div>
		<input type="hidden" value="0" id="strRate" name="strRate">
	</div>
	<div class="st-captcha">
		<div class="st-text">Защита от автоматических сообщений</div>
		<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
		<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
		<div class="st-text">Введите слово на картинке<span class="req">*</span></div>
		<input type="text" name="captcha_word" size="30" maxlength="50" value="">
	</div>
	<div class="feedback_buttons">
		<button id="feedbackSubmit" class="submit">Отправить отзыв</button>
	</div>
</form>
