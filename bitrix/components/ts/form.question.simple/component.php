<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if($_REQUEST["frmFeedbackSent"] == "Y" && CModule::IncludeModule("iblock"))
{
	$el = new CIBlockElement;
	
	if ( !empty($_REQUEST["strName"]) && !empty($_REQUEST["strMessage"]) ) {$strContact = true;}

	if(empty($strContact)) {
		$arResult["ERROR"] = 'Не заполнены поля формы';
	} else {

		$arLoadProductArray = array(
			"IBLOCK_ID" => 14,
			"ACTIVE" => "N",
			"NAME" => $_REQUEST["strName"],
			"ACTIVE_FROM" => date("d.m.Y H:i:s"),
			"PROPERTY_VALUES" => array(
				//"R_REVIEW['TEXT']" => htmlspecialchars($_REQUEST["strMessage"]),
				"R_EMAIL" => $_REQUEST["strMail"],
				"R_RATE" => $_REQUEST["strRate"]
			),
			"PREVIEW_TEXT" => $_REQUEST["strMessage"]
		);

		if($newFeedbackID = $el->Add($arLoadProductArray)) {
			$rsI = CIBlockElement::GetList(Array(), array(
				"IBLOCK_ID" => 14, "ID" => $newFeedbackID
			));
			if($obI = $rsI->GetNextElement()) {
				$arData = $obI -> GetFields();
				$strBody = '<strong>Имя</strong>: '.$arData["NAME"].'<br>';
				$strBody .= '<strong>Текст отзыва</strong>: '.$arData["PREVIEW_TEXT"].'<br>';
				
				CEvent::Send("review_message", array("s1"), array(
					"DATE" => date("d.m.Y H:i:s"),
					"EMAIL_TO" => (check_email($arParams["EMAIL_TO"])?$arParams["EMAIL_TO"]:'d.katunin@demis.ru'),
					"TEXT" => $strBody,
					"DATE" => date("d.m.Y H:i:s"),
					"SUBJECT" => "Новый отзыв на сайт ".date("d.m.Y H:i:s")
				));
			}

			LocalRedirect($APPLICATION->GetCurPage().'?success=Y');
		}
	}
}

include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
			$captcha_code = $_POST["captcha_sid"];
			$captcha_word = $_POST["captcha_word"];
			$cpt = new CCaptcha();
			$captchaPass = COption::GetOptionString("main", "captcha_password", "");
			if (strlen($captcha_word) > 0 && strlen($captcha_code) > 0)
			{
				if (!$cpt->CheckCodeCrypt($captcha_word, $captcha_code, $captchaPass))
					$arResult["ERROR_MESSAGE"][] = GetMessage("TS_CAPTCHA_WRONG");
			}
			else {
				$arResult["ERROR_MESSAGE"][] = GetMessage("TS_CAPTHCA_EMPTY");
			}		
$arResult["capCode"] =  htmlspecialcharsbx($APPLICATION->CaptchaGetCode());
$this->IncludeComponentTemplate();

$APPLICATION->SetPageProperty("skContentClass", "feedback");
?>
