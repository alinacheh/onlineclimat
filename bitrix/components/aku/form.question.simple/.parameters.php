<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"EMAIL_TO" => array(
			"PARENT" => "BASE",
			"NAME" => "Email получателя сообщений",
			"TYPE" => "STRING",
			"DEFAULT" => 'otpravtepismo@gmail.com',
		),
		"AJAX_MODE" => array()
	),
);
?>