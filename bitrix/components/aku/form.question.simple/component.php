<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if($_REQUEST["frmFeedbackSent"] == "Y" && CModule::IncludeModule("iblock"))
{
	$el = new CIBlockElement;

if (!empty($_REQUEST["strName"])) { 
$arResult["ERROR"] = '';
	}

	if ( empty($_REQUEST["strCod"]) || empty($_REQUEST["strMessage"]) ) { 
		$arResult["ERROR"] = 'Не заполнены поля формы';
	}
/*
	$captcha_code = $_POST["captcha_sid"];
	$captcha_word = $_POST["captcha_word"];
	$cpt = new CCaptcha();
	$captchaPass = COption::GetOptionString("main", "captcha_password", "");
	if (strlen($captcha_word) > 0 && strlen($captcha_code) > 0)
	{
		if (!$cpt->CheckCodeCrypt($captcha_word, $captcha_code, $captchaPass))
			$arResult["ERROR"] = 'Код безопасности введен неверно';
	}
	else {
		$arResult["ERROR"] = 'Введите код безопасности';
	}	
*/

	if(!checkGoogleCaptcha()) {
		$arResult["ERROR"] = 'Введите код безопасности';
	}

	$chr_en = "a-zA-Z0-9\s`~!@#$%^&*()_+-={}|:;<>?,.\/\"\'\\\[\]";		
	if(strlen($_REQUEST["strCod"]) > 0 && preg_match("/^[$chr_en]+$/", $_REQUEST["strCod"]))
	{
		$arResult["ERROR"] = 'В поле имя возможны только русские буквы';
	}



	if(!isset($arResult["ERROR"])) {

		$arLoadProductArray = array(
			"IBLOCK_ID" => 14,
			"ACTIVE" => "Y",
			"NAME" => $_REQUEST["strCod"],
			"ACTIVE_FROM" => date("d.m.Y H:i:s"),
			"PROPERTY_VALUES" => array(
				//"R_REVIEW['TEXT']" => htmlspecialchars($_REQUEST["strMessage"]),
				"R_EMAIL" => $_REQUEST["strMail"],
				"R_RATE" => $_REQUEST["strRate"]
			),
			"PREVIEW_TEXT" => $_REQUEST["strMessage"]
		);

		if($newFeedbackID = $el->Add($arLoadProductArray)) {
			$rsI = CIBlockElement::GetList(Array(), array(
				"IBLOCK_ID" => 14, "ID" => $newFeedbackID
			));
			if($obI = $rsI->GetNextElement()) {
				$arData = $obI -> GetFields();
				$strBody = '<strong>Имя</strong>: '.$arData["NAME"].'<br>';
				$strBody .= '<strong>Текст отзыва</strong>: '.$arData["PREVIEW_TEXT"].'<br>';
				
				CEvent::Send("review_message", array("s1"), array(
					"DATE" => date("d.m.Y H:i:s"),
					"EMAIL_TO" => (check_email($arParams["EMAIL_TO"])?$arParams["EMAIL_TO"]:'langly2010@mail.ru'),
					"TEXT" => $strBody,
					"DATE" => date("d.m.Y H:i:s"),
					"SUBJECT" => "Новый отзыв на сайт ".date("d.m.Y H:i:s")
				));
			}

			LocalRedirect($APPLICATION->GetCurPage().'?success=Y');
		}
	}
}

$arResult["capCode"] =  htmlspecialcharsbx($APPLICATION->CaptchaGetCode());
$this->IncludeComponentTemplate();

$APPLICATION->SetPageProperty("skContentClass", "feedback");
?>
