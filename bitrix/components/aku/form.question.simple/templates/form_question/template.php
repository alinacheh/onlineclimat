<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?=$arResult["ERROR"];?>
<form method="post" enctype="multipart/form-data" action="<?=$APPLICATION->GetCurPage()?>">
	<input type="hidden" name="frmFeedbackSent" value="Y">
	<div class="form form-validate">
		<div class="block-form block-form-cod">
			<div class="st-cod">Ваш имя<span class="req">*</span></div>
			<input type="text" name="strCod" class="input-text" placeholder="">
		</div>
		<div class="block-form block-form-name hidden">
			<div class="st-name">Ваше имя</div>
			<input type="text" name="strName" class="input-text" placeholder="">
		</div>
		<div class="block-form block-form-mail">
			<div class="st-mail">Ваш E-Mail<span class="req">*</span></div>
			<input type="email" name="strMail" class="input-text" placeholder="">
		</div>
		<div class="block-form">
			<div class="st-question">Сообщение <span class="req">*</span></div>
			<textarea name="strMessage" placeholder=""></textarea>
		</div>
		<input type="hidden" value="0" id="strRate" name="strRate">
	</div>
	<div class="st-captcha">
		<div class="st-text">Защита от автоматических сообщений</div>
		<div class="g-recaptcha" data-sitekey="6LfDCrkUAAAAAMQ0QEGwE0Yk9SclMM80z77KkHKq"></div>
	</div>
	<div class="feedback_buttons">
		<button id="feedbackSubmit" class="submit">Отправить отзыв</button>
	</div>
</form>
