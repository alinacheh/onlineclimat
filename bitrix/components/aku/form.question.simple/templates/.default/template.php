<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!empty($arResult["ERROR"])) {?>
	<script type="text/javascript">
		$(function() {
			obHelper.showError('<?=$arResult["ERROR"]?>');
		})
	</script><?
} elseif($_REQUEST["success"] == "Y") {
	?>
	<script type="text/javascript">
		$(function() {
			obHelper.showNote('���� ��������� ������� ����������.');
		})
	</script><?
}

?>
<form method="post" enctype="multipart/form-data" action="<?=$APPLICATION->GetCurPage()?>">
	<input type="hidden" name="frmFeedbackSent" value="Y">
	<div class="form form-validate">
		<div class="block-form block-form-name">
			<label>���� ���</label>
			<input type="text" name="strName" class="input-text">
		</div>
		<div class="block-form block-form-phone">
			<label>�������</label>
			<input id="feedbackPhone" name="strPhone" type="tel" class="input-text">
			<p class="error-massage">�� ��������� ������������ ����</p>
		</div>
		<div class="block-form">
			<label>����������� �����</label>
			<input id="feedbackEmail" name="strEmail" type="email" class="input-text">
			<p class="error-massage">�� ��������� ������������ ����</p>
		</div>
		<div class="block-form">
			<label class="massage">���������</label>
			<textarea name="strMessage"></textarea>
		</div>

		<div>
			<div>
				<label>���������� ����</label>
				<input name="arFile[]" type="file" class="input-file" id="input_file">
				<label for="input_file">������� �� �����</label>
			</div>
			<div class="add_file"><span>�������� ���</span></div>
		</div>
	</div>
	<div class="feedback_buttons">
		<button id="feedbackSubmit" class="submit">���������</button>
	</div>
</form>