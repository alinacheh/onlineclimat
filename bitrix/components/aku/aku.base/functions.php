<?php

	function set_product_canonical_url($arParams) {
		$element_id = 0;

		if(isset($_REQUEST) and isset($_REQUEST[$arParams["product_id_variable"]])) {
			$element_id = $_REQUEST[$arParams["product_id_variable"]];
		}

		$findFilter = array(
			"IBLOCK_ID" => $arParams["iblock_id"],
			"IBLOCK_LID" => SITE_ID,
			"ACTIVE_DATE" => "Y",
			"CHECK_PERMISSIONS" => "Y",
			"MIN_PERMISSION" => 'R',
		);

		$element_id = CIBlockFindTools::GetElementID(
			$element_id,
			$arParams["element_vars"]["ELEMENT_CODE"],
			false,
			false,
			$findFilter
		);

		$product = CIBlockElement::GetByID($element_id)->GetNext();
		$element = $product;

		if($element) {
			do {
				$element = CIBlockSection::GetByID($element['IBLOCK_SECTION_ID'])->GetNext();

				if($element) {
					$parent = $element;
				}
			} while($element);

			return (CMain::IsHTTPS() ? "https://" : "http://").SITE_SERVER_NAME.$parent['SECTION_PAGE_URL'].$product['CODE'].'/';
		}				
	}

	function get_neighbouring_categories($arParams) {
		$sections = array();		
		$cache_id = $arParams['iblock_section_id'];

		if(!$cache_id) {
			$cache_id = 'root';
		}

		$child_sections = returnResultCache(86400, 'sections_'.$cache_id, 'get_child_sections', $arParams['iblock_section_id'], 'child_sections');

		foreach ($child_sections as $index => $section) {
			if($section['ID'] == $arParams['self_id']) {
				unset($child_sections[$index]);
			}
		}

		return $child_sections;
	}

	function get_child_sections($section_id) {
		$result = array();

		$child_sections = CIBlockSection::GetList(
		    array(),
		    array("IBLOCK_ID" => 1, "ACTIVE" => "Y", "SECTION_ID" => $section_id)
		);

		while ($child_section = $child_sections->GetNext()) {
			$result[] = $child_section;
		}

		return $result;
	}

	function generate_brand_block($arParams) {
		if(!empty($arParams['brand_id'])) {
			$section = CIBlockSection::GetByID($arParams['iblock_section_id'])->GetNext();
			$parent = CIBlockSection::GetByID($section['IBLOCK_SECTION_ID'])->GetNext();

			$brand = CIBlockElement::GetList(
			    array("SORT"=>"ASC"),
			    array("IBLOCK_ID" => 2, "ID" => $arParams['brand_id']),
			    false,
			    false,
			    array('ID', 'NAME')
			);

			$brand = $brand->Fetch();

			$result = array(
				'title' => $brand['NAME'],
				'href' => $parent['SECTION_PAGE_URL']
			);

			return $result;
		}

		return false;
	}

	function generate_characteristics($arParams) {
		$result = array();
		$allowed_properties = array();

		$categories_map = array(
			'armatura_dlya_sistem_otopleniya' => [
				'Диаметр подключения',
				'Рабочее давление, Бар',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'konvektory_otopleniya_vodyanye' => [
				'Размеры (ВхГхШ)',
				'Мощность нагревателя, кВт',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'kanalizatsionnye_ustanovki' => [
				'Размеры (ВхГхШ)',
				'Мощность нагревателя, кВт',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'obogrevateli_i_teplovye_zavesy' => [
				'Размеры (ВхГхШ)',
				'Мощность нагревателя, кВт',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'bytovye_ventilyatory' => [
				'Размеры (ВхГхШ)',
				'Мощность нагревателя, кВт',
				'Страна производства',
				'Гарантия',
				'Артикул',
				'Цвет'
			],
			'sistema_zashchity_ot_protechek' => [
				'Диаметр подключения',
				'Страна производства',
				'Гарантия',
				'Артикул',
			],
			'uvlazhniteli_i_moyki_vozdukha' => [
				'Обслуживаемая площадь, кв. м',
				'Объём, л',
				'Размеры (ВхГхШ)',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'ventilyatsionnye_ustanovki' => [
				'Мощность нагревателя, кВт',
				'Производительность, м³/час',
				'Размеры (ВхГхШ)',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'sistemy_ochistki_vody' => [
				'Размеры (ВхГхШ)',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'polotentsesushiteli' => [
				'Мощность нагревателя, кВт',
				'Диаметр подключения',
				'Размеры (ВхГхШ)',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'installyatsii' => [
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'cushilki_dlya_ruk' => [
				'Цвет',
				'Размеры (ВхГхШ)',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'generatory' => [
				'Мощность нагревателя, кВт',
				'Уровень шума, дБ',
				'Размеры (ВхГхШ)',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'rehau' => [
				'Диаметр подключения',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'santekhnicheskie_lyuki' => [
				'Размеры (ВхГхШ)',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'inzhenernaya_santekhnika' => [
				'Размеры (ВхГхШ)',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'ochistiteli_vozdukha' => [
				'Обслуживаемая площадь, кв. м',
				'Объём, л',
				'Размеры (ВхГхШ)',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'konditsionery' => [
				'Обслуживаемая площадь, кв. м',
				'Размеры внутреннего блока (ШхВхГ), мм',
				'Размеры внешнего блока (ШхВхГ), мм',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'radiatory_otopleniya' => [
				'Обслуживаемая площадь, кв. м',
				'Размеры (ВхГхШ)',
				'Рабочее давление, Бар',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'nakopitelnye_vodonagrevateli' => [
				'Объём, л',
				'Мощность нагревателя, кВт',
				'Размеры (ВхГхШ)',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'protochnye_vodonagrevateli' => [
				'Мощность,кВт',
				'Производительность, л/мин, при DТ=27 °C',
				'Размеры (ВхГхШ)	',
				'Страна производства',
				'Гарантия',
				'Артикул'
			],
			'teplye_poly' => [
				'Обслуживаемая площадь, кв. м',
				'Мощность нагревателя, кВт',
				'Страна производства',
				'Гарантия',
				'Артикул'
			]
		);

		
		$element = CIBlockElement::GetByID($arParams['element_id'])->GetNext();

		if($element and $arParams['element_id']) {
			do {
				$section = CIBlockSection::GetByID($element['IBLOCK_SECTION_ID'])->GetNext();

				if(isset($categories_map[$section['CODE']])) {
					$allowed_properties = $categories_map[$section['CODE']];
					break;
				} else {
					$element = $section;
				}
			} while($element);		
		}

		$allowed_properties = array_map('trim', $allowed_properties);

		if($arParams["properties"]) {
			foreach($arParams["properties"] as $arProp){
				if(in_array(trim($arProp["NAME"]), $allowed_properties) and $arProp["ID"] != '99') {
					if(isset($arProp['VALUE']) and !empty(trim($arProp['VALUE']))) {
						$result[] = $arProp;
					}
				}
			}
		}

		return $result;
	}

	function generate_sitemap() {
		$result = array();

		$catalog_id=\Bitrix\Main\Config\Option::get("aspro.optimus", "CATALOG_IBLOCK_ID", COptimusCache::$arIBlocks[SITE_ID]['aspro_optimus_catalog']['aspro_optimus_catalog'][0]);

		$arSections = COptimusCache::CIBlockSection_GetList(array('SORT' => 'ASC', 'ID' => 'ASC', 'CACHE' => array('TAG' => COptimusCache::GetIBlockCacheTag($catalog_id), 'GROUP' => array('ID'))), array('IBLOCK_ID' => $catalog_id, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y', 'ACTIVE_DATE' => 'Y', '<DEPTH_LEVEL' =>\Bitrix\Main\Config\Option::get("aspro.optimus", "MAX_DEPTH_MENU", 5)), false, array("ID", "NAME", "PICTURE", "LEFT_MARGIN", "RIGHT_MARGIN", "DEPTH_LEVEL", "SECTION_PAGE_URL", "IBLOCK_SECTION_ID"));

		if($arSections){

			$arTmpResult = array();
			$cur_page = $GLOBALS['APPLICATION']->GetCurPage(true);
			$cur_page_no_index = $GLOBALS['APPLICATION']->GetCurPage(false);

			foreach($arSections as $ID => $arSection){
				if($arSection['IBLOCK_SECTION_ID']){
					if(!isset($arSections[$arSection['IBLOCK_SECTION_ID']]['CHILD'])){
						$arSections[$arSection['IBLOCK_SECTION_ID']]['CHILD'] = array();
					}
					$arSections[$arSection['IBLOCK_SECTION_ID']]['CHILD'][] = &$arSections[$arSection['ID']];
				}

				if($arSection['DEPTH_LEVEL'] == 1){
					$result['categories'][] = &$arSections[$arSection['ID']];
				}
			}
		}

		$result['bottom_links_info'] = $GLOBALS['APPLICATION']->GetMenu( "bottom_info", true );
		$result['bottom_links_company'] = $GLOBALS['APPLICATION']->GetMenu( "bottom_company", true );
		$result['bottom_links_help'] = $GLOBALS['APPLICATION']->GetMenu( "bottom_help", true );

		return $result;
	}