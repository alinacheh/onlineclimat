<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

include_once('functions.php');

if(isset($arParams['module'])) {
	if($arParams['module'] == 'sitemap') {
		if ($this->StartResultCache(7*86400)) {
			$arResult = generate_sitemap();
			$this->IncludeComponentTemplate();
		}
	} elseif ($arParams['module'] == 'characteristics') {
		$arResult = returnResultCache(60*86400, 'characteristics_'.$arParams['element_id'], 'generate_characteristics', $arParams, 'characteristics');
		//$arResult = generate_characteristics($arParams);
		$this->IncludeComponentTemplate();	
	} elseif ($arParams['module'] == 'brand') {
		if ($this->StartResultCache(7*86400,$arParams['brand_id'].'_'.$arParams['iblock_section_id'])) {
			$arResult = generate_brand_block($arParams);
			$this->IncludeComponentTemplate();
		}
	} elseif ($arParams['module'] == 'neighbouring_categories') {
		$arResult = get_neighbouring_categories($arParams);
		$this->IncludeComponentTemplate();
	} elseif ($arParams['module'] == 'canonical') {
		$arResult = set_product_canonical_url($arParams);
		$this->IncludeComponentTemplate();
	}
}
