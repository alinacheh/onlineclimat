<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$html = '<ul class="characteristics-list-block">';

$i = 1;
foreach ($arResult as $prop) {
    if ($i > 4) {
        break;
    }
	$html .= '<li>'.$prop['NAME'].': '.$prop['VALUE'];

	if($prop['CODE'] == 'COUNTRY_OF_ORIGIN') {
		$html .= ' <div class="country_of_origin_flag" style="background-image:url('.SITE_TEMPLATE_PATH.'/images/flags/'.strtolower(transliterate($prop['VALUE'])).'.png)"></div>';
	}

	$html .= '</li>';
	$i++;
}

if (count($prop) > 4) {
    $html .= '<li><a href="#characteristics" class="detail-element-link">Все характеристики</a>';
}

$html .= '</ul>';

echo $html;
/*
$dbRes = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 1, "!PROPERTY_COUNTRY_OF_ORIGIN" => false), array("PROPERTY_COUNTRY_OF_ORIGIN"));
while($row = $dbRes->Fetch()) {
	var_dump(strtolower(transliterate($row['PROPERTY_COUNTRY_OF_ORIGIN_VALUE'])));
}
die();
*/