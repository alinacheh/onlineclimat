<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	function show_simple_menu ( $menu_arr ) {
		foreach( $menu_arr->arMenu as $item ) {
			echo '<li>';
				echo '<a href="'.$item[1].'" >'.$item[0].'</a>';
			echo '</li>';
		}
	}

	function handle_menu_item ( $arItem ) {
		echo '<li>';
		echo '<a href="'.$arItem["SECTION_PAGE_URL"].'" >'.$arItem["NAME"].'</a>';

		if( isset( $arItem["CHILD"] ) ) {
			echo '<ul>';
				foreach( $arItem["CHILD"] as $item ) {
					if( isset( $item["CHILD"] ) ) {
						handle_menu_item ( $item );
					} else {
						echo '<li>';
							echo '<a href="'.$item["SECTION_PAGE_URL"].'" >'.$item["NAME"].'</a>';
						echo '</li>';						
					}
				}
			echo '</ul>';
		}

		echo '</li>';
	}

?>

<ul>
	<? foreach ( $arResult['categories'] as $arItem ) { ?>
		<?php handle_menu_item ( $arItem ) ?>
	<? } ?>

	<? show_simple_menu ( $arResult['bottom_links_info'] ); ?>
	<? show_simple_menu ( $arResult['bottom_links_company'] ); ?>
	<? show_simple_menu ( $arResult['bottom_links_help'] ); ?>
</ul>
