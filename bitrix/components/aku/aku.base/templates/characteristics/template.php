<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$html = '<ul class="characteristics-list-block">';

foreach ($arResult as $prop) {
	$html .= '<li>'.$prop['NAME'].': '.$prop['VALUE'].'</li>';
}

$html .= '</ul>';

echo $html;