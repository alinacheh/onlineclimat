<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$html = '<ul class="characteristics-list-block">';

foreach ($arResult as $prop) {
    if ($prop["CODE"] == 'GARANTY_PICTURE') {
        continue;
    }
	$html .= '<li>'.$prop['NAME'].': '.$prop['VALUE'].'</li>';
}

$html .= '</ul>';

echo $html;