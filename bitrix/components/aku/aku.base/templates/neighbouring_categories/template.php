<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

		<div class="bottom_slider specials neighbouring_slider_wrapp subcat" style="padding-top:0px;">
			<div class="top_blocks">
				<ul class="slider_navigation top custom_flex border">
					<li class="tabs_slider_navigation subcat_nav cur" data-code="subcat">
						<ul class="flex-direction-nav">
						</ul>
					</li>
				</ul>
			</div>

			<h2>Смежные категории</h2>
			
			<ul class="tabs_content" style="">
				<li class="tab subcat_wrapp cur" data-code="subcat" data-unhover="260" data-hover="260" style="margin-top: 30px;">
					<div class="flex-viewport" style="overflow: hidden; position: relative;">
						<ul class="tabs_slider subcat_slides 345 wr" style="width: 2000%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
							<?foreach( $arResult as $arItems ){
								?>
								<li class="catalog_item" id="bx_subcat" style="height:175px;">
								<div class="image_wrapper_block">
								<?if($arItems["PICTURE"]["SRC"]):?>
								<?$img = CFile::ResizeImageGet($arItems["PICTURE"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
								<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=$img["src"]?>" alt="" title="" /></a>
								<?elseif($arItems["~PICTURE"]):?>
								<?$img = CFile::ResizeImageGet($arItems["~PICTURE"], array( "width" => 390, "height" => 220 ), BX_RESIZE_IMAGE_EXACT, true );?>
								<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=$img["src"]?>" alt="" title="" /></a>
								<?else:?>
								<a href="<?=$arItems["SECTION_PAGE_URL"]?>" class="thumb"><img style="display: block; margin:auto;" src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_medium.png" alt="" title="" height="90" /></a>
								<?endif;?>
								</div>
									<a style="font-size:14px;" href="<?=$arItems["SECTION_PAGE_URL"]?>"><span><?=$arItems["NAME"]?></span></a>
								</li>
							<?}?>
						</ul>
					</div>
				</li>
			</ul>
		</div>


		<script type="text/javascript">
			$(document).ready(function(){

				$('.neighbouring_slider_wrapp.subcat .tabs > li').first().addClass('cur');
				$('.neighbouring_slider_wrapp.subcat .slider_navigation > li').first().addClass('cur');
				$('.neighbouring_slider_wrapp.subcat .tabs_content > li').first().addClass('cur');

				var flexsliderItemWidth = 220;
				var flexsliderItemMargin = 12;

				var sliderWidth = $('.neighbouring_slider_wrapp.subcat').outerWidth();
				var flexsliderMinItems = Math.floor(sliderWidth / (flexsliderItemWidth + flexsliderItemMargin));
				$('.neighbouring_slider_wrapp.subcat .tabs_content > li.cur').flexslider({
					animation: 'slide',
					selector: '.tabs_slider .catalog_item',
					slideshow: false,
					animationSpeed: 600,
					directionNav: true,
					controlNav: false,
					pauseOnHover: true,
					animationLoop: true,
					itemWidth: flexsliderItemWidth,
					itemMargin: flexsliderItemMargin,
					minItems: flexsliderMinItems,
					controlsContainer: '.neighbouring_slider_wrapp .tabs_slider_navigation.cur',
					start: function(slider){
						slider.find('li').css('opacity', 1);
					}
				});

				$('.neighbouring_slider_wrapp.subcat .tabs > li').on('click', function(){
					var sliderIndex = $(this).index();
					if(!$('.neighbouring_slider_wrapp.subcat .tabs_content > li.cur .flex-viewport').length){
						$('.neighbouring_slider_wrapp.subcat .tabs_content > li.cur').flexslider({
							animation: 'slide',
							selector: '.tabs_slider .catalog_item',
							slideshow: false,
							animationSpeed: 600,
							directionNav: true,
							controlNav: false,
							pauseOnHover: true,
							animationLoop: true,
							itemWidth: flexsliderItemWidth,
							itemMargin: flexsliderItemMargin,
							minItems: flexsliderMinItems,
							controlsContainer: '.neighbouring_slider_wrapp .tabs_slider_navigation.cur',
						});
						setHeightBlockSlider();
					}

				});

				setHeightBlockSlider();

				$(document).on({
					mouseover: function(e){
						var tabsContentHover = $(this).closest('.neighbouring_slider_wrapp .tab').attr('data-hover') * 1;
						$(this).closest('.neighbouring_slider_wrapp .tab').fadeTo(100, 1);
						$(this).closest('.neighbouring_slider_wrapp .tab').stop().css({'height': tabsContentHover});
						$(this).find('.neighbouring_slider_wrapp .buttons_block').fadeIn(450, 'easeOutCirc');
					},
					mouseleave: function(e){
						var tabsContentUnhoverHover = $(this).closest('.neighbouring_slider_wrapp .tab').attr('data-unhover') * 1;
						$(this).closest('.neighbouring_slider_wrapp .tab').stop().animate({'height': tabsContentUnhoverHover}, 100);
						$(this).find('.neighbouring_slider_wrapp .buttons_block').stop().fadeOut(233);
					}
				}, '.neighbouring_slider_wrapp.subcat .tabs_slider > li');
			})
		</script>