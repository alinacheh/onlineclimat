<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Монтаж VRV-системы Daikin с канальными внутренними блоками в центре Москвы");

$APPLICATION->SetPageProperty("ContentId", 'wide');
$APPLICATION->SetPageProperty("ContentClass", 'wide');
?>

  <link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
  <link rel="stylesheet" href="./PhotoSwipe/photoswipe.css">
  <link rel="stylesheet" href="./PhotoSwipe/default-skin/default-skin.css">
  <link rel="stylesheet" href="./styles.css">


  <div class="our-works">
    <div class="our-works-item">
      <div class="our-works-item__gallery">
        <div class="our-works-item__gallery-main-slider js-our-works-item__gallery-main-slider">
          <div>
            <div class="our-works-item__gallery-main-slider-item js-our-works-item__gallery-main-slider-item">
              <img src="https://picsum.photos/1200/800/?m1" alt="">
            </div>
          </div>
          <div>
            <div class="our-works-item__gallery-main-slider-item js-our-works-item__gallery-main-slider-item">
              <img src="https://picsum.photos/1200/800/?m2" alt="">
            </div>
          </div>
          <div>
            <div class="our-works-item__gallery-main-slider-item js-our-works-item__gallery-main-slider-item">
              <img src="https://picsum.photos/1200/800/?m3" alt="">
            </div>
          </div>
          <div>
            <div class="our-works-item__gallery-main-slider-item js-our-works-item__gallery-main-slider-item">
              <img src="https://picsum.photos/1200/800/?m4" alt="">
            </div>
          </div>
          <div>
            <div class="our-works-item__gallery-main-slider-item js-our-works-item__gallery-main-slider-item">
              <img src="https://picsum.photos/1200/800/?m5" alt="">
            </div>
          </div>
          <div>
            <div class="our-works-item__gallery-main-slider-item js-our-works-item__gallery-main-slider-item">
              <img src="https://picsum.photos/1200/800/?m6" alt="">
            </div>
          </div>
        </div>
        <div class="our-works-item__gallery-add-slider js-our-works-item__gallery-add-slider">
          <div>
            <div class="our-works-item__gallery-add-slider-item">
              <div>
                <img src="https://picsum.photos/1200/800/?l6" alt="">
              </div>
            </div>
          </div>
          <div>
            <div class="our-works-item__gallery-add-slider-item">
              <div>
                <img src="https://picsum.photos/1200/800/?l7" alt="">
              </div>
            </div>
          </div>
          <div>
            <div class="our-works-item__gallery-add-slider-item">
              <div>
                <img src="https://picsum.photos/1200/800/?l8" alt="">
              </div>
            </div>
          </div>
          <div>
            <div class="our-works-item__gallery-add-slider-item">
              <div>
                <img src="https://picsum.photos/1200/800/?l11" alt="">
              </div>
            </div>
          </div>
          <div>
            <div class="our-works-item__gallery-add-slider-item">
              <div>
                <img src="https://picsum.photos/1200/800/?l9" alt="">
              </div>
            </div>
          </div>
          <div>
            <div class="our-works-item__gallery-add-slider-item">
              <div>
                <img src="https://picsum.photos/1200/800/?l10" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="our-works-item__content">
        <p>Установка VRV системы Daikin осуществлялась в городской квартире, расположенной в самом сердце столицы.
          Наружный блок Daikin RXYQ10T был установлен заранее на крыше здания компанией-застройщиком. На объекте были
          использованы внутренние блоки канального типа Daikin мощностью 2,2 кВт, 2,5 кВт и 7,1 кВт.
          Монтаж осуществлен в два этапа. На первом были установлены внутренние блоки кондиционера и сделана разводка
          гибких воздуховодов за потолками. На втором этапе произведены пуско-наладочные работы системы
          кондиционирования и установка декоративных решеток на воздуховоды.</p>
        <a href="#" class="our-works-item__content-button">
          Хочу так же
        </a>
        <h2>Используемые услуги в этой работе:</h2>
        <p>На первом были установлены внутренние блоки кондиционера и сделана разводка
          гибких воздуховодов за потолками. На втором этапе произведены пуско-наладочные работы системы
          кондиционирования и установка декоративных решеток на воздуховоды.</p>
      </div>
    </div>
    <h2>Используемые товары в этой работе:</h2>
    <div class="our-works-products">
      <div class="our-works-products__item">
        <a href="#" class="our-works-products__item-link">
          <div class="our-works-products__item-img">
            <img src="https://picsum.photos/1200/800/?l9" alt="">
          </div>
          <div class="our-works-products__item-title">
            Внутренний блок VRV Daikin FXMQ63P7
          </div>
          <div class="our-works-products__item-price">
            63 900 р.
          </div>
        </a>
      </div>

      <div class="our-works-products__item">
        <a href="#" class="our-works-products__item-link">
          <div class="our-works-products__item-img">
            <img src="https://picsum.photos/1200/800/?l9" alt="">
          </div>
          <div class="our-works-products__item-title">
            Внутренний блок VRV Daikin FXMQ63P7
          </div>
          <div class="our-works-products__item-price">
            63 900 р.
          </div>
        </a>
      </div>

      <div class="our-works-products__item">
        <a href="#" class="our-works-products__item-link">
          <div class="our-works-products__item-img">
            <img src="https://picsum.photos/1200/800/?l9" alt="">
          </div>
          <div class="our-works-products__item-title">
            Внутренний блок VRV Daikin FXMQ63P7
          </div>
          <div class="our-works-products__item-price">
            63 900 р.
          </div>
        </a>
      </div>

      <div class="our-works-products__item">
        <a href="#" class="our-works-products__item-link">
          <div class="our-works-products__item-img">
            <img src="https://picsum.photos/1200/800/?l9" alt="">
          </div>
          <div class="our-works-products__item-title">
            Внутренний блок VRV Daikin FXMQ63P7
          </div>
          <div class="our-works-products__item-price">
            63 900 р.
          </div>
        </a>
      </div>

      <div class="our-works-products__item">
        <a href="#" class="our-works-products__item-link">
          <div class="our-works-products__item-img">
            <img src="https://picsum.photos/1200/800/?l9" alt="">
          </div>
          <div class="our-works-products__item-title">
            Внутренний блок VRV Daikin FXMQ63P7
          </div>
          <div class="our-works-products__item-price">
            63 900 р.
          </div>
        </a>
      </div>

      <div class="our-works-products__item">
        <a href="#" class="our-works-products__item-link">
          <div class="our-works-products__item-img">
            <img src="https://picsum.photos/1200/800/?l9" alt="">
          </div>
          <div class="our-works-products__item-title">
            Внутренний блок VRV Daikin FXMQ63P7
          </div>
          <div class="our-works-products__item-price">
            63 900 р.
          </div>
        </a>
      </div>

      <div class="our-works-products__item">
        <a href="#" class="our-works-products__item-link">
          <div class="our-works-products__item-img">
            <img src="https://picsum.photos/1200/800/?l9" alt="">
          </div>
          <div class="our-works-products__item-title">
            Внутренний блок VRV Daikin FXMQ63P7
          </div>
          <div class="our-works-products__item-price">
            63 900 р.
          </div>
        </a>
      </div>
    </div>
  </div>

  <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <script src="./PhotoSwipe/photoswipe.min.js"></script>
  <script src="./PhotoSwipe/photoswipe-ui-default.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('.js-our-works-item__gallery-main-slider').slick({
        arrows: false,
        asNavFor: '.js-our-works-item__gallery-add-slider'
      });

      $('.js-our-works-item__gallery-add-slider').slick({
        arrows: false,
        centerMode: true,
        focusOnSelect: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.js-our-works-item__gallery-main-slider'
      });

      const pswpElement = document.querySelectorAll('.pswp')[0];

      const items = [
        {
          src: 'https://picsum.photos/1200/800/?m1',
          w: 1200,
          h: 800
        },
        {
          src: 'https://picsum.photos/1200/800/?m2',
          w: 1200,
          h: 800
        },
        {
          src: 'https://picsum.photos/1200/800/?m3',
          w: 1200,
          h: 800
        },
        {
          src: 'https://picsum.photos/1200/800/?m4',
          w: 1200,
          h: 800
        },
        {
          src: 'https://picsum.photos/1200/800/?m5',
          w: 1200,
          h: 800
        },
        {
          src: 'https://picsum.photos/1200/800/?m6',
          w: 1200,
          h: 800
        }
      ];

      const mainPhotos = document.querySelectorAll('.js-our-works-item__gallery-main-slider-item');
      [].forEach.call(mainPhotos, function (photo, index) {

        photo.addEventListener('click', function () {
          const options = {
            index: index - 1,
            shareEl: false,
          };

          const gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
          gallery.init();
        });
      })
    });
  </script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>