<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Наши работы");
$APPLICATION->SetPageProperty("ContentId", 'wide');
$APPLICATION->SetPageProperty("ContentClass", 'wide');
?>
  <link rel="stylesheet" href="./styles.css">


  <div class="our-works">

    <div class="our-works-categories">
      <div>
        <a href="#">Квартиры</a>
      </div>
      <div>
        <a href="#">Коттеджи</a>
      </div>
      <div>
        <a href="#">Офисы</a>
      </div>
      <div>
        <a href="#">Кафе</a>
      </div>
      <div>
        <a href="#">Рестораны</a>
      </div>
    </div>

    <div class="our-works-list">

      <a href="#" class="our-works-list__item">
        <div class="our-works-list__item-wrapper">
          <img src="https://picsum.photos/600/400/?l1" alt="">
          <div>
            Установка сплит систем Mitsubishi Heavy в два этапа
          </div>
        </div>
      </a>

      <a href="#" class="our-works-list__item">
        <div class="our-works-list__item-wrapper">
          <img src="https://picsum.photos/600/400/?l2" alt="">
          <div>
            Установка сплит систем Mitsubishi Heavy в два этапа
          </div>
        </div>
      </a>

      <a href="#" class="our-works-list__item">
        <div class="our-works-list__item-wrapper">
          <img src="https://picsum.photos/600/400/?l3" alt="">
          <div>
            Установка сплит систем Mitsubishi Heavy в два этапа
          </div>
        </div>
      </a>

      <a href="#" class="our-works-list__item">
        <div class="our-works-list__item-wrapper">
          <img src="https://picsum.photos/600/400/?l4" alt="">
          <div>
            Установка сплит систем Mitsubishi Heavy в два этапа
          </div>
        </div>
      </a>

      <a href="#" class="our-works-list__item">
        <div class="our-works-list__item-wrapper">
          <img src="https://picsum.photos/600/400/?l5" alt="">
          <div>
            Установка сплит систем Mitsubishi Heavy в два этапа
          </div>
        </div>
      </a>

      <a href="#" class="our-works-list__item">
        <div class="our-works-list__item-wrapper">
          <img src="https://picsum.photos/600/400/?l6" alt="">
          <div>
            Установка сплит систем Mitsubishi Heavy в два этапа
          </div>
        </div>
      </a>
    </div>
  </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>