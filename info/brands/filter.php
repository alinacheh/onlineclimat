<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

$objectType = null;
$arObjectTypes = array();

use Bitrix\Highloadblock\HighloadBlockTable;

CModule::IncludeModule('highloadblock');

$arHLBlock = HighloadBlockTable::getById(5)->fetch();
$obEntity = HighloadBlockTable::compileEntity($arHLBlock);
$strEntityDataClass = $obEntity->getDataClass();
$resData = $strEntityDataClass::getList(array(
	'select' => array('UF_NAME','UF_XML_ID'),
	'order' => array('ID' => 'ASC'),
	'limit' => 100
));
while ($arRes = $resData->fetch()) {
	$arObjectTypes[$arRes['UF_XML_ID']] = $arRes['UF_NAME'];
}

if (!empty($_REQUEST['object_type']) && array_key_exists($_REQUEST['object_type'], $arObjectTypes)) {
	$objectType = $_REQUEST['object_type'];
	$GLOBALS['arrPortfolioFilter'] = array('PROPERTY_TYPE' => $objectType);
}

if (!empty($arObjectTypes)):
	?>
	<div class="portfolio-type-filter-wrap">
		<select id="portfolio-type-filter-select">
			<option value="">Все объекты</option>
			<?foreach ($arObjectTypes as $typeCode => $typeName):?>
				<option value="<?=$typeCode;?>" <?if ($typeCode === $objectType):?>selected="selected"<?endif;?>><?=$typeName;?></option>
			<?endforeach;?>
		</select>
	</div>
	<script>
		$('#portfolio-type-filter-select').on('change', function(e) {
			var value = e.target.value;
			if (value.length) {
				location.href = '/info/brands/types/' + value + '/';
			} else {
				location.href = '/info/brands/';
			}
		});
	</script>
	<?
endif;?>