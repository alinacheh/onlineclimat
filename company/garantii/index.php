<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Гарантии");
?> 
<h2>Радиаторы отопления</h2>
 
<table class="garantii__table">
   <tbody>
      <tr style="">
         <td>Бренд
            <br>
         </td>
         <td>Срок гарантии
            <br>
         </td>
      </tr>
      <tr>
         <td> <img src="/upload/medialibrary/e5d/e5dc819710109aa729b1872752f2c091.jpg" border="0" alt="zender-min.jpg" width="100">Zehnder
            <br>
         </td>
         <td>10 лет
            <br>
         </td>
      </tr>
      <tr>
         <td> <img src="/upload/medialibrary/a5a/a5ab79989378b635fe9c9179c0f58c05.jpg" border="0" alt="irsap-min.jpg" width="100">Irsap
            <br>
         </td>
         <td>20 лет на серию Теsi ГОСТ P
            <br>
            2 года на остальные радиаторы
            <br>
         </td>
      </tr>
      <tr>
         <td> <img src="/upload/medialibrary/747/747fd8e193660d8da86b8593082f5af5.jpg" border="0" alt="arbonia-min.jpg" width="100">Arbonia
            <br>
         </td>
         <td>3 года
            <br>
         </td>
      </tr>
      <tr>
         <td> <img src="/upload/medialibrary/30b/30b7cd5b988795c9d5b5c2b622101f13.jpg" border="0" alt="kzto-min.jpg" width="100">КЗТО
            <br>
         </td>
         <td>5 лет
            <br>
         </td>
      </tr>
      <tr>
         <td> <img src="/upload/medialibrary/ea0/ea04cccd9bade5ed1c72c48807312c1a.jpg" border="0" alt="solira-min.jpg" width="100">Solira
            <br>
         </td>
         <td>5 лет
            <br>
         </td>
      </tr>
      <tr>
         <td> <img src="/upload/medialibrary/854/854cbc87be276b354702cd2805daecb5.jpg" border="0" alt="loten.jpg" width="100">Loten
            <br>
         </td>
         <td>5 лет
            <br>
         </td>
      </tr>
      <tr>
         <td><img src="/upload/medialibrary/57b/57b4dc24fdd1c3193125559bff777a0b.jpg" border="0" alt="loten.jpg" width="100">Rifar</td>
         <td>25 лет серия MONOLIT

            <br>
10 лет остальные модели
         </td>
      </tr>
      <tr>
         <td><img src="/upload/medialibrary/247/2474adeb89c2da15da9aed8c68819f3a.jpg" border="0" alt="loten.jpg" width="100">Royal Thermo</td>
         <td>25 лет     
         </td>
      </tr>
      <tr>
         <td><img src="/upload/medialibrary/59a/59a12299b0f27314484b4ae5ee70c8fb.jpg" border="0" alt="loten.jpg" width="100">Global</td>
         <td> 10 лет        
         </td>
      </tr>
      <tr>
         <td><img src="/upload/medialibrary/5ba/5ba57b46aa864ad5ee156e9075b5b991.jpg" border="0" alt="loten.jpg" width="100">Terma</td>
         <td>8 лет
         </td>
      </tr>
            <tr>
         <td><img src="/upload/medialibrary/0b8/0b86ff45c48b443baecb48214cfe708f.jpg" border="0" alt="loten.jpg" width="100">Varmann</td>
         <td>10 лет
         </td>
      </tr>
            <tr>
         <td><img src="/upload/medialibrary/4f3/4f31cc46c43d77d1e8c06a39fef2a01b.jpg" border="0" alt="loten.jpg" width="100">Buderus Logatrend</td>
         <td>5 лет

      </tr>
            <tr>
         <td><img src="/upload/medialibrary/4d9/4d95eb337ec19a75af39fb9c1efaa5a4.jpg" border="0" alt="loten.jpg" width="100">Kermi</td>
         <td>5 лет

      </tr>
            <tr>
         <td><img src="/upload/medialibrary/440/4404791a37087441a60858e4f0d214ee.jpg" border="0" alt="loten.jpg" width="100">Stelrad</td>
         <td>10 лет

      </tr>
            <tr>
         <td><img src="/upload/medialibrary/981/98108689d3b36f39afdc6fc5908b141c.jpg" border="0" alt="loten.jpg" width="100">Exemet</td>
         <td>5 лет

      </tr>
            <tr>
         <td><img src="/upload/medialibrary/448/4487f35c4f85fb7594a5271820c8d768.jpg" border="0" alt="loten.jpg" width="100">RETROstyle</td>
         <td>5 лет
      </tr>
   </tbody>
</table>



<style>
	table.garantii__table {
    width: 100%;
    border-collapse: collapse;
}

table.garantii__table td {
    padding: 20px;
    border: 1px solid #eee;
    text-align: center;
    vertical-align: middle;
}

table.garantii__table td img {
    display: inline-block;
    vertical-align: middle;
    margin-right: 10px;
}

table.garantii__table tr:nth-child(1) {
    background: #eee;
}
</style>
 
<p>&nbsp;</p>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>