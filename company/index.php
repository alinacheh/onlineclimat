<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?> 
<p align="justify"> Компания «Онлайн Климат» благодарит Вас за интерес, проявленный к нам! Мы - команда экспертов рынка климатической техники. Наши специалисты знают ответ на вопрос «Как сделать погоду в Вашем доме или офисе благоприятной».</p>
 
<div align="center"><img src="/upload/medialibrary/33a/33af391ad13ca89aeb7165046923f27a.jpeg" border="0" width="455" height="342"  /> <img src="/upload/medialibrary/3f6/3f6386d2e8b0afef35868570e23f110a.jpeg" border="0" width="455" height="342"  /></div>
 
<p align="justify">Основываясь на передовых разработках ведущих производителей, мы предлагаем лучшие решения по созданию благоприятного микроклимата для Вашего дома или офиса</p>
 
<p align="justify">Наши специалисты помогут Вам сэкономить время и деньги, проконсультируют по любым вопросам, помогут подобрать оптимальный вариант оборудования и материалов, а также произведут монтаж и пуско-наладку системы.</p>
 
<p align="justify">«Онлайн Климат» профессионально оказывает полный комплекс услуг в области проектирования, комплектации, монтажа, пусконаладочных работ, гарантийного и сервисного обслуживания систем кондиционирования и вентиляции воздуха, отопления, теплоснабжения, водоснабжения и теплого пола.</p>
 
<div align="center"><img src="/upload/medialibrary/c68/c681de50274e0250def071ac80b7a0cb.jpeg" border="0" width="400" height="533"  /> <img src="/upload/medialibrary/edb/edb089cc7249b96bffd5842b90f8e35e.jpeg" title="o" border="0" width="400" height="533"  /></div>
 
<p align="justify">Наша компания выполняет работы полного цикла:</p>
 
<ul> 		 
  <li>Предпроектное обследование объекта, </li>
 		 
  <li>Комплектация объектов высококачественными материалами и оборудованием, </li>
 		 
  <li>Монтажные работы, </li>
 		 
  <li>Гарантийное и сервисное обслуживание, </li>
 		 
  <li>Пусконаладочные работы, </li>
 		 
  <li>Информационная поддержка, консультирование и обучение персонала, </li>
 		 
  <li>Проектные работы, </li>
 		 
  <li>Подготовка рабочей документации и сдача объекта «под ключ».</li>
 	</ul>
 
<p align="justify"> «Онлайн Климат» в своей работе использует оборудование и материалы известных мировых производителей, представленных на рынке. Всё оборудование имеет высокие стандарты качества и надежности, сертифицировано и имеет длительный срок гарантии.</p>
 
<div align="center"><img src="/upload/medialibrary/360/3600759ed6dfd3728bc56969d92705fa.jpeg" border="0" width="400" height="533"  /> <img src="/upload/medialibrary/44e/44e56269f8b787efe25318f91100c641.jpeg" border="0" width="400" height="533"  /> </div>
 
<p align="justify">Мы работаем только с проверенными поставщиками.</p>
 
<p align="justify">Поставка оборудования, включает в себя не только основное оборудование, но и широкий спектр дополнительных товаров, способствующих достижению максимального комфорта и удобства при использовании техники. На всех этапах работы с заказчиком – от выполнения проекта до сервисного обслуживания – Вы можете быть уверены в профессионализме и компетентности сотрудников нашей компании. </p>
 
<h2 id="sert">Сертификаты</h2>
 
<div align="center"> <img src="/upload/medialibrary/298/298d18c2b94c6449acd0ff3adf293a89.jpg" title="Сертификат Zehnder" border="0" alt="Сертификат Zehnder" width="212" height="300"  /> <img src="/upload/medialibrary/a07/a07e05b8d2e2c6d9ba681d75088b93bd.jpeg" height="300" title="Abion" alt="Abion"  /><img src="/upload/medialibrary/905/90563da6e2a3a926235623aba03c77d0.jpeg" height="300" title="Daikin" alt="Daikin"  /> <img src="/upload/medialibrary/548/5488cb197501994d41371a33d5eea79a.jpeg" height="300" title="Varmann" alt="Varmann"  /> 
  <br />
 
  <br />
 <img src="/upload/medialibrary/d92/d92d606022356162db0422777e058b0d.jpeg" height="300" title="Shivaki" alt="Shivaki"  /> <img src="/upload/medialibrary/fc1/fc1e9ac61cbb1b15558bb2ab3964d79a.jpeg" height="300" title="Noirot" alt="Noirot"  /><img alt="Electrolux" src="/upload/medialibrary/65b/65b012c300014f866445989833cc2cc0.jpg" height="300" title="Electrolux"  /><img height="300" alt="Mitsubishi" src="/upload/medialibrary/c6c/c6c9319d826bbda2aa7e697615f640a3.jpg" title="Mitsubishi"  /> 
  <br />
 
  <br />
 <img src="/upload/medialibrary/7de/7def2191fe625e8249b7fb2fea8c4650.jpeg" style="height: 300px;" title="Cherbrooke" alt="Cherbrooke"  /><img style="height: 300px;" src="/upload/medialibrary/adc/adc0bee176e2814c21875af9e97bdc19.jpg" title="Irsap" alt="Irsap"  /> </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>