<img src="/images/sale/ban-akcia-tece.jpg">
<p>Сантехническое оборудование от немецкой компании с мировым именем Tece доступно по выгодной акции! Интернет-магазин «Онлайн Климат» предлагает скидку 10% на следующие группы товаров:</p>
<ul>
<li><a href="/catalog/inzhenernaya_santekhnika/drenazhnye-kanaly-i-dushevye-trapy-tece/gotovye_resheniya/">готовые решения для монтажа дренажных каналов, </a></li>
<li><a href="/catalog/inzhenernaya_santekhnika/drenazhnye-kanaly-i-dushevye-trapy-tece/dushevye_trapy_tece/">душевые трапы для дренажных систем, </a></li>
<li><a href="/catalog/installyatsii/teceprofil/komplekty/">комплекты инсталляций для подвесных унитазов. </a></li>
</ul>
<p>Сделать ремонт в ванной комнате легко с прочными системами и оборудованием Tece.</p>

<hr class="long">