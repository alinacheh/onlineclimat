<img src="/images/sale/ban-akcia-irsap.jpg">
В интернет-магазине «Онлайн Климат» можно приобрести итальянские радиаторы Irsap со скидкой в 35%! Серия Tesi полностью соответствует ГОСТ и имеет самую длинную среди стальных трубчатых радиаторов гарантию в 20 лет. Радиаторы застрахованы от протекания и станут отличным вариантом для дизайнерского ремонта. 
Скидка действует на модели с <a href="/catalog/radiatory_otopleniya/stalnye_trubchatye/irsap-radiator/irsap_bokovoe_podklyuchenie/">боковым</a> и <a href="https://on-lineclimat.ru/catalog/radiatory_otopleniya/stalnye_trubchatye/irsap-radiator/irsap_nizhnee_podklyuchenie/">нижним</a> подключением. Товары в наличии можно забрать самовывозом в день заказа со склада на Нахимовском. 


<hr class="long">