<img src="/images/sale/ban-akcia-Mitsubishi.jpg">
<p>Готовьтесь к летней жаре уже сейчас! Закажите два кондиционера Mitsubishi Electric и получите скидку 10% на покупку! Расчет окончательной цены производится при добавлении товаров в корзину. Стоимость в каталоге указана без скидок.</p>

<p>В интернет-магазине «Онлайн Климат» доступны <a href="/catalog/konditsionery/nastennye_split_sistemy/mitsubishi_electric/">настенные сплит-системы</a>, <a href="/catalog/konditsionery/multi_split_sistemy/vnutrennie_bloki/mitsubishi_electric_/">мульти</a>, <a href="/catalog/konditsionery/kanalnye_split_sistemy/mitsubishi_electric_2/">канальные</a> и <a href="/catalog/konditsionery/kassetnye_split_sistemy/mitsubishi_electric4/">кассетные</a> популярных серий от простых до дизайнерских.</p>


<hr class="long">