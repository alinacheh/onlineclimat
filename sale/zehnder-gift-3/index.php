<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Подарок Увлажнитель Royal Clima Мурррзио Чёрный к радиаторам Zehnder 3180 TL от 6 до 12 секций");
CModule::IncludeModule("sale");
	// Теперь достаем новым методом скидки с условиями. P.S. Старым методом этого делать не нужно из-за очень высокой нагрузки (уже тестировал)
	$actions = \Bitrix\Sale\Internals\DiscountTable::getList(array(
	  'select' => array("ID","ACTIONS_LIST","CONDITIONS_LIST"),
	  'filter' => array("ACTIVE"=>"Y","USE_COUPONS"=>"N","DISCOUNT_TYPE"=>"P","ID"=>98,
	  array(
		"LOGIC" => "OR",
		array(
		  "<=ACTIVE_FROM"=>$DB->FormatDate(date("Y-m-d H:i:s"),"YYYY-MM-DD HH:MI:SS",\CSite::GetDateFormat("FULL")),
		  ">=ACTIVE_TO"=>$DB->FormatDate(date("Y-m-d H:i:s"),"YYYY-MM-DD HH:MI:SS",\CSite::GetDateFormat("FULL"))
		),
		array(
		  "=ACTIVE_FROM"=>false,
		  ">=ACTIVE_TO"=>$DB->FormatDate(date("Y-m-d H:i:s"),"YYYY-MM-DD HH:MI:SS",\CSite::GetDateFormat("FULL"))
		),
		array(
		  "<=ACTIVE_FROM"=>$DB->FormatDate(date("Y-m-d H:i:s"),"YYYY-MM-DD HH:MI:SS",\CSite::GetDateFormat("FULL")),
		  "=ACTIVE_TO"=>false
		),
		array(
		  "=ACTIVE_FROM"=>false,
		  "=ACTIVE_TO"=>false
		),
	  ))
	));


	// Перебираем каждую скидку и подготавливаем условия фильтрации для CIBlockElement::GetList
	while($arrAction = $actions->fetch()){

		foreach($arrAction["ACTIONS_LIST"]["CHILDREN"][0]["CHILDREN"][0]["CHILDREN"] as $arSeccc){
			if($arSeccc["DATA"]["value"] > 0)
				$arrActions[$arSeccc["DATA"]["value"]]['ID'] = $arSeccc["DATA"]["value"];
		}

		if($arrAction["CONDITIONS_LIST"]["CHILDREN"][0]["CHILDREN"][0]["DATA"]["value"]){
			$arrActions[$arrAction['ID']]['ID'] = $arrAction["CONDITIONS_LIST"]["CHILDREN"][0]["CHILDREN"][0]["DATA"]["value"];
		}
	}


	$arSection["SECTION_FILTER"] = [];
	foreach($arrActions as $arSect){
		$arSection["SECTION_FILTER"][] = $arSect["ID"];

		$rsParentSection = CIBlockSection::GetByID($arSect["ID"]);
		if ($arParentSection = $rsParentSection->GetNext())
		{
		   $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],
							"ACTIVE" => "Y", '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],
							'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']
						);
		   $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter, false, array("ID", "NAME", "IBLOCK_SECTION_ID"));
		   while ($arSect_w = $rsSect->GetNext())
		   {
			   //$arSection["SECTION_FILTER"][] = $arSect_w;
			   $arSection["SECTION_FILTER"][] = $arSect_w["ID"];
		   }
		}
	}

// Вывод подарков
foreach($arSection["SECTION_FILTER"][0] as $key => $ids)
{
	$arFilter = Array("IBLOCK_ID"=>1, "ID"=>$ids, "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, false);
	while($ob = $res->GetNext())
	{
		global $arrFilter;
		$arrFilter["ID"][] = $ob["ID"];

	}
}


?>
<?if($_GET["PAGEN_1"] == 1 || !isset($_GET["PAGEN_1"])){?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "page",
	"AREA_FILE_SUFFIX" => "inc",
	"EDIT_TEMPLATE" => "standard.php"
	),
	false
);?>
<?}?>
<?if(!empty($arrFilter)){?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"catalog_block_sale",
	array(
		"SEF_MODE" => "Y",
		"SEF_RULE" => "",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "1",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "CATALOG_PRICE_1",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "asc",
		"USE_FILTER" => "Y",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"PAGE_ELEMENT_COUNT" => "20",
		"LINE_ELEMENT_COUNT" => "4",
		"PROPERTY_CODE" => array(
			0 => "54",
			1 => "CML2_LINK",
			2 => "",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "NAME",
			1 => "CML2_LINK",
			2 => "DETAIL_PAGE_URL",
			3 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "CML2_ARTICLE",
			1 => "SIZE",
			2 => "COLOR",
			3 => "CONF",
			4 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFERS_LIMIT" => "2",
		"PRICE_CODE" => array(
			0 => "main",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"USE_PRODUCT_QUANTITY" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"BACKGROUND_IMAGE" => "-",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"COMPATIBLE_MODE" => "Y",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"CUSTOM_FILTER" => "",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_OLD_PRICE" => "Y",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"CONVERT_CURRENCY" => "Y",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"DISPLAY_COMPARE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"COMPONENT_TEMPLATE" => "catalog_block_sale",
		"SECTION_CODE_PATH" => "",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CURRENCY_ID" => "RUB"
	),
	false
);?>
<?}?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>