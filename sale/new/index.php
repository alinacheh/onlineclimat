<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Акции");

global $arrFilter;
$arrFilter = array(
    array(
        "LOGIC" => "OR",
        array("!PROPERTY_IS_NEW" => false),
        array("!PROPERTY_IS_POP" => false),
        array("!PROPERTY_PROP_MONT_GIFT" => false),
        array("!PROPERTY_PROP_FREE_DIV" => false),
        array("!PROPERTY_IS_PRESENT" => false),
    ),
);


$arSort = Array('ID' => 'DESC');
$arSelect = Array("ID", "NAME", "PROPERTY_IS_NEW", "PROPERTY_IS_POP", "PROPERTY_PROP_MONT_GIFT",
"PROPERTY_PROP_FREE_DIV", "PROPERTY_IS_PRESENT");

$arFilter = Array(
	'IBLOCK_ID' => 1,
	'ACTIVE' => 'Y',
	array(
		"LOGIC" => "OR",
		array('!PROPERTY_IS_NEW' => false),
		array('!PROPERTY_IS_POP' => false),
	),
);
$res = CIBlockElement::GetList($arSort, $arFilter, false, Array("nPageSize"=>500), $arSelect);
while($ob = $res->GetNext())
{
	$arrFilter["ID"][] = $ob["ID"];
}
/*$arFilter = Array(
	'IBLOCK_ID' => 1,
	'ACTIVE' => 'Y',
	array(
		"LOGIC" => "OR",
		array('!PROPERTY_PROP_MONT_GIFT' => false),
		array('!PROPERTY_PROP_FREE_DIV' => false),
	),
);
$res = CIBlockElement::GetList($arSort, $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNext())
{
	$arrFilter["ID"][] = $ob["ID"];
}
$arFilter = Array(
	'IBLOCK_ID' => 1,
	'ACTIVE' => 'Y',
	'!PROPERTY_IS_PRESENT' => false,
);
$res = CIBlockElement::GetList($arSort, $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNext())
{
	$arrFilter["ID"][] = $ob["ID"];
}


echo "<pre>";
var_dump($arrFilter["ID"]);
echo "</pre>";
*/

?>
<div class="block">
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"catalog_block",
	array(
		"SEF_MODE" => "Y",
		"SEF_RULE" => "",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "1",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "CATALOG_PRICE_1",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "asc",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"PAGE_ELEMENT_COUNT" => "20",
		"LINE_ELEMENT_COUNT" => "4",
		"PROPERTY_CODE" => array(
			0 => "54",
			1 => "CML2_LINK",
			2 => "",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "NAME",
			1 => "CML2_LINK",
			2 => "DETAIL_PAGE_URL",
			3 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "CML2_ARTICLE",
			1 => "SIZE",
			2 => "COLOR",
			3 => "CONF",
			4 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFERS_LIMIT" => "2",
		"PRICE_CODE" => array(
			0 => "main",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"USE_PRODUCT_QUANTITY" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"BACKGROUND_IMAGE" => "-",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"COMPATIBLE_MODE" => "Y",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"CUSTOM_FILTER" => "",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"CONVERT_CURRENCY" => "Y",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"DISPLAY_COMPARE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"COMPONENT_TEMPLATE" => "catalog_block",
		"SECTION_CODE_PATH" => "",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CURRENCY_ID" => "RUB"
	),
	false
);?>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>