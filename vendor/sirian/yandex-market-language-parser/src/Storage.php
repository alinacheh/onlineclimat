<?php

namespace Sirian\YMLParser;

class Storage
{
    public $file;

    protected $shopXML;

    public $offers;

    protected $offersCount = 0;

    public function __construct()
    {
        $this->file = tmpfile();
    }

    public function addOfferXML($xml)
    {
        $this->offersCount++;
        fputs($this->file, json_encode($xml, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES) . "\n");

        $this->offers[] = json_encode($xml, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    }

    public function getShopXML()
    {
        return $this->shopXML;
    }

    public function setShopXML($shopXML)
    {
        $this->shopXML = $shopXML;

        return $this;
    }

    public function getNextOfferXML()
    {
        fseek($this->file, 0);
        while ($line = fgets($this->file)) {
            yield $line;
        }
    }

    public function getOffersCount()
    {
        return $this->offersCount;
    }
}
