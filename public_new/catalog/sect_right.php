<aside class="right filter">
	<div id="smart_filter"></div>
	<?if ($APPLICATION->GetPageProperty('CatalogElement') == 'Y'):
		$b_id = $APPLICATION->GetPageProperty("BRAND_IBLOCK");
		$b_name = $APPLICATION->GetPageProperty("BRAND");
		$service = $APPLICATION->GetPageProperty("SERVICE");?>
		<?$APPLICATION->IncludeComponent("custom:empty", "catalog_info", Array(
			"IBLOCK_TYPE" => "catalog",
			"IBLOCK_ID" => "0",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "3600",
			"SHOW_DELIVERY_PRICE" => "Y",
			"BRAND_ID" => $b_id,
			"BRAND_NAME" => $b_name,
			"SERVICE" => $service
			),
			false
		);?>
	<?else:?>
		<script>$(function(){if($('#section_article article').length < 1) $('#section_article').remove();})</script>
		<?
			$sid = $APPLICATION->GetPageProperty("SECTION_ID"); if (!$sid) $sid=-1;
			$GLOBALS['sideFilter']['PROPERTY_CATALOG_SECTION'] = $sid;
		?>
		<div class="articles" id="section_article">
		 	<br />
     		<?$APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"list_ext",
				Array(
					"IBLOCK_TYPE" => "info",
					"IBLOCK_ID" => "0",
					"NEWS_COUNT" => "3",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "sideFilter",
					"FIELD_CODE" => array(0=>"",1=>"",),
					"PROPERTY_CODE" => array(0=>"",1=>"",),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"CACHE_NOTES" => "",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"PREVIEW_TRUNCATE_LEN" => "150",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "N",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "Статьи",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"DISPLAY_DATE" => "N",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "N",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_OPTION_ADDITIONAL" => ""
				)
			);?>
		</div>
	<?endif?>
	<?$APPLICATION->IncludeComponent("bitrix:sale.viewed.product", "template1", Array(
		"VIEWED_COUNT" => "2",
		"VIEWED_NAME" => "Y",
		"VIEWED_IMAGE" => "Y",
		"VIEWED_PRICE" => "N",
		"VIEWED_CANBUY" => "N",
		"VIEWED_CANBUSKET" => "Y",
		"VIEWED_IMG_HEIGHT" => "130",
		"VIEWED_IMG_WIDTH" => "130",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"SET_TITLE" => "N",
		),
		false
	);?>
</aside>