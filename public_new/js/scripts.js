var timer_1; var timer_tabs;
$(document).ready(function(e) {

    $('#content p:empty').remove();
	if ($('#content').hasClass('catalog_element')) {
		$('#tovar .right .title').prepend($('h1'));
		$('#content').prepend($('#path').addClass('alt').css('max-width','95%')).prepend('<div class="print right"><a target="_blank" href="?print=Y"></a></div>');
	} else if ($('#content').hasClass('left')) {
		$('#content').prepend($('h1').css('max-width', $('#content').width()));
	}
	//if (move_h1) $('#content').prepend($('h1').css('padding-bottom', '15px'));
	$('#pagination').html($('#pagination_hidden').html());
	$('#path span:last').addClass('current');
	var basket_t = $('#basket .title').offset();
	$('#add_in_basket').css('top',basket_t.top).css('left',basket_t.left+95);
	if ($('#no_items').length) $('#view, #pagination, #sort2 .left, table.view3').hide();
	if ($('.filtren .acc').length<1) $('div.filter').hide();
	$("input[type=text], textarea").textPlaceholder();
    $("#comparator_inner > .right").mCustomScrollbar();
	$('.box .images').jcarousel({
        vertical: true,
        itemFallbackDimension: 75
    });
	$(".catalog_element input[type=radio], .subscription-format input[type=radio]").styler();
	$("#tovar .img.big a").lightBox({
		/*fixedNavigation: false,*/
		matched: '#tovar li.img a',
		site_dir: SITE_DIR()
	});
	$('.box .images .img a').click(function(e){
		e.preventDefault();
		$('.box .images .img').removeClass("current");
		$(this).parent(".img").addClass("current");
		$(this).attr("href")
		$("#tovar .box .img.big a").attr("href", $(this).attr("href"));
		//$("#tovar .box .img.big img").attr("src", $(this).attr("href"));
		$("#tovar .box .img.big img").attr("src", $(this).attr("data-src"));
		return false;
    });

	$(".pm .m").click(function(e) {
		e.preventDefault();
		var parent = $(this).parents(".pm");
		var url = parent.next("input"); if (!url.length) url = parent.prev("input");
		var count = url.attr("value");
		if (count > 0) {url.attr("value", parseInt(count)-1)}
		if (parent.hasClass('submit')) parent.parents('form').submit();
	});
	$('.basketitem input.count.submit').change(function(){$(this).parents('form').submit();})
	$(".pm .p").click(function(e) {
		e.preventDefault();
		var parent = $(this).parents(".pm");
		var url = parent.next("input"); if (!url.length) url = parent.prev("input");
		var count = url.attr("value");
		url.attr("value", parseInt(count)+1)
		if (parent.hasClass('submit')) parent.parents('form').submit();
	});

	$(".window").hide();
	//$(".window2").hide();
	$('#comparator').hide();

	$(".relative .btn").click(function(e)
	{
		var Link = $(this);
		var Window = $("+ .window",this);
		if (!Window.length || Link.parents('.window') == Window) return;
		$(".window").fadeOut(200);
		e.preventDefault();
		Window.children('small').hide();
		Window.children('form').show();
		var active = $(this).attr('data-active');
		if(active == 1)
		{
			Window.fadeOut(200);
			$(this).attr('data-active', '0');
		}
		else
		{
			Window.fadeIn(200);
			$(this).attr('data-active', '1');
		}
	});
	$(".window").mouseup(function()
	{
		return false
	});
	$(".relative .btn").mouseup(function()
	{
		var st = $(this).attr('data-active');
		$(".relative .btn").attr('data-active', '0');
		if(st=='1'){
			$(this).attr('data-active', '1');
		}
		else {
			$(this).attr('data-active', '0');
		}
		return false
	});
	$(document).mouseup(function()
	{
		$(".window").fadeOut(200);
		$(".relative .btn").attr('data-active', '0');
	});
	$(".window > .close").mouseup(function()
	{
		$(".window").fadeOut(200);
		$(".relative .btn").attr('data-active', '0');
		$("#shadow").fadeOut();
	});
	$(".window2 > .close").mouseup(function()
	{
		$(".window2").fadeOut(200);
		$(".relative .btn").attr('data-active', '0');
		$("#shadow").fadeOut();
	});
	/*$("div.compareList a").mouseup(function(e){
		e.preventDefault();
		$(".window2.compareList").fadeIn();
		$("#shadow").fadeIn();
	})*/
	$("#shadow").mouseup(function()
	{
		$("#comparator").fadeOut(200);
		$(".relative .btn").attr('data-active', '0');
		$("#shadow").fadeOut();
	});


	$("div.acc > div").hide();
	$("div.acc > a.current + div").fadeIn(300);
	$("div.acc > a:not(.current)").toggle(
		function(e){
			e.preventDefault();
			$(this).addClass("current");
			$("+div", this).fadeIn(300);
		},
		function(e){
			e.preventDefault();
			$(this).removeClass("current");
			$("+div", this).fadeOut(0);
	});
	$("div.acc > a.current").toggle(
		function(e){
			e.preventDefault();
			$(this).removeClass("current");
			$("+div", this).fadeOut(0);
		},
		function(e){
			e.preventDefault();
			$(this).addClass("current");
			$("+div", this).fadeIn(300);
	});

	$('.slider').each(function(){
		var did = $(this).data('id')
		,	min_val = parseInt($('.min-price'+did).val())
		,	max_val = parseInt($('.max-price'+did).val())
		,	step = Math.round((max_val-min_val) / 100)
		,	slider = $(this);
		$('.min-price'+did).data('orig', min_val);
		$('.max-price'+did).data('orig', max_val);
		slider.slider({
			'min': min_val,
			'max': max_val,
			'values': [min_val, max_val],
			step: step > 10 ? step : 10,
			range: true,
			stop: function(event, ui) {
				$('.min-price'+did).val(slider.slider("values",0));
				$('.max-price'+did).val(slider.slider("values",1));
			},
			slide: function(event, ui){
				$('.min-price'+did).val(slider.slider("values",0));
				$('.max-price'+did).val(slider.slider("values",1));
			}
		});
		$('.min-price').change(function(){
			var value1=$('.min-price'+did).val();
			var value2=$('.max-price'+did).val();
			if (parseInt(value1) > parseInt(value2)){
				value1 = value2;
				$('.min-price'+did).val(value1);
			}
			if (value1 < min_val) { value1 = min_val; min.val(value1)}
			$('#slider'+did).slider("values",0,value1);
		});
		$('.max-price').change(function(){
			var value1=$('.min-price'+did).val();
			var value2=$('.max-price'+did).val();
			if (value2 > max_val) { value2 = max_val; max.val(value2)}
			if (parseInt(value1) > parseInt(value2)){
				value2 = value1;
				$('.max-price'+did).val(value2);
			}
			$('#slider'+did).slider("values",1,value2);
		});
	})

	/*
	var price_form=$('.formPrice:first');
	if (price_form.length) {
		var min = price_form.find('.min-price')
		,	max = price_form.find('.max-price')
		,	min_val = parseInt(min.val())
		,	max_val = parseInt(max.val())
		,	step = Math.round((max_val-min_val) / 100);

		jQuery("#slider").slider({
			'min': min_val,
			'max': max_val,
			'values': [min_val, max_val],
			step: step > 10 ? step : 10,
			range: true,
			stop: function(event, ui) {
				min.val(jQuery("#slider").slider("values",0));
				max.val(jQuery("#slider").slider("values",1));
		    },
		    slide: function(event, ui){
				min.val(jQuery("#slider").slider("values",0));
				max.val(jQuery("#slider").slider("values",1));
		    }
		});
		min.change(function(){
			var value1=min.val();
			var value2=max.val();
		    if (parseInt(value1) > parseInt(value2)){
				value1 = value2;
				min.val(value1);
			}
			if (value1 < min_val) { value1 = min_val; min.val(value1)}
			jQuery("#slider").slider("values",0,value1);
		});
		max.change(function(){
			var value1=min.val();
			var value2=max.val();
			if (value2 > max_val) { value2 = max_val; max.val(value2)}
			if (parseInt(value1) > parseInt(value2)){
				value2 = value1;
				max.val(value2);
			}
			jQuery("#slider").slider("values",1,value2);
		});
	}
	*/

	setTimeout(function(){
		$('.filtren .acc').each(function(){
			var div = $(this)
			,	a   = div.children('a')
			,	ul  = div.children('div');
			if (a.hasClass('current')) return;
			else if (div.find('input:checked').length) {
				a.click(); ul.show();
			} else if (ul.children('.formCost')) {
				var min = ul.find('.min-price')
				,	max = ul.find('.max-price');
				if (min.val() != min.data('orig') || max.val() != max.data('orig')) {
					a.click(); ul.show();
				}
			}
		})
	}, 500);

	jQuery('input.min-price, input.max-price').keypress(function(event){
		var key, keyChar;
		if(!event) var event = window.event;

		if (event.keyCode) key = event.keyCode;
		else if(event.which) key = event.which;

		if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
		keyChar=String.fromCharCode(key);

		if(!/\d/.test(keyChar))	return false;
	});

	$('#tabs_wrapper .sort a, #tabs_wrapper .category a').live('click', function() {
		if (timer_tabs) clearInterval(timer_tabs);
	    var tab_id=$(this).attr('id');
	    tabClick(tab_id);
	    return false;
	});
	$('#view a').click(function(){
		$.post(SITE_DIR() + 'ajax/setCatalogView.php', {view:$(this).data('view')}, function(data){location.reload()})
		return false;
	});
	$('#sort2 .how a').click(function(){
		$.post(SITE_DIR() + 'ajax/setCatalogView.php', {sort:$(this).data('f')+'|'+$(this).data('s')}, function(data){location.reload()})
		return false;
	});
	$('a.add2basket, a.add2basketq').click(function(){
		if (!$(this).hasClass('active')) moveToCart($(this));
		return false;
	});
	$('#callMe').submit(function(){
		var err = false
		,	form = $(this);
		form.find('input').each(function(){
			if (!$(this).val()) {$(this).addClass('error');err=true;}
			else $(this).removeClass('error');
		})
		if (!err) $.post(SITE_DIR() + 'ajax/callMe.php', form.serialize(), function(data){
			if (data == 'OK') {
				//form.parents('.window').find('.close').mouseup();
				form.hide().next('small').show();
			} else {
				form.find('.message_error').html(data).show();
			}
		});
		return false;
	});
	$('#setEview a').click(function(){
		$.post(SITE_DIR() + 'ajax/setCatalogView.php', {eview:$(this).data('t')}, function(){location.reload()})
		return false;
	});
	$('.basketitem a.close').click(function(){
		$('#DELETE_' + $(this).data('id')).val('Y');
		$(this).parents('form').submit();
		return false;
	});
	$('.basketitem a.delay').click(function(){
		$('#DELAY_' + $(this).data('id')).val('Y');
		$(this).parents('form').submit();
		return false;
	});
	$('.basketitem a.undelay').click(function(){
		$('#DELAY_' + $(this).data('id')).val('');
		$(this).parents('form').submit();
		return false;
	});
	$('.compare').live('click', function(){
		var e = $(this);
		set_in_compare(e, e.data('id'), false);
		if (e.hasClass('tableview')) return false;
	});
	$('.show_comparator').click(function(e){
		e.preventDefault();
		$("#shadow").fadeIn();
		$("#comparator_inner .right").mCustomScrollbar("destroy");
		$('#comparator').show();
		$("#comparator_inner .right").mCustomScrollbar();
	})
	$("#comparator_inner .options a").live('click', function(e){
		e.preventDefault();
		if ($(this).parent().hasClass('current')) return;
		$('#comparator_inner .options div').removeClass('current');
		$(this).parent().addClass('current');
		var diff = $(this).data('diff');
		$('#comparator_inner .trow').show();
		for (var i=0; i<sames.length; i++) {
			if (diff) $('#comparator_inner .trow' + sames[i]).hide();
		}
		for (var i=0; i<empties.length; i++) {
			$('.trow' + empties[i]).hide();
		}
	});
	$('a.qorder').click(function(e){
		e.preventDefault();
		$('#qorder_items').val($(this).data('pid'));
		$("#shadow").fadeIn();
		var ofs = $(this).offset()
		$('#qorder').css('top', ofs.top+40).css('left', ofs.left).show();
	});
	$('#qorder_form').submit(function(){
		var err = false
		,	form = $(this);
		form.find('input').each(function(){
			if (!$(this).val()) {$(this).addClass('error');err=true;}
			else $(this).removeClass('error');
		});
		if (!err) $.post(SITE_DIR() + 'ajax/qOrder.php', form.serialize(), function(data){
			if (data == 'OK' || data=='reload') {
				alert('Ваша заявка отправлена');
				form.parents('.window').find('.close').mouseup();
				if (data=='reload') location.reload();
			} else alert(data);
		});
		return false;
	});
	$('.notify').click(function(e){
		e.preventDefault();
		var link = $(this);
		if ($(this).parent().hasClass('active')) {
			$('#popup_form_notify_already')
				.css('top',link.offset().top + 38)
				.css('left',link.offset().left)
				.css('z-index',10001)
				.fadeIn("slow", function(){
    				window.clearTimeout(timer_1);
					timer_1 = setTimeout(function(){$("#popup_form_notify_already").fadeOut("slow")}, 2500);
    			});
			return;
		};
		if ($('#popup_form_notify').length) {
			$('#notify_id').val(link.data('id'));
			$('#popup_form_notify').find('input,label,div').show();
			$('#popup_n_error').hide();
			$('#success_notify').hide();
			$('#popup_form_notify')
				.css('top',link.offset().top + 38)
				.css('left',link.offset().left)
				.css('z-index',10000)
				.show();
		} else {
			var id = link.data('id');
			$.post(SITE_DIR() + 'ajax/addToCart.php', {id:id,subscribe:'Y'}, function(data){
				if (data != 'OK') alert(data);
				$('#popup_form_notify_success')
					.css('top',link.offset().top + 38)
					.css('left',link.offset().left)
					.css('z-index',10001)
					.show();
				set_in_subscribe(id);
			});
		}
		return false;
	})
	$('#popup_form_notify_form').submit(function(){
		var form = $('#popup_form_notify');
		$.post(SITE_DIR() + 'ajax/addToCart.php', $(this).serialize(), function(data){
			if (data != 'OK') {
				$('#popup_n_error').html(data).show();
			} else {
				$('#popup_form_notify_success')
					.css('top',form.offset().top)
					.css('left',form.offset().left)
					.css('z-index',10001)
					.show();
				$('#popup_form_notify, #popup_n_error').hide();
				set_in_subscribe($('#notify_id').val());
			}
		});
		return false;
	});
	$('.setInstall').click(function(){
		var t = $(this)
		,	val = t.data('inst')
		,	bid = t.data('id');
		$.post(SITE_DIR() + 'ajax/setInstall.php', {bid:bid, install:val}, function(){location.reload()});
	})
	if (in_compare.length==1 && in_compare[0]==0) $('.show_comparator').parent().removeClass('active');
	for (var i=0; i<in_basket.length; i++) set_in_basket(in_basket[i]);
	for (var i=0; i<in_compare.length; i++) set_in_compare(false, in_compare[i], true);
	for (var i=0; i<in_subscribe.length; i++) set_in_subscribe(in_subscribe[i]);
});
function set_in_subscribe(id) {$('.notify' + id).addClass('active').parent().addClass('active');}
function set_in_compare(e, id, fake) {
	var value = 0; var first = false;
	if (e) {value = e.hasClass('active') ? 0 : 1;}
	else if (fake) {value=1;}
	else first = true;
	$('.compare' + id).each(function(){
		var elem = $(this);
		if (first) {first=false; value=elem.hasClass('active') ? 0 : 1;}
		if (!value) {
			elem.removeClass('active').attr('checked', false);
		} else {
			elem.addClass('active').attr('checked', true);
		}
	});
	if (!fake) $.post(SITE_DIR() + 'ajax/addToCompare.php', {id:id, value:value}, function(data){
		//if (e.hasClass('reload')) location.reload(true);
		$('#comparator_inner').html(data);
		if (in_compare[0]==0) $('.show_comparator').parent().removeClass('active');
		else $('.show_comparator').parent().addClass('active');
		$("#comparator_inner .right").mCustomScrollbar("destroy");
		$("#comparator_inner .right").mCustomScrollbar();
	});
}
function tabClick(tab_id) {
	if (tab_id != $('#tabs_wrapper .right a.active').attr('id') ) {
	    $('#tabs_wrapper .tabs').removeClass('active').removeClass('current');
	    $('#'+tab_id).addClass('active').parent().addClass('current');
	    $('#con_' + tab_id).addClass('active');
	}
}
function set_in_basket(id) {
	if (!id) return;
	var ordb = $('#order_cart_header');
	if (!ordb.hasClass('active')) {
		ordb.addClass("active").children('a').each(function(){$(this).attr('href', $(this).data('href'));})
	}
	var item = $('a.add2basket'+id);
	item.each(function(e){
		var elem = $(this);
		if (elem.hasClass('add2basketq')) {
			elem.parent().children('div,input').hide();
			elem.parent().children('div.have').show();
		}
		if (elem.hasClass('tableview')) elem.html('Уже в корзине');
		if (elem.hasClass('blockview')) elem.html('Уже в корзине').parent().removeClass('buy').addClass('active');
		if (elem.hasClass('viewP2')) elem.html('Заказан').parent().removeClass('buy').addClass('active alt');
		if (elem.hasClass('viewP3')) elem.removeClass('viewP3').addClass('cart left').html('').parent().removeClass('btn');
	});
	item.addClass('active');
	$('.countbasket'+id).remove();
	//var elem = $('a#add2basket'+id);
	//if (elem.length) elem.html('in cart').parent().removeClass('buy');
}
function moveToCart(item) {
	var count = 1;
	if (!item.hasClass('tableview')) {
		var ic = item.data('parent'); if(!ic) ic='productBlock';
		animate(item.parents('.'+ic).find('img:first'), $('header #basket'), 400);
		if (item.hasClass('add2basketq')) {
			var pc = item.data('class');
			if (pc) count = item.parents('.'+pc).find('input.count').val();
			else count = item.parent().find('input').val();
		}
	}
	$.post(SITE_DIR() + 'ajax/addToCart.php', {id:item.data('id'), count:count}, function(data){$('#basket').html(data)});
	set_in_basket(item.data('id'));
}
function animate(image, goal, timeout) {
    var position = image.offset()
    ,	goal_position = goal.offset()
    ,	flyImage = image.clone()
    ,	max_h = 83, max_w = 170
    ,	destw = Math.min(max_w, image.width())
    ,	img_k = image.width() / image.height()
    ,	desth = destw / img_k; //width is base
    if (desth > max_h) {
    	desth = max_h
    	destw = desth * img_k; //height is base
	}
    flyImage.css({"position":"absolute","left":position.left,"top":position.top,'z-index':100000});
    $('body').append(flyImage);
    flyImage.animate({
    	left:goal_position.left,
    	top:goal_position.top,
    	width:destw, height:desth
    }, timeout, 'linear', function(){
    	$(this).remove();
    	$("#add_in_basket").fadeIn("slow", function(){
    		window.clearTimeout(timer);
			timer = setTimeout(function(){$("#add_in_basket").fadeOut("slow")}, 5000);
    	});
    });
    return false;
}
