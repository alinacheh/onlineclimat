<?
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    global $USER;
	$iblock_id = 9;
    if(SITE_CHARSET == "windows-1251"){
        $_POST['name'] = $APPLICATION->ConvertCharset($_POST['name'],"utf-8","windows-1251");
        $_POST['question'] = $APPLICATION->ConvertCharset($_POST['question'],"utf-8","windows-1251");
    }
    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $phone = trim($_POST['phone']);
    $message = trim($_POST['question']);

    if (!$name || !$email || !$message) {
		echo 'Заполните все поля';
		die();
    }
    if (!$USER->IsAuthorized()) {
		if (!$APPLICATION->CaptchaCheckCode($_POST["captcha_word"], $_POST["captcha_code"])) {
			echo 'Неправильно введен защитный код';
			die();
		}
    }
    CModule::IncludeModule("iblock");
    $el = new CIBlockElement();
    $id = $el->Add(array(
		'IBLOCK_ID' => $iblock_id,
		'NAME' => $message,
		'PROPERTY_VALUES' => array(
			'PHONE' => $phone,
			'EMAIL' => $email,
			'NAME' => $name
		)
    ));
    if (!$id) echo $el->LAST_ERROR;
    else echo 'OK';