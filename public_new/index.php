<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("NO_H1", 'Y');
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetPageProperty("ContentClass", "left");
$APPLICATION->SetPageProperty("TitleExact", "Y");
$APPLICATION->SetTitle("Интернет-магазин климатической техники");
?>
<?$APPLICATION->IncludeComponent("custom:empty", "index_items", array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => "1",
	"CACHE_TYPE" => "Y",
	"CACHE_TIME" => "3600",
	"COUNT" => "8",
	"WITHOUT_BUY" => "N",
	"PRICES" => array(
		0 => "main",
		1 => "other",
	),
	"MAIN_PRICES" => array(
		0 => "main",
	),
	"OTHER_PRICES" => array(
		0 => "other",
	)
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>