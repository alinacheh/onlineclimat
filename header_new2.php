<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

IncludeTemplateLangFile(__FILE__);
global $APPLICATION, $TEMPLATE_OPTIONS, $arSite;
$arSite = CSite::GetByID(SITE_ID)->Fetch();
$htmlClass = ($_REQUEST && isset($_REQUEST['print']) ? 'print' : false);
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>" <?=($htmlClass ? 'class="'.$htmlClass.'"' : '')?>>
<head>
	<title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowMeta("viewport");?>
	<?$APPLICATION->ShowMeta("HandheldFriendly");?>
	<?$APPLICATION->ShowMeta("apple-mobile-web-app-capable", "yes");?>
	<?$APPLICATION->ShowMeta("apple-mobile-web-app-status-bar-style");?>
	<?$APPLICATION->ShowMeta("SKYPE_TOOLBAR");?>
	<?$APPLICATION->ShowHead();?>
	<?$APPLICATION->AddHeadString('<script>BX.message('.CUtil::PhpToJSObject( $MESS, false ).')</script>', true);?>
	<?if(CModule::IncludeModule("aspro.optimus")) {COptimus::Start(SITE_ID);}?>
	<!--[if gte IE 9]><style type="text/css">.basket_button, .button30, .icon {filter: none;}</style><![endif]-->
	<link href='<?=CMain::IsHTTPS() ? 'https' : 'http'?>://fonts.googleapis.com/css?family=Ubuntu:400,500,700,400italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="/css/owl.carousel.min.css" rel="stylesheet">
    <!-- <link href="/css/owl.theme.default.min.css" rel="stylesheet"> -->
    
	<link rel="stylesheet" href="<?php echo SITE_TEMPLATE_PATH ?>/css/modification.css">

        <meta name="viewport" content="initial-scale=1.0, width=device-width">
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,400italic&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
       <?// <link rel="stylesheet" href="https://on-lineclimat.ru/bitrix/cache/css/s1/aspro_optimus/template_ea43f9ceaa5d69408753fa53262762c6/template_ea43f9ceaa5d69408753fa53262762c6_v1.css?1610621244645510">
        ?>
		<link rel="stylesheet" href="/dev/style.css?<?=time()?>">
        <title><?$APPLICATION->ShowTitle()?></title>
</head>


   <body id="main">
		<div id="panel"><?$APPLICATION->ShowPanel();?></div>
		<?if(!CModule::IncludeModule("aspro.optimus")){?><center><?$APPLICATION->IncludeFile(SITE_DIR."include/error_include_module.php");?></center></body></html><?die();?><?}?>
		<?$APPLICATION->IncludeComponent("aspro:theme.optimus", ".default", array("COMPONENT_TEMPLATE" => ".default"), false);?>
		<?COptimus::SetJSOptions();?>
		
        <header class="header" id="header">
            <div class="top-header top-header1">
                <div class="wrapper_inner">
                    <div class="top-header__content">
						<?$APPLICATION->IncludeComponent(
							"bitrix:menu",
							"top_menu_new",
							array(
								"ROOT_MENU_TYPE" => "top_menu1",
								"MENU_CACHE_TYPE" => "A",
								"MENU_CACHE_TIME" => "0",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"MENU_THEME" => "site",
								"CACHE_SELECTED_ITEMS" => "N",
								"MENU_CACHE_GET_VARS" => array(),
								"MAX_LEVEL" => "1",
								"CHILD_MENU_TYPE" => "",
								"USE_EXT" => "Y",
								"DELAY" => "N",
								"ALLOW_MULTI_SELECT" => "N",
								"COMPONENT_TEMPLATE" => "top_menu_new"
							),
							false
						);?>
                        <div class="top-right">
                            <div class="top-adress">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/template/icon_map.svg" width="12" alt="">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
									array(
										"COMPONENT_TEMPLATE" => ".default",
										"PATH" => SITE_DIR."include/header_new/address.php",
										"AREA_FILE_SHOW" => "file",
										"AREA_FILE_SUFFIX" => "",
										"AREA_FILE_RECURSIVE" => "Y",
										"EDIT_TEMPLATE" => "standard.php"
									),
									false
								);?>                                
                            </div>
                            <a href="mailto:mail@on-lineclimat.ru" class="top-mail"><img src="<?=SITE_TEMPLATE_PATH?>/images/template/icon_email.svg" width="16" alt=""> mail@on-lineclimat.ru</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="middle-header">
                <div class="wrapper_inner">
                    <div class="middle-header__content">
                        <div class="middle-header__left">
                            <a href="/" class="logo">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/template/logo2.png" alt="">
                            </a>
                            <div class="descr-header">
                                <a href="#" class="dopmenuview">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/template/icon_burger.png" width="17" alt="">
                                </a>
								<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
									array(
										"COMPONENT_TEMPLATE" => ".default",
										"PATH" => SITE_DIR."include/header_new/descr.php",
										"AREA_FILE_SHOW" => "file",
										"AREA_FILE_SUFFIX" => "",
										"AREA_FILE_RECURSIVE" => "Y",
										"EDIT_TEMPLATE" => "standard.php"
									),
									false
								);?>                                
                            </div>
                        </div>
                        <div class="middle-header__right">
                            <div class="phones">
                                <span class="reg-mos">Регионы РФ</span>
                                <div class="h-phone">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
									array(
										"COMPONENT_TEMPLATE" => ".default",
										"PATH" => SITE_DIR."include/header_new/phone.php",
										"AREA_FILE_SHOW" => "file",
										"AREA_FILE_SUFFIX" => "",
										"AREA_FILE_RECURSIVE" => "Y",
										"EDIT_TEMPLATE" => "standard.php"
									),
									false
								);?>
								</div>											
                                <a href="https://wa.me/79037203651?text=Добрый%20день%20хочу%20уточнить%20информацию%20по%20вашим%20товарам" class="flex align-items-center justify-content-end middle-wa"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/icon_wa.svg" width="25" alt=""> <span>Мы в WhatsApp</span></a>
                            </div>
                            <div class="phones">
                                <span class="reg-mos">Москва</span>
                                <div class="h-phone">
									<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
									array(
										"COMPONENT_TEMPLATE" => ".default",
										"PATH" => SITE_DIR."include/header_new/phone_dop.php",
										"AREA_FILE_SHOW" => "file",
										"AREA_FILE_SUFFIX" => "",
										"AREA_FILE_RECURSIVE" => "Y",
										"EDIT_TEMPLATE" => "standard.php"
									),
									false
								);?>
								<br/></div>		
							
								<a href="#" class="callback_btn flex align-items-center justify-content-end middle-wa" onclick="yaCounter37461485.reachGoal('waitingCallFooter'); return true;"> <span>Заказать звонок</span></a>
					
						   </div>
                        </div>
                    </div>
						<div class="basket_wrapp">
							<div class="header-cart fly" id="basket_line">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
									array(
										"COMPONENT_TEMPLATE" => ".default",
										"PATH" => SITE_DIR."include/top_page/comp_basket_top.php",
										"AREA_FILE_SHOW" => "file",
										"AREA_FILE_SUFFIX" => "",
										"AREA_FILE_RECURSIVE" => "Y",
										"EDIT_TEMPLATE" => "standard.php"
									),
									false
								);?>
							</div>
						</div>
                </div>
            </div>
            <div class="top-header top-header2">
                <div class="wrapper_inner">
                    <div class="top-header__content">
						<?$APPLICATION->IncludeComponent(
							"bitrix:menu",
							"top_menu_new",
							array(
								"ROOT_MENU_TYPE" => "top_menu2",
								"MENU_CACHE_TYPE" => "A",
								"MENU_CACHE_TIME" => "0",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"MENU_THEME" => "site",
								"CACHE_SELECTED_ITEMS" => "N",
								"MENU_CACHE_GET_VARS" => array(),
								"MAX_LEVEL" => "1",
								"CHILD_MENU_TYPE" => "",
								"USE_EXT" => "Y",
								"DELAY" => "N",
								"ALLOW_MULTI_SELECT" => "N",
								"COMPONENT_TEMPLATE" => "top_menu_new"
							),
							false
						);?>

                        <div class="top-right">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
								array(
									"COMPONENT_TEMPLATE" => ".default",
									"PATH" => SITE_DIR."include/header_new/search.title.catalog.php",
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => "standard.php"
								),
								false
							);?>                 
                        </div>
                    </div>
                </div>
            </div>
			<?/*
            <nav class="mainmenu-block">
                <div class="wrapper_inner">
                    <nav>
                        <a href="#" class="mobview"><img src="<?=SITE_TEMPLATE_PATH?>/images/template/icon_burger.png" width="15" alt=""> Меню</a>
                        <ul class="mainmenu">
                            <li class="mainmenu__item">
                                <a href="#" class="mainmenu__link catalogLink"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/icon_burger.png" width="15" alt=""> Каталог</a>
                                <ul class="submenu">
                                    <li>
                                        <a href="#"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/submenu1.png" alt="" />Арматура и трубы <span>(3459)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/submenu22.png" alt="" />Водонагреватели <span>(101)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/submenu33.png" alt="" />Теплые полы электрические <span>(88)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/submenu2.png" alt="" />Отопительные приборы <span>(1340)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/submenu55.png" alt="" />Расширительные баки <span>(69)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/submenu66.png" alt="" />Системы предотвращения протеканий </a> <span>(3459)</span>
                                    </li>
                                    <li>
                                        <a href="#"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/submenu3.png" alt="" />Фильтры, редукторы и сопутсвтующее</a> <span>(3459)</span>
                                    </li>
                                    <li>
                                        <a href="#"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/submenu8.png" alt="" />Полотенцесушители <span>(3459)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/submenu9.png" alt="" />Теплоноситель (антифриз) <span>(3459)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/submenu5.png" alt="" />Канализация <span>(3459)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/submenu11.png" alt="" />Котлы, бойлеры, котелки и еще что-то</a> <span>(3459)</span>
                                    </li>
                                    <li>
                                        <a href="#"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/submenu12.png" alt="" />Уцененные товары <span>(3459)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/submenu6.png" alt="" />Насосное оборудование <span>(3459)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"> <img src="<?=SITE_TEMPLATE_PATH?>/images/template/submenu14.png" alt="" />Счетчики воды <span>(3459)</span></a>
                                    </li>
                                    <li>
                                        <a href="#"> Распродажа <span>(3459)</span></a>
                                    </li>
                                </ul>
                            </li>
							
							
							
                            <li class="mainmenu__item">
                                <a href="#" class="mainmenu__link">Кондиционеры</a>
                            </li>
                            <li class="mainmenu__item">
                                <a href="#" class="mainmenu__link">Радиаторы отопления</a>
                            </li>
                            <li class="mainmenu__item">
                                <a href="#" class="mainmenu__link">Полотенцесушители</a>
                            </li>
                            <li class="mainmenu__item">
                                <a href="#" class="mainmenu__link">Вентиляционные установки</a>
                            </li>
                            <li class="mainmenu__item">
                                <a href="#" class="mainmenu__link">Водонагреватели</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </nav>*/?>
			
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"top_content_multilevel_new",
	array(
		"ROOT_MENU_TYPE" => "top_catalog",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "0",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_THEME" => "site",
		"CACHE_SELECTED_ITEMS" => "N",
		"MENU_CACHE_GET_VARS" => array(),
		"MAX_LEVEL" => "3",
		"CHILD_MENU_TYPE" => "top_catalog_child",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "top_content_multilevel_new"
	),
	false
);?>

        </header>
       <aside class="left-aside">
           <div class="left-asideClose"> x </div>
		   <?$APPLICATION->IncludeComponent(
				"bitrix:menu",
				"top_menu_new_mobi",
				array(
					"ROOT_MENU_TYPE" => "top_menu3",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "0",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_THEME" => "site",
					"CACHE_SELECTED_ITEMS" => "N",
					"MENU_CACHE_GET_VARS" => array(),
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "",
					"USE_EXT" => "Y",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N",
					"COMPONENT_TEMPLATE" => "top_menu_new_mobi"
				),
				false
			);?>
       </aside>
	   <div class="wrapper <?=(COptimus::getCurrentPageClass());?> basket_<?=strToLower($TEMPLATE_OPTIONS["BASKET"]["CURRENT_VALUE"]);?> <?=strToLower($TEMPLATE_OPTIONS["MENU_COLOR"]["CURRENT_VALUE"]);?> banner_auto">

        <?//<script src="https://on-lineclimat.ru/bitrix/js/main/jquery/jquery-1.8.3.min.js?157665075293637"></script>?>
        <script>
		$(document).ready(function() {
			

            /*$(".catalogLink").on('click', function(e) {
                e.preventDefault();
				var data = "." + $(this).attr("data");
				console.log(data);
                //$('.submenu').slideToggle();
                $('.mainmenu-block .submenu').hide();
                $(data).slideToggle();
            })*/
			
			/*$(".catalogLink").on('click', function(e) {
                e.preventDefault();
				var parent_a = $(this).parents();
                parent_a.find('.submenu').slideToggle();
            })*/


            $(".mobview").on('click', function (e) {
                e.preventDefault();
                $('.mainmenu').slideToggle();
            })

            $(".dopmenuview").on('click', function (e) {
                e.preventDefault();
                $('.left-aside').addClass('active');
            })

            $(".left-asideClose").on('click', function (e) {
                e.preventDefault();
                $('.left-aside').removeClass('active');
            })
			
			$(".submenu2-brands").hover(function () {
                $(this).parent().addClass('active');
            }, function() {
                    $(this).parent().removeClass('active')
            })
		});
        </script>
		
			<div class="wraps" id="content">
				<div class="wrapper_inner <?=(COptimus::IsMainPage() ? "front" : "");?> <?=((COptimus::IsOrderPage() || COptimus::IsBasketPage()) ? "wide_page" : "");?>">
					<?if(!COptimus::IsOrderPage() && !COptimus::IsBasketPage()){?>
						<div class="left_block">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
								array(
									"COMPONENT_TEMPLATE" => ".default",
									"PATH" => SITE_DIR."include/left_block/menu.left_menu.php",
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => "standard.php"
								),
								false
							);?>

							<?$APPLICATION->ShowViewContent('left_menu');?>

							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
								array(
									"COMPONENT_TEMPLATE" => ".default",
									"PATH" => SITE_DIR."include/left_block/comp_banners_left.php",
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => "standard.php"
								),
								false
							);?>
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
								array(
									"COMPONENT_TEMPLATE" => ".default",
									"PATH" => SITE_DIR."include/left_block/comp_subscribe.php",
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => "standard.php"
								),
								false
							);?>

	<?php 
		$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getRequestedPageDirectory();
		$url_parts = explode('/', $request);
		$product = end($url_parts);

		$objFindTools = new CIBlockFindTools();
		$elementID = $objFindTools->GetElementID(false, $product, false, false, array("IBLOCK_ID" => 1));

		$products_cities = getCitiesList();
		if(isset($products_cities[$elementID])) {
			$html = '
			<div class="product_banner_image">
				<div class="subscribe-form" id="subscribe-form">
					<div class="wrap_bg">
						<div class="top_block box-sizing">
							<div class="text">
								<div class="title">За последние 7 дней этот товар был отправлен в города</div>
								<ul class="products-cities-list">
			';

			foreach($products_cities[$elementID]["cities"] as $city) {
				$html .= '<li>'.$city.'</li>';
			}
									
			$html .= '
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			';

			echo $html;
		}

		if($elementID > 0 && get_parent_product_section($elementID) == 83) {
			echo '
			<div class="product_banner_image">
				<a href="/catalog/radiatory_otopleniya/stalnye_trubchatye/irsap-radiator/irsap_eksklyuziv/" target="_blank">
					<img src="/upload/images/irsap-ban-11092019-bok.jpg" alt="">
				</a>
			</div>
			';
		}

	?> 
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/left_block/comp_news.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/left_block/comp_news_articles.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
						</div>
						<div class="right_block">
					<?}?>
						<div class="middle">
							<?if(!COptimus::IsMainPage()):?>
								<div class="container">
									<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "optimus", array(
										"START_FROM" => "0",
										"PATH" => "",
										"SITE_ID" => "-",
										"SHOW_SUBSECTIONS" => "N"
										),
										false
									);?>
									<?$APPLICATION->ShowViewContent('section_bnr_content');?>
									<!--title_content-->
									<h1 id="pagetitle"><?=$APPLICATION->ShowTitle(false);?><?if (isset($_GET['PAGEN_1']) && intval($_GET['PAGEN_1'])>0) { echo ' страница - '.intval($_GET['PAGEN_1']);}?></h1>
									<!--end-title_content-->
										<?endif;?>
<?if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == "xmlhttprequest") $APPLICATION->RestartBuffer();?>



						

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/comp_banners_top_slider.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false
);?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/comp_tizers.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false
);?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/comp_banners_float.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false
);?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/comp_catalog_hit.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false
);?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/comp_news_akc.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false
);?>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/inc_company.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false
);?>	

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/comp_brands.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>