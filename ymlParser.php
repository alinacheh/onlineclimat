<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('catalog');
require_once 'vendor/autoload.php';

use Sirian\YMLParser\Offer\Offer;
use Sirian\YMLParser\Parser;

$file = isset($_GET['file']) ? $_GET['file'] : 'catalog.xml';
$parser = new Parser();
$result = $parser->parse($file);

$shop = $result->getShop();
$i = isset($_GET['offset']) ? $_GET['offset'] : 0;
$before = $i;
foreach ($result->getAllOffers($i) as $offer) {

    findOrNewCategoryTree($offer->getCategory(), $offer);

    $productID = findProductByExternalId($offer->getAttribute('id'));

    if ($productID) {
        $object = new CIBlockElement;

        $elementId = $object->Update(
            $productID,
            array(
                'IBLOCK_SECTION_ID' => tryToFindCategoryId($offer->getCategory()->getName())
            )
        );
    } else {
        $object = new CIBlockElement;

        $elementId = $object->Add(
            array(
                'CODE' => URLify::filter(getStringForUrl($offer)),
                'EXTERNAL_ID' => $offer->getAttribute('id'),
                'NAME' => getName($offer),
                'IBLOCK_ID' => getIblockId(),
                'IBLOCK_SECTION_ID' => tryToFindCategoryId($offer->getCategory()->getName()),
                'ACTIVE' => 'Y',
                'DATE_ACTIVE_FROM' => (new DateTime())->format('d.m.Y H:i:s'),
                'SORT' => 500,
                'PREVIEW_PICTURE' => getPicture($offer),
                'DETAIL_TEXT' => 'описание',
                'DETAIL_PICTURE' => getPicture($offer),
                'DATE_CREATE' => (new DateTime())->format('d.m.Y H:i:s'),
                'CREATED_BY' => $USER->GetID(),
                'PROPERTY_VALUES' => array_merge(
                    array(
                        'ARTICUL' => $offer->getCode(),
                        'MORE_PHOTO' => getPictures($offer),
                        'BRAND_IBLOCK' => getVendor($offer),
                    ),
                    resolveParameters($offer)
                ),
            )
        );

        if ($elementId) {

            $product = new CCatalogProduct;

            $productId = $product->add(
                array(
                    'ID' => $elementId,
                    'AVAILABLE' => 'Y',
                    'VAT_ID' => 1,
                    'VAT_INCLUDED' => 'Y'
                )
            );

            $price = new CPrice;

            $price->add(
                array(
                    'PRODUCT_ID' => $elementId,
                    'CATALOG_GROUP_ID' => 1,
                    'PRICE' => $offer->getPriceRub(),
                    'CURRENCY' => 'RUB'
                )
            );
        }
    }

    $i++;

    if (($i - $before) == 49) {
        echo '<head><META HTTP-EQUIV=REFRESH CONTENT="3; http://on-lineclimat.ru/ymlParser.php?offset=' . $i . '"></head>';
    }
}

function findProductByExternalId($id) {
    $product = CIBlockElement::GetList(
        array("SORT" => "ASC"),
        array('IBLOCK_ID' => getIblockId(), 'EXTERNAL_ID' => $id)
    )->getNext();

    if ($product['ID']) {
        return $product['ID'];
    }

    return false;
}

function resolveParameters(\Sirian\YMLParser\Offer\Offer $offer)
{
    $attributesContent = $offer->getParameters();

    $dom = new DOMDocument();
    $dom->loadHTML(mb_convert_encoding($attributesContent, 'HTML-ENTITIES', 'UTF-8'));

    $rows = $dom->getElementsByTagName('tr');

    $parameters = array();

    foreach ($rows as $row) {
        $parameters[findOrNewParameter($row->childNodes[0]->textContent)] = $row->childNodes[1]->textContent;
    }
    return $parameters;
}

function findOrNewParameter($parameterName)
{
    $excitedParameter = CIBlockProperty::GetList(
        array('ORDER' => 'ASC'),
        array('NAME' => $parameterName, 'IBLOCK_ID' => getIblockId())
    )->getNext();

    if ($excitedParameter['CODE']) {
        return $excitedParameter['CODE'];
    } else if ($excitedParameter[0]['CODE']) {
        return $excitedParameter[0]['CODE'];
    } else {
        $object = new CIBlockProperty;
        $object->Add(
            array(
                'CODE' => str_replace('-', '_', strtoupper(URLify::filter($parameterName, 200))),
                'IBLOCK_ID' => getIblockId(),
                'NAME' => $parameterName,
                'ACTIVE' => 'Y',
                'SORT' => 500,
                'PROPERTY_TYPE' => 'S',
                'MULTIPLE' => 'N',
            )
        );

        return str_replace('-', '_', strtoupper(URLify::filter($parameterName, 200)));
    }
}

function getVendor(\Sirian\YMLParser\Offer\Offer $offer)
{
    if ($offer instanceof \Sirian\YMLParser\Offer\VendorModelOffer) {
        $vendor = CIBlockElement::GetList(
            array("SORT" => "ASC"),
            array('IBLOCK_ID' => 2, 'NAME' => $offer->getVendor())
        )->getNext();

        return $vendor['ID'];
    }

    return false;
}

function getPicture(\Sirian\YMLParser\Offer\Offer $offer)
{
    return makeFile(getFirstPicture($offer));
}

function makeFile($name)
{
    return CFile::MakeFileArray(
        $_SERVER['DOCUMENT_ROOT'] . '/goods/big/' . $name
    );
}

function getPictures(\Sirian\YMLParser\Offer\Offer $offer)
{
    $pictures = getPicturesExcludeFirst($offer);

    if ($pictures) {
        $picturesArray = array();
        foreach ($pictures as $picture) {
            $picturesArray[] = makeFile($picture);
        }

        return $picturesArray;
    }

    return false;
}

function getPicturesExcludeFirst(\Sirian\YMLParser\Offer\Offer $offer)
{
    $pictures = $offer->getPictures();
    unset($pictures[0]);

    if (count($pictures) > 0) {
        return $pictures;
    }

    return false;
}

function getFirstPicture(\Sirian\YMLParser\Offer\Offer $offer)
{
    $pictures = $offer->getPictures();
    return $pictures[0];
}

function getName(\Sirian\YMLParser\Offer\Offer $offer)
{
    if ($offer instanceof \Sirian\YMLParser\Offer\VendorModelOffer) {
        return $offer->getModel();
    }

    return $offer->getName();
}

function getStringForUrl(\Sirian\YMLParser\Offer\Offer $offer)
{
    if ($offer instanceof \Sirian\YMLParser\Offer\VendorModelOffer) {
        return URLify::filter($offer->getModel(), 200);
    }

    return URLify::filter($offer->getName(), 200);
}

function getIblockId()
{
    return isset($_GET['iblock_id']) ? $_GET['iblock_id'] : 1;
}

function tryToFindCategoryId($name)
{
    $result = CIBlockSection::GetList(
        array('SORT' => 'ASC'),
        array(
            'IBLOCK_ID' => getIblockId(),
            'NAME' => $name
        ),
        false,
        array('ID')
    )->getNext();

    if ($result['ID']) {
        return $result['ID'];
    }

    return false;
}

function findOrNewCategoryTree(\Sirian\YMLParser\Category $category, Offer $offer)
{
    $excitedCategory = tryToFindCategoryId($category->getName());

    if ($excitedCategory) {
        return $excitedCategory;
    } else {
        if ($category->getParent()) {
            $parent = findOrNewCategoryTree($category->getParent(), $offer);
        } else {
            $parent = null;
        }
        $object = new CIBlockSection;

        $object->Add(
            array(
                'IBLOCK_ID' => getIblockId(),
                'CODE' => strtolower(URLify::filter($category->getName(), 200)),
                'ACTIVE' => 'Y',
                'NAME' => $category->getName(),
                'IBLOCK_SECTION_ID' => $parent,
                'PICTURE' => getPicture($offer),
                'DETAIL_PICTURE' => getPicture($offer)
            )
        );
    }
}