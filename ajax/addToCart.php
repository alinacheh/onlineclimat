<?
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    $id = intval($_POST['id']);
    $count = intval($_POST['count']); if ($count<0) $count=0;
    if ($id > 0 && $count) {
        CModule::IncludeModule('catalog');
        if((float)SM_VERSION >= 12.5){
            Add2BasketByProductID($id, $count, array('PRODUCT_PROVIDER_CLASS'=>'CCatalogProductProviderClimate'), array(Refs::getProductInstallParam()));
        }else{
            Add2BasketByProductID($id, $count, array('CALLBACK_FUNC'=>'Handlers::GetBasketPrice', 'ORDER_CALLBACK_FUNC'=>'Handlers::GetBasketOrderPrice'), array(Refs::getProductInstallParam()));
        }
        CSaleBasket::UpdateBasketPrices(CSaleBasket::GetBasketUserID(), SITE_ID);
    } elseif ($id && $_POST['subscribe']) {
        global $USER;
        if (!$USER->IsAuthorized()) {
            CModule::IncludeModule('sale');
            $user_mail = trim($_POST['user_mail']);
            if (strlen($user_mail) <= 0) {
                echo 'Введите Email';
                die();
            }
            $res = CUser::GetList($b, $o, array("=EMAIL" => $user_mail));
            if ($res->Fetch()) {
                echo 'Пользователь с таким Email адресом уже существует.';
                die();
            }
            $arErrors = array();
            $user_id = CSaleUser::DoAutoRegisterUser($user_mail, array(), SITE_ID, $arErrors);
            if ($user_id > 0) {
                $USER->Authorize($user_id);
            } else {
                echo $arErrors[0]['TEXT'];
                die();
            }
        }
        CModule::IncludeModule('catalog');
        Add2BasketByProductID($id, 0, array("IGNORE_CALLBACK_FUNC"=>'Y', "SUBSCRIBE" => "Y", "CAN_BUY" => "N"), array());
        echo 'OK';
        die();
    }
    $APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "template1", Array(
        "PATH_TO_BASKET" => SITE_DIR."personal/cart/",    
        "PATH_TO_ORDER" => SITE_DIR."personal/cart/",    
        ),
        false
    );