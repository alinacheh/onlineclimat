<?
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    $bid = intval($_POST['bid']);
    $val = intval($_POST['install']);
    if ($bid) {
		CModule::IncludeModule('sale');
		$install = Refs::getProductInstallParam($val);
		$res = CSaleBasket::GetPropsList(array('ID' => "ASC"), array("BASKET_ID" => $bid, 'CODE'=>$install['CODE']));
		while ($r = $res->Fetch()) $old_value = $r['VALUE'];
		if ($old_value != $val) {
			$arFields = CSaleBasket::GetByID($bid);
			if ($arFields) {
				$pid = intval($arFields['PRODUCT_ID']);
				unset($arFields['ORDER_ID']);
				$arFields['PROPS'] = array($install);
				$r = CSaleBasket::Update($bid, $arFields);
			}
		}
    }