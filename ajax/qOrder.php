<?
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    $iblock_id = 8;
    $name = trim($_POST['name']);
    //$email = trim($_POST['email']);
    $phone = trim($_POST['phone']);
    $qpage = trim($_POST['page']);
    $items = explode(',', $_POST['items']);
    if (!$name || /*!$email || */!$phone || !count($items)) {
		echo 'Заполните все поля';
		die();
    }
    $basket_mode = (count($items) > 1);
    $ids = array();
    $data = array();
    foreach ($items as $itemq) {
		$item = explode(':', $itemq);
		$item[0] = intval($item[0]);
		$ids[] = $item[0];
		$data[$item[0]] = array('Q'=>$item[1]);
    }

    CModule::IncludeModule("iblock");
    $res = CIBlockElement::GetList(array('ID'=>'ASC'), array('ID'=>$ids), false, false, array('ID', 'NAME'));
    $list = '';

    while ($r = $res->Fetch()) {
    	$r['ID'] = intval($r['ID']);
		$data[$r['ID']]['NAME'] = $r['NAME'];
		//if ($basket_mode) {
		$value = Refs::getBasketInstallValue(false, $r['ID']);
		if ($value == 'Y') $r['NAME'] .= ' + монтаж';
		//}
		$list .= $r['NAME'] . ' -- ' . $data[$r['ID']]['Q'] . "\r\n";
    }
    $el = new CIBlockElement();
    $id = $el->Add(array(
		'IBLOCK_ID' => $iblock_id,
		'NAME' => date('Y-m-d H:i:s'),
		'PREVIEW_TEXT' => $list,
		'PROPERTY_VALUES' => array(
			'NAME' => $name,
			//'EMAIL' => $email,
			'PHONE' => $phone,
			'PAGE' => $qpage,
			'ITEMS' => $ids
		)
    ));
    if (!$id) echo $el->LAST_ERROR;
    else {
    	CEvent::Send('Q_ORDER', SITE_ID, array('NAME'=>$name,'PHONE'=>$phone,'PAGE' => $qpage,/*'EMAIL'=>$email,*/'LIST'=>$list));
    	if ($basket_mode) {
    		CModule::IncludeModule('sale');
			CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
			echo 'reload';
    	} else echo 'OK';
	}
