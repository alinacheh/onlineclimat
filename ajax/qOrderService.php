<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');
$res = \CIBlock::GetList(array(), array('CODE' => 'qorder_service'))->Fetch();
$iblock_id = $res['ID'];
$name = trim($_POST['name']);
$phone = trim($_POST['phone']);
$items = explode(',', $_POST['items']);
if (!$name ||
    !$phone || !count($items)) {
    echo 'Заполните все поля';
    die();
}
$basket_mode = (count($items) > 1);
$ids = array();
$data = array();
foreach ($items as $itemq) {
    $item = explode(':', $itemq);
    $item[0] = intval($item[0]);
    $ids[] = $item[0];
    $data[$item[0]] = array('Q' => $item[1]);
}

$res = CIBlockElement::GetList(array('ID' => 'ASC'), array('ID' => $ids), false, false, array('ID', 'NAME'));
$list = '';

while ($r = $res->Fetch()) {
    $r['ID'] = intval($r['ID']);
    $data[$r['ID']]['NAME'] = $r['NAME'];
    $list .= $r['NAME'] . "\r\n";
}
$el = new CIBlockElement();
$id = $el->Add(array(
    'IBLOCK_ID' => $iblock_id,
    'NAME' => date('Y-m-d H:i:s'),
    'PREVIEW_TEXT' => $list,
    'PROPERTY_VALUES' => array(
        'NAME' => $name,
        'PHONE' => $phone,
        'ITEMS' => $ids
    )
));
if (!$id) {
    echo $el->LAST_ERROR;
} else {
    CEvent::Send('Q_ORDER_SERVICE', SITE_ID, array(
        'NAME' => $name,
        'PHONE' => $phone,
        'LIST' => $list
    ));
    echo 'OK';
}