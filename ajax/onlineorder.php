<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
/*
echo '<pre>';
print_r($_POST);
print_r($_FILES);
echo '</pre>';
*/
if(!$APPLICATION->CaptchaCheckCode($_POST["captcha_word"], $_POST["captcha_code"])) {
    include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
    $cpt = new CCaptcha();
    $captchaPass = COption::GetOptionString("main", "captcha_password", "");
    if(strlen($captchaPass) <= 0)
    {
        $captchaPass = randString(10);
        COption::SetOptionString("main", "captcha_password", $captchaPass);
    }
    $cpt->SetCodeCrypt($captchaPass);?>
    <span>Проверочный код</span>
    <input name="captcha_code" value="<?=htmlspecialchars($cpt->GetCodeCrypt());?>" type="hidden">
    <input id="captcha_word" name="captcha_word" type="text">
    <img src="/bitrix/tools/captcha.php?captcha_code=<?=htmlspecialchars($cpt->GetCodeCrypt());?>">
<?
    showerror("Неправильный код с картинки!");
}
else  {
    $arFields = array(
        "EMAIL" => $_POST['email'],
        "MESSAGE" => $_POST['message'],
        "NAME" => $_POST['name'],
    );
    if(!empty($_FILES['file'])) {
        $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/upload/tmp_fast/';
        $uploadfile = $uploaddir . basename($_FILES['file']['name']);
        if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
            $filePath = $uploadfile;
            echo "удалось";
        } else {
            $filePath = "";
            echo "не удалось";
        }
    }
    
    SendAttache("NEW_FAST_ORDER", "S1", $arFields, $filePath);
    echo "success";
}