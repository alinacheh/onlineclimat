<?
//IBLOCK_ID !!!
    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	}
    $iblock_id = 1;
    $comparename = 'CATALOG_COMPARE_LIST';
    $id = intval($_POST['id']);
    if ($id) {
	    $value = intval($_POST['value']);
	    if ($value) $_SESSION[$comparename][$iblock_id]["ITEMS"][$id] = $id;
	    else unset($_SESSION[$comparename][$iblock_id]["ITEMS"][$id]);
	}
	CModule::IncludeModule('iblock');
	$res = CIBlockProperty::GetList(array('id'=>'ASC'), array('IBLOCK_ID'=>$iblock_id));
	$codes = array();
	$exclude = array(
		'IS_SPECIAL' => 1,
		'IS_NEW' => 1,
		'IS_POP' => 1,
		'BRAND_IBLOCK' => 1,
		'BRAND' => 1,
		'IS_SLIDER' => 1,
		'SLIDER' => 1,
		'ARTICUL' => 1,
		'FILES' => 1,
		'MORE_PHOTO' => 1,
		'INSTALL' => 1,
	);
	while ($r = $res->Fetch()) {
		if ($r['CODE']) {
			if (isset($exclude[$r['CODE']]) || mb_substr($r['CODE'], 0, 5) == 'CML2_') continue;
		}
		$codes[] = $r['CODE']?$r['CODE']:$r['ID'];
	}

    $APPLICATION->IncludeComponent(
		"bitrix:catalog.compare.result",
		"",
		Array(
			"IBLOCK_TYPE" => 'catalog',
			"IBLOCK_ID" => $iblock_id,
			"BASKET_URL" => SITE_DIR.'personal/cart/',
			"ACTION_VARIABLE" => 'action',
			"PRODUCT_ID_VARIABLE" => 'id',
			"SECTION_ID_VARIABLE" => 'SECTION_ID',
			"FIELD_CODE" => array(
				0 => "DETAIL_PICTURE",
				1 => "",
			),
			"PROPERTY_CODE" => $codes,
			"NAME" => $comparename,
			"CACHE_TYPE" => 'N',
			"CACHE_TIME" => '3600',
			"PRICE_CODE" => array(
				0 => "main",
				1 => "other",
			),
			"USE_PRICE_COUNT" => 'N',
			"SHOW_PRICE_COUNT" => 1,
			"PRICE_VAT_INCLUDE" => 'Y',
			"PRICE_VAT_SHOW_VALUE" => 'N',
			"DISPLAY_ELEMENT_SELECT_BOX" => "N",
			"ELEMENT_SORT_FIELD_BOX" => '',
			"ELEMENT_SORT_ORDER_BOX" => '',
			"ELEMENT_SORT_FIELD" => "sort",
			"ELEMENT_SORT_ORDER" => "asc",
			"DETAIL_URL" => SITE_DIR.'catalog/'."#SECTION_CODE#/#ELEMENT_ID#.html",
			"OFFERS_FIELD_CODE" => '',
			"OFFERS_PROPERTY_CODE" => '',
			"OFFERS_CART_PROPERTIES" => '',
			'CONVERT_CURRENCY' => "Y",
			'CURRENCY_ID' => "RUB",
		),
		false
	);
	?><script>in_compare = [<?foreach ($_SESSION[$comparename][$iblock_id]["ITEMS"] as $v=>$e) echo $v.','?>0];</script>