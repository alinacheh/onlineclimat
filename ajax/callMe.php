<?
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    $name = trim($_POST['name']);
    $iblock_id = 6;
    $phone = trim($_POST['phone']);
    if (!$name || !$phone) {
		echo 'Заполните все поля';
		die();
    }
    CModule::IncludeModule("iblock");
    $el = new CIBlockElement();
    $id = $el->Add(array(
		'IBLOCK_ID' => $iblock_id,
		'NAME' => $name,
		'PREVIEW_TEXT' => $phone
    ));
    CEvent::Send('CALLME', SITE_ID, array('NAME'=>$name,'PHONE'=>$phone));
    if (!$id) echo $el->LAST_ERROR;
    else echo 'OK';