<?
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    global $USER;
	$iblock_id = 7;
    $item = trim($_POST['product']);
	$name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $rate = trim($_POST['rate']);
    $message = trim($_POST['review']);

    if (!$item || !$name || !$email || !$rate || !$message) {
		echo 'Заполните все поля';
		die();
    }
    if (!$USER->IsAuthorized()) {
		if (!$APPLICATION->CaptchaCheckCode($_POST["captcha_word"], $_POST["captcha_code"])) {
			echo 'Неправильно введен защитный код';
			die();
		}
    }
    CModule::IncludeModule("iblock");
    $el = new CIBlockElement();
    $id = $el->Add(array(
		'IBLOCK_ID' => $iblock_id,
		'NAME' => date('Y-m-d H:i:s'),
		'PREVIEW_TEXT' => $message,
		'PROPERTY_VALUES' => array(
			'ITEM' => $item,
			'RATE' => $rate,
			'AUTHOR_EMAIL' => $email,
			'AUTHOR_NAME' => $name
		)
    ));
    if (!$id) echo $el->LAST_ERROR;
    else echo 'OK';