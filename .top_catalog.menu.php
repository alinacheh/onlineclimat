<?
$aMenuLinks = Array(
	Array(
		"Каталог",
		"/catalog/",
		Array(),
		Array("DATA" => "child-block-0", "CLASS_PARENT" => "submenu"),
		""
	),
);

$query = new \Bitrix\Main\Entity\Query(\Bitrix\Iblock\Model\Section::compileEntityByIblock(IBLOCK_ID_CATALOG));
$query
   ->setOrder(array('SORT' => 'ASC'))
   ->setFilter( 
      array( 
         "IBLOCK_ID" => IBLOCK_ID_CATALOG, 
         "ACTIVE" => "Y", 
         "UF_HEADER_MENU" => 1,  
      ) 
   ) 
   ->setSelect(
      array( "ID", "IBLOCK_ID", "NAME", "UF_HEADER_MENU", "UF_BRAND_BLOCK", 'SECTION_PAGE_URL_RAW' => 'IBLOCK.SECTION_PAGE_URL')
   ) 
   ->setLimit(6);    
$result= $query->exec(); 
while ($arSection = $result->fetch()) { 
	$arSection['SECTION_PAGE_URL'] = \CIBlock::ReplaceDetailUrl($arSection['SECTION_PAGE_URL_RAW'], $arSection, true, 'S'); // формируем URL
    $aMenuLinks[] = Array(
		$arSection["NAME"],
		$arSection["SECTION_PAGE_URL"],
		Array(),
		Array("SECTION_ID" => $arSection["ID"], "CLASS_PARENT" => "submenu2", "DIV" => "submenu2-brands", "UF_BRAND_BLOCK" => $arSection["UF_BRAND_BLOCK"], "DATA" => "child-block-".$arSection["ID"]),
		""
	);
}	


?>